<div class="modal fade" id="editsafetytips{{$dst->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'disasteradmin.safety_tips.update']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit {{$dst->hazard}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$dst->id}}">
				<div class="row">
					<div class="col-md-4 text-left top10">
						<label>Hazard</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="hazard" v-model="hazard_edit" class="form-control" @change="validateEdit">
							<option value="" selected disabled>Select Hazard</option>
							@if($disaster_safety_tips->where('hazard','Drought')->count() < 1 || $dst->hazard == 'Drought')
								<option value="Drought">Drought</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Earthquake')->count() < 1 || $dst->hazard == 'Earthquake')
								<option value="Earthquake">Earthquake</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Flood')->count() < 1  || $dst->hazard == 'Flood')
								<option value="Flood">Flood</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Heavy Rains')->count() < 1  || $dst->hazard == 'Heavy Rains')
								<option value="Heavy Rains">Heavy Rains</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Landslide/Mudslide')->count() < 1  || $dst->hazard == 'Landslide/Mudslide')
								<option value="Landslide/Mudslide">Landslide/Mudslide</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Lightning Incidents')->count() < 1  || $dst->hazard == 'Lightning Incidents')
								<option value="Lightning Incidents">Lightning Incidents</option>
							@endif
							<option value="Others">Others</option>
							@if($disaster_safety_tips->where('hazard','Sinkhole')->count() < 1  || $dst->hazard == 'Sinkhole')
								<option value="Sinkhole">Sinkhole</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Storm Surge')->count() < 1  || $dst->hazard == 'Storm Surge')
								<option value="Storm Surge">Storm Surge</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Tornadoes/Whirlwinds')->count() < 1  || $dst->hazard == 'Tornadoes/Whirlwinds')
								<option value="Tornadoes/Whirlwinds">Tornadoes/Whirlwinds</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Tsunami')->count() < 1  || $dst->hazard == 'Tsunami')
								<option value="Tsunami">Tsunami</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Typhoon')->count() < 1  || $dst->hazard == 'Typhoon')
								<option value="Typhoon">Typhoon</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Volcanic Eruption')->count() < 1  || $dst->hazard == 'Volcanic Eruption')
								<option value="Volcanic Eruption">Volcanic Eruption</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Wild Fire')->count() < 1  || $dst->hazard == 'Wild Fire')
								<option value="Wild Fire">Wild Fire</option>
							@endif
						</select>
						<div class="top10" v-if="hazard_edit == 'Others'" v-cloak>
							<input type="text" class="form-control" name="hazard_others" v-model="hazard_others_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
						</div>
					</div>
					<div class="col-md-4 text-left top10">
						<label>Description</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="description" class="form-control" rows="3" v-model="description_edit" style="margin-bottom: 5px;width:100% !important" @keyup="validateEdit"></textarea>
					</div>
					<div class="col-md-4 text-left top10">
						<label>Facts</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="facts" class="form-control" rows="3" v-model="facts_edit" style="margin-bottom: 5px;width:100% !important" @keyup="validateEdit"></textarea>
					</div>
					<div class="col-md-4 text-left top10">
						<label>Safety Tips</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="tips" class="form-control" rows="3" v-model="tips_edit" style="margin-bottom: 5px;width:100% !important" @keyup="validateEdit"></textarea>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 10px">
						<label><strong>What To Do</strong></label>
					</div>
					<div class="col-md-4 text-left top10">
						<label>Before</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="before" class="form-control" rows="3" v-model="before_edit" style="margin-bottom: 5px;width:100% !important" @keyup="validateEdit"></textarea>
					</div>
					<div class="col-md-4 text-left top10">
						<label>During</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="during" class="form-control" rows="3" v-model="during_edit" style="margin-bottom: 5px;width:100% !important" @keyup="validateEdit"></textarea>
					</div>
					<div class="col-md-4 text-left top10">
						<label>After</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="after" class="form-control" rows="3" v-model="after_edit" style="margin-bottom: 5px;width:100% !important" @keyup="validateEdit"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Update</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Update</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>