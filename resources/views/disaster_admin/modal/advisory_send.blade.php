<div class="modal fade" id="sendadvisories{{$da->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'disasteradmin.advisory.send','files'=>true]) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Send {{$da->subject}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$da->id}}">
				<input type="hidden" name="user_id" value="{{Session::get('logUser')->id}}">
				<div class="row">
					<input type="hidden" name="user_id" value="{{Session::get('logUser')->id}}">
					<div class="col-md-4 top10 text-left">
						<label>Subject</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						
						<input type="text" class="form-control" name="subject" v-model="subject_edit" style="margin-bottom: 5px;" @keyup="validateEdit" @if($da->status == 'Sent') readonly @endif>
					</div>
					<div class="col-md-4 top10 text-left">
						<label>Type</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="hidden" name="type" v-model="type_edit">
						
						<select name="type" v-model="type_edit" class="form-control" @change="validateEdit" @if($da->status == 'Sent') readonly disabled @endif>
							<option value="" selected disabled>Select Advisory Type</option>
							<option value="Prevention">Prevention</option>
							<option value="Mitigation">Mitigation</option>
							<option value="Preparedness">Preparedness</option>
						</select>
					</div>
					<div class="col-md-4 top10 text-left">
						<label>Message</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						
						<textarea name="message" v-model="message_edit" class="form-control" rows="5" style="margin-bottom: 5px;width:100% !important" @change="validateEdit" @if($da->status == 'Sent') readonly @endif></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<center><span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Send</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Send</button>
				</span></center>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>