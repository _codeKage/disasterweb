<div class="modal fade" id="addevacuationcenters">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'disasteradmin.evacuation_center.store','files'=>true]) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Evacuation Center</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-12" style="border:1px black solid;margin:auto;padding:5px">
							<label>Get Location Details using:</label>
							<div class="radio">
								<label style="margin-left:5px;margin-right: 5px">
									<input type="radio" v-model="choose_add" value="0">
									Address
								</label>
								<label style="margin-left:5px;margin-right: 5px">
									<input type="radio" v-model="choose_add" value="1">
									Coordinates
								</label>
							</div>
							<div class="row col-md-12" style="margin-top: 10px;margin-bottom: 10px;margin:auto">
								<label>Get Location by clicking on the map:</label>
	    						<div id="map_add" style="height: 300px"></div>
							</div>
							<div class="text-left" v-if="choose_add == 0">
								<div class="col-md-4 top10">
									<label>Address</label>
								</div>
								<div class="col-md-8" style="margin-top: 5px">
									<div class="form-group" :class="{ 'has-error': isErrorAddress_add }">
										<input type="text" ref="autocomplete" class="form-control" name="address" placeholder="address" v-model="address_add" @keyup="validateAdd">
										<span v-html="unknownAddress_add"></span>
									</div>
								</div>
								<div class="col-md-offset-4 col-md-4" style="margin-top: 5px">
									<span v-if="isLoadingGetAddress_add == false">
										<button type="button" class="btn btn-primary" @click='addressDetailsAdd()' style="width: 100%">Find</button>
									</span>
									<span v-else v-cloak>
										<button class="btn btn-default" disabled><i class="fa fa-spinner fa-spin"></i> Please wait</button>
									</span>
								</div>
							</div>
							<div class="text-left" v-if="choose_add == 1">
								<div class="form-group" :class="{ 'has-error': isErrorCoordinates_add }">
									<div class="col-md-4 top10">
										<label>Latitude</label>
									</div>
									<div class="col-md-8" style="margin-top: 5px">
										<input type="number" class="form-control" name="latitude" placeholder="Latitude" v-model.number="latitude_add" step="0.0000001" style="margin-bottom: 5px" @keyup="validateAdd">
									</div>
									<div class="col-md-4 top10">
										<label>Longitude</label>
									</div>
									<div class="col-md-8">
										<input type="number" class="form-control" name="longitude" placeholder="longitude" v-model.number="longitude_add" step="0.0000001" style="margin-bottom: 5px" @keyup="validateAdd">
									</div>
									<div class="col-md-12">
										<span v-html="unknownCoordinates_add"></span>
									</div>
								</div>
								<div class="col-md-offset-4 col-md-4" style="margin-top: 5px">
									<span v-if="isLoadingGetCoordinates_add == false">
										<button type="button" class="btn btn-primary" @click='coordinatesDetailsAdd()' style="width: 100%">Find</button>
									</span>
									<span v-else v-cloak>
										<button class="btn btn-default" disabled><i class="fa fa-spinner fa-spin"></i> Please wait</button>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div v-if="choose_add == 0">
						<div class="form-group" :class="{ 'has-error': isErrorCoordinates_add }">
							<div class="col-md-4 top10">
								<label>Latitude</label>
							</div>
							<div class="col-md-8 text-left" style="margin-top: 5px">
								<input type="number" class="form-control" name="latitude" placeholder="Latitude" v-model.number="latitude_add" step="0.0000001" style="margin-bottom: 5px" @keyup="validateAdd">
							</div>
							<div class="col-md-4 top10">
								<label>Longitude</label>
							</div>
							<div class="col-md-8 text-left" style="margin-top: 5px">
								<input type="number" class="form-control" name="longitude" placeholder="longitude" v-model.number="longitude_add" step="0.0000001" style="margin-bottom: 5px" @keyup="validateAdd">
							</div>
							<div class="col-md-12">
								<span v-html="unknownCoordinates_add"></span>
							</div>
						</div>
					</div>
					<div v-if="choose_add == 1">
						<div class="col-md-4 top10">
							<label>Address</label>
						</div>
						<div class="col-md-8 text-left" :class="{ 'has-error': isErrorAddress_add }" style="margin-top: 5px">
							<input type="text" class="form-control" name="address" placeholder="address" v-model="address_add" @keyup="validateAdd">
							<span v-html="unknownAddress_add"></span>
						</div>
					</div>
					<div class="col-md-4 top10">
						<label>Purok</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" name="purok" v-model="purok_add" class="form-control" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Barangay</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" name="barangay" v-model="barangay_add" class="form-control" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Street</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" name="street" v-model="street_add" class="form-control" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Municipality</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" name="municipality" v-model="municipality_add" class="form-control" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Province</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" name="province" v-model="province_add" class="form-control" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Evacuation Center Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" name="name" v-model="name_add" class="form-control" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Capacity</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="number" class="form-control" name="capacity" placeholder="No. Of Persons it can handle" v-model.number="capacity_add" step="1" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10 text-left">
						<label>Type</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="type" v-model="type_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled>Select Evacuation Center Type</option>
							<option value="Barangay Hall">Barangay Hall</option>
							<option value="Barangay Health Center">Barangay Health Center</option>
							<option value="Church">Church</option>
							<option value="Covered Court/Gym">Covered Court/Gym</option>
							<option value="Government Office">Government Office</option>
							<option value="Residential">Residential</option>
							<option value="School">School</option>
							<option value="Others">Others</option>
						</select>
					</div>
					<div class="col-md-4 text-left top10">
						<label>Condition</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="condition" v-model="condition_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled>Select Evacuation Center Condition</option>
							<option value="Good Condition">Good Condition</option>
							<option value="For Repair">For Repair</option>
							<option value="Overloaded">Overloaded</option>
						</select>
					</div>
					<div class="col-md-4 top10">
						<label>Person in-charge</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" name="incharge" v-model="incharge_add" class="form-control" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Contact</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" name="contact" class="form-control" @keyup="validateAdd">
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<label>Facilities (Check if Applicable)</label>
						<div class="checkbox">
							<div class="row">
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="facility_toilet" value="0">
										<input type="checkbox" name="facility_toilet" value="1">
										Toilet
									</label>
								</div>
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="facility_access_to_water" value="0">
										<input type="checkbox" name="facility_access_to_water" value="1">
										Access To Water
									</label>	
								</div>
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="facility_with_electricity" value="0">
										<input type="checkbox" name="facility_with_electricity" value="1">
										With Electricity
									</label>
								</div>
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="facility_working_generator" value="0">
										<input type="checkbox" name="facility_working_generator" value="1">
										Working Generator
									</label>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="facility_with_private_rooms" value="0">
										<input type="checkbox" name="facility_with_private_rooms" value="1">
										With Private Rooms
									</label>
								</div>
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="facility_with_lactating_room" value="0">
										<input type="checkbox" name="facility_with_lactating_room" value="1">
										With Lactating Room
									</label>		
								</div>
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="facility_kitchen" value="0">
										<input type="checkbox" name="facility_kitchen" value="1">
										Kitchen
									</label>
								</div>
							</div>

						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<label>Applicable Center (Check if Applicable)</label>
						<div class="checkbox">
							<div class="row">
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="applicable_typhoon" value="0">
										<input type="checkbox" name="applicable_typhoon" value="1">
										Typhoon
									</label>
								</div>
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="applicable_flood" value="0">
										<input type="checkbox" name="applicable_flood" value="1">
										Flood
									</label>
								</div>
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="applicable_storm_surge" value="0">
										<input type="checkbox" name="applicable_storm_surge" value="1">
										Storm Surge
									</label>		
								</div>
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="applicable_landslide" value="0">
										<input type="checkbox" name="applicable_landslide" value="1">
										Landslide / Mudslide
									</label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="applicable_drought" value="0">
										<input type="checkbox" name="applicable_drought" value="1">
										Drought
									</label>
								</div>
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="applicable_volcanic_eruption" value="0">
										<input type="checkbox" name="applicable_volcanic_eruption" value="1">
										Volcanic Eruption
									</label>	
								</div>
								<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
										<input type="hidden" name="applicable_sinkhole" value="0">
										<input type="checkbox" name="applicable_sinkhole" value="1">
										Sinkhole
									</label>	
								</div>								
							</div>						
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<span v-if="enableAdd == true" v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>