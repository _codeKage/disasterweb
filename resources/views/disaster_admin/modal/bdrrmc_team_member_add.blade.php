<div class="modal fade" id="addbdrrmcteammember">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'disasteradmin.bdrrmc_team_member.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add BDRRMC Team Member</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 top10">
						<label>First Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="first_name" v-model="first_name_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Middle Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="middle_name" v-model="middle_name_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Last Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="last_name" v-model="last_name_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Gender</label>
					</div>
					<div class="col-md-8"  style="margin-top: 5px">
						
						<select name="gender" v-model="gender_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled>Select Gender</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
					</div>
					<div class="col-md-4 top10">
						<label>Birthdate</label>
					</div>
					<div class="col-md-8"  style="margin-top: 5px">
						
						<input type="date" class="form-control" name="birthdate" v-model="birthdate_add" style="margin-bottom: 5px;width:100% !important" @change="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Team</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="disaster_team_id" v-model="disaster_team_id_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled>Select Team</option>
							@foreach($disaster_bdrrmc_teams as $bdt)
							<option value="{{$bdt->id}}">{{$bdt->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-12 text-left">
						<div class="form-group" :class="{ 'has-error': isErrorPeriod_add }">
							<div class="row">
								<div class="col-md-4 top10">
									<label>Period From</label>
								</div>
								<div class="col-md-8">
									<input type="date" class="form-control" name="period_from" v-model="period_from_add" style="margin-top: 5px;width:100% !important" @change="validateAdd">
								</div>
								<div class="col-md-4 top10">
									<label>Period To</label>
								</div>
								<div class="col-md-8">
									<input type="date" class="form-control" name="period_to" v-model="period_to_add" style="margin-top: 5px;width:100% !important" @change="validateAdd">
								</div>
								<div class="col-md-4"> </div>
								<div class="col-md-8"><span v-html="errorPeriod_add"></span></div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<label>Contact</label>
					</div>
					<div class="col-md-8 text-left">
						<input type="text" class="form-control" name="contact" v-model="contact_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAdd == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>