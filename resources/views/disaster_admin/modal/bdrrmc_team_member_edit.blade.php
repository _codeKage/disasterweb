<div class="modal fade" id="editbdrrmcteammember{{$dbtm->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'disasteradmin.bdrrmc_team_member.update']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit {{$dbtm->last_name}}, {{$dbtm->first_name}} {{$dbtm->middle_name}}</h4>
			</div>
			<div class="modal-body" >
				<input type="hidden" name="id" value="{{$dbtm->id}}">
				<div class="row">
					<div class="col-md-4 text-left top10">
						<label>First Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="first_name" v-model="first_name_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Middle Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="middle_name" v-model="middle_name_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Last Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="last_name" v-model="last_name_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Gender</label>
					</div>
					<div class="col-md-8" style="margin-top: 5px">
						<select name="gender" v-model="gender_edit" class="form-control" @change="validateEdit">
							<option value="" selected disabled>Select Gender</option>
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
					</div>
					<div class="col-md-4 text-left top10">
						<label>Birthdate</label>
					</div>
					<div class="col-md-8"  style="margin-top: 5px">
						<input type="date" class="form-control" name="birthdate" v-model="birthdate_edit" style="margin-bottom: 5px;width:100% !important" @change="validateEdit">
					</div>

					<div class="col-md-4 text-left top10">
						<label>Team</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="disaster_team_id" v-model="disaster_team_id_edit" class="form-control" @change="validateEdit">
							<option value="" selected disabled>Select Team</option>
							@foreach($disaster_bdrrmc_teams as $bdt)
							<option value="{{$bdt->id}}">{{$bdt->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-12 text-left">
						<div class="form-group" :class="{ 'has-error': isErrorPeriod_edit }">
							<div class="row">
								<div class="col-md-4 top10">
									<label>Period From</label>
								</div>
								<div class="col-md-8">
									<input type="date" class="form-control" name="period_from" v-model="period_from_edit" style="margin-top: 5px;width:100% !important" @change="validateEdit">
								</div>
								<div class="col-md-4 top10">
									<label>Period To</label>
								</div>
								<div class="col-md-8">
									<input type="date" class="form-control" name="period_to" v-model="period_to_edit" style="margin-top: 5px;width:100% !important" @change="validateEdit">
								</div>
								<div class="col-md-4"> </div>
								<div class="col-md-8"><span v-html="errorPeriod_edit"></span></div>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-left">
						<label>Contact</label>
					</div>
					<div class="col-md-8 text-left">
						<input type="text" class="form-control" name="contact" v-model="contact_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Update</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Update</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>	