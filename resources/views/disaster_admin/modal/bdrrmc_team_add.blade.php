<div class="modal fade" id="addbdrrmcteam">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'disasteradmin.bdrrmc_team.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add BDRRMC Team</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 top10">
						<label>Name of the Team</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name" v-model="name_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Group Code</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="code" v-model="code_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Functional Roles and Responsibilities</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="roles" class="form-control" rows="3" v-model="roles_add" style="margin-bottom: 5px;width:100% !important" @keyup="validateAdd"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAdd == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>