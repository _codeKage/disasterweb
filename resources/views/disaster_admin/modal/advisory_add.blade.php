<div class="modal fade" id="addadvisories">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'disasteradmin.advisory.store','files'=>true]) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Advisory Message</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" name="user_id" value="{{Session::get('logUser')->id}}">
					<div class="col-md-4 top10">
						<label>Subject</label>
					</div>
					<div class="col-md-6 text-left" style="margin-top: 5px">	
						<input type="text" class="form-control" name="subject" v-model="subject_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Type</label>
					</div>
					<div class="col-md-6 text-left" style="margin-top: 5px">
						<select name="type" v-model="type_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled>Select Advisory Type</option>
							<option value="Prevention">Prevention</option>
							<option value="Mitigation">Mitigation</option>
							<option value="Preparedness">Preparedness</option>
						</select>
					</div>
					<div class="col-md-4 top10">
						<label>Message</label>
					</div>
					<div class="col-md-6 text-left" style="margin-top: 5px">
						<textarea name="message" v-model="message_add" class="form-control" rows="5" style="margin-bottom: 5px;width:100% !important" @keyup="validateAdd"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<center><span v-if="enableAdd == true" v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
					<input type="submit" name="submit" value="Send" class="btn btn-success">
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Send</button>
					<input type="submit" name="submit" value="Send" class="btn btn-success" disabled>
				</span></center>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>