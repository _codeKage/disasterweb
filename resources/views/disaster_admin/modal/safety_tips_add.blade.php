<div class="modal fade" id="addsafetytips">
	<div class="modal-dialog">
		<div class="modal-content">
			{!! Form::open(['route'=>'disasteradmin.safety_tips.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Hazard</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 top10">
						<label>Hazard</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="hazard" v-model="hazard_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled>Select Hazard</option>
							@if($disaster_safety_tips->where('hazard','Drought')->count() < 1)
								<option value="Drought">Drought</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Earthquake')->count() < 1)
								<option value="Earthquake">Earthquake</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Flood')->count() < 1)
								<option value="Flood">Flood</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Heavy Rains')->count() < 1)
								<option value="Heavy Rains">Heavy Rains</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Landslide/Mudslide')->count() < 1)
								<option value="Landslide/Mudslide">Landslide/Mudslide</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Lightning Incidents')->count() < 1)
								<option value="Lightning Incidents">Lightning Incidents</option>
							@endif
							<option value="Others">Others</option>
							@if($disaster_safety_tips->where('hazard','Sinkhole')->count() < 1)
								<option value="Sinkhole">Sinkhole</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Storm Surge')->count() < 1)
								<option value="Storm Surge">Storm Surge</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Tornadoes/Whirlwinds')->count() < 1)
								<option value="Tornadoes/Whirlwinds">Tornadoes/Whirlwinds</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Tsunami')->count() < 1)
								<option value="Tsunami">Tsunami</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Typhoon')->count() < 1)
								<option value="Typhoon">Typhoon</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Volcanic Eruption')->count() < 1)
								<option value="Volcanic Eruption">Volcanic Eruption</option>
							@endif
							@if($disaster_safety_tips->where('hazard','Wild Fire')->count() < 1)
								<option value="Wild Fire">Wild Fire</option>
							@endif
						</select>
						<div class="top10" v-if="hazard_add == 'Others'" v-cloak>
							<input type="text" class="form-control" name="hazard_others" v-model="hazard_others_add" style="margin-bottom: 5px;" @keyup="validateAdd">
						</div>
					</div>
					<div class="col-md-4 top10">
						<label>Description</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="description" class="form-control" rows="3" v-model="description_add" style="margin-bottom: 5px;width:100% !important" @keyup="validateAdd"></textarea>
					</div>
					<div class="col-md-4 top10">
						<label>Facts</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="facts" class="form-control" rows="3" v-model="facts_add" style="margin-bottom: 5px;width:100% !important" @keyup="validateAdd"></textarea>
					</div>
					<div class="col-md-4 top10">
						<label>Safety Tips</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="tips" class="form-control" rows="3" v-model="tips_add" style="margin-bottom: 5px;width:100% !important" @keyup="validateAdd"></textarea>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 10px">
						<label><strong>What To Do</strong></label>
					</div>
					<div class="col-md-4 top10">
						<label>Before</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="before" class="form-control" rows="3" v-model="before_add" style="margin-bottom: 5px;width:100% !important" @keyup="validateAdd"></textarea>
					</div>
					<div class="col-md-4 top10">
						<label>During</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="during" class="form-control" rows="3" v-model="during_add" style="margin-bottom: 5px;width:100% !important" @keyup="validateAdd"></textarea>
					</div>
					<div class="col-md-4 top10">
						<label>After</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="after" class="form-control" rows="3" v-model="after_add" style="margin-bottom: 5px;width:100% !important" @keyup="validateAdd"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAdd == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>