

<div class="modal fade" id="editevacuationcenters{{$dec->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'disasteradmin.evacuation_center.update','files'=>true]) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit Evacuation Center</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$dec->id}}">
				<div class="col-md-12 text-left" style="margin-top: 5px">
					<div class="col-md-12" style="border:1px black solid;margin:auto;padding:5px">
						<label>Get Location Details using:</label>
						<div class="radio">
							<label style="margin-left:5px;margin-right: 5px">
								<input type="radio" v-model="choose_edit" value="0">
								Address
							</label>
							<label style="margin-left:5px;margin-right: 5px">
								<input type="radio" v-model="choose_edit" value="1">
								Coordinates
							</label>
						</div>
						<div class="row col-md-12" style="margin-top: 10px;margin-bottom: 10px;margin:auto">
								<label>Get Location by clicking on the map:</label>
	    						<div id="map_edit{{$dec->id}}" style="height: 300px"></div>
							</div>
						<div class="text-left" v-if="choose_edit == 0">
							<div class="col-md-4 text-left top10">
								<label>Address</label>
							</div>
							<div class="col-md-8" style="margin-top: 5px">
								<label>Address</label>
								<div class="form-group" :class="{ 'has-error': isErrorAddress_edit }">
									<input type="text" ref="autocomplete" class="form-control" name="address" placeholder="address" v-model="address_edit" @keyup="validateEdit">
									<span v-html="unknownAddress_edit"></span>
								</div>
							</div>
							<div class="col-md-offset-4 col-md-4 text-left" style="margin-top: 5px">
								<span v-if="isLoadingGetAddress_edit == false">
									<button type="button" class="btn btn-primary" @click='addressDetailsEdit()' style="width: 100%">Find</button>
								</span>
								<span v-else v-cloak>
									<button class="btn btn-default" disabled><i class="fa fa-spinner fa-spin"></i> Please wait</button>
								</span>
							</div>
						</div>
						<div class="text-left" v-else>
							<div class="form-group" :class="{ 'has-error': isErrorCoordinates_edit }">
								<div class="col-md-4 text-left top10">
									<label>Latitude</label>
								</div>
								<div class="col-md-12" style="margin-top: 5px">
									<input type="number" class="form-control" name="latitude" placeholder="Latitude" v-model.number="latitude_edit" step="0.0000001" style="margin-bottom: 5px" @keyup="validateEdit">
								</div>
								<div class="col-md-4 text-left top10">
									<label>Longitude</label>
								</div>
								<div class="col-md-12">
									<input type="number" class="form-control" name="longitude" placeholder="longitude" v-model.number="longitude_edit" step="0.0000001" style="margin-bottom: 5px" @keyup="validateEdit">
								</div>
								<span v-html="unknownCoordinates_edit"></span>
							</div>
							<div class="col-md-offset-4 col-md-4 text-left" style="margin-top: 5px">
								<span v-if="isLoadingGetCoordinates_edit == false">
									<button type="button" class="btn btn-primary" @click='coordinatesDetailsEdit()' style="width: 100%">Find</button>
								</span>
								<span v-else v-cloak>
									<button class="btn btn-default" disabled><i class="fa fa-spinner fa-spin"></i> Please wait</button>
								</span>
							</div>
						</div>
					</div>
				</div>
				<div v-if="choose_edit == 0">
					<div class="col-md-4 text-left top10">
						<label>Latitude</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="number" class="form-control" name="latitude" placeholder="Latitude" v-model.number="latitude_edit" step="0.0000001" style="margin-bottom: 5px" @keyup="validateEdit">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Longitude</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="number" class="form-control" name="longitude" placeholder="longitude" v-model.number="longitude_edit" step="0.0000001" style="margin-bottom: 5px" @keyup="validateEdit">
					</div>
				</div>
				<div v-else>
					<div class="col-md-4 text-left top10">
						<label>Address</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="address" placeholder="address" v-model="address_edit" @keyup="validateEdit">
					</div>
				</div>
				<div class="col-md-4 text-left top10">
					<label>Purok</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					<input type="text" name="purok" v-model="purok_edit" class="form-control" @keyup="validateEdit">
				</div>
				<div class="col-md-4 text-left top10">
					<label>Barangay</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					<input type="text" name="barangay" v-model="barangay_edit" class="form-control" @keyup="validateEdit">
				</div>
				<div class="col-md-4 text-left top10">
					<label>Street</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					<input type="text" name="street" v-model="street_edit" class="form-control" @keyup="validateEdit">
				</div>
				<div class="col-md-4 text-left top10">
					<label>Municipality</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					<input type="text" name="municipality" v-model="municipality_edit" class="form-control" @keyup="validateEdit">
				</div>
				<div class="col-md-4 text-left top10">
					<label>Province</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					<input type="text" name="province" v-model="province_edit" class="form-control" @keyup="validateEdit">
				</div>
				<div class="col-md-4 text-left top10">
					<label>Evacuation Center Name</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					<input type="text" name="name" v-model="name_edit" class="form-control" @keyup="validateEdit">
				</div>
				<div class="col-md-4 text-left top10">
					<label>Capacity (No. of Persons)</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					<input type="number" class="form-control" name="capacity" placeholder="no of persons" v-model.number="capacity_edit" step="1" style="margin-bottom: 5px" @keyup="validateEdit">
				</div>
				<div class="col-md-4 text-left top10">
					<label>People Inside</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">	
					<input type="number" class="form-control" name="no_people" placeholder="number of people inside" v-model.number="no_people_edit" step="1" style="margin-bottom: 5px" @keyup="validateEdit">
				</div>
				<div class="col-md-4 top10">
					<label>Type</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					<select name="type" v-model="type_edit" class="form-control" @change="validateEdit">
						<option value="" selected disabled>Select Evacuation Center Type</option>
							<option value="Barangay Hall">Barangay Hall</option>
							<option value="Barangay Health Center">Barangay Health Center</option>
							<option value="Church">Church</option>
							<option value="Covered Court/Gym">Covered Court/Gym</option>
							<option value="Government Office">Government Office</option>
							<option value="Residential">Residential</option>
							<option value="School">School</option>
							<option value="Others">Others</option>
					</select>
				</div>
				<div class="col-md-4 top10">
					<label>Condition</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					<select name="condition" v-model="condition_edit" class="form-control" @change="validateEdit">
						<option value="" selected disabled>Select Evacuation Center Condition</option>
						<option value="Good Condition">Good Condition</option>
						<option value="For Repair">For Repair</option>
						<option value="Overloaded">Overloaded</option>
					</select>
				</div>
				<div class="col-md-4 text-left top10">
					<label>Person in-charge</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					
					<input type="text" name="incharge" v-model="incharge_edit" class="form-control" @keyup="validateEdit">
				</div>
				<div class="col-md-4 text-left top10">
					<label>Contact</label>
				</div>
				<div class="col-md-8 text-left" style="margin-top: 5px">
					
					<input type="text" name="contact" v-model="contact_edit" class="form-control" @keyup="validateEdit">
				</div>
				<div class="col-md-12 text-left" style="margin-top: 5px">
					<label>Facilities (Check if Applicable)</label>
					<div class="checkbox">
						<div class="row">
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="facility_toilet" value="0">
									<input type="checkbox" name="facility_toilet" value="1" @if($dec->facility_toilet == 1) checked @endif>
									Toilet
								</label>	
							</div>
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="facility_access_to_water" value="0">
									<input type="checkbox" name="facility_access_to_water" value="1" @if($dec->facility_access_to_water == 1) checked @endif>
									Access To Water
								</label>
							</div>
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="facility_with_electricity" value="0">
									<input type="checkbox" name="facility_with_electricity" value="1" @if($dec->facility_with_electricity == 1) checked @endif>
									With Electricity
								</label>	
							</div>
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="facility_working_generator" value="0">
									<input type="checkbox" name="facility_working_generator" value="1" @if($dec->facility_working_generator == 1) checked @endif>
									Working Generator
								</label>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="facility_with_private_rooms" value="0">
									<input type="checkbox" name="facility_with_private_rooms" value="1" @if($dec->facility_with_private_rooms == 1) checked @endif>
									With Private Rooms
								</label>
							</div>
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="facility_with_lactating_room" value="0">
									<input type="checkbox" name="facility_with_lactating_room" value="1" @if($dec->facility_with_lactating_room == 1) checked @endif>
									With Lactating Room
								</label>
							</div>
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="facility_kitchen" value="0">
									<input type="checkbox" name="facility_kitchen" value="1" @if($dec->facility_kitchen == 1) checked @endif>
									Kitchen
								</label>
							</div>
						</div>
						
						
						
						
						
					</div>
				</div>
				<div class="col-md-12 text-left" style="margin-top: 5px">
					<label>Applicable Center (Check if Applicable)</label>
					<div class="checkbox">
						<div class="row">
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="applicable_typhoon" value="0">
									<input type="checkbox" name="applicable_typhoon" value="1" @if($dec->applicable_typhoon == 1) checked @endif>
									Typhoon
								</label>
							</div>
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="applicable_flood" value="0">
									<input type="checkbox" name="applicable_flood" value="1" @if($dec->applicable_flood == 1) checked @endif>
									Flood
								</label>
							</div>
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="applicable_storm_surge" value="0">
									<input type="checkbox" name="applicable_storm_surge" value="1" @if($dec->applicable_storm_surge == 1) checked @endif>
									Storm Surge
								</label>
							</div>
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="applicable_landslide" value="0">
									<input type="checkbox" name="applicable_landslide" value="1" @if($dec->applicable_landslide == 1) checked @endif>
									Landslide/Mudslide
								</label>	
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="applicable_drought" value="0">
									<input type="checkbox" name="applicable_drought" value="1" @if($dec->applicable_drought == 1) checked @endif>
									Drought
								</label>
							</div>
							<div class="col-md-3">
								<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="applicable_volcanic_eruption" value="0">
									<input type="checkbox" name="applicable_volcanic_eruption" value="1" @if($dec->applicable_volcanic_eruption == 1) checked @endif>
									Volcanic Eruption
								</label>
							</div>
							<div class="col-md-3">
									<label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
									<input type="hidden" name="applicable_sinkhole" value="0">
									<input type="checkbox" name="applicable_sinkhole" value="1" @if($dec->applicable_sinkhole == 1) checked @endif>
									Sinkhole
								</label>
							</div>

						</div>
						<div class="pull-right">
							<span v-if="enableEdit == true" v-cloak>
								<button type="submit" class="btn btn-success">Update</button>
							</span>
							<span v-else>
								<button type="submit" class="btn btn-success" disabled>Update</button>
							</span>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
			
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>	