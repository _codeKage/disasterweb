<div class="modal fade" id="editadvisories{{$da->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'disasteradmin.advisory.update','files'=>true]) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit {{$da->subject}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$da->id}}">
				<div class="row">
					<input type="hidden" name="user_id" value="{{Session::get('logUser')->id}}">
					<input type="hidden" name="user_id" value="{{Session::get('logUser')->id}}">
					<div class="col-md-4 top10 text-left">
						<label>Subject</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="subject" value="{{$da->subject}}" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
					<div class="col-md-4 top10 text-left">
						<label>Type</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="type" value="{{$da->type}}" class="form-control" @change="validateEdit">
							<option value="" disabled>Select Advisory Type</option>
							<option value="Prevention" selected>Prevention</option>
							<option value="Mitigation">Mitigation</option>
							<option value="Preparedness">Preparedness</option>
						</select>
					</div>
					<div class="col-md-4 top10 text-left">
						<label>Message</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="message"  class="form-control" rows="5" style="margin-bottom: 5px;width:100% !important" @change="validateEdit">{{$da->message}}</textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<center><span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Update</button>
					<input type="submit" name="submit" value="Send" class="btn btn-success">
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Update</button>
					<input type="submit" name="submit" value="Send" class="btn btn-success" disabled>
				</span></center>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>