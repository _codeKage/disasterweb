@extends('layouts.app')

@section('title')
	Disaster Admin Advisory
@endsection

@section('css')
	<!-- internal styles -->
	<style>
		.panel-heading{
			background-color: #1b3c5b!important;
			color: white !important;
		}
	</style>
@endsection

@section('body')
	<div id="advisory" class="barangay top20">
		<div class="panel panel-default" >
			<div class="panel-heading" style="color: black;  ">
				<h3 class="panel-title" ><b>ADVISORY</b></h3>
			</div>
			<div class="panel-body" style="height: 65vh;">
				<div class="container1">
					@if(Session::has('flash_message'))
						<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
					@endif
					<div class="col-sm-2" style="padding-top:8px;padding-bottom: 8px; margin-right:5px">
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<a class="btn btn-primary" data-toggle="modal" href='#addadvisories' data-backdrop="static" style="height:35px">Add Advisory Message</a>
							@include('disaster_admin.modal.advisory_add')
						@endif
					</div>
					<div class="col-sm-1" style="padding-bottom: 8px; margin-left:5px">
						{{ Form::open(['route'=>'disasteradmin.advisory.batchDelete','method'=>'get', 'class'=>'navbar-form']) }}
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<button type="submit" class="btn btn-primary" style="height:35px">Delete Advisories</button>
						@endif
						{{ Form::close() }}
					</div>
					<div class="col-sm-8 text-right">
						{{ Form::open(['route'=>'disasteradmin.advisory','method'=>'get', 'class'=>'navbar-form']) }}
						<div class="form-group" >
							<input type="text" class="form-control" placeholder="Search" name="search">
							<span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
						</div>
						<a href="{{route('disasteradmin.advisory')}}" class="btn btn-primary">View All</a>
						{{ Form::close() }}
					</div>
					<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
						<tr style="border: 1px solid black;">
							<th>Date Entered</th>
							<th>Subject</th>
							<th>Message</th>
							<th>Type</th>
							<th>Status</th>
							<th>Last User</th>
							<th>Last Update</th>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
								<th>Actions</th>
							@endif
						</tr>
						@forelse($disaster_advisories as $da)
							<tr style="border: 1px solid black; @if($da->deleted_at != null) background-color: #f87272; @endif">
								<td>{{$da->date}}</td>
								<td class="text-left">{{$da->subject}}</td>
								<td class="text-left">{{$da->message}}</td>
								<td class="text-left">{{$da->type}}</td>
								<td>{{$da->status}}</td>
								<td class="text-left">{{$da->last_name}}, {{$da->first_name}} {{$da->middle_initial}}</td>
								<td>{{$da->updated_at}}</td>
								@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
									<td>
										@if($da->deleted_at == null)
											<div class="dropdown">
												<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
													<span class="caret"></span></button>
												<ul class="dropdown-menu">
													<li>
														{!! Form::open(['route'=>'disasteradmin.advisory.resend','files'=>true, 'id' => $da->id])!!}
														<input type="hidden" name="id" value="{{$da->id}}">
														<a onclick="document.getElementById('{{$da->id	}}').submit();" data-backdrop="static" title="Send" @click='editData({{json_encode($da->type)}})' href="#" style="color: black; text-decoration: none"><i class="fa fa-paper-plane margin-right"></i>&nbsp;&nbsp;Send</a>
														{!! Form::close() !!}
													</li>
													@if($da->status != 'Sent')
														<li>
															<a data-toggle="modal" href='#editadvisories{{$da->id}}' data-backdrop="static" title="Edit" @click='editData({{json_encode($da->type)}})'><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;	Edit</a>
														</li>
													@endif
													<li>
														<a href="{{route('disasteradmin.advisory.delete',['id'=>$da->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
													</li>
												</ul>
											</div>
											@include('disaster_admin.modal.advisory_edit')
											@include('disaster_admin.modal.advisory_send')
										@else
											<div class="text-center">Message Deleted</div>
										@endif
									</td>
								@endif
							</tr>
						@empty
							<tr><td colspan="@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary') 8 @else 7 @endif"><p style="text-center">No Available Advisories</p></td></tr>
						@endforelse
					</table>
				</div>
			</div>
		</div>
		<div class="text-right">{{$disaster_advisories->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
	</div>
@endsection

@section('js')
	<!-- internal scripts -->
	{{Html::script('js/vue.min.js')}}
	<script>
		var app = new Vue({
			el: '#advisory',
			delimiters: ["[[","]]"],
			data: {
				subject_add: '',
				message_add: '',
				type_add: '',
				subject_edit: '',
				message_edit: '',
				type_edit: '',
				user: '',
				updated_at:'',
				enableAdd: false,
				enableEdit: false,

			},
			methods: {
				validateAdd(){
					if(this.subject_add != '' && this.message_add != '' && this.type_add != ''){
						this.enableAdd = true;
					} else {
						this.enableAdd = false;
					}
				},
				validateEdit(){
					if(this.subject_edit != '' && this.message_edit != '' && this.type_edit != ''){
						this.enableEdit = true;
					} else {
						this.enableEdit = false;
					}
				},
				editData(advisory){
					this.subject_edit = advisory.subject;
					this.message_edit = advisory.message;
					this.type_edit = advisory.type;
					this.validateEdit();
				},
			}
		});
	</script>
@endsection