@extends('layouts.app')

@section('title')
	Disaster Admin Evacuation Center
@endsection

@section('css')
<!-- internal styles -->
<style type="text/css">
	.table tr {
    text-align: left; 
	}
	.panel-heading{
		background-color: #1b3c5b!important;
		color: white !important;
	}
</style>

@endsection

@section('body')
<div id="evacuationcenter" class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="color: black;  ">
			<h3 class="panel-title"><b>Evacuation Center</b></h3>
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
		    @if(Session::has('flash_message'))
				<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
			@endif
		    	<div class="col-sm-3" style="padding-top:8px;padding-bottom: 8px">
		    		@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					<a class="btn btn-primary" data-toggle="modal" href='#addevacuationcenters' data-backdrop="static" style="height:35px">Add Evacuation Center</a>
					@include('disaster_admin.modal.evacuation_center_add')
					@endif
		    	</div>
		    	<div class="col-sm-9 text-right">
			    {{ Form::open(['route'=>'disasteradmin.evacuation_center','method'=>'get', 'class'=>'navbar-form']) }}  
			        <div class="form-group" >
			            <input type="text" class="form-control" placeholder="Search" name="search">
			            <span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
			            	<a href="{{route('disasteradmin.evacuation_center')}}" class="btn btn-primary">View All</a>
			        </div>
			    {{ Form::close() }}
				</div>
				<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th>Name of Evacuation Center</th>
						<th>Address</th>
						<th>Capacity (No. of Person)</th>
						<th>Condition</th>
						<th>Type</th>
						<th>Status</th>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
				 		<th>Actions</th> 
				 		@endif	
					</tr>
					@forelse($disaster_evacuation_centers as $dec)
					<tr style="border: 1px solid black; background-color: white; @if($dec->deleted_at != null) background-color: #f87272; @endif">
						<td class="text-left">{{$dec->name}}</td>
						<td >{{$dec->address}}</td>
						<td class="text-center">{{$dec->capacity}}</td>
						<td class="text-center">{{$dec->condition}}</td>
						<td class="text-center">{{$dec->type}}</td>
						<td>
							@if($dec->deleted_at == null)
								Active
							@else
								Deleted
							@endif
						</td>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<td>
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
								@if($dec->deleted_at == null)
									<li>
										<a data-toggle="modal" href='#editevacuationcenters{{$dec->id}}' data-backdrop="static" title="Edit" @click='editData("{{$dec->id}}","{{$dec->address}}","{{$dec->latitude}}","{{$dec->longitude}}","{{$dec->purok}}","{{$dec->street}}","{{$dec->barangay}}","{{$dec->municipality}}","{{$dec->province}}","{{$dec->name}}","{{$dec->capacity}}","{{$dec->type}}","{{$dec->condition}}","{{$dec->incharge}}","{{$dec->no_people}}","{{$dec->contact}}")'><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
									</li>
									<li>
										<a href="{{route('disasteradmin.evacuation_center.delete',['id'=>$dec->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;	Delete</a>
									</li>
								@else
									<li>
										<a href="{{route('disasteradmin.evacuation_center.restore',['id'=>$dec->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
									</li>
								@endif
								</ul>
							</div>
							@include('disaster_admin.modal.evacuation_center_edit')
						</td>
						@endif
					</tr>
					@empty
						<tr><td colspan="@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary') 7 @else 6 @endif"><p style="text-center">No Available Evacuation Center</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="text-right">{{$disaster_evacuation_centers->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJVX1wI8LOsRMUAlVbh1pTrXQvBFJoA4w">
</script>
{{Html::script('js/vue.min.js')}}
{{Html::script('js/axios.min.js')}}
<script>
	var myLatLng = {lat: {{Session::get('barangayAbout')->latitude}}, lng: {{Session::get('barangayAbout')->longitude}}};
	var marker = [];
    var app = new Vue({
        el: '#evacuationcenter',
       	delimiters: ["[[","]]"],
        data: {
        	latitude_add: '',
            longitude_add: '',
            address_add: '',
            street_add: '',
            barangay_add: '',
            purok_add: '',
            municipality_add: '',
            province_add: '',
            name_add: '',
            type_add: '',
            condition_add: '',
            incharge_add: '',
            capacity_add: '',
            address_edit: '',
			latitude_edit: '',
			longitude_edit: '',
			purok_edit: '',
			street_edit: '',
			barangay_edit: '',
			municipality_edit: '',
			province_edit: '',
			name_edit: '',
			capacity_edit: '',
			type_edit: '',
			condition_edit: '',
			incharge_edit: '',
			no_people_edit: '',
			contact_edit: '',
            user: '',
            updated_at:'',
            enableAdd: false,
            enableEdit: false,
            isLoadingGetAddress_add: false,
            isLoadingGetCoordinates_add: false,
            unknownAddress_add: '',
            isErrorAddress_add: false,
            unknownCoordinates_add: '',
            isErrorCoordinates_add: false,
            isLoadingGetAddress_edit: false,
            isLoadingGetCoordinates_edit: false,
            unknownAddress_edit: '',
            isErrorAddress_edit: false,
            unknownCoordinates_edit: '',
            isErrorCoordinates_edit: false,
            choose_add: 0,
            choose_edit: 0,
            gMap_add: '',
            id_edit: '',
            @foreach($disaster_evacuation_centers as $dec)
            gMap_edit{{$dec->id}}: '',
            @endforeach
        },
        mounted(){
        	let vm = this;
	        vm.gMap_add = new google.maps.Map(document.getElementById('map_add'), {
	          zoom: 16,
	          center: myLatLng
	        });
			google.maps.event.addListener(this.gMap_add, "click", function (event) {
			    vm.latitude_add = parseFloat(event.latLng.lat()).toFixed(7);
                vm.longitude_add = parseFloat(event.latLng.lng()).toFixed(7);
			    myLatLng = {lat: event.latLng.lat(), lng: event.latLng.lng()};
			    vm.addressDetailsCoordsAdd();
			});
			@foreach($disaster_evacuation_centers as $dec)
			vm.gMap_edit{{$dec->id}} = new google.maps.Map(document.getElementById('map_edit{{$dec->id}}'), {
	          zoom: 16,
	          center: {lat: {{$dec->latitude}}, lng:  {{$dec->longitude}}}
	        });
			marker = new google.maps.Marker({
	          position: {lat: {{$dec->latitude}}, lng:  {{$dec->longitude}}},
	          map: this.gMap_edit{{$dec->id}},
	      	});
			google.maps.event.addListener(this.gMap_edit{{$dec->id}}, "click", function (event) {
			    vm.latitude_edit = parseFloat(event.latLng.lat()).toFixed(7);
			    vm.longitude_edit = parseFloat(event.latLng.lng()).toFixed(7);
			    myLatLng = {lat: event.latLng.lat(), lng: event.latLng.lng()};
			    vm.addressDetailsCoordsEdit();
			});
			@endforeach
        },
        methods: {
        	validateAdd(){
        		if(this.latitude_add != '' && this.longitude_add != '' && this.address_add != '' && this.street_add != '' && this.municipality_add != '' && this.province_add != '' && this.name_add != '' && this.type_add != '' && this.condition_add != '' && this.incharge_add != '' && this.capacity_add != '' && this.barangay_add != '' && this.purok_add != ''){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.capacity_edit < this.no_people_edit){
        			this.condition_edit = 'Overloaded'
        		}
        		if(this.latitude_edit != '' && this.longitude_edit != '' && this.address_edit != '' && this.street_edit != '' && this.municipality_edit != '' && this.province_edit != '' && this.name_edit != '' && this.type_edit != '' && this.condition_edit != '' && this.incharge_edit != '' && this.capacity_edit != '' && this.barangay_edit != '' && this.barangay_edit != ''){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	editData(id,address,latitude,longitude,purok,street,barangay,municipality,province,name,capacity,type,condition,incharge,no_people,contact){
        		this.id_edit = id;
        		this.address_edit = address;
				this.latitude_edit = latitude;
				this.longitude_edit = longitude;
				this.purok_edit = purok;
				this.street_edit = street;
				this.barangay_edit = barangay;
				this.municipality_edit = municipality;
				this.province_edit = province;
				this.name_edit = name;
				this.capacity_edit = capacity;
				this.type_edit = type;
				this.condition_edit = condition;
				this.incharge_edit = incharge;
				this.no_people_edit = no_people;
				this.contact_edit = contact;
	            this.validateEdit();
        	},
        	async addressDetailsAdd() {
        		vm = this;
                try{
	                vm.isLoadingGetAddress_add = true;
	                vm.isErrorAddress_add = false;
	                vm.unknownAddress_add = '';
	                /*getting the longitude and latitude from the address*/
	                const responseLatLong = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?address='+vm.address_add+'&key=AIzaSyAJVX1wI8LOsRMUAlVbh1pTrXQvBFJoA4w');
	                responseLatLong.data.results.forEach((values) =>{
	                	vm.latitude_add = values.geometry.location.lat;
	                	vm.longitude_add = values.geometry.location.lng;
	                });
	                /*getting the municipal and province from the longitude and latitude of the address*/
				    const responseData = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+vm.latitude_add+','+vm.longitude_add+'&key=AIzaSyAJVX1wI8LOsRMUAlVbh1pTrXQvBFJoA4w');
				    vm.address_add = responseData.data.results[0].formatted_address;
				    responseData.data.results.forEach((values) => {
				    	var filtered_street_number = values.address_components.filter(function(address_component){
							return address_component.types.includes("street_number");
						}); 
				    	var filtered_street = values.address_components.filter(function(address_component){
							return address_component.types.includes("route");
						}); 
						if(filtered_street.length){
							vm.street_add = filtered_street_number[0].long_name+' '+filtered_street[0].long_name;
						}
				        var filtered_municipality = values.address_components.filter(function(address_component){
							return address_component.types.includes("locality");
						}); 
						if(filtered_municipality.length){
							vm.municipality_add = filtered_municipality[0].long_name;
						}
				        var filtered_province = values.address_components.filter(function(address_component){
							return address_component.types.includes("administrative_area_level_2");
						}); 
						if(filtered_province.length){
							vm.province_add = filtered_province[0].long_name;
						}
				    })
				    if (marker && marker.setMap) {
                        marker.setMap(null);
                    }
                    marker = new google.maps.Marker({
                      position: {lat: vm.latitude_add, lng: vm.longitude_add},
                      map: vm.gMap_add,
                    });
			    	vm.longitude_add = parseFloat(vm.longitude_add).toFixed(7);
				    vm.latitude_add = parseFloat(vm.latitude_add).toFixed(7);
				   	vm.isLoadingGetAddress_add = false;
				} catch (error) {
				    vm.isLoadingGetAddress_add = false;
				    vm.isErrorAddress_add = true;
				    vm.latitude_add = null;
				    vm.longitude_add = null;
				    vm.province_add = null;
				    vm.municipality_add = null;
				    vm.street_add = null;
				    vm.address_add = null;
				    vm.unknownAddress_add = '<span style="color:red">Cannot find the address.</span>';
				    console.log(error);
				}
            },
            async coordinatesDetailsAdd() {
            	vm = this;
                try{
	                vm.isLoadingCoordinates_add = true;
	                vm.isErrorCoordinates_add = false;
	                vm.unknownCoordinates_add = '';
	                /*getting the municipal and province from the longitude and latitude of the address*/
				    const responseData = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+vm.latitude_add+','+vm.longitude_add+'&key=AIzaSyAJVX1wI8LOsRMUAlVbh1pTrXQvBFJoA4w');
				    vm.address_add = responseData.data.results[0].formatted_address;
				    responseData.data.results.forEach((values) => {
				    	var filtered_street_number = values.address_components.filter(function(address_component){
							return address_component.types.includes("street_number");
						}); 
				    	var filtered_street = values.address_components.filter(function(address_component){
							return address_component.types.includes("route");
						}); 
						if(filtered_street.length){
							vm.street_add = filtered_street_number[0].long_name+' '+filtered_street[0].long_name;
						}
				        var filtered_municipality = values.address_components.filter(function(address_component){
							return address_component.types.includes("locality");
						}); 
						if(filtered_municipality.length){
							vm.municipality_add = filtered_municipality[0].long_name;
						}
				        var filtered_province = values.address_components.filter(function(address_component){
							return address_component.types.includes("administrative_area_level_2");
						}); 
						if(filtered_province.length){
							vm.province_add = filtered_province[0].long_name;
						}
				    })
				    if (marker && marker.setMap) {
                        marker.setMap(null);
                    }
                    marker = new google.maps.Marker({
                      position: {lat: parseFloat(vm.latitude_add), lng: parseFloat(vm.longitude_add)},
                      map: vm.gMap_add,
                    });
				   	vm.isLoadingGetCoordinates_add = false;
				} catch (error) {
				    vm.isLoadingGetCoordinates_add = false;
				    vm.isErrorCoordinates_add = true;
				    vm.latitude_add = null;
				    vm.longitude_add = null;
				    vm.province_add = null;
				    vm.municipality_add = null;
				    vm.street_add = null;
				    vm.address_add = null;
				    vm.unknownCoordinates_add = '<span style="color:red">Cannot find the coordinates.</span>';
				    console.log(error);
				}
            },
            async addressDetailsCoordsAdd() {
            	vm = this;
            	try{
                	vm.unknownAddress_add = '';
                	vm.isErrorAddress_add = false;
                	vm.unknownCoordinates_add = '';
                	vm.isErrorCoordinates_add = false;
                	vm.barangay_add = '';
					vm.purok_add = '';
                	let street_address ='';
                 	if (marker && marker.setMap) {
					    marker.setMap(null);
					}
                	marker = new google.maps.Marker({
			          position: myLatLng,
			          map: vm.gMap_add,
			      	});
                	/*getting the municipal and province from the longitude and latitude of the address*/
			      	const responseData = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+vm.latitude_add+','+vm.longitude_add+'&key=AIzaSyAIOnshF_fHAIQAYl1tvNKqcTlXTlW-l4o');
			      	vm.address_add = responseData.data.results[0].formatted_address;
			        responseData.data.results.forEach((values) => {
			            var filtered_street_number = values.address_components.filter(function(address_component){
							return address_component.types.includes("street_number");
						}); 
				    	var filtered_street = values.address_components.filter(function(address_component){
							return address_component.types.includes("route");
						}); 
						if(filtered_street.length > 0 && filtered_street_number.length > 0){
							vm.street_add = filtered_street_number[0].long_name+' '+filtered_street[0].long_name;
						} else if (filtered_street.length > 0) {
							vm.street_add = filtered_street[0].long_name;
						}
				        var filtered_municipality = values.address_components.filter(function(address_component){
							return address_component.types.includes("locality");
						}); 
						if(filtered_municipality.length){
							vm.municipality_add = filtered_municipality[0].long_name;
						}
				        var filtered_province = values.address_components.filter(function(address_component){
							return address_component.types.includes("administrative_area_level_2");
						}); 
						if(filtered_province.length){
							vm.province_add = filtered_province[0].long_name;
						}
			        })
			        vm.validateAdd();
			    } catch (error) {
			    	vm.isErrorAddress_add = false;
					vm.isErrorCoordinates_add = false;
			    	vm.latitude_add = null;
				    vm.longitude_add = null;
				    vm.province_add = null;
				    vm.municipality_add = null;
				    vm.street_add = null;
				    vm.address_add = null;
			    	vm.unknownAddress_add = '<span style="color:red">Cannot find the address.</span>';
			    	vm.unknownCoordinates_add = '<span style="color:red">Cannot find the coordinates.</span>';
			    	console.log(error);
			    }
            },
            async addressDetailsEdit() {
            	vm = this;
                try{
	                vm.isLoadingGetAddress_edit = true;
	                vm.isErrorAddress_edit = false;
	                vm.unknownAddress_edit = '';
	                /*getting the longitude and latitude from the address*/
	                const responseLatLong = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?address='+vm.address_edit+'&key=AIzaSyAJVX1wI8LOsRMUAlVbh1pTrXQvBFJoA4w');
	                responseLatLong.data.results.forEach((values) =>{
	                	vm.latitude_edit = values.geometry.location.lat;
	                	vm.longitude_edit = values.geometry.location.lng;
	                });
	                /*getting the municipal and province from the longitude and latitude of the address*/
				    const responseData = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+vm.latitude_edit+','+vm.longitude_edit+'&key=AIzaSyAJVX1wI8LOsRMUAlVbh1pTrXQvBFJoA4w');
				    responseData.data.results.forEach((values) => {
				    	var filtered_street_number = values.address_components.filter(function(address_component){
							return address_component.types.includes("street_number");
						}); 
				    	var filtered_street = values.address_components.filter(function(address_component){
							return address_component.types.includes("route");
						}); 
						if(filtered_street.length){
							vm.street_edit = filtered_street_number[0].long_name+' '+filtered_street[0].long_name;
						}
				        var filtered_municipality = values.address_components.filter(function(address_component){
							return address_component.types.includes("locality");
						}); 
						if(filtered_municipality.length){
							vm.municipality_edit = filtered_municipality[0].long_name;
						}
				        var filtered_province = values.address_components.filter(function(address_component){
							return address_component.types.includes("administrative_area_level_2");
						}); 
						if(filtered_province.length){
							vm.province_edit = filtered_province[0].long_name;
						}
						vm.address_edit = vm.street_edit+", "+vm.municipality_edit+", "+vm.province_edit;
				    })
				    if (marker && marker.setMap) {
                        marker.setMap(null);
                    }
                    @foreach($disaster_evacuation_centers as $decade)
                        if({{$decade->id}} == vm.id_edit){
                            marker = new google.maps.Marker({
                              position: {lat: vm.latitude_edit, lng: vm.longitude_edit},
                              map: vm.gMap_edit{{$decade->id}},
                            });
                        }
                    @endforeach
			    	vm.longitude_add = parseFloat(vm.longitude_add).toFixed(7);
				    vm.latitude_add = parseFloat(vm.latitude_add).toFixed(7);
				   	vm.isLoadingGetAddress_edit = false;
				} catch (error) {
				    vm.isLoadingGetAddress_edit = false;
				    vm.isErrorAddress_edit = true;
				    vm.latitude_edit = null;
				    vm.longitude_edit = null;
				    vm.province_edit = null;
				    vm.municipality_edit = null;
				    vm.street_edit = null;
				    vm.address_edit = null;
				    vm.unknownAddress_edit = '<span style="color:red">Cannot find the address.</span>';
				    console.log(error);
				}
            },
            async coordinatesDetailsEdit() {
            	vm = this;
                try{
	                vm.isLoadingCoordinates_edit = true;
	                vm.isErrorCoordinates_edit = false;
	                vm.unknownCoordinates_edit = '';
	                /*getting the municipal and province from the longitude and latitude of the address*/
				    const responseData = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+vm.latitude_edit+','+vm.longitude_edit+'&key=AIzaSyAJVX1wI8LOsRMUAlVbh1pTrXQvBFJoA4w');
				    responseData.data.results.forEach((values) => {
				    	var filtered_street_number = values.address_components.filter(function(address_component){
							return address_component.types.includes("street_number");
						}); 
				    	var filtered_street = values.address_components.filter(function(address_component){
							return address_component.types.includes("route");
						}); 
						if(filtered_street.length){
							vm.street_edit = filtered_street_number[0].long_name+' '+filtered_street[0].long_name;
						}
				        var filtered_municipality = values.address_components.filter(function(address_component){
							return address_component.types.includes("locality");
						}); 
						if(filtered_municipality.length){
							vm.municipality_edit = filtered_municipality[0].long_name;
						}
				        var filtered_province = values.address_components.filter(function(address_component){
							return address_component.types.includes("administrative_area_level_2");
						}); 
						if(filtered_province.length){
							vm.province_edit = filtered_province[0].long_name;
						}
						vm.address_edit = vm.street_edit+", "+vm.municipality_edit+", "+vm.province_edit;
				    })
				    if (marker && marker.setMap) {
                        marker.setMap(null);
                    }
                    @foreach($disaster_evacuation_centers as $deccde)
                        if({{$deccde->id}} == vm.id_edit){
                            marker = new google.maps.Marker({
                              position: {lat: parseFloat(vm.latitude_edit), lng: parseFloat(vm.longitude_edit)},
                              map: vm.gMap_edit{{$deccde->id}},
                            });
                        }
                    @endforeach
				   	vm.isLoadingGetCoordinates_edit = false;
				    // vm.validate();
				} catch (error) {
				    vm.isLoadingGetCoordinates_edit = false;
				    vm.isErrorCoordinates_edit = true;
				    vm.latitude_edit = null;
				    vm.longitude_edit = null;
				    vm.province_edit = null;
				    vm.municipality_edit = null;
				    vm.street_edit = null;
				    vm.address_edit = null;
				    vm.unknownCoordinates_edit = '<span style="color:red">Cannot find the coordinates.</span>';
				    console.log(error);
				}
            },
            async addressDetailsCoordsEdit() {
            	vm = this;
            	try{
                	vm.unknownAddress_edit = '';
                	vm.isErrorAddress_edit = false;
                	vm.unknownCoordinates_edit = '';
                	vm.isErrorCoordinates_edit = false;
                	vm.barangay_edit = '';
					vm.purok_edit = '';
                	let street_address ='';
                 	if (marker && marker.setMap) {
					    marker.setMap(null);
					}
					@foreach($disaster_evacuation_centers as $decadce)
						if({{$decadce->id}} == vm.id_edit){
							marker = new google.maps.Marker({
					          position: myLatLng,
					          map: vm.gMap_edit{{$decadce->id}},
					      	});
						}
			      	@endforeach
                	/*getting the municipal and province from the longitude and latitude of the address*/
			      	const responseData = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+vm.latitude_edit+','+vm.longitude_edit+'&key=AIzaSyAIOnshF_fHAIQAYl1tvNKqcTlXTlW-l4o');
			      	vm.address_edit = responseData.data.results[0].formatted_address;
			        responseData.data.results.forEach((values) => {
			            var filtered_street_number = values.address_components.filter(function(address_component){
							return address_component.types.includes("street_number");
						}); 
				    	var filtered_street = values.address_components.filter(function(address_component){
							return address_component.types.includes("route");
						}); 
						if(filtered_street.length > 0 && filtered_street_number.length > 0){
							vm.street_edit = filtered_street_number[0].long_name+' '+filtered_street[0].long_name;
						} else if (filtered_street.length > 0) {
							vm.street_edit = filtered_street[0].long_name;
						}
				        var filtered_municipality = values.address_components.filter(function(address_component){
							return address_component.types.includes("locality");
						}); 
						if(filtered_municipality.length){
							vm.municipality_edit = filtered_municipality[0].long_name;
						}
				        var filtered_province = values.address_components.filter(function(address_component){
							return address_component.types.includes("administrative_area_level_2");
						}); 
						if(filtered_province.length){
							vm.province_edit = filtered_province[0].long_name;
						}
			        })
			        vm.longitude_edit = parseFloat(vm.longitude_edit).toFixed(7);
				    vm.latitude_edit = parseFloat(vm.latitude_edit).toFixed(7);
			        vm.validateEdit();
			    } catch (error) {
			    	vm.isErrorAddress_edit = false;
					vm.isErrorCoordinates_edit = false;
			    	vm.latitude_edit = null;
				    vm.longitude_edit = null;
				    vm.province_edit = null;
				    vm.municipality_edit = null;
				    vm.street_edit = null;
				    vm.address_edit = null;
			    	vm.unknownAddress_edit = '<span style="color:red">Cannot find the address.</span>';
			    	vm.unknownCoordinates_edit = '<span style="color:red">Cannot find the coordinates.</span>';
			    	console.log(error);
			    }
            },
        }
    });
</script>
@endsection