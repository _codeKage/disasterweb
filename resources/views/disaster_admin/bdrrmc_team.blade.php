@extends('layouts.app')

@section('title')
	Disaster Admin BDRRMC Team
@endsection

@section('css')
<!-- internal styles -->
	<style>
		.panel-heading{
			background-color: #1b3c5b!important;
			color: white !important;
		}
	</style>
@endsection

@section('body')
<div id="bdrrmc_team" class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="color: black;  ">
			<h3 class="panel-title"><b>BDRRMC Team</b></h3>
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
		    	@if(Session::has('flash_message'))
					<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
				@endif
		    	<div class="col-sm-3" style="padding-top:8px;padding-bottom: 8px">
		    		@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					<a class="btn btn-primary" data-toggle="modal" href='#addbdrrmcteam' data-backdrop="static" style="height:35px">Add BDRRMC Team</a>
					@include('disaster_admin.modal.bdrrmc_team_add')
					@endif
		    	</div>
		    	<div class="col-sm-9 text-right">
			    {{ Form::open(['route'=>'disasteradmin.bdrrmc_team','method'=>'get', 'class'=>'navbar-form']) }}  
			        <div class="form-group" >
			            <input type="text" class="form-control" placeholder="Search" name="search">
			            <span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
			            <a href="{{route('disasteradmin.bdrrmc_team')}}" class="btn btn-primary">View All</a>
			        </div>
			    {{ Form::close() }}
				</div>
				<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					
					<tr style="border: 1px solid black;">
						<th>Code</th>
						<th>Name</th>
						<th>Roles</th>
						<th>Status</th>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<th>Action</th>	
						@endif
					</tr>
					@forelse($disaster_bdrrmc_teams as $dbt)
					<tr style="border: 1px solid black; @if($dbt->deleted_at != null) background-color: #f87272; @endif">
						<td class="text-left">{{$dbt->code}}</td>
						<td class="text-left">{{$dbt->name}}</td>
						<td class="text-left">{{$dbt->roles}}</td>
						<td>
							@if($dbt->deleted_at == null)
								Active
							@else
								Deleted
							@endif
						</td>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<td>
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
								@if($dbt->deleted_at == null)
									<li>
										<a data-toggle="modal" href='#editbdrrmcteam{{$dbt->id}}' data-backdrop="static" title="Edit" @click='editData("{{$dbt->code}}","{{$dbt->name}}","{{$dbt->roles}}")'><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
									</li>
									<li>
										<a href="{{route('disasteradmin.bdrrmc_team.delete',['id'=>$dbt->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
									</li>
								@else
									<li>
										<a href="{{route('disasteradmin.bdrrmc_team.restore',['id'=>$dbt->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
									</li>
								@endif
								</ul>
							</div>
							@include('disaster_admin.modal.bdrrmc_team_edit')	
						</td>
						@endif
					</tr>
					@empty
						<tr><td colspan="@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary') 5 @else 4 @endif"><p style="text-center">No Available BDRRMC Teams</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="text-right">{{$disaster_bdrrmc_teams->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
<script>
    var app = new Vue({
        el: '#bdrrmc_team',
       	delimiters: ["[[","]]"],
        data: {
        	code_add: '',
	        name_add: '',
	        roles_add: '',
            code_edit: '',
	        name_edit: '',
	        roles_edit: '',
            enableAdd: false,
            enableEdit: false,

        },
        methods: {
        	validateAdd(){
        		if(this.code_add != '' && this.name_add != '' && this.roles_add != ''){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.code_edit != '' && this.name_edit != '' && this.roles_edit != ''){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	editData(code,name,roles){
        		this.code_edit = code;
	            this.name_edit = name;
	            this.roles_edit = roles;
	            this.validateEdit();
        	}
        }
    });
</script>
@endsection