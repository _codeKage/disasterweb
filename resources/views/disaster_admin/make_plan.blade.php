@extends('layouts.app')

@section('title')
	Disaster Admin Make Plan
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #1b3c5b; color: white;  ">
            <h3 class="panel-title">MAKE A PLAN</h3>     
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
		    	@if(Session::has('flash_message'))
					<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
				@endif
				<table class="table-striped table" style="border: 1px solid black; font-size: 12px; width: 50%; margin-left: 25%;">
					<caption>Make a Plan</caption>
					<tr style="border: 1px solid black;">
						<th>Steps</th>
						<th>Subject</th>
						<th>Status</th>
						<th>Upload Form</th>	
					</tr>
					<tr style="border: 1px solid black;">
						{!! Form::open(['route'=>'disasteradmin.make_plan.identify_escape_routes','files'=>true,'id'=>'identifyEscapeRoutes']) !!}
						<input type="hidden" name="id" value="{{$plan->id}}">
						<td>1</td>
						<td>Identify Escape Routes</td>
						<td>
							@if($plan->identify_escape_routes != '')
								With File
							@else
								Without File
							@endif
						</td>
						<td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<label for="identify_escape_routes" class="btn btn-primary">Upload New File</label>
							<input type="file" id="identify_escape_routes" name="identify_escape_routes" class="hide" onchange="plan1()">
							@else
							<a href="{{asset('files/disaster_plan/'.$plan->identify_escape_routes)}}" class="btn btn-primary" download></i>Download File</span></a>
							@endif
						</td>
						{!! Form::close() !!}
					</tr>
					<tr style="border: 1px solid black;">
						{!! Form::open(['route'=>'disasteradmin.make_plan.establish_meeting_places','files'=>true,'id'=>'establishMeetingPlaces']) !!}
						<input type="hidden" name="id" value="{{$plan->id}}">
						<td>2</td>
						<td>Establish Meeting Places</td>
						<td>
							@if($plan->establish_meeting_places != '')
								With File
							@else
								Without File
							@endif
						</td>
						<td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<label for="establish_meeting_places" class="btn btn-primary">Upload New File</label>
							<input type="file" id="establish_meeting_places" name="establish_meeting_places" class="hide" onchange="plan2()">
							@else
							<a href="{{asset('files/disaster_plan/'.$plan->establish_meeting_places)}}" class="btn btn-primary" download></i>Download File</span></a>
							@endif
						</td>
						{!! Form::close() !!}
					</tr>
					<tr style="border: 1px solid black;">
						{!! Form::open(['route'=>'disasteradmin.make_plan.plan_for_children','files'=>true,'id'=>'planForChildren']) !!}
						<input type="hidden" name="id" value="{{$plan->id}}">
						<td>3</td>
						<td>Plan For Children</td>
						<td>
							@if($plan->plan_for_children != '')
								With File
							@else
								Without File
							@endif
						</td>
						<td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<label for="plan_for_children" class="btn btn-primary">Upload New File</label>
							<input type="file" id="plan_for_children" name="plan_for_children" class="hide" onchange="plan3()">
							@else
							<a href="{{asset('files/disaster_plan/'.$plan->plan_for_children)}}" class="btn btn-primary" download></i>Download File</span></a>
							@endif
						</td>
						{!! Form::close() !!}
					</tr>
					<tr style="border: 1px solid black;">
						{!! Form::open(['route'=>'disasteradmin.make_plan.address_any_special_health_needs','files'=>true,'id'=>'addressAnySpecialHealthNeeds']) !!}
						<input type="hidden" name="id" value="{{$plan->id}}">
						<td>4</td>
						<td>Address Any Special Health Needs</td>
						<td>
							@if($plan->address_any_special_health_needs != '')
								With File
							@else
								Without File
							@endif
						</td>
						<td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<label for="address_any_special_health_needs" class="btn btn-primary">Upload New File</label>
							<input type="file" id="address_any_special_health_needs" name="address_any_special_health_needs" class="hide" onchange="plan4()">
							@else
							<a href="{{asset('files/disaster_plan/'.$plan->address_any_special_health_needs)}}" class="btn btn-primary" download></i>Download File</span></a>
							@endif
						</td>
						{!! Form::close() !!}
					</tr>
					<tr style="border: 1px solid black;">
						{!! Form::open(['route'=>'disasteradmin.make_plan.plan_for_pets','files'=>true,'id'=>'planForPets']) !!}
						<input type="hidden" name="id" value="{{$plan->id}}">
						<td>5</td>
						<td>Plan For Pets</td>
						<td>
							@if($plan->plan_for_pets != '')
								With File
							@else
								Without File
							@endif
						</td>
						<td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<label for="plan_for_pets" class="btn btn-primary">Upload New File</label>
							<input type="file" id="plan_for_pets" name="plan_for_pets" class="hide" onchange="plan5()">
							@else
							<a href="{{asset('files/disaster_plan/'.$plan->plan_for_pets)}}" class="btn btn-primary" download></i>Download File</span></a>
							@endif
						</td>
						{!! Form::close() !!}
					</tr>
					<tr style="border: 1px solid black;">
						{!! Form::open(['route'=>'disasteradmin.make_plan.plan_for_specific_risks','files'=>true,'id'=>'planForSpecificRisks']) !!}
						<input type="hidden" name="id" value="{{$plan->id}}">
						<td>6</td>
						<td>Plan For Specific Risks</td>
						<td>
							@if($plan->plan_for_specific_risks != '')
								With File
							@else
								Without File
							@endif
						</td>
						<td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<label for="plan_for_specific_risks" class="btn btn-primary">Upload New File</label>
							<input type="file" id="plan_for_specific_risks" name="plan_for_specific_risks" class="hide" onchange="plan6()">
							@else
							<a href="{{asset('files/disaster_plan/'.$plan->plan_for_specific_risks)}}" class="btn btn-primary" download></i>Download File</span></a>
							@endif
						</td>
						{!! Form::close() !!}
					</tr>
					<tr style="border: 1px solid black;">
						{!! Form::open(['route'=>'disasteradmin.make_plan.record_emergency_contact_information','files'=>true,'id'=>'recordEmergencyContactInformation']) !!}
						<input type="hidden" name="id" value="{{$plan->id}}">
						<td>7</td>
						<td>Identify Record Rmergency Contact Information</td>
						<td>
							@if($plan->record_emergency_contact_information != '')
								With File
							@else
								Without File
							@endif
						</td>
						<td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<label for="record_emergency_contact_information" class="btn btn-primary">Upload New File</label>
							<input type="file" id="record_emergency_contact_information" name="record_emergency_contact_information" class="hide" onchange="plan7()">
							@else
							<a href="{{asset('files/disaster_plan/'.$plan->record_emergency_contact_information)}}" class="btn btn-primary" download></i>Download File</span></a>
							@endif
						</td>
						{!! Form::close() !!}
					</tr>
					<tr style="border: 1px solid black;">
						{!! Form::open(['route'=>'disasteradmin.make_plan.complete_safe_home_instructions','files'=>true,'id'=>'completeSafeHomeInstructions']) !!}
						<input type="hidden" name="id" value="{{$plan->id}}">
						<td>8</td>
						<td>Complete Safe Home Instructions File</td>
						<td>
							@if($plan->complete_safe_home_instructions != '')
								With File
							@else
								Without File
							@endif
						</td>
						<td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<label for="complete_safe_home_instructions" class="btn btn-primary">Upload New File</label>
							<input type="file" id="complete_safe_home_instructions" name="complete_safe_home_instructions" class="hide" onchange="plan8()">
							@else
							<a href="{{asset('files/disaster_plan/'.$plan->complete_safe_home_instructions)}}" class="btn btn-primary" download></i>Download File</span></a>
							@endif
						</td>
						{!! Form::close() !!}
					</tr>
					<tr style="border: 1px solid black;">
						{!! Form::open(['route'=>'disasteradmin.make_plan.summary','files'=>true,'id'=>'summaryID']) !!}
						<input type="hidden" name="id" value="{{$plan->id}}">
						<td></td>
						<td>Summary</td>
						<td>
							@if($plan->summary != '')
								With File
							@else
								Without File
							@endif
						</td>
						<td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<label for="summary" class="btn btn-primary">Upload New File</label>
							<input type="file" id="summary" name="summary" class="hide" onchange="plan9()">
							@else
							<a href="{{asset('files/disaster_plan/'.$plan->summary)}}" class="btn btn-primary" download></i>Download File</span></a>
							@endif
						</td>
						{!! Form::close() !!}
					</tr>	
				</table>
			</div>
		</div>
	</div>
</div>		

@endsection

@section('js')
<!-- internal scripts -->
<script>
	function plan1(){
		document.getElementById("identifyEscapeRoutes").submit();
	}
	function plan2(){
		document.getElementById("establishMeetingPlaces").submit();
	}
	function plan3(){
		document.getElementById("planForChildren").submit();
	}
	function plan4(){
		document.getElementById("addressAnySpecialHealthNeeds").submit();
	}
	function plan5(){
		document.getElementById("planForPets").submit();
	}
	function plan6(){
		document.getElementById("planForSpecificRisks").submit();
	}
	function plan7(){
		document.getElementById("recordEmergencyContactInformation").submit();
	}
	function plan8(){
		document.getElementById("completeSafeHomeInstructions").submit();
	}
	function plan9(){
		document.getElementById("summaryID").submit();
	}
</script>
@endsection