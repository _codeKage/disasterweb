@extends('layouts.app')
@section('title')
	Disaster Admin Safety Tips
@endsection
@section('css')
<!-- internal styles -->
<style>
	.panel-heading{
		background-color: #1b3c5b!important;
		color: white !important;
	}
</style>
@endsection
@section('body')
<div id="safety_tips" class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="color: black; ">
			<h3 class="panel-title" ><b>SAFETY TIPS</b></h3>
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
		    	@if(Session::has('flash_message'))
					<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
				@endif
		    	<div class="col-sm-3" style="padding-top:8px;padding-bottom: 8px">
		    		@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					<a class="btn btn-primary" data-toggle="modal" href='#addsafetytips' data-backdrop="static" style="height:35px">Add Hazard</a>
					@include('disaster_admin.modal.safety_tips_add')
					@endif
		    	</div>
		    	<div class="col-sm-9 text-right">
			    {{ Form::open(['route'=>'disasteradmin.safety_tips','method'=>'get', 'class'=>'navbar-form']) }}  
			        <div class="form-group" >
			            <input type="text" class="form-control" placeholder="Search" name="search">
			            <span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
			            <a href="{{route('disasteradmin.safety_tips')}}" class="btn btn-primary">View All</a>
			        </div>
			    {{ Form::close() }}
				</div>
				<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th rowspan="2">Hazard</th>
						<th rowspan="2">Description</th>
						<th colspan="3">What To Do</th>
						<th rowspan="2">Facts</th>
						<th rowspan="2">Safety Tip</th>
						<th rowspan="2">Status</th>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<th rowspan="2">Action</th>
						@endif
					</tr>
					<tr style="border: 1px solid black;">
						<th>Before</th>
						<th>During</th>
						<th>After</th>
					</tr>
					@forelse($disaster_safety_tips as $dst)
					<tr style="border: 1px solid black; @if($dst->deleted_at != null) background-color: #f87272; @endif">
						<td class="text-left">{!!nl2br($dst->hazard)!!}</td>
						<td class="text-left">{!!nl2br($dst->description)!!}</td>
						<td class="text-left">{!!nl2br($dst->before)!!}</td>
						<td class="text-left">{!!nl2br($dst->during)!!}</td>
						<td class="text-left">{!!nl2br($dst->after)!!}</td>
						<td class="text-left">{!!nl2br($dst->facts)!!}</td>
						<td class="text-left">{!!nl2br($dst->tips)!!}</td>
						<td>
							@if($dst->deleted_at == null)
								Active
							@else
								Deleted
							@endif
						</td>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<td>
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
								@if($dst->deleted_at == null)
									<li>
										<a data-toggle="modal" href='#editsafetytips{{$dst->id}}' data-backdrop="static" title="Edit" @click='editData({{json_encode($dst)}})'><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
									</li>
									<li>
										<a href="{{route('disasteradmin.safety_tips.delete',['id'=>$dst->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
									</li>
								@else
									<li>
										<a href="{{route('disasteradmin.safety_tips.restore',['id'=>$dst->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
									</li>
								@endif
								</ul>
							</div>
							@include('disaster_admin.modal.safety_tips_edit')		
						</td>
						@endif
					</tr>
					@empty
						<tr><td colspan="@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary') 9 @else 8 @endif"><p style="text-center">No Available Safety Tips</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="text-right">{{$disaster_safety_tips->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection
@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
<script>
    var app = new Vue({
        el: '#safety_tips',
       	delimiters: ["[[","]]"],
        data: {
        	hazard_add: '',
        	hazard_others_add:'',
	        description_add: '',
	        facts_add: '',
	        tips_add: '',
	        before_add: '',
	        during_add: '',
	        after_add: '',
            hazard_edit: '',
            hazard_others_edit:'',
	        description_edit: '',
	        facts_edit: '',
	        tips_edit: null,
	        before_edit: '',
	        during_edit: '',
	        after_edit: '',
            enableAdd: false,
            enableEdit: false,
        },
        methods: {
        	validateAdd(){
        		if(this.hazard_add != '' && this.description_add != '' && this.facts_add != '' && this.tips_add != '' && this.before_add != '' && this.during_add != '' && this.after_add != ''){
        			if(this.hazard_add == 'Others' && this.hazard_others_add == ''){
        				this.enableAdd = false;
        			} else {
        				this.enableAdd = true;
        			}
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.hazard_edit != '' && this.description_edit != '' && this.facts_edit != '' && this.tips_edit != '' && this.before_edit != '' && this.during_edit != '' && this.after_edit != ''){
        			if(this.hazard_edit == 'Others' && this.hazard_others_edit == ''){
        				this.enableEdit = false;
        			} else {
        				this.enableEdit = true;
        			}
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	editData(disaster_safety_tips){
        		if(disaster_safety_tips.hazard == 'Drought' || disaster_safety_tips.hazard == 'Earthquake' || disaster_safety_tips.hazard == 'Flood' || disaster_safety_tips.hazard == 'Heavy Rains' || disaster_safety_tips.hazard == 'Landslide/Mudslide' || disaster_safety_tips.hazard == 'Lightning Incidents' || disaster_safety_tips.hazard == 'Sinkhole' || disaster_safety_tips.hazard == 'Storm Surge' || disaster_safety_tips.hazard == 'Tornadoes/Whirlwinds' || disaster_safety_tips.hazard == 'Tsunami' || disaster_safety_tips.hazard == 'Typhoon' || disaster_safety_tips.hazard == 'Volcanic Eruption' || disaster_safety_tips.hazard == 'Wild Fire'){
        			this.hazard_edit = disaster_safety_tips.hazard;
        		} else {
        			this.hazard_edit = 'Others';
        			this.hazard_others_edit = disaster_safety_tips.hazard;
        		}
	            this.description_edit = disaster_safety_tips.description;
	            this.facts_edit = disaster_safety_tips.facts;
	            this.tips_edit = disaster_safety_tips.tips;
	            this.before_edit = disaster_safety_tips.before;
	            this.during_edit = disaster_safety_tips.during;
	            this.after_edit = disaster_safety_tips.after;
	            this.validateEdit();
        	}
        }
    });
</script>

@endsection