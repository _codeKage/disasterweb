@extends('layouts.app')

@section('title')
	Disaster Admin BDRRMC Team Member
@endsection

@section('css')
<!-- internal styles -->
<style>
	.panel-heading{
		background-color: #1b3c5b!important;
		color: white !important;
	}
</style>
@endsection

@section('body')
<div id="bdrrmc_team_member" class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="color: black;  ">
			<h3 class="panel-title" ><b>BDRRMC Team Member</b></h3>
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
		    	@if(Session::has('flash_message'))
					<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
				@endif
		    	<div class="col-sm-6">
		    		@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					<a class="btn btn-primary" data-toggle="modal" href='#addbdrrmcteammember' data-backdrop="static" style="height:35px;margin-top:8px;margin-bottom:8px">Add BDRRMC Team Member</a>
					@include('disaster_admin.modal.bdrrmc_team_member_add')
					@endif
		    	</div>
		    	<div class="col-sm-6 text-right">
			    {{ Form::open(['route'=>'disasteradmin.bdrrmc_team_member','method'=>'get', 'class'=>'navbar-form']) }}  
			        <div class="form-group" >
			            <input type="text" class="form-control" placeholder="Search" name="search">
			            <span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
			             <a href="{{route('disasteradmin.bdrrmc_team_member')}}" class="btn btn-primary">View All</a>
			        </div>
			    {{ Form::close() }}
				</div>
				{{-- <div class="text-right" style="margin-top: 10px;">
					<button class="btn btn-primary" onclick="printTable()">Convert To Excel</button>
				</div> --}}
				<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th rowspan ="2">BDRRMC Member Name</th>
						<th rowspan ="2">Team</th>
						<th rowspan ="2">Contact Number</th>
						<th colspan="2">Period</th>
						<th rowspan ="2">Status</th>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<th rowspan ="2">Action</th>	
						@endif
					</tr>
					<tr>
						<th>From</th>
						<th>To</th>
					</tr>

					@forelse($disaster_bdrrmc_team_members as $dbtm)
					<tr style="border: 1px solid black; background-color: white; @if($dbtm->deleted_at != null) background-color: #f87272; @endif">
						<td class="text-left">{{$dbtm->last_name}}, {{$dbtm->first_name}} {{$dbtm->middle_name}}</td>
						<td class="text-left">{{$dbtm->name}}</td>
						<td>{{$dbtm->contact}}</td>
						<td>{{$dbtm->period_from}}</td>
						<td>{{$dbtm->period_to}}</td>
						<td>
							@if($dbtm->deleted_at == null)
								Active
							@else
								Deleted
							@endif
						</td>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<td>
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
								@if($dbtm->deleted_at == null)
									<li>
										<a data-toggle="modal" href='#editbdrrmcteammember{{$dbtm->id}}' data-backdrop="static" title="Edit" @click='editData("{{$dbtm->first_name}}","{{$dbtm->middle_name}}","{{$dbtm->last_name}}","{{$dbtm->gender}}","{{$dbtm->birthdate}}","{{$dbtm->disaster_team_id}}","{{$dbtm->period_from}}","{{$dbtm->period_to}}","{{$dbtm->contact}}")'><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
									</li>
									<li>
										<a href="{{route('disasteradmin.bdrrmc_team_member.delete',['id'=>$dbtm->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
									</li>
								@else
									<li>
										<a href="{{route('disasteradmin.bdrrmc_team_member.restore',['id'=>$dbtm->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
									</li>
								@endif
								</ul>
							</div>
							@include('disaster_admin.modal.bdrrmc_team_member_edit')
						</td>
						@endif
					</tr>
					@empty
						<tr><td colspan="@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary') 7 @else 6 @endif"><p style="text-center">No Available BDRRMC Teams</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3" style="margin-bottom: 5px">
			@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
			{{ Form::open(['route'=>'disasteradmin.bdrrmc_team_member.print','method'=>'get', 'class'=>'navbar-form','target'=>'_blank']) }}  
		        <input type="hidden" name="print" value="{{(isset($_GET["search"]) ? $_GET["search"]:"" )}}">
		        <button type="submit" class="btn btn-primary">Print List</button>
		    {{ Form::close() }}
		    @endif
		</div>
		<div class="col-md-9" style="margin-bottom: 5px">
			<div class="text-right">{{$disaster_bdrrmc_team_members->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
<script>
    var app = new Vue({
        el: '#bdrrmc_team_member',
       	delimiters: ["[[","]]"],
        data: {
        	first_name_add: '',
	        middle_name_add: '',
	        last_name_add: '',
	        gender_add: '',
	        birthdate_add: '',
	        disaster_team_id_add: '',
	        period_from_add: '',
	        period_to_add: '',
	        contact_add: '',
            first_name_edit: '',
	        middle_name_edit: '',
	        last_name_edit: '',
	        gender_edit: '',
	        birthdate_edit: '',
	        disaster_team_id_edit: '',
	        period_from_edit: '',
	        period_to_edit: '',
	        contact_edit: '',
            enableAdd: false,
            enableEdit: false,
            isErrorPeriod_add: false,
            isErrorPeriod_edit: false,
            errorPeriod_add:'',
            errorPeriod_edit:'',

        },
        methods: {
        	validateAdd(){
        		if(this.period_from_add > this.period_to_add){
        			this.isErrorPeriod_add = true;
        			this.errorPeriod_add = '<span style="color:red">Period To cannot be before Period From</span>';
        		} else {
        			this.isErrorPeriod_add = false;
        			this.errorPeriod_add = '';
        		}
        		if(this.first_name_add != '' && this.middle_name_add != '' && this.last_name_add != '' && this.gender_add != '' && this.birthdate_add != '' && this.disaster_team_id_add != '' && this.period_from_add != '' && this.period_to_add != '' && this.contact_add != '' && this.isErrorPeriod_add == false){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.period_from_edit > this.period_to_edit){
        			this.isErrorPeriod_edit = true;
        			this.errorPeriod_edit = '<span style="color:red">Period To cannot be before Period From</span>';
        		} else {
        			this.isErrorPeriod_edit = false;
        			this.errorPeriod_edit = '';
        		}
        		if(this.first_name_edit != '' && this.middle_name_edit != '' && this.last_name_edit != '' && this.gender_edit != '' && this.birthdate_edit != '' && this.disaster_team_id_edit != '' && this.period_from_edit != '' && this.period_to_edit != '' && this.contact_edit != '' && this.isErrorPeriod_edit == false){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	editData(first_name,middle_name,last_name,gender,birthdate,disaster_team_id,period_from,period_to,contact){
        		this.first_name_edit = first_name;
	            this.middle_name_edit = middle_name;
	            this.last_name_edit = last_name;
	            this.gender_edit = gender;
	            this.birthdate_edit = birthdate;
	            this.disaster_team_id_edit = disaster_team_id;
	            this.period_from_edit = period_from;
	            this.period_to_edit = period_to;
	            this.contact_edit = contact;
	            this.validateEdit();
        	}
        }
    });
</script>
@endsection