@extends('layouts.app')

@section('title')
	Disaster Admin Preparedness Kit
@endsection

@section('css')
<!-- internal styles -->

{{Html::style('summernote/summernote.css')}}
<style type="text/css">
	.card-header
	{

		background-color: #1b3c5b;
		color: white;
		text-align: left;
		font-size: 2em;
		text-indent: 6mm;
	}
	.card-block
	{
		border:1px solid #bfbfbf;
		
	}
	.card-block li
	{
		margin-right: 5px;
		margin-left:5px;
		margin-top: 5px;
		margin-bottom: 5px;
		font-size: 16px;
		list-style-type: square;
	}
	.card
	{
		width: 80%;
		margin: auto;
	}
	.container1
	{
		margin-top: 25px;
	}
	.panel-heading{
		background-color: #1b3c5b!important;
		color: white !important;
	}
</style>
@endsection

@section('body')

<div class="top20">
	{!! Form::open(['route'=>'disasteradmin.preparedness_kit.update']) !!}
	<input type="hidden" name="id" value="{{$pk->id}}">
	<div class="panel panel-default" >
        <div class="panel-heading" style="color: black;  ">
			<h3 class="panel-title" ><b>PREPAREDNESS KIT</b></h3>
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
		    	@if(Session::has('flash_message'))
					<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
				@endif
				<div class="card">
					<div class="card-header">
					    Get a Kit
					</div>
					<div class="card-block">
						<textarea  id="summernote" name="kit" class="form-control" name="txt" style="width: 100%; height: 200px">{!!$pk->kit!!}</textarea>
					</div>
				</div>
				<!-- <div class="input-group" style="width: 50%; margin-left: 25%; margin-top: 20px;">
				    <input type="email" class="form-control" name="email" placeholder="Send to my email">
				    <span class="input-group-btn">
				    	<input class="btn btn-success" name="submit"  value="Send" type="submit">
				   	</span>
				</div> -->
			</div>
		</div>
	</div>
	<div  style="float: right; margin-top: 3px; margin-right: 5%;">
	    <input type="submit" name="submit" class="btn btn-success" value="Update">
	</div>
	{!! Form::close() !!}
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('summernote/summernote.js')}}
<script>
	$(document).ready(function() {
	  $('#summernote').summernote({
	  height: 200,
	  minHeight: null,
	  maxHeight: null
		});
	});
</script>
@endsection