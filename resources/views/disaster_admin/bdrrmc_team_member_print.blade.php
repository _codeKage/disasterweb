<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3 style="color: blue;">BDRRMC Team Member</h3>
<div class="content">
	<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
		<tr style="border: 1px solid black;">
			<th rowspan ="2">BDRRMC Member Name</th>
			<th rowspan ="2">Team</th>
			<th rowspan ="2">Contact Number</th>
			<th colspan="2">Period</th>
			<th rowspan ="2">Status</th>	
		</tr>
		<tr>
			<th>From</th>
			<th>To</th>
		</tr>

		@forelse($disaster_bdrrmc_team_members as $dbtm)
		<tr style="border: 1px solid black;">
			<td>{{$dbtm->last_name}}, {{$dbtm->first_name}} {{$dbtm->middle_name}}</td>
			<td>{{$dbtm->name}}</td>
			<td>{{$dbtm->contact}}</td>
			<td>{{$dbtm->period_from}}</td>
			<td>{{$dbtm->period_to}}</td>
			<td>
				@if($dbtm->deleted_at == null)
					Active
				@else
					Deleted
				@endif
			</td>
		</tr>
		@empty
			<tr><td colspan="7"><p style="text-center">No Available BDRRMC Teams</p></td></tr>
		@endforelse
	</table>
</div>