<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="text/html">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style type="text/css">
    	html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 700;
                font-family: 'Lato';
                color: #000;
            }
	    	
    </style>
</head>
<body>

{!!$bodyMessage!!}<br><br>{{$name}}<br>{{$email}}

</body>
</html>

