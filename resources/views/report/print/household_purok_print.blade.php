<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<div class="content">
	<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px">
		<caption>Household by Purok</caption>
		<tr style="border: 1px solid black;">
			<th>Purok</th>
			<th>House<br>Identification Number</th>
			<th>Name of Household Head</th>
			<th>Total No of<br>Household Member</th>
			
		</tr>
		@forelse($resident_households as $rh)
		<tr style="border: 1px solid black;">
			<td>{{$rh->purok}}</td>
			<td>{{$rh->house_identification_number}}</td>
			<td>
				@foreach($rh->resident_household_members->unique('last_name') as $rhrhm)
                    @if($rhrhm->relation == 'Head')
                        {{$rhrhm->last_name}}, {{$rhrhm->first_name}} {{$rhrhm->middle_name}}<br>
                    @endif
                @endforeach
			</td>
			<td>{{count($rh->resident_household_members)}}</td>
		</tr>
		@empty
		<tr>
			<td colspan="4"><div class="text-center">No available household.</div></td>
		</tr>
		@endforelse
	</table>
</div>