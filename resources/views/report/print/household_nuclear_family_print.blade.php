<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<div class="content">
	<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px">
		<caption>Household by Nuclear Family</caption>
		<tr style="border: 1px solid black;">
			<th>Purok</th>
			<th>House<br>Identification Number</th>
			<th>Name of Household Head</th>
			<th>Children</th>
			<th>PWD</th>
			<th>Solo Parent</th>
			<th>Senior Citizen</th>
			<th>Pregnant</th>
			<th>Total No of<br>Household Member</th>
			
		</tr>
		@forelse($resident_households as $rh)
			@foreach($rh->resident_household_members->where('relation','Head') as $rhrhm_head)
				<tr style="border: 1px solid black;">
					<td>{{$rh->purok}}</td>
					<td>{{$rh->house_identification_number}}</td>
					<td>
                        @if($rhrhm_head->relation == 'Head')
                            {{$rhrhm_head->last_name}}, {{$rhrhm_head->first_name}} {{$rhrhm_head->middle_name}}<br>
                        @endif
					</td>
					<td>
						<?php $age = 0; ?>
						@foreach ($rh->resident_household_members as $rhrhm)
						    @if ($rhrhm->age < 18 && $rhrhm->family_belong == $rhrhm_head->family_belong)
						        <?php $age++ ?>    
						    @endif
						@endforeach
						{{ $age }}
					</td>
					<td>
						<?php $pwd = 0; ?>
						@foreach ($rh->resident_household_members as $rhrhm)
						    @if ($rhrhm->disability == 'Yes' && $rhrhm->family_belong == $rhrhm_head->family_belong)
						        <?php $pwd++ ?>    
						    @endif
						@endforeach
						{{ $pwd }}
					</td>
					<td>
						<?php $solo_parent = 0; ?>
						@foreach ($rh->resident_household_members as $rhrhm)
						    @if ($rhrhm->solo_parent == 'Yes' && $rhrhm->family_belong == $rhrhm_head->family_belong)
						        <?php $solo_parent++ ?>    
						    @endif
						@endforeach
						{{ $solo_parent }}
					</td>
					<td>
						<?php $ageO = 0; ?>
						@foreach ($rh->resident_household_members as $rhrhm)
						    @if ($rhrhm->age > 59 && $rhrhm->family_belong == $rhrhm_head->family_belong)
						        <?php $ageO++ ?>    
						    @endif
						@endforeach
						{{ $ageO }}
					</td>		
					<td>
						<?php $pregnant = 0; ?>
						@foreach ($rh->resident_household_members as $rhrhm)
						    @if ($rhrhm->pregnant == 'Yes' && $rhrhm->family_belong == $rhrhm_head->family_belong)
						        <?php $pregnant++ ?>    
						    @endif
						@endforeach
						{{ $pregnant }}
					</td>
					<td>{{$rh->resident_household_members->where('family_belong',$rhrhm_head->family_belong)->count()}}</td>								
				</tr>
			@endforeach
		@empty
		<tr>
			<td colspan="9"><div class="text-center">No available household.</div></td>
		</tr>
		@endforelse
	</table>
</div>