<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3>SOCIO ECONOMIC PROFILE</h3>
<div class="content">
    <table class="table-striped table">
		<thead>
		   	<tr>
				<th>Male</th>
				<th>Female</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center">{{number_format($household_member->where('gender','Male')->where('employed','Yes')->count())}}</td>
				<td class="text-center">{{number_format($household_member->where('gender','Female')->where('employed','Yes')->count())}}</td>
				<td class="text-center">{{number_format($household_member->where('gender','Male')->where('employed','Yes')->count() + $household_member->where('gender','Female')->where('employed','Yes')->count())}}</td>

			</tr>	
			<tr style="text-align: center; background-color: #4472c4; color: white;" rowspan="2">
				<td colspan="2"> </td>
				<td class="text-center">{{number_format((($household_member->where('gender','Male')->where('employed','Yes')->count() + $household_member->where('gender','Female')->where('employed','Yes')->count()) / $household_member->count())*100,2)}}%</td>
			</tr>												
		</tbody>	
	</table>
	<table class="table-striped table othertable">
		<thead>
		   <tr>
				<th class="text-center">Labor Force</th>
				<th class="text-center">Number</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Worked for Private Household</td>
				<td class="text-center">{{number_format($household_member->where('where_occupation','Worked for Private Household')->count())}}</td>
			</tr>	
			<tr>
				<td>Worked for Private Establishment</td>
				<td class="text-center">{{number_format($household_member->where('where_occupation','Worked for Private Establishment')->count())}}</td>
			</tr>
			<tr>
				<td>Worked for government/non-government copporation</td>
				<td class="text-center">{{number_format($household_member->where('where_occupation','Worked for Government/Non-Government Corporation')->count())}}</td>
			</tr>
			<tr>
				<td>Self-Employed without any paid emloyee/td>
				<td class="text-center">{{number_format($household_member->where('where_occupation','Self-Employed Without Any Paid Employee')->count())}}</td>
			</tr>
			<tr>
				<td>Employer in own family-operated farm or business</td>
				<td class="text-center">{{number_format($household_member->where('where_occupation','Employer in Own Family-operated Farm/Business')->count())}}</td>
			</tr>
			<tr>
				<td>Worked with pay in own family-operated farm or business</td>
				<td class="text-center">{{number_format($household_member->where('where_occupation','Worked with Pay in Own Family-operated Farm/Business')->count())}}</td>
			</tr>
			<tr>
				<td>Worked without pay in own family-operated farm or business</td>
				<td class="text-center">{{number_format($household_member->where('where_occupation','Worked without Pay in Own Family-operated Farm/Business')->count())}}</td>
			</tr>
			<tr style="text-align: center; background-color: #4472c4; color: white;" rowspan="2">
				<td class="text-center">TOTAL</td>
				<td class="text-center">{{number_format($household_member->where('where_occupation','Worked for Private Household')->count() + $household_member->where('where_occupation','Worked for Private Establishment')->count() + $household_member->where('where_occupation','Worked for Government/Non-Government Corporation')->count() + $household_member->where('where_occupation','Self-Employed Without Any Paid Employee')->count() + $household_member->where('where_occupation','Employer in Own Family-operated Farm/Business')->count() + $household_member->where('where_occupation','Worked with Pay in Own Family-operated Farm/Business')->count() + $household_member->where('where_occupation','Worked without Pay in Own Family-operated Farm/Business')->count())}}</td>
			</tr>												
		</tbody>	
	</table>
	<table class="table-striped table othertable">
		<thead>
		   <tr>
				<th class="text-center">Occupation Industry</th>
				<th class="text-center">Number</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Officials of government and special interest organization, corporate executives, managers, managing proprietors and supervisors</td>
				<td class="text-center">{{number_format($household_member->where('occupation','Corporate Executive')->count() + $household_member->where('occupation','Manager')->count() + $household_member->where('occupation','Managing Proprietor')->count() + $household_member->where('occupation','Official of the government and Special Interest Organization')->count() + $household_member->where('occupation','Supervisor')->count())}}</td>
			</tr>	
			<tr>
				<td>Professionals</td>
				<td class="text-center">{{number_format($household_member->where('occupation','Professional')->count())}}</td>
			</tr>
			<tr>
				<td>Technicians and associate professionals</td>
				<td class="text-center">{{number_format($household_member->where('occupation','Associate Professional')->count() + $household_member->where('occupation','Technician')->count())}}</td>
			</tr>
			<tr>
				<td>Clerks</td>
				<td class="text-center">{{number_format($household_member->where('occupation','Clerk')->count())}}</td>
			</tr>
			<tr>
				<td>Service workers and shop and market sales workers</td>
				<td class="text-center">{{number_format($household_member->where('occupation','Service Worker')->count() + $household_member->where('occupation','Shop and Market Sales Worker')->count())}}</td>
			</tr>
			<tr>
				<td>Farmers, forestry workers and fisherfolk</td>
				<td class="text-center">{{number_format($household_member->where('occupation','Farmer')->count() + $household_member->where('occupation','Fisherman')->count() + $household_member->where('occupation','Forestry Worker')->count())}}</td>
			</tr>
			<tr>
				<td>Trades and related workers</td>
				<td class="text-center">{{number_format($household_member->where('occupation','Trades and Related Worker')->count())}}</td>
			</tr>
			<tr>
				<td>Plant and machine operators and assemblers</td>
				<td class="text-center">{{number_format($household_member->where('occupation','Assembler')->count() + $household_member->where('occupation','Plant and Machine Operator')->count())}}</td>
			</tr>
			<tr>
				<td>Laborers and unskilled workers</td>
				<td class="text-center">{{number_format($household_member->where('occupation','Laborer')->count() + $household_member->where('occupation','unskilled Worker')->count())}}</td>
			</tr>
			<tr>
				<td>Special occupations</td>
				<td class="text-center">{{number_format($household_member->where('occupation','Special Occupation')->count())}}</td>
			</tr>
			<tr style="text-align: center; background-color: #4472c4; color: white;" rowspan="2">
				<td class="text-center"> TOTAL </td>
				<td class="text-center">{{number_format($household_member->where('occupation','Corporate Executive')->count() + $household_member->where('occupation','Manager')->count() + $household_member->where('occupation','Managing Proprietor')->count() + $household_member->where('occupation','Official of the government and Special Interest Organization')->count() + $household_member->where('occupation','Supervisor')->count() + $household_member->where('occupation','Professional')->count() + $household_member->where('occupation','Clerk')->count() + $household_member->where('occupation','Service Worker')->count() + $household_member->where('occupation','Shop and Market Sales Worker')->count() + $household_member->where('occupation','Farmer')->count() + $household_member->where('occupation','Fisherman')->count() + $household_member->where('occupation','Forestry Worker')->count() + $household_member->where('occupation','Trades and Related Worker')->count() + $household_member->where('occupation','Assembler')->count() + $household_member->where('occupation','Plant and Machine Operator')->count() + $household_member->where('occupation','Laborer')->count() + $household_member->where('occupation','unskilled Worker')->count() + $household_member->where('occupation','Special Occupation')->count())}}</td>
			</tr>												
		</tbody>	
	</table>
</div>