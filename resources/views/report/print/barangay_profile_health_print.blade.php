<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3>Health</h3>
<div class="content">
	<table class="table-striped table othertable">
		<thead>
		   <tr>
				<th class="text-center">Physical Status</th>
				<th class="text-center">Number</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Total blindness</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Total Blindness'))}}</td>
			</tr>	
			<tr>
				<td>Partial blindness</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Partial Blindness'))}}</td>
			</tr>
			<tr>
				<td>Low vision</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Low Vision'))}}</td>
			</tr>
			<tr>
				<td>Totally deaf</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Totally Deaf'))}}</td>
			</tr>
			<tr>
				<td>Partially deaf</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Partially Deaf'))}}</td>
			</tr>
			<tr>
				<td>Oral defect</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Oral Defect'))}}</td>
			</tr>
			<tr>
				<td>One hand</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'One Hand'))}}</td>
			</tr>
			<tr>
				<td>No hands</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'No Hands'))}}</td>
			</tr>
			<tr>
				<td>One leg</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'One Leg'))}}</td>
			</tr>
			<tr>
				<td>No legs</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'No Legs'))}}</td>
			</tr>
			<tr>
				<td>Mild Cerebral palsy</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Mild Cerebral Palsy'))}}</td>
			</tr>
			<tr>
				<td>Severe Cerebral palsy</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Severe Cerebral Palsy'))}}</td>
			</tr>
			<tr>
				<td>Retarted</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Retarded'))}}</td>
			</tr>
			<tr>
				<td>Mentally ill</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Mentally Ill'))}}</td>
			</tr>
			<tr>
				<td>Mental retardation</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Mental Retardation'))}}</td>
			</tr>
			<tr>
				<td>Multiple impairment</td>
				<td class="text-center">{{number_format(substr_count($household_member->implode('disability_type',','),'Multiple Impairment'))}}</td>
			</tr>
			<tr>
				<td>Others</td>
				<td class="text-center">{{$health_others_count}}</td>
			</tr>											
		</tbody>	
	</table>
</div>