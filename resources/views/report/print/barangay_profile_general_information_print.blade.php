<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<div class="content">
	<div class="row col-xs-12">
		<h4>General Information</h4>
		<div class="col-xs-12">
			Barangay: {{$barangay_about->barangay}}
		</div>
		<div class="col-xs-12" style="margin-top: 10px">
			Municipality: {{$barangay_about->municipality}}
		</div>
		<div class="col-xs-12" style="margin-top: 10px">
			Province: {{$barangay_about->province}}
		</div>
		<div class="col-xs-12" style="margin-top: 10px">
			Region: {{$barangay_about->region}}
		</div>
		<div class="col-xs-12" style="margin-top: 10px">
			Barangay Category: {{$barangay_about->barangay_category}}
		</div>
		<div class="col-xs-12" style="margin-top: 10px">
			Major Economic Source: {{$barangay_about->major_economic_source}}
		</div>
	</div>
	<div class="row col-xs-12" style="margin-top: 20px">
		<h4>Physical Characteristics</h4>
		<div class="col-xs-12">
			Land Classification: {{$barangay_about->land_classification}}
		</div>
		<div class="col-xs-12" style="margin-top: 10px">
			Total Land Area: {{$barangay_about->total_land_area}}
		</div>
		<div class="col-xs-12" style="margin-top: 10px">
			Geographical Location: {{$barangay_about->latitude}} Latitude and {{$barangay_about->longitude}} Longitude
		</div>
	</div>
</div>