<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3> WATER SANITATION</h3>
<div class="content">
	<h4> Household with Access to Improved or Safe Water Supply</h4>
	<table class="table-striped table othertable">
		<thead>
		   	<tr>
				<th class="text-center">Occupation Industry</th>
				<th class="text-center">Number</th>
				<th class="text-center">%</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Own use faucet, community water system</td>
				<td class="text-center">{{number_format($household->where('water_supply','Own use faucet, community water system')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_supply','Own use faucet, community water system')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Shared faucet, community water system</td>
				<td class="text-center">{{number_format($household->where('water_supply','Shared faucet, community water system')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_supply','Shared faucet, community water system')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Own use tubed/piped deep well</td>
				<td class="text-center">{{number_format($household->where('water_supply','Own use tubed/piped deep well')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_supply','Own use tubed/piped deep well')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Shared tubed/piped deep well</td>
				<td class="text-center">{{number_format($household->where('water_supply','Shared tubed/piped deep well')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_supply','Shared tubed/piped deep well')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Tubed/piped shallow well</td>
				<td class="text-center">{{number_format($household->where('water_supply','Tubed/piped shallow well')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_supply','Tubed/piped shallow well')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Dug well</td>
				<td class="text-center">{{number_format($household->where('water_supply','Dug well')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_supply','Dug well')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Protected spring</td>
				<td class="text-center">{{number_format($household->where('water_supply','Protected spring')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_supply','Protected spring')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Peddler</td>
				<td class="text-center">{{number_format($household->where('water_supply','Peddler')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_supply','Peddler')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Bottle water</td>
				<td class="text-center">{{number_format($household->where('water_supply','Bottle water')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_supply','Bottle water')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Lake, River, Rain, and Others</td>
				<td class="text-center">{{number_format($household->where('water_supply','Lake, River, Rain, and Others')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_supply','Lake, River, Rain, and Others')->count() / $household->count())*100,2)}}</td>
			</tr>


			<tr style="text-align: center; background-color: #4472c4; color: white;">
				<td class="text-center"> TOTAL </td>
				<td class="text-center">{{number_format($household->where('water_supply','Own use faucet, community water system')->count() + $household->where('water_supply','Shared faucet, community water system')->count() + $household->where('water_supply','Own use tubed/piped deep well')->count() + $household->where('water_supply','Shared tubed/piped deep well')->count() + $household->where('water_supply','Tubed/piped shallow well')->count() + $household->where('water_supply','Dug well')->count() + $household->where('water_supply','Protected spring')->count() + $household->where('water_supply','Peddler')->count() + $household->where('water_supply','Bottle water')->count() + $household->where('water_supply','Lake, River, Rain, and Others')->count())}}</td>
				<td class="text-center">{{number_format((($household->where('water_supply','Own use faucet, community water system')->count() + $household->where('water_supply','Shared faucet, community water system')->count() + $household->where('water_supply','Own use tubed/piped deep well')->count() + $household->where('water_supply','Shared tubed/piped deep well')->count() + $household->where('water_supply','Tubed/piped shallow well')->count() + $household->where('water_supply','Dug well')->count() + $household->where('water_supply','Protected spring')->count() + $household->where('water_supply','Peddler')->count() + $household->where('water_supply','Bottle water')->count() + $household->where('water_supply','Lake, River, Rain, and Others')->count()) / $household->count())*100,2)}}</td>
			</tr>												
		</tbody>	
	</table>
	<h3>Toilet Facilities</h3>
	<table class="table-striped table othertable">
		<thead>
		   	<tr>
				<th class="text-center">Toilet Facilities</th>
				<th class="text-center">Number</th>
				<th class="text-center">%</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Owned</td>
				<td class="text-center">{{number_format($household->where('toilet','Owned')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('toilet','Owned')->count() / $household->count())*100,2)}}</td>
			</tr>	
			<tr>
				<td>Shared</td>
				<td class="text-center">{{number_format($household->where('toilet','Shared')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('toilet','Shared')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Closed pit</td>
				<td class="text-center">{{number_format($household->where('toilet','Closed Pit')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('toilet','Closed Pit')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Open pit</td>
				<td class="text-center">{{number_format($household->where('toilet','Open Pit')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('toilet','Open Pit')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Others (pail system,and others)</td>
				<td class="text-center">{{number_format($household->where('toilet','Drop/Overhang')->count() + $household->where('toilet','Pail System')->count() + $household->where('toilet','No Toilet/Field/Bush')->count())}}</td>
				<td class="text-center">{{number_format((($household->where('toilet','Drop/Overhang')->count() + $household->where('toilet','Pail System')->count() + $household->where('toilet','No Toilet/Field/Bush')->count()) / $household->count())*100,2)}}</td>
			</tr>
			<tr style="text-align: center; background-color: #4472c4; color: white;">
				<td class="text-center"> TOTAL </td>
				<td class="text-center">{{number_format($household->where('toilet','Owned')->count() + $household->where('toilet','Shared')->count() + $household->where('toilet','Closed Pit')->count() + $household->where('toilet','Open Pit')->count() + $household->where('toilet','Drop/Overhang')->count() + $household->where('toilet','Pail System')->count() + $household->where('toilet','No Toilet/Field/Bush')->count())}}</td>
				<td class="text-center">{{number_format((($household->where('toilet','Owned')->count() + $household->where('toilet','Shared')->count() + $household->where('toilet','Closed Pit')->count() + $household->where('toilet','Open Pit')->count() + $household->where('toilet','Drop/Overhang')->count() + $household->where('toilet','Pail System')->count() + $household->where('toilet','No Toilet/Field/Bush')->count()) / $household->count())*100,2)}}</td>
			</tr>												
		</tbody>	
	</table>
	<table class="table-striped table othertable">
		<thead>
		   	<tr>
				<th class="text-center">Total No. of Households</th>
				<th class="text-center">HH'S with Water Filtration</th>
				<th class="text-center">%</th>
				<th class="text-center">HH'S without Water Filtration</th>
			    <th class="text-center">%</th>
				</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center">{{number_format($household->count())}}</td>
				<td class="text-center">{{number_format($household->where('water_filtration','Yes')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_filtration','Yes')->count() / $household->count()) * 100,2)}}</td>
				<td class="text-center">{{number_format($household->where('water_filtration','No')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('water_filtration','No')->count() / $household->count()) * 100,2)}}</td>
			</tr>													
		</tbody>	
	</table>
</div>