<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3>DEMOGRAPHY</h3>
<div class="content">
	<table class="table-striped table othertable" id="printDemography">
		<thead>
		   <tr>
				<th>Population</th>
				<th>{{number_format($household_member->count())}}</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td  style="text-indent: 6mm;">Male</td>
				<td>{{number_format($household_member->where('gender','Male')->count())}}</td>
			</tr>
			<tr>
				<td  style="text-indent: 6mm;">Female</td>
				<td>{{number_format($household_member->where('gender','Female')->count())}}</td>
			</tr>
			<tr>
				<td>No. of Households</td>
				<td>{{number_format($household->count())}}</td>
			</tr>
			<tr>
				<td>No. of Families</td>
				<td>{{number_format($household->sum('no_family'))}}</td>
			</tr>
			<tr>
				<td>Average Family Size</td>
				<td>{{number_format($household->sum('no_members') / $household->sum('no_family'),2)}}</td>
			</tr>			
		</tbody>	
	</table>

	<table class="table-striped table" style="border: 1px solid black; font-size: 12px" border="1" >
	  	<tr>
	     	<th rowspan="2" class="text-center">AGE GROUP</th>
	     	<th colspan="3" scope="colgroup" class="text-center">Household Population</th>
	     	<th colspan="3" scope="colgroup" class="text-center">Proportion</th>	
	  	</tr>
	  	<tr>
	    	<th scope="col" class="text-center">Male</th>
		    <th scope="col" class="text-center">Female</th>
		    <th scope="col" class="text-center">Both Sexes</th>
		    <th scope="col" class="text-center">Male</th>
		    <th scope="col" class="text-center">Female</th>
		    <th scope="col" class="text-center">Both Sexes</th>
	  	</tr>
	  	<tr style="border: 1px solid black;">
			<td>Below 1</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age < 1;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age < 1;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age < 1;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age < 1;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age < 1;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age < 1;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>1-4</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>5-9</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>10-14</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>15-19</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>20-24</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>25-29</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>30-34</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>35-39</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>40-44</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>45-49</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>50-54</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>55-59</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>60-64</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>65-69</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>70-74</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>75-79</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>80-84</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td>85 and above</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age > 84;})->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age > 84;})->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->filter(function ($household_member) {return $household_member->age > 84;})->count())}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age > 84;})->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age > 84;})->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->filter(function ($household_member) {return $household_member->age > 84;})->count() / $household_member->count())*100,2)}}</td>
		</tr>
		<tr>
			<th>Total</th>
			<td class="text-center">{{number_format($household_member->where('gender','Male')->count())}}</td>
			<td class="text-center">{{number_format($household_member->where('gender','Female')->count())}}</td>
			<td class="text-center">{{number_format($household_member->count())}}</td>
			<td class="text-center">{{number_format(($household_member->where('gender','Male')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->where('gender','Female')->count() / $household_member->count())*100,2)}}</td>
			<td class="text-center">{{number_format(($household_member->count() / $household_member->count())*100,2)}}</td>
		</tr>
	</table>

    <table class="table-striped table">
		<thead>
		   	<tr>
				<th>Marital Status</th>
				<th>Number</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Single</td>
				<td class="text-center">{{number_format($household_member->where('civil_status','Single')->count())}}</td>
			</tr>
			<tr>
				<td>Married</td>
				<td class="text-center">{{number_format($household_member->where('civil_status','Married')->count())}}</td>
			</tr>
			<tr>
				<td>Widowed</td>
				<td class="text-center">{{number_format($household_member->where('civil_status','Widowed')->count())}}</td>
			</tr>
			<tr>
				<td>Divorced/Separated</td>
				<td class="text-center">{{number_format($household_member->where('civil_status','Divorce/Seperated')->count())}}</td>
			</tr>
			<tr>
				<td>Anulled</td>
				<td class="text-center">{{number_format($household_member->where('civil_status','Annulled')->count())}}</td>
			</tr>
			<tr>
				<td>Common-Law/Live-in</td>
				<td class="text-center">{{number_format($household_member->where('civil_status','Common-law/Live-in')->count())}}</td>
			</tr>
			<tr>
				<td>Unknown</td>
				<td class="text-center">{{number_format($household_member->where('civil_status','Unknown')->count())}}</td>
			</tr>		
			<tr style="text-align: center; background-color: #4472c4; color: white;">
				<td>Total</td>
				<td class="text-center">{{number_format($household_member->where('civil_status','Single')->count() + $household_member->where('civil_status','Married')->count() + $household_member->where('civil_status','Widowed')->count() + $household_member->where('civil_status','Divorce/Seperated')->count() + $household_member->where('civil_status','Annulled')->count() + $household_member->where('civil_status','Common-law/Live-in')->count() + $household_member->where('civil_status','Unknown')->count())}}</td>
			</tr>												
		</tbody>	
	</table>
</div>