<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3>SHELTER</h3> 
<div class="content">
	<table class="table-striped table othertable">
		<thead>
		   	<tr>
				<th class="text-center">Type of Building/Housing Unit </th>
				<th class="text-center">Number</th>
				<th class="text-center">%</th>				
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Concrete</td>
				<td class="text-center">{{number_format($household->where('building_type','Concrete')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('building_type','Concrete')->count() / $household->count())*100,2)}}</td>
			</tr>	
			<tr>
				<td>Semi-Concrete</td>
				<td class="text-center">{{number_format($household->where('building_type','Semi-Concrete')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('building_type','Semi-Concrete')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr>
				<td>Light Materials</td>
				<td class="text-center">{{number_format($household->where('building_type','Light Materials')->count())}}</td>
				<td class="text-center">{{number_format(($household->where('building_type','Light Materials')->count() / $household->count())*100,2)}}</td>
			</tr>
			<tr style="text-align: center; background-color: #4472c4; color: white;">
				<td class="text-center"> TOTAL </td>
				<td class="text-center">{{number_format($household->where('building_type','Concrete')->count() + $household->where('building_type','Semi-Concrete')->count() + $household->where('building_type','Light Materials')->count())}}</td>
				<td class="text-center">{{number_format((($household->where('building_type','Concrete')->count() + $household->where('building_type','Semi-Concrete')->count() + $household->where('building_type','Light Materials')->count()) / $household->count())*100,2)}}</td>
			</tr>		
		</tbody>	
	</table>
	<div class="col-sm-6">
		<table class="table-striped table othertable">
			<thead>
			   	<tr>
					<th class="text-center">Power Generator </th>
					<th class="text-center">Number</th>
					<th>%</th>				
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>With Backup</td>
					<td class="text-center">{{number_format($household->where('power_generator','Yes')->count())}}</td>
					<td class="text-center">{{number_format(($household->where('power_generator','Yes')->count() / $household->count())*100,2)}}</td>
				</tr>	
				<tr>
					<td>Without Backup</td>
					<td class="text-center">{{number_format($household->where('power_generator','No')->count())}}</td>
					<td class="text-center">{{number_format(($household->where('power_generator','No')->count() / $household->count())*100,2)}}</td>
				</tr>
				<tr style="text-align: center; background-color: #4472c4; color: white;">
					<td class="text-center"> TOTAL </td>
					<td class="text-center">{{number_format($household->where('power_generator','Yes')->count() + $household->where('power_generator','No')->count())}}</td>
					<td class="text-center">{{number_format((($household->where('power_generator','Yes')->count() + $household->where('power_generator','No')->count()) / $household->count())*100,2)}}</td>
				</tr>		
			</tbody>	
		</table>
 	</div>
 	<div class="col-sm-6">
		<table class="table-striped table othertable">
			<thead>
			   	<tr>
					<th class="text-center">Electricity </th>
					<th class="text-center">Number</th>
					<th class="text-center">%</th>				
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Electric Company</td>
					<td class="text-center">{{number_format($household->where('electricity','Electric Company')->count())}}</td>
					<td class="text-center">{{number_format(($household->where('electricity','Electric Company')->count() / $household->count())*100,2)}}</td>
				</tr>	
				<tr>
					<td>Generator</td>
					<td class="text-center">{{number_format($household->where('electricity','Generator')->count())}}</td>
					<td class="text-center">{{number_format(($household->where('electricity','Generator')->count() / $household->count())*100,2)}}</td>
				</tr>
				<tr>
					<td>Solar</td>
					<td class="text-center">{{number_format($household->where('electricity','Solar')->count())}}</td>
					<td class="text-center">{{number_format(($household->where('electricity','Solar')->count() / $household->count())*100,2)}}</td>
				</tr>
				<tr>
					<td>Battery</td>
					<td class="text-center">{{number_format($household->where('electricity','Battery')->count())}}</td>
					<td class="text-center">{{number_format(($household->where('electricity','Battery')->count() / $household->count())*100,2)}}</td>
				</tr>
				<tr>
					<td>Others</td>
					<td class="text-center">{{number_format($household->where('electricity','Others')->count())}}</td>
					<td class="text-center">{{number_format(($household->where('electricity','Others')->count() / $household->count())*100,2)}}</td>
				</tr>
				<tr>
					<td>None</td>
					<td class="text-center">{{number_format($household->where('electricity','None')->count())}}</td>
					<td class="text-center">{{number_format(($household->where('electricity','None')->count() / $household->count())*100,2)}}</td>
				</tr>
				<tr style="text-align: center; background-color: #4472c4; color: white;">
					<td class="text-center"> TOTAL </td>
					<td class="text-center">{{number_format($household->where('electricity','Electric Company')->count() + $household->where('electricity','Generator')->count() + $household->where('electricity','Solar')->count() + $household->where('electricity','Battery')->count() + $household->where('electricity','Others')->count() + $household->where('electricity','None')->count())}}</td>
					<td class="text-center">{{number_format((($household->where('electricity','Electric Company')->count() + $household->where('electricity','Generator')->count() + $household->where('electricity','Solar')->count() + $household->where('electricity','Battery')->count() + $household->where('electricity','Others')->count() + $household->where('electricity','None')->count()) / $household->count())*100,2)}}</td>
				</tr>		
			</tbody>	
		</table>
 	</div>
</div>