<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<div class="content">
	<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px">
		<caption>List of Resident by Purok</caption>
		<tr style="border: 1px solid black;">
			<th>Purok</th>
			<th>House<br>Identification Number</th>
			<th>Name of Resident</th>
			<th>Age</th>
			<th>Gender</th>
		</tr>
		@forelse($resident_households as $rh)
			@foreach($rh->resident_household_members as $rhrhm)
				<tr style="border: 1px solid black;">
					<td>{{$rh->purok}}</td>
					<td>{{$rh->house_identification_number}}</td>
					<td class="text-left">{{$rhrhm->last_name}}, {{$rhrhm->first_name}} {{$rhrhm->middle_name}}</td>
					<td>{{$rhrhm->age}}</td>
					<td>{{$rhrhm->gender}}</td>						
				</tr>
			@endforeach
		@empty
		<tr>
			<td colspan="9"><div class="text-center">No available household.</div></td>
		</tr>
		@endforelse
	</table>
</div>