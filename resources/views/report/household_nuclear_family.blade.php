@extends('layouts.app')

@section('title')
	Report Household Nuclear Family
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div class="top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #1b3c5b; color: white;  ">
            <h3 class="panel-title" >HOUSEHOLD BY NUCLEAR FAMILIY</h3>     
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
				<div class="col-sm-3">
				{{ Form::open(['route'=>'reports.household_nuclear_family.print','method'=>'get', 'class'=>'navbar-form','target'=>'_blank']) }}  
			        <input type="hidden" name="print" value="{{(isset($_GET["search"]) ? $_GET["search"]:"" )}}">
			        <button type="submit" class="btn btn-primary">Print Household by Nuclear Family</button>
			    {{ Form::close() }}
				</div>
				{{ Form::open(['route'=>'reports.household_nuclear_family','method'=>'get', 'class'=>'navbar-form']) }}  
		        <div class="col-sm-9 text-right" style="margin-top: 10px">
		            <input type="text" class="form-control" placeholder="Type the purok" name="search">
		            <span class="form-group-btn">
		            	<button type="submit" class="btn btn-default">Search</button>
		            </span>
		             <button type="submit" class="btn btn-primary">View All</button>
		        </div>
			    {{ Form::close() }}
				<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px">
					<caption>Household by Nuclear Family</caption>
					<tr style="border: 1px solid black;">
						<th>Purok</th>
						<th>House<br>Identification Number</th>
						<th>Name of Household Head</th>
						<th>Children</th>
						<th>PWD</th>
						<th>Solo Parent</th>
						<th>Senior Citizen</th>
						<th>Pregnant</th>
						<th>Total No of<br>Household Member</th>
						
					</tr>
					@forelse($resident_households as $rh)
						@foreach($rh->resident_household_members->where('relation','Head') as $rhrhm_head)
							<tr style="border: 1px solid black;">
								<td>{{$rh->purok}}</td>
								<td>{{$rh->house_identification_number}}</td>
								<td class="text-left">
		                            @if($rhrhm_head->relation == 'Head')
		                                {{$rhrhm_head->last_name}}, {{$rhrhm_head->first_name}} {{$rhrhm_head->middle_name}}<br>
		                            @endif
								</td>
								<td>
									<?php $age = 0; ?>
									@foreach ($rh->resident_household_members as $rhrhm)
									    @if ($rhrhm->age < 18 && $rhrhm->family_belong == $rhrhm_head->family_belong)
									        <?php $age++ ?>    
									    @endif
									@endforeach
									{{ $age }}
								</td>
								<td>
									<?php $pwd = 0; ?>
									@foreach ($rh->resident_household_members as $rhrhm)
									    @if ($rhrhm->disability == 'Yes' && $rhrhm->family_belong == $rhrhm_head->family_belong)
									        <?php $pwd++ ?>    
									    @endif
									@endforeach
									{{ $pwd }}
								</td>
								<td>
									<?php $solo_parent = 0; ?>
									@foreach ($rh->resident_household_members as $rhrhm)
									    @if ($rhrhm->solo_parent == 'Yes' && $rhrhm->family_belong == $rhrhm_head->family_belong)
									        <?php $solo_parent++ ?>    
									    @endif
									@endforeach
									{{ $solo_parent }}
								</td>
								<td>
									<?php $ageO = 0; ?>
									@foreach ($rh->resident_household_members as $rhrhm)
									    @if ($rhrhm->age > 59 && $rhrhm->family_belong == $rhrhm_head->family_belong)
									        <?php $ageO++ ?>    
									    @endif
									@endforeach
									{{ $ageO }}
								</td>		
								<td>
									<?php $pregnant = 0; ?>
									@foreach ($rh->resident_household_members as $rhrhm)
									    @if ($rhrhm->pregnant == 'Yes' && $rhrhm->family_belong == $rhrhm_head->family_belong)
									        <?php $pregnant++ ?>    
									    @endif
									@endforeach
									{{ $pregnant }}
								</td>
								<td>{{$rh->resident_household_members->where('family_belong',$rhrhm_head->family_belong)->count()}}</td>								
							</tr>
						@endforeach
					@empty
					<tr>
						<td colspan="9"><div class="text-center">No available household.</div></td>
					</tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="text-right">{{$resident_households->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
@endsection