@extends('layouts.app')

@section('title')
	Report Financial Reports
@endsection

@section('css')
<!-- internal styles -->
{{Html::style('amcharts/plugins/export/export.css')}}
	<style>
		.panel-heading{
			background-color: #1b3c5b!important;
			color: white !important;
		}
	</style>
@endsection

@section('body')
<div class="row">	
	<div class="col-md-4">	
		<div class="panel panel-default">
			<div class="panel-heading" style="color: white">
				<h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i>Calamity Fund - Budget and Expenses</h3>
			</div>
			<div class="panel-body">	
				<table class="table-striped table" style="border: 1px solid black; font-size: 12px">
	                <tr style="border: 1px solid black;">
						<th>Account Name</th>
						<th>Budget</th>
						<th>Expenses</th>
						<th>Balance</th>
					</tr>

					@forelse($budgets as $budget)
					<tr style="border: 1px solid black;">
						<td class="text-left">{{$budget->account_name}}</td>
						<td class="text-right">{{number_format($budget->funding_amount)}}</td>
						<td class="text-right">{{number_format($budget->resources_expenses()->sum('amount'))}}</td>
						<td class="text-right">{{number_format($budget->funding_amount - $budget->resources_expenses()->sum('amount'))}}</td>
					</tr>
					@empty
						<tr><td colspan="8"><p style="text-center">No Available Budgets</p></td></tr>
					@endforelse
	            </table>
			</div>
		</div>	
	</div>	
	<div class="col-md-8">	
		<div class="panel panel-default">
			<div class="panel-heading" style="color: black">
				<h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i><b>Calamity Fund</b></h3>
			</div>
			<div class="panel-body">	
				 <div id="calamity_fund" style="width: 100%; height: 400px;"></div>	
			</div>
		</div>	
	</div>	
</div>		
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('amcharts/amcharts.js')}}
{{Html::script('amcharts/serial.js')}}
{{Html::script('amcharts/pie.js')}}
{{Html::script('amcharts/themes/light.js')}}
{{Html::style('amcharts/plugins/export/export.css')}}
{{Html::script('amcharts/plugins/export/export.min.js')}}

<script>
var chart = AmCharts.makeChart("calamity_fund", {
	"type": "serial",
     "theme": "light",
	"categoryField": "name",
	"rotate": false,
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start",
		"position": "left"
	},
	"trendLines": [],
	"graphs": [
		{
			"balloonText": "Budget:[[value]]",
			"fillColors": "#428bca",
			"fillAlphas": 0.8,
			"id": "AmGraph-1",
			"lineAlpha": 0.2,
			"title": "Budget",
			"type": "column",
			"valueField": "budget"
		},
		{
			"balloonText": "Expense:[[value]]",
			"fillColors": "#d9534f",
			"fillAlphas": 0.8,
			"id": "AmGraph-2",
			"lineAlpha": 0.2,
			"title": "Expense",
			"type": "column",
			"valueField": "expense"
		},
		{
			"balloonText": "Balance:[[value]]",
			"fillColors": "#5cb85c",
			"fillAlphas": 0.8,
			"id": "AmGraph-3",
			"lineAlpha": 0.2,
			"title": "Balance",
			"type": "column",
			"valueField": "balance"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"position": "left",
			"axisAlpha": 0
		}
	],
	"allLabels": [],
	"balloon": {},
	"titles": [],
	"dataProvider": [
		@foreach($budgets as $budget)
		{
			"name": '{{$budget->account_name}}',
			"budget": {{$budget->funding_amount}},
			"expense": {{($budget->resources_expenses()->sum('amount') == null) ? 0 : $budget->resources_expenses()->sum('amount')}},
			"balance": {{$budget->funding_amount - $budget->resources_expenses()->sum('amount')}}
		},
		@endforeach
	],
	"valueScrollbar":{
      "oppositeAxis":true,
      "offset":20,
      "scrollbarHeight":10
    },
    "chartScrollbar": {
    	"oppositeAxis":false,
        "offset":40,
        "scrollbarHeight": 10
    },
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 3,
        "position": "bottom",
        "useGraphSettings": true,
        "markerSize": 10
    },
    "export": {
    	"enabled": true
    }

});
</script>
@endsection