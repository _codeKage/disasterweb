@extends('layouts.app')

@section('title')
	Report Other Reports
@endsection

@section('css')
<!-- internal styles -->
{{Html::style('amcharts/plugins/export/export.css')}}
@endsection

@section('body')
	<div class="row">	
        <div class="col-md-12">  
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #1b3c5b; color: white">
                    <h3 class="panel-title text-center">Households at risk</h3>
                </div>
                <div class="panel-body">
                    <div class="row"> 
                        <div class="col-md-12">
                            <table class="table-striped table" style="border: 1px solid black; font-size: 12px">
                                <tr>
                                    <th>Purok</th>
                                    <th>Households at risks</th>
                                    <th>Household Members at risks</th>
                                </tr>
                                @foreach($puroks as $purok)
                                <tr>
                                    <td>{{$purok->purok}}</td>
                                    <td>{{$household_at_risk->where('purok',$purok->purok)->count()}}</td>
                                    <td>{{$household_members_at_risk->where('resident_households.purok',$purok->purok)->count()}}</td>
                                </tr>
                                @endforeach
                            </table>  
                        </div>
                        <div class="col-md-12">
                            <div id="at_risk" style="width: 100%; height: 400px;"></div> 
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <div class="col-md-12"> 
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #1b3c5b; color: white">
                    <h3 class="panel-title text-center">Households with Emergency Kit</h3>
                </div>
                <div class="panel-body">
                    <div class="row">   
                        <div class="col-md-12">
                            <table class="table-striped table" style="border: 1px solid black; font-size: 12px">
                                <tr>
                                    <th>Households</th>
                                    <th>With Emergency Kit</th>
                                    <th>Without Emergency Kit</th>
                                </tr>
                                <tr>
                                    <td>{{$households->count()}}</td>
                                    <td>{{$checkKit->where('d9','Yes')->count()}}</td>
                                    <td>{{$checkKit->where('d9','No')->count()}}</td>
                                </tr>
                            </table>    
                        </div>
                        <div class="col-md-12">
                             <div id="householdKit" style="width: 100%; height: 350px;"></div>      
                        </div>
                    </div>
                </div>
            </div>  
        </div>    
	</div>	
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('amcharts/amcharts.js')}}
{{Html::script('amcharts/serial.js')}}
{{Html::script('amcharts/pie.js')}}
{{Html::script('amcharts/themes/light.js')}}
{{Html::style('amcharts/plugins/export/export.css')}}
{{Html::script('amcharts/plugins/export/export.min.js')}}

<script>
var chart = AmCharts.makeChart("householdKit", {
  "type": "pie",
  "startDuration": 0,
   "theme": "light",
  "addClassNames": true,
  "legend": {
        "horizontalGap": 10,
        "maxColumns": 3,
        "position": "bottom",
        "markerSize": 10
    },
  "innerRadius": "0%",
  "defs": {
    "filter": [{
      "id": "shadow",
      "width": "200%",
      "height": "200%",
      "feOffset": {
        "result": "offOut",
        "in": "SourceAlpha",
        "dx": 0,
        "dy": 0
      },
      "feGaussianBlur": {
        "result": "blurOut",
        "in": "offOut",
        "stdDeviation": 5
      },
      "feBlend": {
        "in": "SourceGraphic",
        "in2": "blurOut",
        "mode": "normal"
      }
    }]
  },
  "dataProvider": [{
    "households": "With Kit",
    "color": "#5cb85c",
    "kits": {{$checkKit->where('d9','Yes')->count()}}
  }, {
    "households": "Without Kit",
    "color": "#d9534f",
    "kits": {{$checkKit->where('d9','No')->count()}}
  }],
  "valueField": "kits",
  "titleField": "households",
  "colorField": "color",
  "export": {
    "enabled": true
  }
});

var chart = AmCharts.makeChart("at_risk", {
    "type": "serial",
     "theme": "light",
    "categoryField": "name",
    "rotate": false,
    "startDuration": 1,
    "categoryAxis": {
        "gridPosition": "start",
        "position": "left"
    },
    "trendLines": [],
    "graphs": [
        @foreach($puroks as $purok)
        {
            "balloonText": "Purok {{$purok->purok}}:[[value]]",
            "fillAlphas": 0.8,
            "id": "AmGraph-{{$purok->purok}}",
            "lineAlpha": 0.2,
            "title": "Purok {{$purok->purok}}",
            "type": "column",
            "valueField": "Purok {{$purok->purok}}"
        },
        @endforeach
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "position": "left",
            "axisAlpha": 0
        }
    ],
    "allLabels": [],
    "balloon": {},
    "titles": [],
    "dataProvider": [
        {
            "name": 'households at risk',
            @foreach($puroks as $purok)
            "Purok {{$purok->purok}}": {{$household_at_risk->where('purok',$purok->purok)->count()}},
            @endforeach
        },
        {
            "name": 'household members at risk',
            @foreach($puroks as $purok)
            "Purok {{$purok->purok}}": {{$household_members_at_risk->where('resident_households.purok',$purok->purok)->count()}},
            @endforeach
        },
    ],
    "valueScrollbar":{
      "oppositeAxis":true,
      "offset":20,
      "scrollbarHeight":10
    },
    "chartScrollbar": {
        "oppositeAxis":false,
        "offset":40,
        "scrollbarHeight": 10
    },
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 3,
        "position": "bottom",
        "useGraphSettings": true,
        "markerSize": 10
    },
    "export": {
        "enabled": true
    }

});
</script>
@endsection