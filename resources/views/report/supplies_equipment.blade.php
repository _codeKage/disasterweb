@extends('layouts.app')

@section('title')
	Report Barangay Profile
@endsection

@section('css')
{{Html::style('amcharts/plugins/export/export.css')}}
@endsection

@section('body')
	<ul class="nav nav-tabs">
	  <li role="presentation" class="active"><a href="#Supplies" aria-controls="Supplies" role="tab" data-toggle="tab" style="color:black">Supplies</a></li>
	  <li role="presentation"><a href="#Equipment" aria-controls="Equipment" role="tab" data-toggle="tab" style="color:black">Equipment</a></li>
	</ul>

    <div class="tab-content" style="margin: 10px">
      	<div role="tabpanel" class="tab-pane active" id="Supplies">
			<h3>DISASTER SUPPLIES</h3>
		    <div class="col-sm-9" style="margin-top: 10px">
		        <div id="supplies_chart" style="width: 100%; height: 400px;"></div>
		    </div>
		    <div class="col-sm-3" style="margin-top: 10px">
		    	<table class="table-striped table" style="border: 1px solid black; font-size: 12px">
		          	<caption style="font-size:16px">Most Distributed Supplies</caption>
		          	@forelse($most_distributed_supplies as $mds)
		            <tr>
		              	<td>{{$mds->name}}</td>
		            </tr> 
		            @empty
		              	<div class="text-center">No Available Supplies.</div>
		            @endforelse     
		        </table>
		    </div>
		</div>

        <div role="tabpanel" class="tab-pane" id="Equipment">
			<h3>DISASTER EQUIPMENTS</h3>
		    <div class="col-sm-9">
		        <div id="equipment_chart" style="width: 100%; height: 400px;"></div>
		    </div>
		    <div class="col-sm-3">
		    	<div class="col-sm-12" style="margin-top: 10px">
		    		<table class="table-striped table" style="border: 1px solid black; font-size: 12px">
			          	<caption style="font-size:16px">Equipments with the most Good Condition</caption>
			          	@forelse($most_good_equipments as $mge)
			            <tr>
			              	<td>{{$mge->name}}</td>
			            </tr> 
			            @empty
			              	<div class="text-center">No Available Equipments.</div>
			            @endforelse     
			        </table>
		    	</div>
		    	<div class="col-sm-12" style="margin-top: 10px">
		    		<table class="table-striped table" style="border: 1px solid black; font-size: 12px">
			          	<caption style="font-size:16px">Equipments with the most Bad Condition</caption>
			          	@forelse($most_bad_equipments as $mbe)
			            <tr>
			              	<td>{{$mbe->name}}</td>
			            </tr> 
			            @empty
			              	<div class="text-center">No Available Equipments.</div>
			            @endforelse     
			        </table>
		    	</div>
		    </div>
        </div>
    </div>
@endsection

@section('js')
{{Html::script('amcharts/amcharts.js')}}
{{Html::script('amcharts/serial.js')}}
{{Html::script('amcharts/pie.js')}}
{{Html::script('amcharts/themes/light.js')}}
{{Html::style('amcharts/plugins/export/export.css')}}
{{Html::script('amcharts/plugins/export/export.min.js')}}

<script>
	var chart = AmCharts.makeChart("supplies_chart", {
	"type": "serial",
    "theme": "light",
	"categoryField": "supply_name",
	"rotate": true,
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start",
		"position": "left"
	},
	"trendLines": [],
	"graphs": [
		{
			"balloonText": "Total Added:[[value]]",
			"fillColors": "#5cb85c",
			"fillAlphas": 0.8,
			"id": "AmGraph-1",
			"lineAlpha": 0.2,
			"title": "Total Added",
			"type": "column",
			"valueField": "total_added"
		},
		{
			"balloonText": "Total Distributed:[[value]]",
			"fillColors": "#d9534f",
			"fillAlphas": 0.8,
			"id": "AmGraph-2",
			"lineAlpha": 0.2,
			"title": "Total Distributed",
			"type": "column",
			"valueField": "total_distributed"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"position": "top",
			"axisAlpha": 0
		}
	],
	"valueScrollbar":{
      "oppositeAxis":true,
      "offset":20,
      "scrollbarHeight":10
    },
    "chartScrollbar": {
    	"oppositeAxis":false,
        "offset":20,
        "scrollbarHeight": 10
    },
	"allLabels": [],
	"balloon": {},
	"titles": [],
	"dataProvider": [
		@foreach($supplies as $supply)
		{
			"supply_name": '{{$supply->name}}',
			"total_added": {{$supply->total_added}},
			"total_distributed": {{$supply->total_distributed}}
		},
		@endforeach
	],
	"legend": {
        "horizontalGap": 10,
        "maxColumns": 3,
        "position": "bottom",
        "useGraphSettings": true,
        "markerSize": 10
    },
    "export": {
    	"enabled": true
    }
});

var chart = AmCharts.makeChart("equipment_chart", {
    "type": "serial",
	"theme": "light",
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 3,
        "position": "bottom",
		"useGraphSettings": true,
		"markerSize": 10
    },
    "dataProvider": [
    @foreach($equipments as $equipment)
    {
        "name": '{{$equipment->name}}',
        "good_condition": {{$equipment->good_condition}},
        "bad_condition": {{$equipment->bad_condition}},
    },
    @endforeach
    ],
    "valueAxes": [{
        "stackType": "regular",
        "axisAlpha": 0.3,
        "gridAlpha": 0
    }],
    "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillColors": "#5cb85c",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "good condition",
        "type": "column",
		"color": "#000000",
        "valueField": "good_condition"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillColors": "#d9534f",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Bad Condition",
        "type": "column",
		"color": "#000000",
        "valueField": "bad_condition"
    }],
    "categoryField": "name",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
    },
    "valueScrollbar":{
      "oppositeAxis":false,
      "offset":20,
      "scrollbarHeight":10
    },
    "chartScrollbar": {
    	"oppositeAxis":false,
        "offset":40,
        "scrollbarHeight": 10
    },
    "export": {
    	"enabled": true
     }

});
</script>
@endsection