@extends('layouts.app')

@section('title')
	Report Household By Purok
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div class="top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #1b3c5b; color: white;  ">
            <h3 class="panel-title">HOUSEHOLD BY PUROK</h3>     
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
			   	<div class="col-sm-3">
				{{ Form::open(['route'=>'reports.household_purok.print','method'=>'get', 'class'=>'navbar-form','target'=>'_blank']) }}  
			        <input type="hidden" name="print" value="{{(isset($_GET["search"]) ? $_GET["search"]:"" )}}">
			        <button type="submit" class="btn btn-primary">Print Household By Purok</button>
			    {{ Form::close() }}
				</div>
				{{ Form::open(['route'=>'reports.household_purok','method'=>'get', 'class'=>'navbar-form']) }}  
		        <div class="col-sm-9 text-right" style="margin-top: 10px">
		            <input type="text" class="form-control" placeholder="Type the purok" name="search">
		            <span class="form-group-btn">
		            	<button type="submit" class="btn btn-default">Search</button>
		            </span>
		             <button type="submit" class="btn btn-primary">View All</button>
		        </div>
			    {{ Form::close() }}
				<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px">
					<caption class="panel-titlel"> Household by Purok</caption>
					<tr style="border: 1px solid black;">
						<th>Purok</th>
						<th>House<br>Identification Number</th>
						<th>Name of Household Head</th>
						<th>Total No of<br>Household Member</th>
						
					</tr>
					@forelse($resident_households as $rh)
					<tr style="border: 1px solid black;">
						<td>{{$rh->purok}}</td>
						<td>{{$rh->house_identification_number}}</td>
						<td class="text-left">
							@foreach($rh->resident_household_members->unique('last_name') as $rhrhm)
	                            @if($rhrhm->relation == 'Head')
	                                {{$rhrhm->last_name}}, {{$rhrhm->first_name}} {{$rhrhm->middle_name}}<br>
	                            @endif
	                        @endforeach
						</td>
						<td>{{count($rh->resident_household_members)}}</td>
					</tr>
					@empty
					<tr>
						<td colspan="4"><div class="text-center">No available household.</div></td>
					</tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="text-right">{{$resident_households->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
@endsection