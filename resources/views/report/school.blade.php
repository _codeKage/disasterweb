@extends('layouts.app')

@section('title')
	School Availability
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
@if(count($availablePerGrade) > 0 )
<div class="row">
	<div class="col-md-12">
		<h3>Slots Per Grade for year{{$schoolYear}}</h3>
	</div>
	<div class="col-md-12">
		<table class="table">
			@foreach($availablePerGrade as $key=>$grade)
				<tr>
					<td colspan="4">{{$key}}</td>
				</tr>
				<tr>
					<td>Purok</td>
					<td>School Name</td>
                    <td>Maximum Capacity</td>
					<td>Available Slot</td>
				</tr>
				@foreach($grade as $key=>$slots)
			    <tr>
					<td>
						{{$slots->purok}}
					</td>
					<td>
						{{$slots->school_name}}
					</td>
                    <td>
						{{$slots->max_capacity}}
					</td>
					<td>
						{{$slots->available_slot}}
					</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="4" style="border-right: solid 1px #FFF; border-left: solid 1px #FFF;">&nbsp;</td>
				</tr>
			@endforeach

		</table>
	</div>
</div>

@else
<h3>Data Unavailable. </h3>
<h5>Please add data in education</h5>
@endif

@endsection
