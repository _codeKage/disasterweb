@extends('layouts.app')

@section('title')
	Report Forecast
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')

@if(count($yearlyPreparednessKit) > 0)
									
<div class="row">
	<div class="col-md-12">
		<h3>Relief Goods for year {{$yearlyPreparednessKit[0]->year }}</h3>
	</div>
	<div class="col-md-12">
		<div id="bar-example"></div>
	</div>
	
</div>
@else
<h3>Data Unavailable. </h3>
<h5>Please add data in household survey</h5>
@endif


@endsection

@section('js')
<!-- internal scripts -->
<script>
@if(count($yearlyPreparednessKit) > 0)
Morris.Donut({
  element: 'bar-example',
  colors: [
	  '#ff1234', 
	  '#ffc107', 
	  '#120b37', 
	  '#fd5202', 
	  '#212121', 
	  '#2891fc', 
	  '#ff0000', 
	  '#00b2ac',
	  '#fc8800',
	  '#e5cc16'],
  data: [
  	
  		{ 
			label: 'water', 
			value: {{ $yearlyPreparednessKit[0]->water}}  
		},
  		{
			label: 'food', 
			value: {{ $yearlyPreparednessKit[0]->food }}  
		},
  		{
			label: 'match', 
			value: {{ $yearlyPreparednessKit[0]->matches }}  
		},
  		{ 
			label: 'flashlight', 
			value: {{ $yearlyPreparednessKit[0]->flashlight }}  
		},
  		{ 
			label: 'radio', 
			value: {{ $yearlyPreparednessKit[0]->radio }}  
		},
  		{ 
			label: 'candle', 
			value: {{ $yearlyPreparednessKit[0]->candle }}  
		},
  		{ 
			label: 'whistle', 
			value: {{ $yearlyPreparednessKit[0]->whistle }}  
		},
  		{ 
			label: 'medicine', 
			value: {{ $yearlyPreparednessKit[0]->medicine }}  
		},
  		{ 
			label: 'clothes', 
			value: {{ $yearlyPreparednessKit[0]->clothes }}  
		},
  		{
			label: 'blanket', 
			value: {{ $yearlyPreparednessKit[0]->blanket }}  
		}
  ]
});
@endif
</script>
@endsection