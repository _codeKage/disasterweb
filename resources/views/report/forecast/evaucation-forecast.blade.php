@extends('layouts.app')

@section('title')
    Elementary Forecast
@endsection

@section('css')
    <!-- internal styles -->
@endsection

@section('body')
    @if($forecastYear !== 2018)
        <div class="row">
            <div class="col-md-12">
                <h3>Forcasted for Evacuation Centers S.Y.  {{$forecastYear }}</h3>
            </div>
            <div class="col-md-12">
                <div id="bar-example"></div>
            </div>
        </div>
        </div>
    @else
        <div>
            No Data
        </div>
    @endif

@endsection

@section('js')
    <!-- internal scripts -->
    <script>
        let content = [
            {label: 'Total Population',  value: '{{ $numPeople }}' },
            {label: 'Capacity of Evacuation',  value: '{{$totalCapacity}}' },
            {label: 'Available Slots',  value: '{{$availableSlots}}' },
        ];
        Morris.Donut({
            element: 'bar-example',
            colors: ['#ff1234', '#120b37', '#ffc107'],
            data: content
    	});
    </script>
@endsection
	