@extends('layouts.app')

@section('title')
	Budget Forecast
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')

@if(count($yearlyBudgetExpense) > 0)
<div class="row">
	<div class="col-md-8">
		<h3>Disaster Fund</h3>
	</div>
	<div class="col-md-12" style="margin-top:5px">
		<table class="table">	
			<tr>
				<th>Year</th>			
				<th>Actual</th>
				<th>Forecast</th>
			</tr>
			@foreach($yearlyBudgetExpense as $budgetExpense)
			<tr>
				<td> 
					{{$budgetExpense->fiscal_year}}
				</td>		
				<td>
					{{$budgetExpense->expenses}}
				</td>
				<td>
					{{$budgetExpense->forecastBudget}}
				</td>
			</tr>
			@endforeach	
		</table>
		<div class="col-md-12">
			<h3>Forcasted Disaster Fund of Barangay for year {{$forecastYear }}</h3>
		</div>
		<div class="col-md-12">
			<div id="bar-example"></div>
		</div>
	</div>
</div>

@else
<h3>Data Unavailable. </h3>
<h5>Please add data in finance budget</h5>
@endif


@endsection

@section('js')
<!-- internal scripts -->
<script>
	Morris.Donut({
		element: 'bar-example',
		colors: ['#ff1234', '#120b37', '#ffc107'],
		data: [
			{
				label: 'Population',
				value: '{{ end($population)->populationPerBrgy }}'
			},
			{
				label: 'Actual',
				value: '{{ $yearlyBudgetExpense[sizeof($yearlyBudgetExpense) - 2]->expenses }}'
			},
			{
				label: 'Forcast',
				value: '{{ end($yearlyBudgetExpense)->forecastBudget }}'
			}
		]
	});
</script>
@endsection
	