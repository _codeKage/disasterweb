@extends('layouts.app')

@section('title')
	Report Forecast
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
@if(count($yearlyRiskArea) > 0)
<div class="row">
	<div class="col-md-12">
		<h3>Risk Area</h3>
	</div>
	<div class="col-md-12">
		<table class="table">
			@foreach($yearlyRiskArea as $key=>$riskArea)
				<tr>
					<td colspan="3">{{$key}}</td>
				</tr>
				<tr>
					<td>Year</td>
					<td>Actual</td>
					<td>Forecast</td>
				</tr>
				@foreach($yearlyRiskArea[$key] as $purokArea)
				<tr>
					<td>
						{{$purokArea->year}}
					</td>
					<td>
						{{$purokArea->timesExperienced}}
					</td>
					<td>
						{{$purokArea->forecastOccurence}}
					</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="3" style="border-right: solid 1px #FFF; border-left: solid 1px #FFF;">&nbsp;</td>
				</tr>
			@endforeach

		</table>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<h3>Forcasted Risk Area of Barangay</h3>
		</div>
		<div class="col-md-12">
			<div id="bar-example"></div>
		</div>
	</div>
</div>
@else
<h3>Data Unavailable. </h3>
<h5>Please add data in household survey</h5>
@endif


@endsection

@section('js')
<!-- internal scripts -->
<script>
	Morris.Bar({
  element: 'bar-example',
  data: [
  	@foreach($yearlyRiskArea as $riskArea)
  		{ 
			  y: '{{ end($riskArea)->purok }}', 
			  a: '{{$riskArea[((sizeof($riskArea))-2)]->populationPerPurok}}',
			  b: '{{$riskArea[((sizeof($riskArea))-2)]->timesExperienced}}',
			  c: {{ end($riskArea)->forecastOccurence}}
		},
  	@endforeach

  ],
  xkey: 'y',
  ykeys: ['a', 'b', 'c'],
  labels: ['Population: ','Actual: ','Forcast:'],
  options: {
        title: {
            display: true,
            text: 'FORCAST'
        }
    }
});
</script>
@endsection