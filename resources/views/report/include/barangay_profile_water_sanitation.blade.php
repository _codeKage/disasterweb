<div role="tabpanel" class="tab-pane" id="WaterSanitation">
	<div class="col-md-12 row" style="margin-bottom: 10px">
	    <a href="{{route('reports.barangay_profile.water_sanitation.print')}}" target="_blank" class="btn btn-primary">Print Water Sanitation</a>
	</div>
	<h3> WATER SANITATION</h3>
	<h4> Household with Access to Improved or Safe Water Supply</h4>
	<table class="table-striped table">
		<thead>
		   	<tr>
				<th class="text-center">Occupation Industry</th>
				<th class="text-center">Number</th>
				<th class="text-center">%</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-left">Own use faucet, community water system</td>
				<td>{{number_format($household->where('water_supply','Own use faucet, community water system')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_supply','Own use faucet, community water system')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Shared faucet, community water system</td>
				<td>{{number_format($household->where('water_supply','Shared faucet, community water system')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_supply','Shared faucet, community water system')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Own use tubed/piped deep well</td>
				<td>{{number_format($household->where('water_supply','Own use tubed/piped deep well')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_supply','Own use tubed/piped deep well')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Shared tubed/piped deep well</td>
				<td>{{number_format($household->where('water_supply','Shared tubed/piped deep well')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_supply','Shared tubed/piped deep well')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Tubed/piped shallow well</td>
				<td>{{number_format($household->where('water_supply','Tubed/piped shallow well')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_supply','Tubed/piped shallow well')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Dug well</td>
				<td>{{number_format($household->where('water_supply','Dug well')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_supply','Dug well')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Protected spring</td>
				<td>{{number_format($household->where('water_supply','Protected spring')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_supply','Protected spring')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Peddler</td>
				<td>{{number_format($household->where('water_supply','Peddler')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_supply','Peddler')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Bottle water</td>
				<td>{{number_format($household->where('water_supply','Bottle water')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_supply','Bottle water')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Lake, River, Rain, and Others</td>
				<td>{{number_format($household->where('water_supply','Lake, River, Rain, and Others')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_supply','Lake, River, Rain, and Others')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>


			<tr style="text-align: center; background-color: #4472c4; color: white;">
				<td class="text-center"> TOTAL </td>
				<td class="text-center">{{number_format($household->where('water_supply','Own use faucet, community water system')->count() + $household->where('water_supply','Shared faucet, community water system')->count() + $household->where('water_supply','Own use tubed/piped deep well')->count() + $household->where('water_supply','Shared tubed/piped deep well')->count() + $household->where('water_supply','Tubed/piped shallow well')->count() + $household->where('water_supply','Dug well')->count() + $household->where('water_supply','Protected spring')->count() + $household->where('water_supply','Peddler')->count() + $household->where('water_supply','Bottle water')->count() + $household->where('water_supply','Lake, River, Rain, and Others')->count())}}</td>
				<td class="text-center">
					@if($household->count() != 0)
					{{number_format((($household->where('water_supply','Own use faucet, community water system')->count() + $household->where('water_supply','Shared faucet, community water system')->count() + $household->where('water_supply','Own use tubed/piped deep well')->count() + $household->where('water_supply','Shared tubed/piped deep well')->count() + $household->where('water_supply','Tubed/piped shallow well')->count() + $household->where('water_supply','Dug well')->count() + $household->where('water_supply','Protected spring')->count() + $household->where('water_supply','Peddler')->count() + $household->where('water_supply','Bottle water')->count() + $household->where('water_supply','Lake, River, Rain, and Others')->count()) / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>												
		</tbody>	
	</table>
	<h3>Toilet Facilities</h3>
	<table class="table-striped table">
		<thead>
		   	<tr>
				<th class="text-center">Toilet Facilities</th>
				<th class="text-center">Number</th>
				<th class="text-center">%</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-left">Owned</td>
				<td>{{number_format($household->where('toilet','Owned')->count())}}</td>
				<td class="text-center">
					@if($household->count() != 0)
					{{number_format(($household->where('toilet','Owned')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>	
			<tr>
				<td class="text-left">Shared</td>
				<td>{{number_format($household->where('toilet','Shared')->count())}}</td>
				<td class="text-center">
					@if($household->count() != 0)
					{{number_format(($household->where('toilet','Shared')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Closed pit</td>
				<td>{{number_format($household->where('toilet','Closed Pit')->count())}}</td>
				<td class="text-center">
					@if($household->count() != 0)
					{{number_format(($household->where('toilet','Closed Pit')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Open pit</td>
				<td>{{number_format($household->where('toilet','Open Pit')->count())}}</td>
				<td class="text-center">
					@if($household->count() != 0)
					{{number_format(($household->where('toilet','Open Pit')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Others (pail system,and others)</td>
				<td>{{number_format($household->where('toilet','Drop/Overhang')->count() + $household->where('toilet','Pail System')->count() + $household->where('toilet','No Toilet/Field/Bush')->count())}}</td>
				<td class="text-center">
					@if($household->count() != 0)
					{{number_format((($household->where('toilet','Drop/Overhang')->count() + $household->where('toilet','Pail System')->count() + $household->where('toilet','No Toilet/Field/Bush')->count()) / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr style="text-align: center; background-color: #4472c4; color: white;">
				<td class="text-center"> TOTAL </td>
				<td class="text-center">{{number_format($household->where('toilet','Owned')->count() + $household->where('toilet','Shared')->count() + $household->where('toilet','Closed Pit')->count() + $household->where('toilet','Open Pit')->count() + $household->where('toilet','Drop/Overhang')->count() + $household->where('toilet','Pail System')->count() + $household->where('toilet','No Toilet/Field/Bush')->count())}}</td>
				<td class="text-center">
					@if($household->count() != 0)
					{{number_format((($household->where('toilet','Owned')->count() + $household->where('toilet','Shared')->count() + $household->where('toilet','Closed Pit')->count() + $household->where('toilet','Open Pit')->count() + $household->where('toilet','Drop/Overhang')->count() + $household->where('toilet','Pail System')->count() + $household->where('toilet','No Toilet/Field/Bush')->count()) / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>												
		</tbody>	
	</table>
	<table class="table-striped table">
		<thead>
		   	<tr>
				<th class="text-center">Total No. of Households</th>
				<th class="text-center">HH'S with Water Filtration</th>
				<th class="text-center">%</th>
				<th class="text-center">HH'S without Water Filtration</th>
			    <th class="text-center">%</th>
				</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{number_format($household->count())}}</td>
				<td>{{number_format($household->where('water_filtration','Yes')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_filtration','Yes')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
				<td>{{number_format($household->where('water_filtration','No')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('water_filtration','No')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>													
		</tbody>	
	</table>
</div>