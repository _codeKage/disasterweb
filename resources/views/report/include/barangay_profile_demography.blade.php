 <div role="tabpanel" class="tab-pane" id="Demography" >
 	<div class="col-md-12 row" style="margin-bottom: 10px">
	    <a href="{{route('reports.barangay_profile.demography.print')}}" target="_blank" class="btn btn-primary">Print Demography</a>
	</div>
	<h3>DEMOGRAPHY</h3>
	<table class="table-striped table" id="printDemography">
		<thead>
		   <tr>
				<th class="text-left">Population</th>
				<th>{{number_format($household_member->count())}}</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td  style="text-indent: 6mm;" class="text-left">Male</td>
				<td>{{number_format($household_member->where('gender','Male')->count())}}</td>
			</tr>
			<tr>
				<td  style="text-indent: 6mm;" class="text-left">Female</td>
				<td>{{number_format($household_member->where('gender','Female')->count())}}</td>
			</tr>
			<tr>
				<td class="text-left">No. of Households</td>
				<td>{{number_format($household->count())}}</td>
			</tr>
			<tr>
				<td class="text-left">No. of Families</td>
				<td>{{number_format($household->sum('no_family'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Average Family Size</td>
				<td>
					@if($household->sum('no_family') != 0)
					{{number_format($household->sum('no_members') / $household->sum('no_family'),2)}}
					@else
					0
					@endif
				</td>
			</tr>			
		</tbody>	
	</table>

	<table class="table-striped table" style="border: 1px solid black; font-size: 12px" border="1" >
	  	<tr>
	     	<th rowspan="2">AGE GROUP</th>
	     	<th colspan="3" scope="colgroup">Household Population</th>
	     	<th colspan="3" scope="colgroup">Proportion</th>	
	  	</tr>
	  	<tr>
	    	<th scope="col">Male</th>
		    <th scope="col">Female</th>
		    <th scope="col">Both Sexes</th>
		    <th scope="col">Male</th>
		    <th scope="col">Female</th>
		    <th scope="col">Both Sexes</th>
	  	</tr>
	  	<tr style="border: 1px solid black;">
			<td class="text-left">Below 1</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age < 1;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age < 1;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age < 1;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age < 1;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age < 1;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age < 1;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">1-4</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 1 && $household_member->age <= 4;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">5-9</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 5 && $household_member->age <= 9;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">10-14</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 10 && $household_member->age <= 14;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">15-19</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 15 && $household_member->age <= 19;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">20-24</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 20 && $household_member->age <= 24;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">25-29</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 25 && $household_member->age <= 29;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">30-34</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 30 && $household_member->age <= 34;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">35-39</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 35 && $household_member->age <= 39;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">40-44</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 40 && $household_member->age <= 44;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">45-49</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 45 && $household_member->age <= 49;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">50-54</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 50 && $household_member->age <= 54;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">55-59</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 55 && $household_member->age <= 59;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">60-64</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 60 && $household_member->age <= 64;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">65-69</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 65 && $household_member->age <= 69;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">70-74</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 70 && $household_member->age <= 74;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">75-79</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 75 && $household_member->age <= 79;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">80-84</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age >= 80 && $household_member->age <= 84;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr style="border: 1px solid black;">
			<td class="text-left">85 and above</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age > 84;})->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age > 84;})->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->filter(function ($household_member) {return $household_member->age > 84;})->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age > 84;})->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age > 84;})->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->filter(function ($household_member) {return $household_member->age > 84;})->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
		<tr>
			<th>Total</th>
			<td>{{number_format($household_member->where('gender','Male')->count())}}</td>
			<td>{{number_format($household_member->where('gender','Female')->count())}}</td>
			<td>{{number_format($household_member->count())}}</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->where('gender','Male')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->where('gender','Female')->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
			<td>
				@if($household_member->count() != 0)
				{{number_format(($household_member->count() / $household_member->count())*100,2)}}
				@else
				0
				@endif
			</td>
		</tr>
	</table>

    <table class="table-striped table">
		<thead>
		   	<tr>
				<th>Marital Status</th>
				<th>Number</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-left">Single</td>
				<td>{{number_format($household_member->where('civil_status','Single')->count())}}</td>
			</tr>
			<tr>
				<td class="text-left">Married</td>
				<td>{{number_format($household_member->where('civil_status','Married')->count())}}</td>
			</tr>
			<tr>
				<td class="text-left">Widowed</td>
				<td>{{number_format($household_member->where('civil_status','Widowed')->count())}}</td>
			</tr>
			<tr>
				<td class="text-left">Divorced/Separated</td>
				<td>{{number_format($household_member->where('civil_status','Divorce/Seperated')->count())}}</td>
			</tr>
			<tr>
				<td class="text-left">Anulled</td>
				<td>{{number_format($household_member->where('civil_status','Annulled')->count())}}</td>
			</tr>
			<tr>
				<td class="text-left">Common-Law/Live-in</td>
				<td>{{number_format($household_member->where('civil_status','Common-law/Live-in')->count())}}</td>
			</tr>
			<tr>
				<td class="text-left">Unknown</td>
				<td>{{number_format($household_member->where('civil_status','Unknown')->count())}}</td>
			</tr>		
			<tr style="text-align: center; background-color: #4472c4; color: white;">
				<td>Total</td>
				<td>{{number_format($household_member->where('civil_status','Single')->count() + $household_member->where('civil_status','Married')->count() + $household_member->where('civil_status','Widowed')->count() + $household_member->where('civil_status','Divorce/Seperated')->count() + $household_member->where('civil_status','Annulled')->count() + $household_member->where('civil_status','Common-law/Live-in')->count() + $household_member->where('civil_status','Unknown')->count())}}</td>
			</tr>												
		</tbody>	
	</table>
</div>