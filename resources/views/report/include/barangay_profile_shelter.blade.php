<div role="tabpanel" class="tab-pane" id="Shelter">
	<div class="col-md-12 row" style="margin-bottom: 10px">
	    <a href="{{route('reports.barangay_profile.shelter.print')}}" target="_blank" class="btn btn-primary">Print Shelter</a>
	</div>
	<h3>SHELTER </h3> 
	<table class="table-striped table">
		<thead>
		   	<tr>
				<th class="text-center">Type of Building/Housing Unit </th>
				<th class="text-center">Number</th>
				<th class="text-center">%</th>				
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-left">Concrete</td>
				<td>{{number_format($household->where('building_type','Concrete')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('building_type','Concrete')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>	
			<tr>
				<td class="text-left">Semi-Concrete</td>
				<td>{{number_format($household->where('building_type','Semi-Concrete')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('building_type','Semi-Concrete')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr>
				<td class="text-left">Light Materials</td>
				<td>{{number_format($household->where('building_type','Light Materials')->count())}}</td>
				<td>
					@if($household->count() != 0)
					{{number_format(($household->where('building_type','Light Materials')->count() / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>
			<tr style="text-align: center; background-color: #4472c4; color: white;">
				<td class="text-center"> TOTAL </td>
				<td class="text-center">{{number_format($household->where('building_type','Concrete')->count() + $household->where('building_type','Semi-Concrete')->count() + $household->where('building_type','Light Materials')->count())}}</td>
				<td class="text-center">
					@if($household->count() != 0)
					{{number_format((($household->where('building_type','Concrete')->count() + $household->where('building_type','Semi-Concrete')->count() + $household->where('building_type','Light Materials')->count()) / $household->count())*100,2)}}
					@else
					0
					@endif
				</td>
			</tr>		
		</tbody>	
	</table>
	<div class="col-sm-6">
		<table class="table-striped table">
			<thead>
			   	<tr>
					<th class="text-center">Power Generator </th>
					<th class="text-center">Number</th>
					<th>%</th>				
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-left">With Backup</td>
					<td>{{number_format($household->where('power_generator','Yes')->count())}}</td>
					<td>
						@if($household->count() != 0)
						{{number_format(($household->where('power_generator','Yes')->count() / $household->count())*100,2)}}
						@else
						0
						@endif
					</td>
				</tr>	
				<tr>
					<td class="text-left">Without Backup</td>
					<td>{{number_format($household->where('power_generator','No')->count())}}</td>
					<td>
						@if($household->count() != 0)
						{{number_format(($household->where('power_generator','No')->count() / $household->count())*100,2)}}
						@else
						0
						@endif
					</td>
				</tr>
				<tr style="text-align: center; background-color: #4472c4; color: white;">
					<td class="text-center"> TOTAL </td>
					<td class="text-center">{{number_format($household->where('power_generator','Yes')->count() + $household->where('power_generator','No')->count())}}</td>
					<td class="text-center">
						@if($household->count() != 0)
						{{number_format((($household->where('power_generator','Yes')->count() + $household->where('power_generator','No')->count()) / $household->count())*100,2)}}
						@else
						0
						@endif
					</td>
				</tr>		
			</tbody>	
		</table>
 	</div>
 	<div class="col-sm-6">
		<table class="table-striped table">
			<thead>
			   	<tr>
					<th class="text-center">Electricity </th>
					<th class="text-center">Number</th>
					<th class="text-center">%</th>				
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-left">Electric Company</td>
					<td>{{number_format($household->where('electricity','Electric Company')->count())}}</td>
					<td>
						@if($household->count() != 0)
						{{number_format(($household->where('electricity','Electric Company')->count() / $household->count())*100,2)}}
						@else
						0
						@endif
					</td>
				</tr>	
				<tr>
					<td class="text-left">Generator</td>
					<td>{{number_format($household->where('electricity','Generator')->count())}}</td>
					<td>

						@if($household->count() != 0)
							
						{{number_format(($household->where('electricity','Generator')->count() / $household->count())*100,2)}}
						@else
						0
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-left">Solar</td>
					<td>{{number_format($household->where('electricity','Solar')->count())}}</td>
					<td>
						@if($household->count() != 0)
						{{number_format(($household->where('electricity','Solar')->count() / $household->count())*100,2)}}
						@else
						0
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-left">Battery</td>
					<td>{{number_format($household->where('electricity','Battery')->count())}}</td>
					<td>
						@if($household->count() != 0)
						{{number_format(($household->where('electricity','Battery')->count() / $household->count())*100,2)}}
						@else
						0
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-left">Others</td>
					<td>{{number_format($household->where('electricity','Others')->count())}}</td>
					<td>
						@if($household->count() != 0)
						{{number_format(($household->where('electricity','Others')->count() / $household->count())*100,2)}}
						@else
						0
						@endif
					</td>
				</tr>
				<tr>
					<td class="text-left">None</td>
					<td>{{number_format($household->where('electricity','None')->count())}}</td>
					<td>
						@if($household->count() != 0)
						{{number_format(($household->where('electricity','None')->count() / $household->count())*100,2)}}
						@else
						0
						@endif
					</td>
				</tr>
				<tr style="text-align: center; background-color: #4472c4; color: white;">
					<td class="text-center"> TOTAL </td>
					<td class="text-center">{{number_format($household->where('electricity','Electric Company')->count() + $household->where('electricity','Generator')->count() + $household->where('electricity','Solar')->count() + $household->where('electricity','Battery')->count() + $household->where('electricity','Others')->count() + $household->where('electricity','None')->count())}}</td>
					<td class="text-center">
						@if($household->count() != 0)
						{{number_format((($household->where('electricity','Electric Company')->count() + $household->where('electricity','Generator')->count() + $household->where('electricity','Solar')->count() + $household->where('electricity','Battery')->count() + $household->where('electricity','Others')->count() + $household->where('electricity','None')->count()) / $household->count())*100,2)}}
						@else
						0
						@endif
					</td>
				</tr>		
			</tbody>	
		</table>
 	</div>
</div>