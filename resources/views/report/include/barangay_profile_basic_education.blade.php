<div role="tabpanel" class="tab-pane" id="BasicEducation">
	<div class="col-md-12 row" style="margin-bottom: 10px">
	    <a href="{{route('reports.barangay_profile.basic_education.print')}}" target="_blank" class="btn btn-primary">Print Basic Education</a>
	</div>
	<h3>BASIC EDUCATION</h3>
	<div class="col-md-12">
		<h4>Attending School Rate</h4>
		<div class="col-xs-11">
			<div class="col-md-12">
				<strong>Attending:</strong>@if($household_member->count() != 0) {{number_format((($household_member->where('attending_school','Yes')->count()) / $household_member->count())*100,2)}}
				% @else 0 @endif
			</div>
			<div class="col-md-12">
				<strong>Not Attending:</strong>@if($household_member->count() != 0){{number_format((($household_member->where('attending_school','No')->count()) / $household_member->count())*100,2)}}% @else 0 @endif
			</div>
		</div>
		<div class="col-sm-12">
			<table class="table-striped table">
				<thead>
				   	<tr>
						<th class="text-center">Educational Attainment </th>
						<th class="text-center">Number</th>
						<th class="text-center">%</th>				
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-left">No Grade Completed</td>
						<td>{{number_format($household_member->where('highest_level','No Grade Completed')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','No Grade Completed')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr>
						<td class="text-left">Day Care</td>
						<td>{{number_format($household_member->where('highest_level','Day Care')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','Day Care')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr>
						<td class="text-left">Kindergarten/Preparatory</td>
						<td>{{number_format($household_member->where('highest_level','Kindergarten/Preparatory')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','Kindergarten/Preparatory')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr>
						<td class="text-left">Grade 1 to 3</td>
						<td>{{number_format($household_member->where('highest_level','Grade 1')->count() + $household_member->where('highest_level','Grade 2')->count() + $household_member->where('highest_level','Grade 3')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','Grade 1')->count() + $household_member->where('highest_level','Grade 2')->count() + $household_member->where('highest_level','Grade 3')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr>
						<td class="text-left">Grade 4 to 6</td>
						<td>{{number_format($household_member->where('highest_level','Grade 4')->count() + $household_member->where('highest_level','Grade 5')->count() + $household_member->where('highest_level','Grade 6')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','Grade 4')->count() + $household_member->where('highest_level','Grade 5')->count() + $household_member->where('highest_level','Grade 6')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr>
						<td class="text-left">Grade 7 to 9</td>
						<td>{{number_format($household_member->where('highest_level','Grade 7')->count() + $household_member->where('highest_level','Grade 8')->count() + $household_member->where('highest_level','Grade 9')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','Grade 7')->count() + $household_member->where('highest_level','Grade 8')->count() + $household_member->where('highest_level','Grade 9')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr>
						<td class="text-left">Grade 10 to 12</td>
						<td>{{number_format($household_member->where('highest_level','Grade 10')->count() + $household_member->where('highest_level','Grade 11')->count() + $household_member->where('highest_level','Grade 12')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','Grade 10')->count() + $household_member->where('highest_level','Grade 11')->count() + $household_member->where('highest_level','Grade 12')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr>
						<td class="text-left">1st Year PS/NT/TV, 2nd Year PS/NT/TV, 3rd Year PS/NT/TV</td>
						<td>{{number_format($household_member->where('highest_level','1st Year PS/NT/TV')->count() + $household_member->where('highest_level','2nd Year PS/NT/TV')->count() + $household_member->where('highest_level','3rd Year PS/NT/TV')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','1st Year PS/NT/TV')->count() + $household_member->where('highest_level','2nd Year PS/NT/TV')->count() + $household_member->where('highest_level','3rd Year PS/NT/TV')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr>
						<td class="text-left">1st Year College, 2nd Year College, 3rd Year College</td>
						<td>{{number_format($household_member->where('highest_level','1st Year College')->count() + $household_member->where('highest_level','2nd Year College')->count() + $household_member->where('highest_level','3rd Year College')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','1st Year College')->count() + $household_member->where('highest_level','2nd Year College')->count() + $household_member->where('highest_level','3rd Year College')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr>
						<td class="text-left">4th Year College, 5th Year College, 6th Year College</td>
						<td>{{number_format($household_member->where('highest_level','4th Year College')->count() + $household_member->where('highest_level','5th Year College')->count() + $household_member->where('highest_level','6th Year College')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','4th Year College')->count() + $household_member->where('highest_level','5th Year College')->count() + $household_member->where('highest_level','6th Year College')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr>
						<td class="text-left">Masters degree undergraduate, Masters graduate, Doctorate degree undergraduate, Doctorate degree graduate</td>
						<td>{{number_format($household_member->where('highest_level','Masters degree undergraduate')->count() + $household_member->where('highest_level','Masters graduate')->count() + $household_member->where('highest_level','Doctorate degree undergraduate')->count() + $household_member->where('highest_level','Doctorate degree graduate')->count())}}</td>
						<td>
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','Masters degree undergraduate')->count() + $household_member->where('highest_level','Masters graduate')->count() + $household_member->where('highest_level','Doctorate degree undergraduate')->count() + $household_member->where('highest_level','Doctorate degree graduate')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>
					<tr style="text-align: center; background-color: #4472c4; color: white;">
						<td> TOTAL </td>
						<td class="text-center">{{number_format($household_member->where('highest_level','No Grade Completed')->count() + $household_member->where('highest_level','Day Care')->count() + $household_member->where('highest_level','Kindergarten/Preparatory')->count() + $household_member->where('highest_level','Grade 1')->count() + $household_member->where('highest_level','Grade 2')->count() + $household_member->where('highest_level','Grade 3')->count() + $household_member->where('highest_level','Grade 4')->count() + $household_member->where('highest_level','Grade 5')->count() + $household_member->where('highest_level','Grade 6')->count() + $household_member->where('highest_level','Grade 7')->count() + $household_member->where('highest_level','Grade 8')->count() + $household_member->where('highest_level','Grade 9')->count() + $household_member->where('highest_level','Grade 10')->count() + $household_member->where('highest_level','Grade 11')->count() + $household_member->where('highest_level','Grade 12')->count() + $household_member->where('highest_level','1st Year PS/NT/TV')->count() + $household_member->where('highest_level','2nd Year PS/NT/TV')->count() + $household_member->where('highest_level','3rd Year PS/NT/TV')->count() + $household_member->where('highest_level','1st Year College')->count() + $household_member->where('highest_level','2nd Year College')->count() + $household_member->where('highest_level','3rd Year College')->count() + $household_member->where('highest_level','4th Year College')->count() + $household_member->where('highest_level','5th Year College')->count() + $household_member->where('highest_level','6th Year College')->count() + $household_member->where('highest_level','Masters degree undergraduate')->count() + $household_member->where('highest_level','Masters graduate')->count() + $household_member->where('highest_level','Doctorate degree undergraduate')->count() + $household_member->where('highest_level','Doctorate degree graduate')->count())}}</td>
						<td class="text-center">
							@if($household_member->count() != 0)
							{{number_format((($household_member->where('highest_level','No Grade Completed')->count() + $household_member->where('highest_level','Day Care')->count() + $household_member->where('highest_level','Kindergarten/Preparatory')->count() + $household_member->where('highest_level','Grade 1')->count() + $household_member->where('highest_level','Grade 2')->count() + $household_member->where('highest_level','Grade 3')->count() + $household_member->where('highest_level','Grade 4')->count() + $household_member->where('highest_level','Grade 5')->count() + $household_member->where('highest_level','Grade 6')->count() + $household_member->where('highest_level','Grade 7')->count() + $household_member->where('highest_level','Grade 8')->count() + $household_member->where('highest_level','Grade 9')->count() + $household_member->where('highest_level','Grade 10')->count() + $household_member->where('highest_level','Grade 11')->count() + $household_member->where('highest_level','Grade 12')->count() + $household_member->where('highest_level','1st Year PS/NT/TV')->count() + $household_member->where('highest_level','2nd Year PS/NT/TV')->count() + $household_member->where('highest_level','3rd Year PS/NT/TV')->count() + $household_member->where('highest_level','1st Year College')->count() + $household_member->where('highest_level','2nd Year College')->count() + $household_member->where('highest_level','3rd Year College')->count() + $household_member->where('highest_level','4th Year College')->count() + $household_member->where('highest_level','5th Year College')->count() + $household_member->where('highest_level','6th Year College')->count() + $household_member->where('highest_level','Masters degree undergraduate')->count() + $household_member->where('highest_level','Masters graduate')->count() + $household_member->where('highest_level','Doctorate degree undergraduate')->count() + $household_member->where('highest_level','Doctorate degree graduate')->count()) / $household_member->count())*100,2)}}
							@else
							0
							@endif
						</td>
					</tr>		
				</tbody>	
			</table>	
		</div>
	</div>
</div>