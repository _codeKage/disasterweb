<div role="tabpanel" class="tab-pane" id="Health">
	<div class="col-md-12 row" style="margin-bottom: 10px">
	    <a href="{{route('reports.barangay_profile.health.print')}}" target="_blank" class="btn btn-primary">Print Health</a>
	</div>
	<h3>Health </h3>
	<table class="table-striped table">
		<thead>
		   <tr>
				<th class="text-center">Physical Status</th>
				<th class="text-center">Number</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-left">Total blindness</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Total Blindness'))}}</td>
			</tr>	
			<tr>
				<td class="text-left">Partial blindness</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Partial Blindness'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Low vision</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Low Vision'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Totally deaf</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Totally Deaf'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Partially deaf</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Partially Deaf'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Oral defect</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Oral Defect'))}}</td>
			</tr>
			<tr>
				<td class="text-left">One hand</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'One Hand'))}}</td>
			</tr>
			<tr>
				<td class="text-left">No hands</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'No Hands'))}}</td>
			</tr>
			<tr>
				<td class="text-left">One leg</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'One Leg'))}}</td>
			</tr>
			<tr>
				<td class="text-left">No legs</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'No Legs'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Mild Cerebral palsy</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Mild Cerebral Palsy'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Severe Cerebral palsy</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Severe Cerebral Palsy'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Retarted</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Retarded'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Mentally ill</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Mentally Ill'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Mental retardation</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Mental Retardation'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Multiple impairment</td>
				<td>{{number_format(substr_count($household_member->implode('disability_type',','),'Multiple Impairment'))}}</td>
			</tr>
			<tr>
				<td class="text-left">Others</td>
				<td>{{$health_others_count}}</td>
			</tr>											
		</tbody>	
	</table>
</div>