<div role="tabpanel" class="tab-pane active" id="GeneralInformation" style="margin-top: 20px;">
	<div class="col-md-12 row">
	    <a href="{{route('reports.barangay_profile.general_information.print')}}" target="_blank" class="btn btn-primary">Print General Information</a>
	</div>
	<div class="row col-md-12">
		<h3> GENERAL INFORMATION</h3><br>
		<div class="col-md-5">
			<ul class="geninfo">
				<li>Barangay:</li>
				<li>Municipality:</li>
				<li>Province:</li>
				<li>Region:</li>
				<li>Barangay Category:</li>
				<li>Major Economic Source:</li>								
			</ul>
		</div>
		<div class="col-md-7">
			<ul class="geninfo">
				<li>{{$barangay_about->barangay}}</li>
				<li>{{$barangay_about->municipality}}</li>
				<li>{{$barangay_about->province}}</li>
				<li>{{$barangay_about->region}}</li>
				<li>{{$barangay_about->barangay_category}}</li>
				<li>{{$barangay_about->major_economic_source}}</li>											
			</ul><br>
		</div>
	</div>
	<div class="row col-md-12">
		<h3> PHYSICAL CHARACTERISTICS</h3>
		<div class="col-md-5">
			<ul class="geninfo">
				<li>Land Classification:</li>
				<li>Total Land Area:</li>
				<li>Geographical Location:</li>
							
			</ul>
		</div>
		<div class="col-md-7">
			<ul class="geninfo">
				<li>{{$barangay_about->land_classification}}</li>
				<li>{{$barangay_about->total_land_area}}</li>
				<li>{{$barangay_about->latitude}} Latitude and {{$barangay_about->longitude}} Longitude</li>
								
			</ul>
		</div>
	</div>
</div>