@extends('layouts.app')

@section('title')
	Report Barangay Profile
@endsection

@section('css')
<!-- internal styles -->
<style type="text/css">
	.geninfo li
	{
		font-size: 20px;
		list-style-type: none;
	}
	.othertable th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: center;
    background-color: #bfbfbf;
    color: black;
    font-size: 16px;

}
.othertable td
{
	text-align: left;

}

</style>
@endsection

@section('body')
	<ul class="nav nav-tabs hidden-print">
	  <li role="presentation" class="active"><a href="#GeneralInformation" aria-controls="GeneralInformation" role="tab" data-toggle="tab" style="color:black">General Information</a></li>
	  <li role="presentation"><a href="#Demography" aria-controls="Demography" role="tab" data-toggle="tab" style="color:black">Demography</a></li>
	  <li role="presentation"><a href="#Socio-EconomicProfile" aria-controls="Socio-EconomicProfile" role="tab" data-toggle="tab" style="color:black">Socio-Economic Profile</a></li>
	  <li role="presentation"><a href="#Health" aria-controls="Health" role="tab" data-toggle="tab" style="color:black">Health</a></li>
	  <li role="presentation"><a href="#WaterSanitation" aria-controls="WaterSanitation" role="tab" data-toggle="tab" style="color:black">Water Sanitation</a></li>
	  <li role="presentation"><a href="#Shelter" aria-controls="Shelter" role="tab" data-toggle="tab" style="color:black">Shelter</a></li>
	  <li role="presentation"><a href="#BasicEducation" aria-controls="BasicEducation" role="tab" data-toggle="tab" style="color:black">Basic Education</a></li>
	</ul>
    <div class="tab-content" style="margin: 10px">
        @include('report.include.barangay_profile_general_information')	
       	@include('report.include.barangay_profile_demography')
        @include('report.include.barangay_profile_socio-economic_profile')
        @include('report.include.barangay_profile_health')
		@include('report.include.barangay_profile_water_sanitation')
		@include('report.include.barangay_profile_shelter')
		@include('report.include.barangay_profile_basic_education')
    </div>
@endsection

@section('js')
{{Html::script('jQuery.print/jQuery.print.js')}}
<script>
	$('#printGeneralInfo').click(function(){
		$("#printGeneralInformation").print({
        	globalStyles: true,
        	mediaPrint: false,
        	stylesheet: null,
        	noPrintSelector: ".no-print",
        	iframe: true,
        	append: null,
        	prepend: null,
        	manuallyCopyFormValues: true,
        	deferred: $.Deferred(),
        	timeout: 750,
        	title: null,
        	doctype: '<!doctype html>'
		});
	});
	$('#printDemograph').click(function(){
		$("#printDemography").print({
        	globalStyles: true,
        	mediaPrint: false,
        	stylesheet: null,
        	noPrintSelector: ".no-print",
        	iframe: true,
        	append: null,
        	prepend: null,
        	manuallyCopyFormValues: true,
        	deferred: $.Deferred(),
        	timeout: 750,
        	title: null,
        	doctype: '<!doctype html>'
		});
	});
</script>
@endsection