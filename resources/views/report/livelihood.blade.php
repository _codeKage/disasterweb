@extends('layouts.app')

@section('title')
    Livelihood Report
@endsection

@section('css')
    <!-- internal styles -->
    {{Html::style('amcharts/plugins/export/export.css')}}
@endsection

@section('body')
    <div class="row">
        <div class="col-lg-12">
            <table class="table-striped table">
                <thead>
                    <th colspan="3">List of Livelihood for {{Session::get('barangayAbout')->barangay}} Household</th>
                </thead>
                <tbody>
                @foreach($most as $m)
                    <tr>
                        <td>{{$m}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection