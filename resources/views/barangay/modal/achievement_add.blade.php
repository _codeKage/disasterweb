<div class="modal fade" id="addachievements">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'barangayprofile.achievement.store','files'=>true]) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Achievement</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="row col-md-12 text-left" style="margin-top: 5px">
							<label>Achievement Picture</label>
						</div>
						<div class="col-md-3" v-if="image" style="margin-bottom: 5px">
							<img :src="image" class="img-responsive"/>
						</div>
						<div class="col-md-12">
							<input type="file" accept=".jpg,.jpeg,.png" name="image" @change="onFileChange" required>
							<span v-html="errorImage"></span>
						</div>
					</div>
					<div class="col-md-5 top10">
						<label>Type</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input class="form-control selectTypeAchievement0" data-list="Best Barangay on Peace - Order - Security and Human Rights, Best Financially Managed Barangay, Best Managed Barangay, Business Friendly Barangay, Cleanest and Greenest Barangay, Smoke Free Barangay" name="type" v-model="type_add" @keyup="validateAdd" data-minchars="0" />
					</div>
					<div class="col-md-5 top10">
						<label>Name</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name" v-model="name_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-5 top10">
						<label>Date</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="date" v-model="date_add" style="margin-bottom: 5px;width:100% !important" @change="validateAdd">
					</div>
					<div class="col-md-5 top10">
						<label>Details</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="detail" v-model="detail_add" style="margin-bottom: 5px;"  @keyup="validateAdd">
					</div>
					<div class="col-md-5 top10">
						<label>Other Details</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="other_detail" v-model="other_detail_add" style="margin-bottom: 5px;"  @keyup="validateAdd">
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAdd == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>
