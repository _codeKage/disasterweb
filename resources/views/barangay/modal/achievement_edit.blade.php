<div class="modal fade" id="editachievements{{$ba->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'barangayprofile.achievement.update','files'=>true]) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit {{$ba->name}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$ba->id}}">
				<div class="row">
					<div class="col-md-12">
						<div class="rowcol-md-12 text-left" style="margin-top: 5px">
							<label>Achievement Picture (.jpg,.jpeg,.png)</label>
						</div>
						<div class="col-md-3" style="margin-bottom: 5px">
							@if($ba->image != null)
								<span v-if="!image">
									<img src="{{asset('images/achievement/'.$ba->image)}}" class="img-responsive"/>
								</span>
								<span v-else>
									<img :src="image" class="img-responsive"/>
								</span>
							@else
								<img :src="image" class="img-responsive"/>
							@endif
						</div>
						<div class="col-md-12">
							<input type="file" accept=".jpg,.jpeg,.png" name="image" @change="onFileChange">
							<span v-html="errorImage"></span>
						</div>
					</div>
					<div class="col-md-5 top10 text-left">
						<label>Type</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px"> 	
						<input class="form-control selectTypeAchievement{{$ba->id}}" data-list="Best Barangay on Peace - Order - Security and Human Rights, Best Financially Managed Barangay, Best Managed Barangay, Business Friendly Barangay, Cleanest and Greenest Barangay, Smoke Free Barangay" name="type" v-model="type_edit" @keyup="validateEdit" data-minchars="0" />
					</div>
					<div class="col-md-5 top10 text-left">
						<label>Name</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name" v-model="name_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
					<div class="col-md-5 top10 text-left">
						<label>Date</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="date" v-model="date_edit" style="margin-bottom: 5px;width:100% !important" @change="validateEdit">
					</div>
					<div class="col-md-5 top10 text-left">
						<label>Details</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="detail" v-model="detail_edit" style="margin-bottom: 5px;"  @keyup="validateEdit">
					</div>
					<div class="col-md-5 top10 text-left">
						<label>Other Details</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="other_detail" v-model="other_detail_edit" style="margin-bottom: 5px;"  @keyup="validateEdit">
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Update</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Update</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>