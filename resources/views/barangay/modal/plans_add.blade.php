<div class="modal fade" id="addplans">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'barangayprofile.plans.store','files'=>true]) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Plan</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" name="user_id" value="{{Session::get('logUser')->id}}">
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<label>Plan File</label>
						<div class="col-md-12">
							<input type="file" name="file" @change="onFileChange">
						</div>
					</div>
					<div class="col-md-5 top10">
						<label>Name</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name" v-model="name_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-5 top10">
						<label>Date</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="date" v-model="date_add" style="margin-bottom: 5px;width:100% !important" @change="validateAdd">
					</div>
					<div class="col-md-5 top10">
						<label>Detail</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="detail" v-model="detail_add" style="margin-bottom: 5px;"  @keyup="validateAdd">
					</div>
					<div class="col-md-5 top10">
						<label>Type</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<select name="type" v-model="type_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled>Select Plan Type</option>
							<option value="Annual Investment Plan">Annual Investment Plan</option>
							<option value="Barangay Development Plan">Barangay Development Plan</option>
							<option value="Disaster Contingency Plan">Disaster Contingency Plan</option>
							<option value="Disaster Risk Reduction and Management Plan">Disaster Risk Reduction and Management Plan</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAdd == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>