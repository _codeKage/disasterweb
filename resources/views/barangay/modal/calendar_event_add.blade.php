<div class="modal fade" id="addevents">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'barangayprofile.calendar_events.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Event</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-5">
						<label>Type</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input class="form-control selectTypeEvent0" data-list="Barangay Assembly Day, Barangay Fund Raising Activity, Disaster Preparedness Program, Educational Program, Festival, Health Program, Katipunan ng Kabataan Assembly, Planting Program" name="type" v-model="type_add" @keyup="validateAdd" data-minchars="0" />
					</div>
					<div class="col-md-5">
						<label>Name</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name" v-model="name_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-5">
						<label>When</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="when" v-model="when_add" style="margin-bottom: 5px;width:100% !important" @change="validateAdd">
					</div>
					<div class="col-md-5">
						<label>Where</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="where" v-model="where_add" style="margin-bottom: 5px;"  @keyup="validateAdd">
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAdd == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>