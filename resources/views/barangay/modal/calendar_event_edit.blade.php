<div class="modal fade" id="editevents{{$bc->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'barangayprofile.calendar_events.update']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit {{$bc->name}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$bc->id}}">
				<div class="row">
					<div class="col-md-5 text-left">
						<label>Type</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						
						<input class="form-control selectTypeEvent{{$bc->id}}" data-list="Barangay Assembly Day, Barangay Fund Raising Activity, Disaster Preparedness Program, Educational Program, Festival, Health Program, Katipunan ng Kabataan Assembly, Planting Program" name="type" v-model="type_edit" @keyup="validateEdit" data-minchars="0" />
					</div>
					<div class="col-md-5 text-left">
						<label>Name</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name" v-model="name_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
					<div class="col-md-5 text-left">
						<label>When</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="when" v-model="when_edit" style="margin-bottom: 5px;width:100% !important" @change="validateEdit">
					</div>
					<div class="col-md-5 text-left">
						<label>Where</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="where" v-model="where_edit" style="margin-bottom: 5px;"  @keyup="validateEdit">
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Update</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Update</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		