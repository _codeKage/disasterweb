<div class="modal fade" id="editplans{{$bp->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'barangayprofile.plans.update','files'=>true]) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit {{$bp->name}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$bp->id}}">
				<div class="row">
					<input type="hidden" name="user_id" value="{{Session::get('logUser')->id}}">
					<div class="col-md-12" style="margin-bottom: 5px">
						<div class="row col-md-12">
							<div class="row col-md-12 text-left">
								<label style="font-size: 15px">Plan File</label>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="file" name="file">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5 text-left top10">
						<label>Name</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name" v-model="name_edit" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-5 text-left top10">
						<label>Date</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="date" v-model="date_edit" style="margin-bottom: 5px;width:100% !important" @change="validateAdd">
					</div>
					<div class="col-md-5 text-left top10">
						<label>Detail</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="detail" v-model="detail_edit" style="margin-bottom: 5px;"  @keyup="validateAdd">
					</div>
					<div class="col-md-5 text-left top10">
						<label>Type</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<select name="type" v-model="type_edit" class="form-control" @change="validateAdd">
							<option value="" selected disabled>Select Plan Type</option>
							<option value="Annual Investment Plan">Annual Investment Plan</option>
							<option value="Barangay Development Plan">Barangay Development Plan</option>
							<option value="Disaster Contingency Plan">Disaster Contingency Plan</option>
							<option value="Disaster Risk Reduction and Management Plan">Disaster Risk Reduction and Management Plan</option>
						</select>
					</div>
					<div class="col-md-5 text-left top10">
						<label>Last User</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" v-model="user" readonly disabled>
					</div>
					<div class="col-md-5 text-left top10">
						<label>Last Update</label>
					</div>
					<div class="col-md-7 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" v-model="updated_at" readonly disabled>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Update</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Update</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>