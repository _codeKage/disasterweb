@extends('layouts.app')
@section('title')
	About Barangay
@endsection
@section('css')
<!-- internal styles -->
<style>
#map {
	height: 300px;
}
.panel-heading{
    background-color: #1b3c5b!important;
    color: white !important;
}
</style>
@endsection
@section('body')
<div id="aboutBarangay">
	{!! Form::open(['route'=>'barangayprofile.about.update','files'=>true]) !!}
	<div class="barangay top20">
		<input type="hidden" name="id" value="{{$barangayAbout->id}}">
		<div class="panel panel-default" >
			<div class="panel-heading" style="color: black;">
				<h3 class="panel-title"><b>ABOUT BARANGAY</b></h3>
			</div>
			<div class="panel-body" >
		    	<div class="container1">
		    		@if(Session::has('flash_message'))
						<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
					@endif
		    		<div>
		    			<h4><strong>GENERAL INFORMATION</strong></h4>
		    		</div>
		    		<div class="container2">
		    			@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
		    			<div class="row col-md-12" style="margin-bottom: 10px">
		    				Get address by clicking on the map:
		    				<div id="map"></div>
		    			</div>
		    			@endif
		    			<div class="form-group row" :class="{ 'has-error': isErrorAddress }">
							<span  class="col-md-3">Address</span>
							<div class="col-md-3 col-sm-12" style="margin-bottom: 10px">
							    <input type="text" class="form-control" name="address" placeholder="address" v-model="address" @keyup="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif>
							    <span v-html="unknownAddress"></span>
							</div>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<div class="col-md-3">
								<span v-if="isLoadingGetAddress == false">
									<button type="button" class="btn btn-primary" @click='addressDetails()'>Get Address Details</button>
								</span>
								<span v-else v-cloak>
									<button class="btn btn-default" disabled><i class="fa fa-spinner fa-spin"></i> Getting Data Please wait</button>
								</span>
							</div>
							@endif
						</div>
		    			<div class="form-group row">
							<span  class="col-md-3">Geographical Location</span>
							<div class="col-md-3">
							    <input type="number" class="form-control" name="latitude" placeholder="Latitude" v-model.number="latitude" @keyup="validate" step="0.0000001" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif>
							</div>
						</div>
						<div class="form-group row">
							<span  class="col-md-3"></span>
							<div class="col-md-3 col-sm-12">
								<input type="number" class="form-control" name="longitude" placeholder="longitude" v-model.number="longitude" step="0.0000001" @keyup="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif>
							</div>
						</div>
			    		<div class="form-group row">
							<span class="col-md-3">Barangay</span>
							<div class="col-md-3">
								<input type="text" name="barangay" v-model="barangay" class="form-control" @keyup="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif>
							</div>
						</div>
						<div class="form-group row">
							<span  class="col-md-3">Municipality</span>
							<div class="col-md-3">
								<input type="text" name="municipality" v-model="municipality" class="form-control"  @keyup="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif>
							</div>
						</div>
						<div class="form-group row">
							<span  class="col-md-3">Province</span>
							<div class="col-md-3">
							    <input type="text" name="province" v-model="province" class="form-control"  @keyup="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif>
							</div>
						</div>
						<div class="form-group row">
							<span class="col-md-3">Region</span>
							<div class="col-md-3">
							    <input type="text" name="region" v-model="region" class="form-control" @keyup="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif>
							</div>
						</div>
					</div>
					<div style="margin-top: 30px">
						<h4><strong>PHYSICAL INFORMATION</strong></h4>
					</div>
					<div class="container2">
						<div class="form-group row">
							<span  class="col-md-3">Total Land Area</span>
							<div class="col-md-3">
							    <input type="number" class="form-control" name="total_land_area" v-model.number="total_land_area" @keyup="validate" placeholder="Hectares" step="0.0001" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif>
							</div>
						</div>
						<div class="form-group row">
							<span  class="col-md-3">Barangay Category</span>
							<div class="col-md-3">
								<select name="barangay_category" id="barangay_category" v-model="barangay_category" class="form-control" required="required" @change="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif>
									<option value="" disabled selected>Select Barangay Category</option>
									<option value="Urban">Urban</option>
									<option value="Rural">Rural</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<span  class="col-md-3">Land Classification</span>
							<input type="hidden" v-model="land_classification" name="land_classification">
							<div class="col-md-9">
								<div class="row">
									<div class="col-md-2">
										<span style="margin-bottom: 5px">
											<input type="checkbox" v-model="lowland" @change="checkedLandClassification()">
											Lowland
										</span>
									</div>
									<div class="col-md-2">
										<span style="margin-bottom: 5px;">
											<input type="checkbox" v-model="upland" @change="checkedLandClassification()">
											Upland
										</span>	
									</div>
									<div class="col-md-2">
										<span style="margin-bottom: 5px;">
											<input type="checkbox" v-model="landlock" @change="checkedLandClassification()">
											Landlock
										</span>
									</div>
									<div class="col-md-2">
										<span style="margin-bottom: 5px;">
											<input type="checkbox" v-model="coastal" @change="checkedLandClassification()">
											Coastal
										</span>
									</div>
									<div class="col-md-2">
										<span style="margin-bottom: 5px;">
											<input type="checkbox" v-model="other" @change="checkedLandClassification()">
											Others
										</span>
									</div>
									<div class="col-md-12">
										<input type="text" class="form-control" v-model="other_val" v-if="other != ''" @keyup="checkedLandClassification()" placeholder="Input other land classicifation seperated with comma" v-cloak>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<span  class="col-md-3">Major Economic Source</span>
							<input type="hidden" v-model="major_economic_source" name="major_economic_source">
							<div class="col-md-9">
								<div class="row">
									<div class="col-md-2">
										<span style="margin-bottom: 5px">
											<input type="checkbox" v-model="agricultural" @change="checkedMajorEconomicSource()">
											Agricultural
										</span>
									</div>
									<div class="col-md-2">
										<span style="margin-bottom: 5px;">
											<input type="checkbox" v-model="fishing" @change="checkedMajorEconomicSource()">
											Fishing
										</span>	
									</div>
									<div class="col-md-2">
										<span style="margin-bottom: 5px;">
											<input type="checkbox" v-model="industrial" @change="checkedMajorEconomicSource()">
											Industrial
										</span>
									</div>
									<div class="col-md-2">
										<span style="margin-bottom: 5px;">
											<input type="checkbox" v-model="commercial" @change="checkedMajorEconomicSource()">
											Commercial
										</span>
									</div>
									<div class="col-md-2">
										<span style="margin-bottom: 5px;">
											<input type="checkbox" v-model="other_major" placeholder="Input other major economic source seperated with comma" @change="checkedMajorEconomicSource()">
											Others
										</span>
									</div>
									<div class="col-md-12">
										<input type="text" class="form-control" v-model="other_major_val" v-if="other_major != ''" @keyup="checkedMajorEconomicSource()" v-cloak>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="margin-top: 30px">
						<h4> Logo Image</h4>
						<div class="col-md-12">
							<div class="col-md-2">
								@if(Session::get('barangayAbout')->logo != null)
								<span v-if="!image">
									<img src="{{asset('images/logo/'.Session::get('barangayAbout')->logo)}}" class="img-responsive"/>
								</span>
								<span v-else>
									<img :src="image" class="img-responsive"/>
								</span>
								@else
									<img :src="image" class="img-responsive"/>
								@endif
							</div>
							<div class="col-md-12">
							   	<input type="file" accept=".jpg,.jpeg,.png" name="logo" @change="onFileChange" @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif>
							   	<span v-html="errorImage"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 30px">
						<h4> Vision:</h4>
						<div class="col-md-12">
							<textarea style="width: 100%; height: 100px; margin-bottom: 20px;" name="vision" v-model="vision" @keyup="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif></textarea>
						</div>
					</div>
					<div style="margin-top: 30px">
						<h4> Mission:</h4>
						<div class="col-md-12">
							<textarea style="width: 100%; height: 100px; margin-bottom: 20px;" name="mission" v-model="mission" @keyup="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif></textarea>
						</div>
					</div>
					<!-- <div style="margin-top: 30px">
						<h4> History:</h4>
						<div class="col-md-12">
							<textarea style="width: 100%; height: 100px; margin-bottom: 20px;" name="history" v-model="history" @keyup="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif></textarea>
						</div>
					</div>
					<div style="margin-top: 30px">
						<h4> Brief Description:</h4>
						<div class="col-md-12">
							<textarea style="width: 100%; height: 100px; margin-bottom: 20px;" name="brief_description" v-model="brief_description" @keyup="validate" required @if(Session::get('logUser')->usertype != 'Chairman' && Session::get('logUser')->usertype != 'Secretary') disabled @endif></textarea>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
	<div  style="float: right; margin-top: 3px; margin-right: 5%;">
		<span v-if="enableUpdate == true"  v-cloak>
			<button type="submit" class="btn btn-success">Update</button>
		</span>
		<span v-else>
			<button type="submit" class="btn btn-success" disabled>Update</button>
		</span>
	</div>
	@endif
	{!!Form::close()!!}
</div>
@endsection
@section('js')
<script 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJVX1wI8LOsRMUAlVbh1pTrXQvBFJoA4w">
</script>
{{Html::script('js/vue.min.js')}}
{{Html::script('js/axios.min.js')}}
<script>
	var myLatLng = {lat: {{Session::get('barangayAbout')->latitude}}, lng: {{Session::get('barangayAbout')->longitude}}};
	var marker = [];
    var app = new Vue({
        el: '#aboutBarangay',
        delimiters: ["[[","]]"],
        data: {
        	dataAwait: [],
        	dataAsync: [],
        	longitude: '{{$barangayAbout->longitude}}',
        	latitude: '{{$barangayAbout->latitude}}',
        	image: '',
        	province: '{{$barangayAbout->province}}',
        	municipality: '{{$barangayAbout->municipality}}',
        	address: '{{$barangayAbout->address}}',
        	barangay: '{{$barangayAbout->barangay}}',
			region: '{{$barangayAbout->region}}',
			total_land_area: '{{$barangayAbout->total_land_area}}',
			barangay_category: '{{$barangayAbout->barangay_category}}',
			land_classification: '',
			major_economic_source: '',
			vision: '',
			mission: '',
			history: '',
		  	brief_description: '',
        	isLoadingGetAddress: false,
        	enableUpdate: false,
        	unknownAddress: '',
        	errorImage: '',
        	isErrorAddress: false,
        	invalidImage: false,
        	gMap: '',
        	lowland: false,
			upland: false,
			landlock: false,
			coastal: false,
			other: false,
			other_val: '',
			land_classification_arr: [],
			agricultural: '',
			fishing: '',
			industrial: '',
			commercial: '',
			other_major: '',
			other_major_val: '',
			major_economic_source_arr: []
        },
       	mounted(){
       		var arr_land_classification_string = '{!!str_replace('"','',json_encode($barangayAbout->land_classification)) !!}';
       		this.land_classification_arr = arr_land_classification_string.split(",");
       		for (var i=this.land_classification_arr.length-1; i>=0; i--) {
			    if (this.land_classification_arr[i] === 'Lowland' || this.land_classification_arr[i] === 'Upland' || this.land_classification_arr[i] === 'Landlock' || this.land_classification_arr[i] === 'Coastal') {
			        if(this.land_classification_arr[i] === 'Lowland'){
			        	this.lowland = true;
			        }
			        if(this.land_classification_arr[i] === 'Upland'){
			        	this.upland = true;
			        }
			        if(this.land_classification_arr[i] === 'Landlock'){
			        	this.landlock = true;
			        }
			        if(this.land_classification_arr[i] === 'Coastal'){
			        	this.coastal = true;
			        }
			        this.land_classification_arr.splice(i, 1);
			    }
			}
			if(this.land_classification_arr.length > 0){
				this.other = true;
				this.other_val = this.land_classification_arr.toString();
			}
       		this.checkedLandClassification();
       		var arr_major_economic_resource_string = '{!!str_replace('"','',json_encode($barangayAbout->major_economic_source)) !!}';
       		this.major_economic_resource_arr = arr_major_economic_resource_string.split(",");
       		for (var i=this.major_economic_resource_arr.length-1; i>=0; i--) {
			    if (this.major_economic_resource_arr[i] === 'Agricultural' || this.major_economic_resource_arr[i] === 'Fishing' || this.major_economic_resource_arr[i] === 'Industrial' || this.major_economic_resource_arr[i] === 'Commercial') {
			        if(this.major_economic_resource_arr[i] === 'Agricultural'){
			        	this.agricultural = true;
			        }
			        if(this.major_economic_resource_arr[i] === 'Fishing'){
			        	this.fishing = true;
			        }
			        if(this.major_economic_resource_arr[i] === 'Industrial'){
			        	this.industrial = true;
			        }
			        if(this.major_economic_resource_arr[i] === 'Commercial'){
			        	this.commercial = true;
			        }
			        this.major_economic_resource_arr.splice(i, 1);
			    }
			}
			if(this.major_economic_resource_arr.length > 0){
				this.other_major = true;
				this.other_major_val = this.major_economic_resource_arr.toString();
			}
       		this.checkedMajorEconomicSource();
       		this.barangayDetails();
       		let vm = this;
       		this.validate();
	        this.gMap = new google.maps.Map(document.getElementById('map'), {
	          zoom: 15,
	          center: myLatLng
	        });
	        marker = new google.maps.Marker({
	          position: myLatLng,
	          map: this.gMap,
	      	});
	        //Add listener
			google.maps.event.addListener(this.gMap, "click", function (event) {
			    vm.latitude = event.latLng.lat();
			    vm.longitude = event.latLng.lng();
			    myLatLng = {lat: event.latLng.lat(), lng: event.latLng.lng()};
			    vm.addressDetailsCoords();
			}); //end addListener
       	},
        methods: {
        	async barangayDetails(){
				await axios.get('/api/barangayDetails'
				).then(
					responseData=>{
						this.vision = responseData.data.barangayDetails.vision;
						this.mission = responseData.data.barangayDetails.mission;
						this.history = responseData.data.barangayDetails.history;
						this.brief_description = responseData.data.barangayDetails.brief_description;
					}
				)
				.catch(
					error=>{
						console.log("error response ",error.response)
						let response = error.response;
						this.vision = response.data.barangayDetails.vision;
						this.mission = response.data.barangayDetails.mission;
						this.history = response.data.barangayDetails.history;
						this.brief_description = response.data.barangayDetails.brief_description;
					}
				);
				this.validate();
    		},
        	checkedMajorEconomicSource(){
        		let mes_arr = [];
        		if(this.agricultural == true){
        			mes_arr.push('Agricultural');
        		}
        		if(this.fishing == true){
        			mes_arr.push('Fishing');
        		}
        		if(this.industrial == true){
        			mes_arr.push('Industrial');
        		}
        		if(this.commercial == true){
        			mes_arr.push('Commercial');
        		}
        		if(this.other == true && this.other_val != ''){
        			mes_arr.push(this.other_val);
        		}
        		this.major_economic_source = mes_arr.join();
        		this.validate();
        	},
        	checkedLandClassification(){
        		let lc_arr = [];
        		if(this.lowland == true){
        			lc_arr.push('Lowland');
        		}
        		if(this.upland == true){
        			lc_arr.push('Upland');
        		}
        		if(this.landlock == true){
        			lc_arr.push('Landlock');
        		}
        		if(this.coastal == true){
        			lc_arr.push('Coastal');
        		}
        		if(this.other == true && this.other_val != ''){
        			lc_arr.push(this.other_val);
        		}
        		this.land_classification = lc_arr.join();
        		this.validate();
        	},
        	validate(){
        		if(this.longitude != '' && this.latuitude != '' && this.province != '' && this.municipality != '' && this.address != '' && this.barangay != '' && this.region != '' && this.total_land_area != '' && this.barangay_category != '' && this.land_classification != '' && this.major_economic_source != '' && this.vision != '' && this.mission != '' && this.errorImage == ''){
        			this.enableUpdate = true;
        		} else {
        			this.enableUpdate = false;
        		}
        	},
        	onFileChange(e) {
        		this.errorImage = '';
  				var files = e.target.files || e.dataTransfer.files;
					var file = files[0];
				var fileType = file["type"];
				var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png", "image/bmp"];
				if ($.inArray(fileType, ValidImageTypes) < 0) {
				     this.errorImage = '<span style="color:red">Invalid Image format. Only Accepts(jpg,jpeg,png,bmp)</span>';
				} else {
					if (!files.length)
    					return;
  					this.createImage(files[0]);
				}
			},
			createImage(file) {
  				var image = new Image();
  				var reader = new FileReader();
  				var vm = this;
  				reader.onload = (e) => {
    				vm.image = e.target.result;
  				};
  				reader.readAsDataURL(file);
			},
            async addressDetails() {
            	try{
                	this.isLoadingGetAddress = true;
                	this.enableUpdate = false;
                	this.isErrorAddress = false;
                	this.unknownAddress = '';
                	/*getting the longitude and latitude from the address*/
                	const responseLatLong = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?address='+this.address+'&key=AIzaSyAIOnshF_fHAIQAYl1tvNKqcTlXTlW-l4o');
                	responseLatLong.data.results.forEach((values) =>{
                		this.latitude = values.geometry.location.lat;
                		this.longitude = values.geometry.location.lng;
                	});
                	/*getting the municipal and province from the longitude and latitude of the address*/
			      	const responseData = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+this.latitude+','+this.longitude+'&key=AIzaSyAIOnshF_fHAIQAYl1tvNKqcTlXTlW-l4o');
			        responseData.data.results.forEach((values) => {
			            var filtered_municipality = values.address_components.filter(function(address_component){
							return address_component.types.includes("locality");
						}); 
						if(filtered_municipality.length){
							this.municipality = filtered_municipality[0].long_name;
						}
			            var filtered_province = values.address_components.filter(function(address_component){
							return address_component.types.includes("administrative_area_level_2");
						}); 
						if(filtered_province.length){
							this.province = filtered_province[0].long_name;
						}
			        })
			        if (marker && marker.setMap) {
					    marker.setMap(null);
					}
                	marker = new google.maps.Marker({
			          position: {lat: this.latitude, lng: this.longitude},
			          map: this.gMap,
			      	});
			      	this.gMap.setCenter(new google.maps.LatLng(this.latitude, this.longitude));
			        this.isLoadingGetAddress = false;
			        this.validate();
			    } catch (error) {
			    	this.isLoadingGetAddress = false;
			    	this.isErrorAddress = true;
			    	this.latitude = null;
			    	this.longitude = null;
			    	this.province = null;
			    	this.municipality = null;
			    	this.unknownAddress = '<span style="color:red">Cannot find the address.</span>';
			    }
            },
            async addressDetailsCoords() {
            	try{
                	this.isLoadingGetAddress = true;
                	this.enableUpdate = false;
                	this.isErrorAddress = false;
                	this.unknownAddress = '';
                	let street_address ='';
                 	if (marker && marker.setMap) {
					    marker.setMap(null);
					}
                	marker = new google.maps.Marker({
			          position: myLatLng,
			          map: this.gMap,
			      	});
                	/*getting the municipal and province from the longitude and latitude of the address*/
			      	const responseData = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+this.latitude+','+this.longitude+'&key=AIzaSyAIOnshF_fHAIQAYl1tvNKqcTlXTlW-l4o');
			      	this.address = responseData.data.results[0].formatted_address;
			        responseData.data.results.forEach((values) => {
			            var filtered_municipality = values.address_components.filter(function(address_component){
							return address_component.types.includes("locality");
						}); 
						if(filtered_municipality.length){
							this.municipality = filtered_municipality[0].long_name;
						}
			            var filtered_province = values.address_components.filter(function(address_component){
							return address_component.types.includes("administrative_area_level_2");
						}); 
						if(filtered_province.length){
							this.province = filtered_province[0].long_name;
						}
			        })
			        this.isLoadingGetAddress = false;
			        this.barangay = '';
					this.region = '';
					this.longitude = parseFloat(this.longitude).toFixed(7);
				    this.latitude = parseFloat(this.latitude).toFixed(7);
			        this.validate();
			    } catch (error) {
			    	this.isLoadingGetAddress = false;
			    	this.isErrorAddress = true;
			    	this.latitude = null;
			    	this.longitude = null;
			    	this.province = null;
			    	this.municipality = null;
			    	this.unknownAddress = '<span style="color:red">Cannot find the address.</span>';
			    }
            }
        }
    });
</script>
@endsection

