@extends('layouts.app')

@section('title')
	Barangay Calendar Events
@endsection

@section('css')
<!-- internal styles -->
<!-- Awesomplete -->
{{Html::style('awesomplete/awesomplete.css')}}
<style>
	.panel-heading{
		background-color: #1b3c5b!important;
		color: white !important;
	}
</style>
@endsection

@section('body')
<div id="calendar_events" class="barangay top20">
	   <div class="panel panel-default" >
			<div class="panel-heading" style="color: black;  ">
				<h3 class="panel-title"><b>CALENDAR OF EVENTS</b></h3>
			</div>
			<div class="panel-body" style="height: 65vh;">
			    <div class="container1">
			    	@if(Session::has('flash_message'))
						<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
					@endif
			    	<div class="col-sm-3" style="padding-top:8px;padding-bottom: 8px">
			    		@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<a class="btn btn-primary" data-toggle="modal" href='#addevents' data-backdrop="static" style="height: :35px">Add Events</a>
						@include('barangay.modal.calendar_event_add')
						@endif
			    	</div>
			    	<div class="col-sm-9 text-right">
				    {{ Form::open(['route'=>'barangayprofile.calendar_events','method'=>'get', 'class'=>'navbar-form']) }}  
				        <div class="form-group" >
				            <input type="text" class="form-control" placeholder="Search" name="search">
				            <span class="form-group-btn">
				            	<button type="submit" class="btn btn-default">Search</button>
				            </span>
				        </div>
				        	<a href="{{route('barangayprofile.calendar_events')}}" class="btn btn-primary">View All</a>
				    {{ Form::close() }}
					</div>
					<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
						<tr style="border: 1px solid black;">
							<th >Event Name</th>
							<th>When</th>
							<th>Where</th>
							<th>Type</th>
							<th>Status</th>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					 		<th>Actions</th>
					 		@endif
						</tr>
						@forelse($barangay_calendars as $bc)
						<tr style="border: 1px solid black; @if($bc->deleted_at != null) background-color: #f87272; @endif">
							<td class="text-left">{{$bc->name}}</td>
							<td>{{$bc->when}}</td>
							<td class="text-left">{{$bc->where}}</td>
							<td class="text-left">{{$bc->type}}</td>
							<td>
								@if($bc->deleted_at == null)
									Active
								@else
									Deleted
								@endif
							</td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
									@if($bc->deleted_at == null)
										<li>
											<a data-toggle="modal" href='#editevents{{$bc->id}}' data-backdrop="static" title="Edit" @click="editData('{{$bc->name}}','{{$bc->when}}','{{$bc->where}}','{{$bc->type}}')"><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
										</li>
										<li>
											<a href="{{route('barangayprofile.calendar_events.delete',['id'=>$bc->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
										</li>
									@else
										<li>
											<a href="{{route('barangayprofile.calendar_events.restore',['id'=>$bc->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
										</li>
									@endif
									</ul>
								</div>
								@include('barangay.modal.calendar_event_edit')
							</td>
							@endif
						</tr>
						@empty
							<tr><td colspan="@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary') 6 @else 5 @endif"><p style="text-center">No Available Calendar Events</p></td></tr>
						@endforelse
					</table>
				</div>
			</div>
		</div>
		<div class="text-right">{{$barangay_calendars->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}
		</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
{{Html::script('awesomplete/awesomplete.min.js')}}
<script>
    var app = new Vue({
        el: '#calendar_events',
       	delimiters: ["[[","]]"],
        data: {
        	name_add: '',
            when_add: '',
            where_add: '',
            type_add: '',
            name_edit: '',
            when_edit: '',
            where_edit: '',
            type_edit: '',
            enableAdd: false,
            enableEdit: false,

        },
        methods: {
        	validateAdd(){
        		if(this.name_add != '' && this.when_add != '' && this.where_add != '' && this.type_add != ''){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.name_edit != '' && this.when_edit != '' && this.where_edit != '' && this.type_edit != ''){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	editData(name,when,where,type){
        		this.name_edit = name;
	            this.when_edit = when;
	            this.where_edit = where;
	            this.type_edit = type;
	            this.validateEdit();
        	}
        }
    });
	var selectTypeEvent0 = new Awesomplete('input.selectTypeEvent0', {
		replace: function(text) {
			var before = this.input.value.match(/^.+,\s*|/)[0];
			this.input.value = before + text;
            app.type_add = this.input.value;
            app.validateAdd();
		},
        minChars: 0,
        sort: false
	});
	Awesomplete.$('input.selectTypeEvent0').addEventListener("click", function() {
		if (selectTypeEvent0.ul.childNodes.length === 0) {
			selectTypeEvent0.minChars = 0;
			selectTypeEvent0.evaluate();
		}
		else if (selectTypeEvent0.ul.hasAttribute('hidden')) {
			selectTypeEvent0.open();
		}
		else {
			selectTypeEvent0.close();
		}
	});
	@foreach($barangay_calendars as $bc)
		var selectTypeEvent = new Awesomplete('input.selectTypeEvent{{$bc->id}}', {
			replace: function(text) {
				var before = this.input.value.match(/^.+,\s*|/)[0];
				this.input.value = before + text;
	            app.type_edit = this.input.value;
	            app.validateEdit();
			},
	        minChars: 0,
	        sort: false
		});
		Awesomplete.$('input.selectTypeEvent{{$bc->id}}').addEventListener("click", function() {
			if (selectTypeEvent.ul.childNodes.length === 0) {
				selectTypeEvent.minChars = 0;
				selectTypeEvent.evaluate();
			}
			else if (selectTypeEvent.ul.hasAttribute('hidden')) {
				selectTypeEvent.open();
			}
			else {
				selectTypeEvent.close();
			}
		});
	@endforeach
</script>
@endsection