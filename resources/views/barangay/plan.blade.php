@extends('layouts.app')

@section('title')
	Barangay Plan
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div id="plans" class="barangay top20">
	<div class="panel panel-default" >
		<div class="panel-heading" style="background-color: #1b3c5b; color: white;  ">
			<h3 class="panel-title">PLANS</h3>		
		</div>
		<div class="panel-body" style="height: 65vh;">
		    <div class="container1">
		    @if(Session::has('flash_message'))
				<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
			@endif
		    	<div class="col-sm-3" style="padding-top:8px;padding-bottom: 8px">
		    		@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					<a class="btn btn-primary" data-toggle="modal" href='#addplans' data-backdrop="static" style="height:35px">Add Plan</a>
					@include('barangay.modal.plans_add')
					@endif
		    	</div>
		    	<div class="col-sm-9 text-right">
			    {{ Form::open(['route'=>'barangayprofile.plans','method'=>'get', 'class'=>'navbar-form']) }}  
			        <div class="form-group" >
			            <input type="text" class="form-control" placeholder="Search" name="search">
			            <span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
			            <a href="{{route('barangayprofile.plans')}}" class="btn btn-primary">View All</a>
			        </div>
			    {{ Form::close() }}
				</div>
				<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th>Name</th>
						<th>Plan Short Details</th>
						<th>Type</th>
						<th>Date</th>
						<th>Status</th>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
				 		<th>Actions</th> 
				 		@endif	
					</tr>
					@forelse($barangay_plans as $bp)
					<tr style="border: 1px solid black; @if($bp->deleted_at != null) background-color: #f87272; @endif">
						<td  class="text-left">{{$bp->name}}</td>
						<td class="text-left">{{$bp->detail}}</td>
						<td class="text-left">{{$bp->type}}</td>
						<td>{{$bp->date}}</td>
						<td>
							@if($bp->deleted_at == null)
								Active
							@else
								Deleted
							@endif
						</td>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<td>
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
								@if($bp->deleted_at == null)
									<li>
										<a href="{{asset('files/plans/'.$bp->file)}}" download><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;&nbsp;Download File</span></a>
									</li>
									<li>
										<a data-toggle="modal" href='#editplans{{$bp->id}}' data-backdrop="static" title="Edit" @click="editData('{{$bp->name}}','{{$bp->date}}','{{$bp->detail}}','{{$bp->type}}','{{$bp->updated_at}}','{{$bp->last_name}},{{$bp->first_name}} {{$bp->middle_initial}}')"><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
									</li>
									<li>
										<a href="{{route('barangayprofile.plans.delete',['id'=>$bp->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
									</li>
								@else
									<li>
										<a href="{{route('barangayprofile.plans.restore',['id'=>$bp->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
									</li>
								@endif
								</ul>
							</div>
							@include('barangay.modal.plans_edit')		
						</td>
						@endif
					</tr>
					@empty
						<tr><td colspan="@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary') 6 @else 5 @endif"><p style="text-center">No Available Plans</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="text-right">{{$barangay_plans->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
<script>
    var app = new Vue({
        el: '#plans',
       	delimiters: ["[[","]]"],
        data: {
        	file_add: '',
        	name_add: '',
            date_add: '',
            detail_add: '',
            type_add: '',
            name_edit: '',
            date_edit: '',
            detail_edit: '',
            type_edit: '',
            user: '',
            updated_at:'',
            enableAdd: false,
            enableEdit: false,

        },
        methods: {
        	validateAdd(){
        		console.log(this.file_add);
        		if(this.name_add != '' && this.date_add != '' && this.detail_add != '' && this.type_add != '' && this.file_add != ''){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.name_edit != '' && this.date_edit != '' && this.detail_edit != '' && this.type_edit != ''){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	editData(name,date,detail,type,updated_at,user){
        		this.name_edit = name;
	            this.date_edit = date;
	            this.detail_edit = detail;
	            this.type_edit = type;
	            this.updated_at = updated_at;
	            this.user = user;
	            this.validateEdit();
        	},
        	onFileChange(e) {
            	this.file_add = '';
      			var files = e.target.files || e.dataTransfer.files;
  				var file = files[0];
  				console.log(file["size"]);
				if (!files.length)
        			return;
      			this.file_add = files[0]["name"];
      			this.validateAdd();
    		},
        }
    });
</script>
@endsection