@extends('layouts.app')

@section('title')
	Barangay Achievements
@endsection

@section('css')
<!-- internal styles -->
<!-- Awesomplete -->
{{Html::style('awesomplete/awesomplete.css')}}
@endsection

@section('body')
<div id="achievements" class="barangay top20" >
	<div class="panel panel-default" >
			<div class="panel-heading" style="background-color: #1b3c5b; color: white;  ">
				<h3 class="panel-title">ACHIEVEMENTS</h3>		
			</div>
			<div class="panel-body" style="height: 65vh;">
			    <div class="container1">
			    @if(Session::has('flash_message'))
					<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
				@endif
			    	<div class="col-sm-3" style="padding-top:8px;padding-bottom: 8px">
			    		@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<a class="btn btn-primary" data-toggle="modal" href='#addachievements' data-backdrop="static" style="height:35px">Add Achievement</a>
						@include('barangay.modal.achievement_add')
						@endif
			    	</div>
			    	<div class="col-sm-9 text-right">
				    {{ Form::open(['route'=>'barangayprofile.achievement','method'=>'get', 'class'=>'navbar-form']) }}  
				        <div class="form-group" >
				            <input type="text" class="form-control" placeholder="Search" name="search">
				            <span class="form-group-btn">
				            	<button type="submit" class="btn btn-default">Search</button>
				            </span>
				        </div>
				        <a href="{{route('barangayprofile.achievement')}}" class="btn btn-primary">View All</a>
				    {{ Form::close() }}
					</div>
					<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
						<tr style="border: 1px solid black;">
							<th>Name</th>
							<th>Date</th>
							<th>Details</th>
							<th>Other Details</th>
							<th>Type</th>
							<th>Status</th>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					 		<th>Actions</th> 	
					 		@endif
						</tr>
						@forelse($barangay_achievements as $ba)
						<tr style="border: 1px solid black; @if($ba->deleted_at != null) background-color: #f87272; @endif">
							<td  class="text-left">{{$ba->name}}</td>
							<td>{{$ba->date}}</td>
							<td class="text-left">{{$ba->detail}}</td>
							<td class="text-left">{{$ba->other_detail}}</td>
							<td class="text-left">{{$ba->type}}</td>
							<td>
								@if($ba->deleted_at == null)
									Active
								@else
									Deleted
								@endif
							</td>
							@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
									@if($ba->deleted_at == null)
										<li>
											{{-- <a data-toggle="modal" href='#editachievements{{$ba->id}}' data-backdrop="static" title="Edit" @click="editData('{{$ba->name}}','{{$ba->date}}','{{$ba->detail}}','{{$ba->other_detail}}','{{$ba->type}}')"><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a> --}}
											<a data-toggle="modal" href='#editachievements{{$ba->id}}' data-backdrop="static" title="Edit" @click="editData({{json_encode($ba)}})"><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
										</li>
										<li>
											<a href="{{route('barangayprofile.achievement.delete',['id'=>$ba->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
										</li>
									@else
										<li>
											<a href="{{route('barangayprofile.achievement.restore',['id'=>$ba->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
										</li>
									@endif
									</ul>
								</div>
								@include('barangay.modal.achievement_edit')		
							</td>
							@endif
						</tr>
						@empty
							<tr><td colspan="@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary') 7 @else 6 @endif"><p style="text-center">No Available Achievements</p></td></tr>
						@endforelse
					</table>
				</div>
			</div>
		</div>
	<div class="text-right">{{$barangay_achievements->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
{{Html::script('awesomplete/awesomplete.min.js')}}
<script>
    var app = new Vue({
        el: '#achievements',
       	delimiters: ["[[","]]"],
        data: {
        	image: '',
        	name_add: '',
            date_add: '',
            detail_add: '',
            other_detail_add: '',
            type_add: '',
            name_edit: '',
            date_edit: '',
            detail_edit: '',
            other_detail_edit: '',
            type_edit: '',
            errorImage: '',
            enableAdd: false,
            enableEdit: false,

        },
        methods: {
        	validateAdd(){
        		if(this.name_add != '' && this.date_add != '' && this.detail_add != '' && this.other_detail_add != '' && this.type_add != '' && this.errorImage == ''){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.name_edit != '' && this.date_edit != '' && this.detail_edit != '' && this.other_detail_edit != '' && this.type_edit != '' && this.errorImage == ''){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	editData(achievement){
        		this.name_edit = achievement.name;
	            this.date_edit = achievement.date;
	            this.detail_edit = achievement.detail;
	            this.other_detail_edit = achievement.other_detail;
	            this.type_edit = achievement.type;
	            this.validateEdit();
        	},
        	onFileChange(e) {
            	this.errorImage = '';
      			var files = e.target.files || e.dataTransfer.files;
  				var file = files[0];
				var fileType = file["type"];
				var ValidImageTypes = ["image/jpg", "image/jpeg", "image/png", "image/bmp"];
				if ($.inArray(fileType, ValidImageTypes) < 0) {
					this.errorImage = '<span style="color:red">Invalid Image format. Only Accepts(jpg,jpeg,png,bmp)</span>';
				} else {
					if (!files.length)
        				return;
      				this.createImage(files[0]);
				}
    		},
    		createImage(file) {
      			var image = new Image();
      			var reader = new FileReader();
      			var vm = this;

      			reader.onload = (e) => {
        			vm.image = e.target.result;
      			};
      			reader.readAsDataURL(file);
    		},
        }
    });
    var selectTypeAchievement0 = new Awesomplete('input.selectTypeAchievement0', {
		replace: function(text) {
			var before = this.input.value.match(/^.+,\s*|/)[0];
			this.input.value = before + text;
            app.type_add = this.input.value;
            app.validateAdd();
		},
        minChars: 0,
        sort: false
	});
	Awesomplete.$('input.selectTypeAchievement0').addEventListener("click", function() {
		if (selectTypeAchievement0.ul.childNodes.length === 0) {
			selectTypeAchievement0.minChars = 0;
			selectTypeAchievement0.evaluate();
		}
		else if (selectTypeAchievement0.ul.hasAttribute('hidden')) {
			selectTypeAchievement0.open();
		}
		else {
			selectTypeAchievement0.close();
		}
	});
	@foreach($barangay_achievements as $ba)
		var selectTypeAchievement = new Awesomplete('input.selectTypeAchievement{{$ba->id}}', {
			replace: function(text) {
				var before = this.input.value.match(/^.+,\s*|/)[0];
				this.input.value = before + text;
	            app.type_edit = this.input.value;
	            app.validateEdit();
			},
	        minChars: 0,
	        sort: false
		});
		Awesomplete.$('input.selectTypeAchievement{{$ba->id}}').addEventListener("click", function() {
			if (selectTypeAchievement.ul.childNodes.length === 0) {
				selectTypeAchievement.minChars = 0;
				selectTypeAchievement.evaluate();
			}
			else if (selectTypeAchievement.ul.hasAttribute('hidden')) {
				selectTypeAchievement.open();
			}
			else {
				selectTypeAchievement.close();
			}
		});
	@endforeach
</script>
@endsection