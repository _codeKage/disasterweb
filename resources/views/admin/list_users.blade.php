@extends('layouts.admin')

@section('title')
	Barangays
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div class="row">
	<div class="col-md-12">
		<h3>Barangays</h3> 
	</div>
	<div class="col-lg-12">
		@if(Session::has('flash_message'))
			<div class="alert alert-success">{{Session::get('flash_message')}}</div>
		@endif

		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
	</div>
	<div class="col-md-12">
		<a class="btn btn-primary" href='/admin/barangay/create' style="height:35px">Add New BRGY</a>
	</div>
	<div class="col-md-12">
		<table class="table">
			<tr>
				<td>ID</td>
				<td>Barangay Name</td>
				<td></td>
			</tr>
			@foreach($brgy as $x)
			<tr>
				<td>{{ $x->id }}</td>
				<td>{{ $x->barangay }}</td>
				<td>
					<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
						<span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li>
								<a href="/admin/barangay/{{$x->id}}/add_chairman" title="View Member"><i class="fa fa-plus margin-right"></i>&nbsp;&nbsp;Add Chairman Account</a>
								
							</li>
							<li>
								<a href="" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete Barangay</a>
							</li>
						</ul>
					</div>

				</td>
			</tr>
			@endforeach
		</table>
	</div>
	<div class="col-md-4">
	</div>
	<div class="col-md-4">
		{{$brgy->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}
	</div>
	<div class="col-md-4">
	</div>

</div>


@endsection

@section('js')
<!-- internal scripts -->

@endsection