@extends('layouts.admin')

@section('title')
	Chairman
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div class="row">
	<div class="col-md-12">
		&nbsp;
	</div>
	<div class="col-lg-12">
		@if(Session::has('flash_message'))
			<div class="alert alert-success">{{Session::get('flash_message')}}</div>
		@endif

		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
		@if($errors->has())
		   @foreach ($errors->all() as $error)
		      <div>{{ $error }}</div>
		  @endforeach
		@endif
	</div>
	<div class="col-md-12">
		{{ Form::open(array('url' => '/admin/barangay/'.$id.'/store_chairman', 'method' => 'store')) }}
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label>Username</label>
					<input type="hidden" name="brgy_id" value="{{$id}}"/>
					{{ Form::text('username','',array('class'=>'form-control span6','placeholder' => 'Username')) }}
					<span class="errors" style="color:#FF0000">{{$errors->first('username')}}</span>
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label>Password</label>
					<input type="password" class="form-control" name="password" placeholder="password">
					<span class="errors" style="color:#FF0000">{{$errors->first('password')}}</span>
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label>First Name</label>					
					{{ Form::text('first_name','',array('class'=>'form-control span6','placeholder' => 'First Name')) }}
					<span class="errors" style="color:#FF0000">{{$errors->first('first_name')}}</span>
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label>Middle Initial</label>
					{{ Form::text('middle_name','',array('class'=>'form-control span6','placeholder' => 'Middle Initial','maxlength'=>'1')) }}
					<span class="errors" style="color:#FF0000">{{$errors->first('middle_name')}}</span>
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>		
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label>last Name</label>
					{{ Form::text('last_name','',array('class'=>'form-control span6','placeholder' => 'Last Name')) }}					
					<span class="errors" style="color:#FF0000">{{$errors->first('last_name')}}</span>
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>				
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label>Email</label>
					{{ Form::email('email','',array('class'=>'form-control span6','placeholder' => 'Email')) }}		
					<span class="errors" style="color:#FF0000">{{$errors->first('email')}}</span>
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<input type="submit" class="btn btn-primary">
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>
		{{ form::close() }}
	</div>
	
</div>


@endsection

@section('js')
<!-- internal scripts -->

@endsection