@extends('layouts.admin')

@section('title')
	Barangays
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div class="row">
	<div class="col-md-12">
		&nbsp;
	</div>
	<div class="col-lg-12">
		@if(Session::has('flash_message'))
			<div class="alert alert-success">{{Session::get('flash_message')}}</div>
		@endif

		@if(Session::has('flash_error'))
			<div class="alert alert-danger">{{Session::get('flash_error')}}</div>
		@endif
	</div>
	<div class="col-md-12">
		{{ Form::open(array('url' => '/admin/barangay/store', 'method' => 'store')) }}
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label>Barangay Name</label>
					<input type="text" class="form-control" name="brgy_name" placeholder="Barangay Name">
					<span class="errors" style="color:#FF0000">{{$errors->first('brgy_name')}}</span>
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label>Municipality</label>
					<input type="text" class="form-control" name="municipality" placeholder="Municipality">
					<span class="errors" style="color:#FF0000">{{$errors->first('municipality')}}</span>
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label>Province</label>
					<input type="text" class="form-control" name="province" placeholder="Province">
					<span class="errors" style="color:#FF0000">{{$errors->first('province')}}</span>
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<label>Region</label>
					<input type="text" class="form-control" name="region" placeholder="Region">
					<span class="errors" style="color:#FF0000">{{$errors->first('region')}}</span>
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="form-group">
					<input type="submit" class="btn btn-primary">
				 </div>
			</div>
			<div class="col-lg-4">				
			</div>
			<div class="col-lg-4">
			</div>
		</div>
		{{ form::close() }}
	</div>
	
</div>


@endsection

@section('js')
<!-- internal scripts -->

@endsection