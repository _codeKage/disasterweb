<div class="modal fade" id="idleTimerModal" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Expiring Session</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="progress">
							<div class="progress-bar progress-bar-danger progress-bar-striped active" aria-valuenow="100" role="progressbar" id="progressBar" aria-valuemin="0" aria-valuemax="100" style="width:100%">
						    	<center><div id="timer" class="text-center" style="font-size:1.1em"></div></center>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						You have been idle for too long your session will expire and you will be logout. Do you want to continue browsing?
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="{{route('logout')}}" class="btn btn-default">Logout</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<a href="{{route('barangayprofile.about')}}" class="btn btn-primary">Continue</a>
			</div>
		</div>
	</div>
</div>

<script>
	function modalExpire(){
		var counterExpire=0;
		var timeleftExpire = 301;
		var timer = document.getElementById('timer');
		var progress = document.getElementById('progressBar');
		setInterval(timerExpires,1000);

		function convertSeconds(s){
                var min = Math.floor( (s / 60)%60 );
                var sec = s % 60;
                if(min > 1 && sec > 1){
                	return min + " minutes and " + sec + " seconds remaining.";
                }
                else if(min > 1 && sec <= 1){
                	return min + " minutes and " + sec + " second remaining.";
                }
                else if(min <=1 && sec > 1){
                	return min + " minute and " + sec + " seconds remaining.";
                }
                else{
                	return min + " minute and " + sec + " second remaining.";
                }
            }

		function timerExpires(){
		    if(counterExpire<timeleftExpire){
		        counterExpire++;
		        timer.innerHTML=convertSeconds(timeleftExpire-counterExpire);
		    }
		}
	}

</script>