<!-- jQuery -->
{{Html::script('js/jquery.js')}}

<!-- Bootstrap Core JavaScript -->
{{Html::script('js/bootstrap.min.js')}}

<!-- Morris Charts JavaScript -->
{{Html::script('js/plugins/morris/raphael.min.js')}}
{{Html::script('js/plugins/morris/morris.min.js')}}

{{Html::script('js/moment.js')}}
{{Html::script('js/bootstrap/bootstrap-datetimepicker.js')}}

<!-- SweetAlert -->
{{Html::script('sweetAlert/js/sweetalert.min.js')}}

@include('sweet::alert')

<script type="text/javascript">
	$('.confirmation').on('click', function () {
		return confirm('Are you sure?');
	});
</script>
<script type="text/javascript">
	var d = new Date();  
	$(function () {
		$('#dtp1').datetimepicker({
			format:"YYYY-MM-DD",
			defaultDate: d,
			// maxDate:d,
		});
	});

	$(function () {
		$('#dtp2').datetimepicker({
			format:"YYYY-MM-DD",
			// maxDate:d,
		});
	});

</script>

@if(Session::has('print_now'))
<script>		
$('#myModal').modal('show');

</script>
@endif

<script type="text/javascript">
	$('.navbar1-toggle').on('click', function() {
    	$('.side-nav').toggleClass("side-nav-collapsed");
   	 	$('.container-fluid').toggleClass("page-wrapper-toggle");
	});
</script>

<script>
	$(document).ready(function() {
		window.onload = function () {
			activateDateTime();
		    idleTimer();
		};
		// window.onload = idleTimer;
		// window.onload = activateDateTime;

		function idleTimer(){
			var counter=0;
			var timeleft = {{$countdownIdle}};
			setInterval(timerIt,1000);

			function timerIt(){
			    if(counter<timeleft){
			        counter++;
			    }

			   	if(timeleft-counter == 311){
			   		modalExpire();
			   	}

			    if((timeleft-counter) == 310){
			        $('#idleTimerModal').modal('show');
			    }

			    if((timeleft-counter) == 10){
			        document.location.href = '{!! route('logout') !!}';
			    }
			}
		}

		function activateDateTime(){
			var counter=0;
			var timeleft = {{$countdownIdle}};
			setInterval(activeDateTime,1000);

			function activeDateTime(){
				var dt = new Date();
				document.getElementById("activedatetime").innerHTML = dt.toLocaleString();
			}
		}

	});
</script>

@yield('js')