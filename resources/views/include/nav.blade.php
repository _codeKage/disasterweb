<style>
	.activeNav{
		background-color: gray;
		color: white !important;
	}
</style>
{{-- Navigation --}}
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	{{-- Brand and toggle get grouped for better mobile display --}}
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/">Barangay {{Session::get('barangayAbout')->barangay}}</a>
	</div>
	{{-- Top Menu Items
	START MAILLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL --}}
	<div class="collapse navbar-collapse" style="background-image: url("{{ asset('images/bg.png') }}") !important;">
		<ul class="nav navbar-left top-nav ">
			<li class="dropdown">
				<a class="navbar1-toggle" ><i class="fa fa-fw fa-bars "></i> </a>
			</li>
		</ul>
		<span class="navbar-brand" style="margin-right: 15px;color: white">Disaster Get-Ready Application</span>
		<ul class="nav navbar-right top-nav ">
			<span style="color: white; margin-right: 10px">Welcome, {{Session::get('logUser')->username}}</span>
			<span id="activedatetime" style="margin-right: 15px;color: white"></span>
			<img src="{{asset('images/logo/'.Session::get('barangayAbout')->logo)}}" width="50" height="50" style="margin-right: 20px;">
		</ul>
	</div>
	{{-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens
	START SIDE BARRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR --}}
	<div class="collapse navbar-collapse navbar-ex1-collapse navbar-header1">
		<ul class="nav navbar-nav side-nav">
			@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
			<li>
				<a class="{{ (strpos(\Request::route()->getName(),'barangayprofile') !== false) ? 'activeNav' : '' }}" href="javascript:;" data-toggle="collapse" data-target="#barangayprofile"> Barangay Profile <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="barangayprofile" class="collapse {{ (strpos(\Request::route()->getName(),'barangayprofile') !== false) ? 'in' : '' }}">
					<li>
						<a class="{{(\Request::route()->getName() == 'barangayprofile.about') ? 'activeNav' : ''}}" href="{{route('barangayprofile.about')}}">About Barangay</a>
					</li>
					<li>
						<a class="{{(\Request::route()->getName() == 'barangayprofile.calendar_events') ? 'activeNav' : ''}}" href="{{route('barangayprofile.calendar_events')}}">Calendar of Events</a>
					</li>
					<!--Temporarily Removed Barangay Achievements and Plans  -->
					<!-- <li>
						<a class="{{(\Request::route()->getName() == 'barangayprofile.achievement') ? 'activeNav' : ''}}" href="{{route('barangayprofile.achievement')}}">Achievements</a>
					</li>
					<li>
						<a class="{{(\Request::route()->getName() == 'barangayprofile.plans') ? 'activeNav' : ''}}" href="{{route('barangayprofile.plans')}}">Plans</a>
					</li>	 -->
				</ul>
			</li>
			@endif
				@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
			<li>
				<a class="{{ (strpos(\Request::route()->getName(),'residentprofile') !== false) ? 'activeNav' : '' }}" href="javascript:;" data-toggle="collapse" data-target="#residentprofile"> Resident Profile <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="residentprofile" class="collapse {{ (strpos(\Request::route()->getName(),'residentprofile') !== false) ? 'in' : '' }}">
					<li>
						<a class="{{(\Request::route()->getName() == 'residentprofile.household') ? 'activeNav' : ''}}" href="{{route('residentprofile.household')}}">Household</a>
					</li>
					<li>
						<a class="{{(\Request::route()->getName() == 'residentprofile.search_resident') ? 'activeNav' : ''}}" href="{{route('residentprofile.household.member')}}">Household Members</a>
					</li>

				</ul>
			</li>
				@endif
				@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
			<li>
				<a class="{{ (strpos(\Request::route()->getName(),'disasteradmin') !== false) ? 'activeNav' : '' }}" href="javascript:;" data-toggle="collapse" data-target="#disasteradmin"> Disaster Admin <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="disasteradmin" class="collapse {{ (strpos(\Request::route()->getName(),'disasteradmin') !== false) ? 'in' : '' }}">
					<li>
						<a class="{{(\Request::route()->getName() == 'disasteradmin.advisory') ? 'activeNav' : ''}}" href="{{route('disasteradmin.advisory')}}">Advisory</a>
					</li>
					<li>
						<a class="{{(\Request::route()->getName() == 'disasteradmin.safety_tips') ? 'activeNav' : ''}}" href="{{route('disasteradmin.safety_tips')}}">Safety Tips</a>
					</li>
					<!--Temporarily Removed Make A Plan  -->
					<!-- <li>
						<a class="{{(\Request::route()->getName() == 'disasteradmin.make_plan') ? 'activeNav' : ''}}" href="{{route('disasteradmin.make_plan')}}">Make a Plan</a>
					</li>	 -->
					<li>
						<a class="{{(\Request::route()->getName() == 'disasteradmin.preparedness_kit') ? 'activeNav' : ''}}" href="{{route('disasteradmin.preparedness_kit')}}">Preparedness Kit</a>
					</li>
					<li>
						<a class="{{(\Request::route()->getName() == 'disasteradmin.bdrrmc_team') ? 'activeNav' : ''}}" href="{{route('disasteradmin.bdrrmc_team')}}">BDRRMC Team</a>
					</li>
					<li>
						<a class="{{(\Request::route()->getName() == 'disasteradmin.bdrrmc_team_member') ? 'activeNav' : ''}}" href="{{route('disasteradmin.bdrrmc_team_member')}}">BDRRMC Team Members</a>
					</li>
					<li>
						<a class="{{(\Request::route()->getName() == 'disasteradmin.evacuation_center') ? 'activeNav' : ''}}" href="{{route('disasteradmin.evacuation_center')}}">Evacuation Center</a>
					</li>
				</ul>
			</li>
				@endif
			<!-- Changed Resources into Financial and Temporarily removed Supplies and Equipment  -->
				@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Treasurer')
			<li>
				<a class="{{ (strpos(\Request::route()->getName(),'financial') !== false) ? 'activeNav' : '' }}" href="javascript:;" data-toggle="collapse" data-target="#financial">Financial <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="financial" class="collapse {{ (strpos(\Request::route()->getName(),'financial') !== false) ? 'in' : '' }}">

					<!-- <li>
						<a class="{{(\Request::route()->getName() == 'resources.supplies') ? 'activeNav' : ''}}" href="{{route('resources.supplies')}}">Supplies</a>
					</li>
					<li>
						<a class="{{(\Request::route()->getName() == 'resources.equipments') ? 'activeNav' : ''}}" href="{{route('resources.equipments')}}">Equipment</a>
					</li> -->
					<li>
						<a class="{{(\Request::route()->getName() == 'financial.budget') ? 'activeNav' : ''}}" href="{{route('financial.budget')}}">Budget</a>
					</li>
					{{-- <li>
						<a class="{{(\Request::route()->getName() == 'financial.expenses') ? 'activeNav' : ''}}" href="{{route('financial.expenses')}}">Expenses</a>
					</li>	 --}}
				</ul>
			</li>
				@endif
			@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Teacher')
			<li>
				<a class="{{ (strpos(\Request::route()->getName(),'education') !== false) ? 'activeNav' : '' }}" href="{{route('education.school')}}" data-target="#education"> Education</a>
			</li>
			@endif
			@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary' || Session::get('logUser')->usertype == 'Treasurer')
			<li>
				<a class="{{ (strpos(\Request::route()->getName(),'reports') !== false) ? 'activeNav' : '' }}" href="javascript:;" data-toggle="collapse" data-target="#reports"> Reports <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="reports" class="collapse {{ (strpos(\Request::route()->getName(),'reports') !== false) ? 'in' : '' }}">
					<!-- <li>
						<a class="{{(\Request::route()->getName() == 'reports.household_purok') ? 'activeNav' : ''}}" href="{{route('reports.household_purok')}}">Household by Purok</a>
					</li> -->
					<!-- <li>
						<a class="{{(\Request::route()->getName() == 'reports.household_member_status') ? 'activeNav' : ''}}" href="{{route('reports.household_member_status')}}">Household by Member Status</a>
					</li>	 -->
					<!-- <li>
						<a class="{{(\Request::route()->getName() == 'reports.resident_purok') ? 'activeNav' : ''}}" href="{{route('reports.resident_purok')}}">Resident By Purok</a>
					</li>	 -->
					<!-- <li>
						<a class="{{(\Request::route()->getName() == 'reports.household_nuclear_family') ? 'activeNav' : ''}}" href="{{route('reports.household_nuclear_family')}}">Household by Nuclear Family</a>
					</li> -->
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					<li>
						<a class="{{(\Request::route()->getName() == 'reports.barangay_profile') ? 'activeNav' : ''}}" href="{{route('reports.barangay_profile')}}">Barangay Profile</a>
					</li>
						@endif
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					<li>
						<a class="{{(\Request::route()->getName() == 'education.school.report') ? 'activeNav' : ''}}" href="{{route('education.school.report')}}">Slots per Grade</a>
					</li>
							<li>
						<a class="{{(\Request::route()->getName() == 'reports.livelihood') ? 'activeNav' : ''}}" href="{{route('reports.livelihood')}}">Livelihood</a>
					</li>

						@endif
					<!-- <li>
						<a class="{{(\Request::route()->getName() == 'reports.supplies_equipment') ? 'activeNav' : ''}}" href="{{route('reports.supplies_equipment')}}">Supplies/Equipment</a>
					</li> -->
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary' || Session::get('logUser')->usertype == 'Treasurer')
					<li>
						<a class="{{(\Request::route()->getName() == 'reports.financial_reports') ? 'activeNav' : ''}}" href="{{route('reports.financial_reports')}}">Financial Reports</a>
					</li>
							@endif
				</ul>
			</li>
			@endif
			@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
			<li>
				<a class="{{ (strpos(\Request::route()->getName(),'forecast') !== false) ? 'activeNav' : '' }}" href="javascript:;" data-toggle="collapse" data-target="#forecast"> Forecast <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="forecast" class="collapse {{ (strpos(\Request::route()->getName(),'forecast') !== false) ? 'in' : '' }}">

					<li>
						<a style="color: #999;" href="{{route('forecast.riskarea')}}">Risk Area</a>
					</li>
					<li>
						<a style="color: #999;" href="{{route('forecast.budget')}}">Disaster Fund</a>
					</li>
					<li>
						<a style="color: #999;" href="{{route('forecast.relief')}}">Relief</a>
					</li>


					<li>
						<a style="color: #999;" href="{{route('forecast.elementary')}}">Elementary</a>
					</li>

					<li>
						<a style="color: #999;" href="{{route('forecast.hs')}}">High School</a>
					</li>

					<li>
						<a style="color: #999;" href="{{route('forecast.shs')}}">Senior High School</a>
					</li>

					<li>
						<a style="color: #999;" href="{{route('forecast.evacuation-forecast')}}">Evacuation Centers</a>
					</li>
				</ul>
			</li>
			@endif
			
			@if(Session::get('logUser')->usertype == 'Chairman')
			<li>
				<a class="{{ (strpos(\Request::route()->getName(),'systemtool') !== false) ? 'activeNav' : '' }}" href="javascript:;" data-toggle="collapse" data-target="#systemtool"> System Tools <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="systemtool" class="collapse {{ (strpos(\Request::route()->getName(),'systemtool') !== false) ? 'in' : '' }}">
					<li>
						<a class="{{(\Request::route()->getName() == 'systemtool.change_password') ? 'activeNav' : ''}}" href="{{route('systemtool.change_password')}}">Change Password</a>
					</li>
					@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					<li>
						<a class="{{(\Request::route()->getName() == 'systemtool.system_user') ? 'activeNav' : ''}}" href="{{route('systemtool.system_user')}}">System User</a>
					</li>
					<!--Temporarily Removed Acivity Logs  -->
					<!-- <li>
						<a class="{{(\Request::route()->getName() == 'systemtool.user_log') ? 'activeNav' : ''}}" href="{{route('systemtool.user_log')}}">Activity Log</a>
					</li> -->
					{{--<li>--}}
						{{--<a class="{{(\Request::route()->getName() == 'systemtool.request_change_password') ? 'activeNav' : ''}}" href="{{route('systemtool.request_change_password')}}">Request Change Password</a>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<a class="{{(\Request::route()->getName() == 'systemtool.user_temp_password') ? 'activeNav' : ''}}" href="{{route('systemtool.user_temp_password')}}">User w/ Temp. Password</a>--}}
					{{--</li>--}}
					{{-- <li>
						<a class="{{(\Request::route()->getName() == 'systemtool.database_backup.index') ? 'activeNav' : ''}}" href="{{route('systemtool.database_backup.index')}}">Database Backup</a>
					</li> --}}
					@endif
				</ul>
			</li>
				@endif
			<li>
				<a href="{{route('logout')}}">Logout</a>
			</li>
		</ul>
	</div>
	{{-- /.navbar-collapse
	END SIDE BARRRRRRRRRRRRRRRRRRRRRRRRR --}}
</nav>