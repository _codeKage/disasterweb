<!-- Bootstrap Core CSS -->
{{Html::style('css/bootstrap.min.css')}}

<!-- Custom CSS -->
{{Html::style('css/sb-admin.css')}}

<!-- Morris Charts CSS -->
{{Html::style('css/plugins/morris.css')}}

<!-- Custom Fonts -->
{{Html::style('font-awesome/css/font-awesome.min.css')}}

{{Html::style('css/bootstrap-datetimepicker.css')}}
{{Html::style('css/datetimepicker.css')}}

{{Html::style('css/angular-material.min.css')}}

<!-- Global Custom CSS -->
{{Html::style('css/custom.css')}}

<!-- SweetAlert -->
{{Html::style('sweetAlert/css/sweetalert.css')}}


@yield('css')

{{-- {{Html::script('js/ui-bootstrap.js')}} --}}