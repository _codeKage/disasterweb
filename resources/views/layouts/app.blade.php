<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">

    <title>@yield('title')</title>
    @include('include.css')
</head>
<body>
    <div id="wrapper" style="margin-left: 20px;"> 
        @include('include.nav')
        @include('include.idle_timer')
        <div id="page-wrapper" class="mnwidth">
    		<div class="container-fluid" style="margin-top: 60px;">
    			@yield('body')
    		</div>
    	</div>
    </div>
    @include('include.js')
</body>
</html>
