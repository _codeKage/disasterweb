<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">

	<title>@yield('title')</title>
	@include('include.css')
</head>
<body>
	<div id="wrapper" style="margin-left: 20px;"> 
	   <style>
	.activeNav{
		background-color: gray;
		color: white !important;
	}
</style>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">Barangay List</a>
	</div>

	<div class="collapse navbar-collapse">
		<ul class="nav navbar-left top-nav ">
			<li class="dropdown">
				<a class="navbar1-toggle" ><i class="fa fa-fw fa-bars "></i> </a>
			</li>
		</ul>
		<span class="navbar-brand" style="margin-right: 15px;color: white">Disaster Get-Ready Application</span>
		<ul class="nav navbar-right top-nav ">
			<span style="color:white; margin-right: 10px;">Welcome, Admin</span>
			<span id="activedatetime" style="margin-right: 15px;color: white"></span>
			<img src="" width="50" height="50" style="margin-right: 20px;">        
		</ul>
	</div>

	<div class="collapse navbar-collapse navbar-ex1-collapse navbar-header1">
		<ul class="nav navbar-nav side-nav">
			<li>
				<a href="{{route('admin.brgy')}}">Barangay</a>
			</li>
			<li>
				<a href="{{route('admin.logout')}}">Logout</a>
			</li>
		</ul>
	</div>

</nav>
		@include('include.idle_timer')
		<div id="page-wrapper" class="mnwidth">
			<div class="container-fluid" style="margin-top: 60px;">
				@yield('body')
			</div>
		</div>
	</div>
	@include('include.js')
</body>
</html>
