<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Disaster Web | Login</title>
    <!-- Bootstrap 3.3.7 -->
    {{Html::style('css/bootstrap.min.css')}}
    {{Html::style('css/login.css')}}

    <style type="text/css">
        body
        {
            background-image: url("{{ asset('images/bg.png') }}") !important;
            background-position: center;
            background-size: 77% 97%;
            background-color: #333;
            background-repeat: no-repeat;
            min-height: 100vh;
            background-attachment: fixed;
            font-family: 'Roboto', sans-serif;
        }
    </style>

</head>

  <body class="login-img3-body">
    <div class="col-lg-4 col-sm-4">
    </div>
    <div class="col-lg-4 col-sm-4 login" style="margin-left: auto;margin-right: auto;background-color:transparent;">
        {!! Form::open(['route'=>'login.attempt','class'=>'login-form']) !!}
        <div class="row" style="margin-top: 30%;">
            <div class="col-lg-1 col-sm-1">
            </div>
            <div class="col-lg-10 col-sm-10" style="background-color:whitesmoke;padding: 20px 30px 20px 30px;margin-top: 33%;opacity: 0.9">

             @if(Session::has('error'))
                <div class="alert alert-danger text-center" role="alert" style="margin-top: 10px;">
                    <span class="sr-only">Error:</span> &nbsp;{{Session::get('error')}}
                </div>
            @endif
                <div class="input">
                      <label for="username">Username</label>
                      <input type="text" name="username" id="username" style="height:40px; margin-top:30px">
                      <span class="spin"></span>
                  </div>

                  <div class="input">
                      <label for="password">Password</label>
                      <input type="password" name="password" id="password" style="height:40px; margin-top:30px">
                      <span class="spin"></span>
                  </div>
                  <div class="controls">
                      <input class="form-control span6 btn btn-primary" style="margin-top:25px"  type="submit" value="Login">
                  </div>
                  <p class="pull-right"></p>
            </div>
            <div class="col-lg-1 col-sm-1">

            </div>
        </div>
        {{ Form::close() }}
    <div class="col-lg-4 col-sm-4">
    </div>
    <!-- jQuery -->
    {{Html::script('js/jquery.js')}}
  </body>
  <script type="text/javascript">
    $(function() {

   $(".input input").focus(function() {

      $(this).parent(".input").each(function() {
         $("label", this).css({
            "line-height": "18px",
            "font-size": "18px",
            "font-weight": "100",
            "top": "0px"
         })
         $(".spin", this).css({
            "width": "100%"
         })
      });
   }).blur(function() {
      $(".spin").css({
         "width": "0px"
      })
      if ($(this).val() == "") {
         $(this).parent(".input").each(function() {
            $("label", this).css({
               "line-height": "60px",
               "font-size": "18px",
               "font-weight": "300",
               "top": "10px"
            })
         });

      }
   });

   $(".button").click(function(e) {
      var pX = e.pageX,
         pY = e.pageY,
         oX = parseInt($(this).offset().left),
         oY = parseInt($(this).offset().top);

      $(this).append('<span class="click-efect x-' + oX + ' y-' + oY + '" style="margin-left:' + (pX - oX) + 'px;margin-top:' + (pY - oY) + 'px;"></span>')
      $('.x-' + oX + '.y-' + oY + '').animate({
         "width": "500px",
         "height": "500px",
         "top": "-250px",
         "left": "-250px",

      }, 600);
      $("button", this).addClass('active');
   })

   $(".alt-2").click(function() {
      if (!$(this).hasClass('material-button')) {
         $(".shape").css({
            "width": "100%",
            "height": "100%",
            "transform": "rotate(0deg)"
         })

         setTimeout(function() {
            $(".overbox").css({
               "overflow": "initial"
            })
         }, 600)

         $(this).animate({
            "width": "140px",
            "height": "140px"
         }, 500, function() {
            $(".box").removeClass("back");

            $(this).removeClass('active')
         });

         $(".overbox .title").fadeOut(300);
         $(".overbox .input").fadeOut(300);
         $(".overbox .button").fadeOut(300);

         $(".alt-2").addClass('material-buton');
      }

   })

   $(".material-button").click(function() {

      if ($(this).hasClass('material-button')) {
         setTimeout(function() {
            $(".overbox").css({
               "overflow": "hidden"
            })
            $(".box").addClass("back");
         }, 200)
         $(this).addClass('active').animate({
            "width": "700px",
            "height": "700px"
         });

         setTimeout(function() {
            $(".shape").css({
               "width": "50%",
               "height": "50%",
               "transform": "rotate(45deg)"
            })

            $(".overbox .title").fadeIn(300);
            $(".overbox .input").fadeIn(300);
            $(".overbox .button").fadeIn(300);
         }, 700)

         $(this).removeClass('material-button');

      }

      if ($(".alt-2").hasClass('material-buton')) {
         $(".alt-2").removeClass('material-buton');
         $(".alt-2").addClass('material-button');
      }

   });

});
</script>
</html>
  
