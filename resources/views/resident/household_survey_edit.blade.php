@extends('layouts.app')

@section('title')
	Residents Profile Edit Survey
@endsection

@section('css')
<!-- internal styles -->
{{Html::style('css/bootstrap-nav-wizard.css')}}
<style>
	@media only screen and (min-width: 800px) {
	    .modal-dialog {
	        width:60%;
	    }
	}
</style>
<!-- Awesomplete -->
{{Html::style('css/bootstrap-nav-wizard.css')}}
@endsection

@section('body')
<div id="householdsurvey" class="barangay top20">

    {!! Form::open(['route'=>'residentprofile.household.survey.update']) !!}
	   <div class="panel panel-default" >
            <div class="panel-heading" style="background-color: #1b3c5b; color: white;  ">
                <h3 class="panel-title">Update Survey of household number {{$resident_household->house_identification_number}} </h3>     
            </div>
            <div class="panel-body" style="height: 65vh;">
                <input type="hidden" name="resident_household_id" value="{{$resident_household->id}}">
        	    <div style="padding: 10px">
        	    	@if(Session::has('flash_message'))
        				<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
        			@endif
                    <ul class="nav nav-pills nav-wizard">
                        <li class="col-md-2 col-sm-12 active"><a data-toggle="pill" href="#surveya" style="font-size: 14px">A. Disaster Risk</a></li>
                        <li class="col-md-3 col-sm-12"><a data-toggle="pill" href="#surveyb" style="font-size: 14px">B. Awareness of Government Action</a></li>
                        <li class="col-md-3 col-sm-12"><a data-toggle="pill" href="#surveyc" style="font-size: 14px">C. Nearest Evacuation Center</a></li>
                        <li class="col-md-4 col-sm-12"><a data-toggle="pill" href="#surveyd" style="font-size: 14px">D. Disaster Prevention & Preparedeness</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="surveya" class="tab-pane fade in active">
                            @include('resident.survey_includes.survey_a')
                            <div class="row text-right">
                                <a class="btn btn-primary" style="margin-top: 10px;margin-bottom: 10px;margin-right: 10px" @click="btnNext()">Next</a>
                            </div>
                        </div>
                        <div id="surveyb" class="tab-pane fade">
                            @include('resident.survey_includes.survey_b')
                            <div class="row text-right">
                                <a class="btn btn-primary" style="margin-top: 10px;margin-bottom: 10px;margin-right: 10px" @click="btnPrev()">Prev</a>
                                <a class="btn btn-primary" style="margin-top: 10px;margin-bottom: 10px;margin-right: 10px" @click="btnNext()">Next</a>
                            </div>
                        </div>
                        <div id="surveyc" class="tab-pane fade">
                            @include('resident.survey_includes.survey_c')
                            <div class="row text-right">
                                <a class="btn btn-primary" style="margin-top: 10px;margin-bottom: 10px;margin-right: 10px" @click="btnPrev()">Prev</a>
                                <a class="btn btn-primary" style="margin-top: 10px;margin-bottom: 10px;margin-right: 10px" @click="btnNext()">Next</a>
                            </div>
                        </div>
                        <div id="surveyd" class="tab-pane fade">
                            @include('resident.survey_includes.survey_d')
                            <div class="row text-right">
                                <a class="btn btn-primary" style="margin-top: 10px;margin-bottom: 10px;margin-right: 10px" @click="btnPrev()">Prev</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	   </div>
        <div class="text-right" style="margin-top: 10px">
            <span v-if="enableEdit == true"  v-cloak>
                <button type="submit" class="btn btn-success">Update</button>
            </span>
            <span v-else v-cloak>
                <button type="submit" class="btn btn-success" disabled>Update</button>
            </span>
        </div>
    {!! Form::close() !!}
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
{{Html::script('js/axios.min.js')}}
<script>
    var app = new Vue({
        el: '#householdsurvey',
       	delimiters: ["[[","]]"],
        data: {
            a1: '{{$resident_household->survey_a->a1}}',
            a2_1: {{$resident_household->survey_a->a2_1}},
            a3_1: '{{$resident_household->survey_a->a3_1}}',
            a4_1: '{{$resident_household->survey_a->a4_1}}',
            a5_1_1: {{$resident_household->survey_a->a5_1_1}},
            a5_1_2: {{$resident_household->survey_a->a5_1_2}},
            a5_1_3: {{$resident_household->survey_a->a5_1_3}},
            a5_1_4: {{$resident_household->survey_a->a5_1_4}},
            a5_1_5: {{$resident_household->survey_a->a5_1_5}},
            a2_2: {{$resident_household->survey_a->a2_2}},
            a3_2: '{{$resident_household->survey_a->a3_2}}',
            a4_2: '{{$resident_household->survey_a->a4_2}}',
            a5_2_1: {{$resident_household->survey_a->a5_2_1}},
            a5_2_2: {{$resident_household->survey_a->a5_2_2}},
            a5_2_3: {{$resident_household->survey_a->a5_2_3}},
            a5_2_4: {{$resident_household->survey_a->a5_2_4}},
            a5_2_5: {{$resident_household->survey_a->a5_2_5}},
            a2_3: {{$resident_household->survey_a->a2_3}},
            a3_3: '{{$resident_household->survey_a->a3_3}}',
            a4_3: '{{$resident_household->survey_a->a4_3}}',
            a5_3_1: {{$resident_household->survey_a->a5_3_1}},
            a5_3_2: {{$resident_household->survey_a->a5_3_2}},
            a5_3_3: {{$resident_household->survey_a->a5_3_3}},
            a5_3_4: {{$resident_household->survey_a->a5_3_4}},
            a5_3_5: {{$resident_household->survey_a->a5_3_5}},
            a2_4: {{$resident_household->survey_a->a2_4}},
            a3_4: '{{$resident_household->survey_a->a3_4}}',
            a4_4: '{{$resident_household->survey_a->a4_4}}',
            a5_4_1: {{$resident_household->survey_a->a5_4_1}},
            a5_4_2: {{$resident_household->survey_a->a5_4_2}},
            a5_4_3: {{$resident_household->survey_a->a5_4_3}},
            a5_4_4: {{$resident_household->survey_a->a5_4_4}},
            a5_4_5: {{$resident_household->survey_a->a5_4_5}},
            a2_5: {{$resident_household->survey_a->a2_5}},
            a3_5: '{{$resident_household->survey_a->a3_5}}',
            a4_5: '{{$resident_household->survey_a->a4_5}}',
            a5_5_1: {{$resident_household->survey_a->a5_5_1}},
            a5_5_2: {{$resident_household->survey_a->a5_5_2}},
            a5_5_3: {{$resident_household->survey_a->a5_5_3}},
            a5_5_4: {{$resident_household->survey_a->a5_5_4}},
            a5_5_5: {{$resident_household->survey_a->a5_5_5}},
            a2_6: {{$resident_household->survey_a->a2_6}},
            a3_6: '{{$resident_household->survey_a->a3_6}}',
            a4_6: '{{$resident_household->survey_a->a4_6}}',
            a5_6_1: {{$resident_household->survey_a->a5_6_1}},
            a5_6_2: {{$resident_household->survey_a->a5_6_2}},
            a5_6_3: {{$resident_household->survey_a->a5_6_3}},
            a5_6_4: {{$resident_household->survey_a->a5_6_4}},
            a5_6_5: {{$resident_household->survey_a->a5_6_5}},
            a2_7: {{$resident_household->survey_a->a2_7}},
            a3_7: '{{$resident_household->survey_a->a3_7}}',
            a4_7: '{{$resident_household->survey_a->a4_7}}',
            a5_7_1: {{$resident_household->survey_a->a5_7_1}},
            a5_7_2: {{$resident_household->survey_a->a5_7_2}},
            a5_7_3: {{$resident_household->survey_a->a5_7_3}},
            a5_7_4: {{$resident_household->survey_a->a5_7_4}},
            a5_7_5: {{$resident_household->survey_a->a5_7_5}},
            a2_8: {{$resident_household->survey_a->a2_8}},
            a3_8: '{{$resident_household->survey_a->a3_8}}',
            a4_8: '{{$resident_household->survey_a->a4_8}}',
            a5_8_1: {{$resident_household->survey_a->a5_8_1}},
            a5_8_2: {{$resident_household->survey_a->a5_8_2}},
            a5_8_3: {{$resident_household->survey_a->a5_8_3}},
            a5_8_4: {{$resident_household->survey_a->a5_8_4}},
            a5_8_5: {{$resident_household->survey_a->a5_8_5}},
            a2_9: {{$resident_household->survey_a->a2_9}},
            a3_9: '{{$resident_household->survey_a->a3_9}}',
            a4_9: '{{$resident_household->survey_a->a4_9}}',
            a5_9_1: {{$resident_household->survey_a->a5_9_1}},
            a5_9_2: {{$resident_household->survey_a->a5_9_2}},
            a5_9_3: {{$resident_household->survey_a->a5_9_3}},
            a5_9_4: {{$resident_household->survey_a->a5_9_4}},
            a5_9_5: {{$resident_household->survey_a->a5_9_5}},
            a2_10: {{$resident_household->survey_a->a2_10}},
            a3_10: '{{$resident_household->survey_a->a3_10}}',
            a4_10: '{{$resident_household->survey_a->a4_10}}',
            a5_10_1: {{$resident_household->survey_a->a5_10_1}},
            a5_10_2: {{$resident_household->survey_a->a5_10_2}},
            a5_10_3: {{$resident_household->survey_a->a5_10_3}},
            a5_10_4: {{$resident_household->survey_a->a5_10_4}},
            a5_10_5: {{$resident_household->survey_a->a5_10_5}},
            a2_11: {{$resident_household->survey_a->a2_11}},
            a3_11: '{{$resident_household->survey_a->a3_11}}',
            a4_11: '{{$resident_household->survey_a->a4_11}}',
            a5_11_1: {{$resident_household->survey_a->a5_11_1}},
            a5_11_2: {{$resident_household->survey_a->a5_11_2}},
            a5_11_3: {{$resident_household->survey_a->a5_11_3}},
            a5_11_4: {{$resident_household->survey_a->a5_11_4}},
            a5_11_5: {{$resident_household->survey_a->a5_11_5}},
            a2_12: {{$resident_household->survey_a->a2_12}},
            a3_12: '{{$resident_household->survey_a->a3_12}}',
            a4_12: '{{$resident_household->survey_a->a4_12}}',
            a5_12_1: {{$resident_household->survey_a->a5_12_1}},
            a5_12_2: {{$resident_household->survey_a->a5_12_2}},
            a5_12_3: {{$resident_household->survey_a->a5_12_3}},
            a5_12_4: {{$resident_household->survey_a->a5_12_4}},
            a5_12_5: {{$resident_household->survey_a->a5_12_5}},
            a2_13: {{$resident_household->survey_a->a2_13}},
            a3_13: '{{$resident_household->survey_a->a3_13}}',
            a4_13: '{{$resident_household->survey_a->a4_13}}',
            a5_13_1: {{$resident_household->survey_a->a5_13_1}},
            a5_13_2: {{$resident_household->survey_a->a5_13_2}},
            a5_13_3: {{$resident_household->survey_a->a5_13_3}},
            a5_13_4: {{$resident_household->survey_a->a5_13_4}},
            a5_13_5: {{$resident_household->survey_a->a5_13_5}},
            b1: '{{$resident_household->survey_b->b1}}',
            b2_1: {{$resident_household->survey_b->b2_1}},
            b2_2: {{$resident_household->survey_b->b2_2}},
            b2_3: {{$resident_household->survey_b->b2_3}},
            b2_4: {{$resident_household->survey_b->b2_4}},
            b2_5: {{$resident_household->survey_b->b2_5}},
            b2_6: {{$resident_household->survey_b->b2_6}},
            b2_7: {{$resident_household->survey_b->b2_7}},
            b2_8: {{$resident_household->survey_b->b2_8}},
            b2_9: {{$resident_household->survey_b->b2_9}},
            b2_10: {{$resident_household->survey_b->b2_10}},
            b2_11: {{$resident_household->survey_b->b2_11}},
            b2_12: {{$resident_household->survey_b->b2_12}},
            b2_13: {{$resident_household->survey_b->b2_13}},
            b2_14: {{$resident_household->survey_b->b2_14}},
            b3_1: {{$resident_household->survey_b->b3_1}},
            b3_2: {{$resident_household->survey_b->b3_2}},
            b3_3: {{$resident_household->survey_b->b3_3}},
            b3_4: {{$resident_household->survey_b->b3_4}},
            b3_5: {{$resident_household->survey_b->b3_5}},
            b3_6: {{$resident_household->survey_b->b3_6}},
            b3_7: {{$resident_household->survey_b->b3_7}},
            b3_8: {{$resident_household->survey_b->b3_8}},
            b3_9: {{$resident_household->survey_b->b3_9}},
            b3_10: {{$resident_household->survey_b->b3_10}},
            b3_11: {{$resident_household->survey_b->b3_11}},
            b3_12: {{$resident_household->survey_b->b3_12}},
            b3_13: {{$resident_household->survey_b->b3_13}},
            b3_14: {{$resident_household->survey_b->b3_14}},
            b3_15: {{$resident_household->survey_b->b3_15}},
            b4: '{{$resident_household->survey_b->b4}}',
            c1: '{{$resident_household->survey_c->c1}}',
            c2: '{{$resident_household->survey_c->c2}}',
            c3: '{{$resident_household->survey_c->c3}}',
            c4_1: {{$resident_household->survey_c->c4_1}},
            c4_2: {{$resident_household->survey_c->c4_2}},
            c4_3: {{$resident_household->survey_c->c4_3}},
            c4_4: {{$resident_household->survey_c->c4_4}},
            c4_5: {{$resident_household->survey_c->c4_5}},
            c4_6: {{$resident_household->survey_c->c4_6}},
            c5: '{{$resident_household->survey_c->c5}}',
            c6: '{{$resident_household->survey_c->c6}}',
            d1: '{{$resident_household->survey_d->d1}}',
            d2: '{{$resident_household->survey_d->d2}}',
            d3: '{{$resident_household->survey_d->d3}}',
            d4: '{{$resident_household->survey_d->d4}}',
            d5: '{{$resident_household->survey_d->d5}}',
            d6: '{{$resident_household->survey_d->d6}}',
            d7: '{{$resident_household->survey_d->d7}}',
            d8: '{{$resident_household->survey_d->d8}}',
            d9: '{{$resident_household->survey_d->d9}}',
            d10_1: {{$resident_household->survey_d->d10_1}},
            d10_2: {{$resident_household->survey_d->d10_2}},
            d10_3: {{$resident_household->survey_d->d10_3}},
            d10_4: {{$resident_household->survey_d->d10_4}},
            d10_5: {{$resident_household->survey_d->d10_5}},
            d10_6: {{$resident_household->survey_d->d10_6}},
            d10_7: {{$resident_household->survey_d->d10_7}},
            d10_8: {{$resident_household->survey_d->d10_8}},
            d10_9: {{$resident_household->survey_d->d10_9}},
            d10_10: {{$resident_household->survey_d->d10_10}},
            d10_11: {{$resident_household->survey_d->d10_11}},
            d10_12: {{$resident_household->survey_d->d10_12}},
            d10_13: {{$resident_household->survey_d->d10_13}},
            d10_14: {{$resident_household->survey_d->d10_14}},
            enableEdit: false,
        },
        mounted(){
            this.validate();
        },
        methods: {
        	validate(){
                if(this.a1 != 'Yes'){
                    this.a2_1 = '';
                    this.a2_2 = '';
                    this.a2_3 = '';
                    this.a2_4 = '';
                    this.a2_5 = '';
                    this.a2_6 = '';
                    this.a2_7 = '';
                    this.a2_8 = '';
                    this.a2_9 = '';
                    this.a2_10 = '';
                    this.a2_11 = '';
                    this.a2_12 = '';
                    this.a2_13 = '';
                }
                if(this.a2_1 != 1){
                    this.a3_1 = '';
                    this.a4_1 = '';
                    this.a5_1_1 = '';
                    this.a5_1_2 = '';
                    this.a5_1_3 = '';
                    this.a5_1_4 = '';
                    this.a5_1_5 = '';
                }
                if(this.a2_2 != 1){
                    this.a3_2 = '';
                    this.a4_2 = '';
                    this.a5_2_1 = '';
                    this.a5_2_2 = '';
                    this.a5_2_3 = '';
                    this.a5_2_4 = '';
                    this.a5_2_5 = '';
                }
                if(this.a2_3 != 1){
                    this.a3_3 = '';
                    this.a4_3 = '';
                    this.a5_3_1 = '';
                    this.a5_3_2 = '';
                    this.a5_3_3 = '';
                    this.a5_3_4 = '';
                    this.a5_3_5 = '';
                }
                if(this.a2_4 != 1){
                    this.a3_4 = '';
                    this.a4_4 = '';
                    this.a5_4_1 = '';
                    this.a5_4_2 = '';
                    this.a5_4_3 = '';
                    this.a5_4_4 = '';
                    this.a5_4_5 = '';
                }
                if(this.a2_5 != 1){
                    this.a3_5 = '';
                    this.a4_5 = '';
                    this.a5_5_1 = '';
                    this.a5_5_2 = '';
                    this.a5_5_3 = '';
                    this.a5_5_4 = '';
                    this.a5_5_5 = '';
                }
                if(this.a2_6 != 1){
                    this.a3_6 = '';
                    this.a4_6 = '';
                    this.a5_6_1 = '';
                    this.a5_6_2 = '';
                    this.a5_6_3 = '';
                    this.a5_6_4 = '';
                    this.a5_6_5 = '';
                }
                if(this.a2_7 != 1){
                    this.a3_7 = '';
                    this.a4_7 = '';
                    this.a5_7_1 = '';
                    this.a5_7_2 = '';
                    this.a5_7_3 = '';
                    this.a5_7_4 = '';
                    this.a5_7_5 = '';
                }
                if(this.a2_8 != 1){
                    this.a3_8 = '';
                    this.a4_8 = '';
                    this.a5_8_1 = '';
                    this.a5_8_2 = '';
                    this.a5_8_3 = '';
                    this.a5_8_4 = '';
                    this.a5_8_5 = '';
                }
                if(this.a2_9 != 1){
                    this.a3_9 = '';
                    this.a4_9 = '';
                    this.a5_9_1 = '';
                    this.a5_9_2 = '';
                    this.a5_9_3 = '';
                    this.a5_9_4 = '';
                    this.a5_9_5 = '';
                }
                if(this.a2_10 != 1){
                    this.a3_10 = '';
                    this.a4_10 = '';
                    this.a5_10_1 = '';
                    this.a5_10_2 = '';
                    this.a5_10_3 = '';
                    this.a5_10_4 = '';
                    this.a5_10_5 = '';
                }
                if(this.a2_11 != 1){
                    this.a3_11 = '';
                    this.a4_11 = '';
                    this.a5_11_1 = '';
                    this.a5_11_2 = '';
                    this.a5_11_3 = '';
                    this.a5_11_4 = '';
                    this.a5_11_5 = '';
                }
                if(this.a2_12 != 1){
                    this.a3_12 = '';
                    this.a4_12 = '';
                    this.a5_12_1 = '';
                    this.a5_12_2 = '';
                    this.a5_12_3 = '';
                    this.a5_12_4 = '';
                    this.a5_12_5 = '';
                }
                if(this.a2_13 != 1){
                    this.a3_13 = '';
                    this.a4_13 = '';
                    this.a5_13_1 = '';
                    this.a5_13_2 = '';
                    this.a5_13_3 = '';
                    this.a5_13_4 = '';
                    this.a5_13_5 = '';
                }
                if(this.a4_1 != 'Yes'){
                    this.a5_1_1 = '';
                    this.a5_1_2 = '';
                    this.a5_1_3 = '';
                    this.a5_1_4 = '';
                    this.a5_1_5 = '';
                }
                if(this.a4_2 != 'Yes'){
                    this.a5_2_1 = '';
                    this.a5_2_2 = '';
                    this.a5_2_3 = '';
                    this.a5_2_4 = '';
                    this.a5_2_5 = '';
                }
                if(this.a4_3 != 'Yes'){
                    this.a5_3_1 = '';
                    this.a5_3_2 = '';
                    this.a5_3_3 = '';
                    this.a5_3_4 = '';
                    this.a5_3_5 = '';
                }
                if(this.a4_4 != 'Yes'){
                    this.a5_4_1 = '';
                    this.a5_4_2 = '';
                    this.a5_4_3 = '';
                    this.a5_4_4 = '';
                    this.a5_4_5 = '';
                }
                if(this.a4_5 != 'Yes'){
                    this.a5_5_1 = '';
                    this.a5_5_2 = '';
                    this.a5_5_3 = '';
                    this.a5_5_4 = '';
                    this.a5_5_5 = '';
                }
                if(this.a4_6 != 'Yes'){
                    this.a5_6_1 = '';
                    this.a5_6_2 = '';
                    this.a5_6_3 = '';
                    this.a5_6_4 = '';
                    this.a5_6_5 = '';
                }
                if(this.a4_7 != 'Yes'){
                    this.a5_7_1 = '';
                    this.a5_7_2 = '';
                    this.a5_7_3 = '';
                    this.a5_7_4 = '';
                    this.a5_7_5 = '';
                }
                if(this.a4_8 != 'Yes'){
                    this.a5_8_1 = '';
                    this.a5_8_2 = '';
                    this.a5_8_3 = '';
                    this.a5_8_4 = '';
                    this.a5_8_5 = '';
                }
                if(this.a4_9 != 'Yes'){
                    this.a5_9_1 = '';
                    this.a5_9_2 = '';
                    this.a5_9_3 = '';
                    this.a5_9_4 = '';
                    this.a5_9_5 = '';
                }
                if(this.a4_10 != 'Yes'){
                    this.a5_10_1 = '';
                    this.a5_10_2 = '';
                    this.a5_10_3 = '';
                    this.a5_10_4 = '';
                    this.a5_10_5 = '';
                }
                if(this.a4_11 != 'Yes'){
                    this.a5_11_1 = '';
                    this.a5_11_2 = '';
                    this.a5_11_3 = '';
                    this.a5_11_4 = '';
                    this.a5_11_5 = '';
                }
                if(this.a4_12 != 'Yes'){
                    this.a5_12_1 = '';
                    this.a5_12_2 = '';
                    this.a5_12_3 = '';
                    this.a5_12_4 = '';
                    this.a5_12_5 = '';
                }
                if(this.a4_13 != 'Yes'){
                    this.a5_13_1 = '';
                    this.a5_13_2 = '';
                    this.a5_13_3 = '';
                    this.a5_13_4 = '';
                    this.a5_13_5 = '';
                }
                if(this.b1 != 'Yes'){
                    this.b2_1 = '';
                    this.b2_2 = '';
                    this.b2_3 = '';
                    this.b2_4 = '';
                    this.b2_5 = '';
                    this.b2_6 = '';
                    this.b2_7 = '';
                    this.b2_8 = '';
                    this.b2_9 = '';
                    this.b2_10 = '';
                    this.b2_11 = '';
                    this.b2_12 = '';
                    this.b2_13 = '';
                    this.b2_14 = '';
                }
                if(this.c1 != 'No'){
                    this.c2 = '';
                    this.c3 = '';
                    this.c4_1 = '';
                    this.c4_2 = '';
                    this.c4_3 = '';
                    this.c4_4 = '';
                    this.c4_5 = '';
                    this.c4_6 = '';
                }
                if(this.c1 != 'Yes'){
                    this.c5 = '';
                    this.c6 = '';
                }
                if(this.d9 != 'Yes'){
                    this.d10_1 = '';
                    this.d10_2 = '';
                    this.d10_3 = '';
                    this.d10_4 = '';
                    this.d10_5 = '';
                    this.d10_6 = '';
                    this.d10_7 = '';
                    this.d10_8 = '';
                    this.d10_9 = '';
                    this.d10_10 = '';
                    this.d10_11 = '';
                    this.d10_12 = '';
                    this.d10_13 = '';
                    this.d10_14 = '';
                }
                if(this.a1 != '' && this.b1 != '' && this.b4 != '' && this.c1 != '' && this.d1 != '' && this.d2 != '' && this.d3 != '' && this.d4 != '' && this.d5 != '' && this.d6 != '' && this.d7 != '' && this.d8 != '' && this.d9 != ''){
                        if((this.a2_1 == 1 && (this.a3_1 == '' || this.a4_1 == '' || this.a5_1 == '')) || (this.a2_2 == 1 && (this.a3_2 == '' || this.a4_2 == '' || this.a5_2 == '')) || (this.a2_3 == 1 && (this.a3_3 == '' || this.a4_3 == '' || this.a5_3 == '')) || (this.a2_4 == 1 && (this.a3_4 == '' || this.a4_4 == '' || this.a5_4 == '')) || (this.a2_5 == 1 && (this.a3_5 == '' || this.a4_5 == '' || this.a5_5 == '')) || (this.a2_6 == 1 && (this.a3_6 == '' || this.a4_6 == '' || this.a5_6 == '')) || (this.a2_7 == 1 && (this.a3_7 == '' || this.a4_7 == '' || this.a5_7 == '')) || (this.a2_8 == 1 && (this.a3_8 == '' || this.a4_8 == '' || this.a5_8 == '')) || (this.a2_9 == 1 && (this.a3_9 == '' || this.a4_9 == '' || this.a5_9 == '')) || (this.a2_10 == 1 && (this.a3_10 == '' || this.a4_10 == '' || this.a5_10 == '')) || (this.a2_11 == 1 && (this.a3_11 == '' || this.a4_11 == '' || this.a5_11 == '')) || (this.a2_12 == 1 && (this.a3_12 == '' || this.a4_12 == '' || this.a5_12 == '')) || (this.a2_13 == 1 && (this.a3_13 == '' || this.a4_13 == '' || this.a5_13 == '')) || (this.c1 == 'No' && (this.c2 == '' || this.c3 == '') || (this.c1 == 'Yes' && (this.c5 == '' || this.c6 == '')))){
                            this.enableEdit = false;
                        } else {
                            this.enableEdit = true;
                        }
                } else {
                    this.enableEdit = false;
                }
            },
            btnNext(){
                $('.nav > .active').next('li').find('a').trigger('click');
            },
            btnPrev(){
                $('.nav > .active').prev('li').find('a').trigger('click');
            }
        }
    });
</script>
@endsection