<div class="modal fade" id="addhousehold">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'residentprofile.household.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Household</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<h4 style="padding: 15px"> I. DEMOGRAPHICS PROFILE</h4>
					<h4 style="padding-left: 25px;padding-right: 5px"> A. HOUSEHOLD LOCATION</h4>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-5" style="padding-left: 30px; font-size: 16px;">
							A.1 Province
						</div>
						<div class="col-md-7">
							<input type="text" name="province" v-model="province_add" class="form-control" readonly @keyup="validateAdd">
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-5" style="padding-left: 30px; font-size: 16px;">
							A.2 Municipality
						</div>
						<div class="col-md-7">
							<input type="text" name="municipality" class="form-control" v-model="municipality_add" readonly @keyup="validateAdd">
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-5" style="padding-left: 30px; font-size: 16px;">
							A.3 Barangay
						</div>
						<div class="col-md-7">
							<input type="text" name="barangay" v-model="barangay_add" class="form-control" readonly @keyup="validateAdd">
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-5" style="padding-left: 30px; font-size: 16px;">
							A.4 Purok/Sitio
						</div>
						<div class="col-md-7">
							<input type="text" name="purok" class="form-control" v-model="purok_add" @keyup="validateAdd">
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-5" style="padding-left: 30px; font-size: 16px;">
							A.5 Street
						</div>
						<div class="col-md-7">
							<input type="text" name="street" class="form-control" v-model="street_add" @keyup="validateAdd">
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-5" style="padding-left: 30px; font-size: 16px;">
							A.6 House Identification Number
						</div>
						<div class="col-md-7" :class="{ 'has-error': isHouseholdNumberNotUnique_add }">
							<input type="text" name="house_identification_number" class="form-control" v-model="house_identification_number_add" min="0" @keyup="checkHouseholdNumberAdd">
							<span v-html="isUnique_add"></span>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-5" style="padding-left: 30px; font-size: 16px;">
							A.7 Coordinates
						</div>
						<div class="col-md-7">
							<div class="col-md-12 row" style="margin-top: 10px;margin-bottom: 10px;margin:auto">
								<label>Get Coordinates by clicking on the map:</label>
	    						<div id="map_add" style="height: 300px"></div>
							</div>
							<div class="col-md-12 row" style="margin-top: 10px;margin-bottom: 10px;margin:auto">
								<span v-if="isLoadingGetCoordinates_add == false">
									<button type="button" class="btn btn-primary" @click='getCoordinatesAdd()' :disabled="street_add == ''">Get Coordinates Using The Street</button>
								</span>
								<span v-else v-cloak>
									<button class="btn btn-default" disabled><i class="fa fa-spinner fa-spin"></i> Please wait</button>
								</span>
							</div>
						</div>
						<div class="col-md-offset-1 col-md-4" style="margin-top: 5px; font-size: 16px">
							Latitude
						</div>
						<div class="form-group" :class="{ 'has-error': isErrorCoordinates_add }">
							<div class="col-md-7" style="margin-top: 5px">
								<input type="number" name="latitude" class="form-control" step="0.0000001" v-model.number="latitude_add" @keyup="validateAdd">
							</div>
							<div class="col-md-offset-1 col-md-4" style="margin-top: 5px; font-size: 16px">
								Longitude
							</div>
							<div class="col-md-7" style="margin-top: 5px">
								<input type="number" name="longitude" class="form-control" step="0.0000001" v-model.number="longitude_add" @keyup="validateAdd">
								<span v-html="unknownCoordinates_add"></span>
							</div>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-5" style="padding-left: 30px; font-size: 16px;">
							A.8 Name of Respondent
						</div>
						<div class="col-md-7">
							<input type="text" name="name_of_respondent" class="form-control" v-model="name_of_respondent_add" @keyup="validateAdd">
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px;margin-bottom: 10px">
						<div class="col-md-5" style="padding-left: 30px; font-size: 16px;">
							A.9 Household Contact Number add
						</div>
						<div class="col-md-7">
							<input type="number" name="contact" class="form-control" placeholder="Enter 63 + 10 digit mobile number" v-model.number="contact_add" @keyup="validateAdd">
						</div>
					</div>
					<h4 style="padding-left: 25px;padding-right: 5px;"> B. HOUSEHOLD CHARACTERISTICS</h4>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-9" style="padding-left: 30px; font-size: 16px">
							B.1 How many families are there in the household?
						</div>
						<div class="col-md-3">
							<input type="number" name="no_family" v-model.number="no_family_add" min="0"  step="1" class="form-control" @keyup="validateAdd">
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-9" style="padding-left: 30px; font-size: 16px">
							B.2 How many household members are there in the household?
						</div>
						<div class="col-md-3">
							<input type="number" name="no_members" min="1" v-model="no_members_add" step="1" class="form-control" @keyup="validateAdd">
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-9" style="padding-left: 30px; font-size: 16px">
							B.3 Is there any of the members of the household with disability?
						</div>
						<div class="col-md-3">
							<select name="disability" v-model="disability_add" class="form-control" @change="validateAdd">
								<option value="" selected disabled></option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-9" style="padding-left: 30px; font-size: 16px">
							B.4 <i>[If Yes in Question No B.3]</i> How many?
						</div>
						<div class="col-md-3">
							<input type="number" name="no_disability" v-model.number="no_disability_add" min="1" step="1" class="form-control" :disabled="disability_add != 'Yes'" @keyup="validateAdd">
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-9" style="padding-left: 30px; font-size: 16px">
							B.5 In what type of building does the household reside?
						</div>
						<div class="col-md-3">
							<select name="building_type" v-model="building_type_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled></option>
							<option value="Concrete">Concrete</option>
							<option value="Semi-Concrete">Semi-Concrete</option>
							<option value="Light Materials">Light Materials</option>
						</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-9" style="padding-left: 30px; font-size: 16px">
							B.6 What is the household’s main source of water supply?
						</div>
						<div class="col-md-3">
							<select name="water_supply" v-model="water_supply_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled></option>
							<option value="Own use faucet, community water system">Own use faucet, community water system</option>
							<option value="Shared faucet, community water system">Shared faucet, community water system</option>
							<option value="Own use tubed/piped deep well">Own use tubed/piped deep well</option>
							<option value="Shared tubed/piped deep well">Shared tubed/piped deep well</option>
							<option value="Tubed/piped shallow well">Tubed/piped shallow well</option>
							<option value="Dug well">Dug well</option>
							<option value="Protected spring">Protected spring</option>
							<option value="Peddler">Peddler</option>
							<option value="Bottle water">Bottle water</option>
							<option value="Lake, River, Rain, and Others">Lake, River, Rain, and Others</option>
						</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-9" style="padding-left: 30px; font-size: 16px">
							B.7 What type of toilet facility does the household have?
						</div>
						<div class="col-md-3">
							<select name="toilet" v-model="toilet_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled></option>
							<option value="Owned">Owned</option>
							<option value="Shared">Shared</option>
							<option value="Closed Pit">Closed Pit</option>
							<option value="Open Pit">Open Pit</option>
							<option value="Drop/Overhang">Drop/Overhang</option>
							<option value="Pail System">Pail System</option>
							<option value="No Toilet/Field/Bush">No Toilet/Field/Bush</option>
						</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-9" style="padding-left: 30px; font-size: 16px">
							B.8 Does the household have water filtration?
						</div>
						<div class="col-md-3">
							<select name="water_filtration" v-model="water_filtration_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled></option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-9" style="padding-left: 30px; font-size: 16px">
							B.9 What is the source of electricity in the housing unit?
						</div>
						<div class="col-md-3">
							<select name="electricity" v-model="electricity_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled></option>
							<option value="Electric Company">Electric Company</option>
							<option value="Generator">Generator</option>
							<option value="Solar">Solar</option>
							<option value="Battery">Battery</option>
							<option value="Others">Others</option>
							<option value="None">None</option>
						</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-9" style="padding-left: 30px; font-size: 16px">
							B.10 Is there a back-up power generator/battery?
						</div>
						<div class="col-md-3">
							<select name="power_generator" v-model="power_generator_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled></option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAdd == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>
