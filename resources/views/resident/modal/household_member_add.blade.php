﻿<div class="modal fade" id="addhouseholdmember{{$rh->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'residentprofile.household.member.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Household Member</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="resident_household_id" value="{{$rh->id}}">
				<input type="hidden" name="no_members" value="{{$rh->no_members}}">
				<input type="hidden" name="age" v-model="age">
				<div class="row">
					<div class="col-md-12 text-left">
						<div class="col-md-12" style="margin-top: 5px; font-size: 16px;">
							C.1 What is the name of the family member?
						</div>
						<div class="col-md-4" style="margin-top: 5px">
							<label>First Name</label>
							<input type="text" name="first_name" v-model="first_name" placeholder="First Name" class="form-control" @keyup="validateMember">
						</div>
						<div class="col-md-4" style="margin-top: 5px">
							<label>Middle Name</label>
							<input type="text" name="middle_name" v-model="middle_name" placeholder="Middle Name" class="form-control" @keyup="validateMember">
						</div>
						<div class="col-md-4" style="margin-top: 5px">
							<label>Last Name</label>
							<input type="text" name="last_name" v-model="last_name" placeholder="Last Name" class="form-control" @keyup="validateMember">
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px" style="font-size: 16px">
							C.2 In which family does this family member belong?
						</div>
						<div class="col-md-5">
							<select name="family_belong" v-model="family_belong" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								@for($i = 1; $i <= $rh->no_family; $i++)
								<option value="{{$i}}">{{$i}}</option>
								@endfor
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.3 What is the relation to the  household head?
						</div>
						<div class="col-md-5">
							<select name="relation" v-model="relation" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="Head">Head</option>
								<option value="Spouse">Spouse</option>
								<option value="Son/Daughter">Son/Daughter</option>
								<option value="Brother/Sister">Brother/Sister</option>
								<option value="Son/Daughter-in-law">Son/Daughter-in-law</option>
								<option value="Grandchildren">Grandchildren</option>
								<option value="Parents">Parents</option>
								<option value="Other Relative">Other Relative</option>
								<option value="Housemaid/Boy">Housemaid/Boy</option>
								<option value="Friends">Friends</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.4 What is his/her civil status?
						</div>
						<div class="col-md-5">
							<select name="civil_status" v-model="civil_status" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="Single">Single</option>
								<option value="Married">Married</option>
								<option value="Widowed">Widowed</option>
								<option value="Divorce/Seperated">Divorce/Seperated</option>
								<option value="Annulled">Annulled</option>
								<option value="Common-law/Live-in">Common-law/Live-in</option>
								<option value="Unknown">Unknown</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.5 Is the member male of female?
						</div>
						<div class="col-md-5">
							<select name="gender" v-model="gender" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.6 What is the birth date of the family member?
						</div>
						<div class="col-md-5">
							<input type="date" class="form-control" name="birthdate" v-model="birthdate" style="margin-bottom: 5px;width:100% !important" @change="getAge" max="{{date('Y-m-d')}}" />
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.7 Is he/she currently staying on the same address?
						</div>
						<div class="col-md-5">
							<select name="same_address" v-model="same_address" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.8 Is this family member attending school?
						</div>
						<div class="col-md-5">
							<select name="attending_school" v-model="attending_school" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.9 [If Yes in Question C.8]. What grade or year level is he/she presently attending?
						</div>
						<div class="col-md-5">
							<select name="year_level" v-model="year_level" class="form-control" @change="validateMember" :disabled="attending_school != 'Yes'">
								<option value="" selected disabled></option>
								<option value="No Grade Completed">No Grade Completed</option>
								<option value="Day Care">Day Care</option>
								<option value="Kindergarten/Preparatory">Kindergarten/Preparatory</option>
								<option value="Grade 1">Grade 1</option>
								<option value="Grade 2">Grade 2</option>
								<option value="Grade 3">Grade 3</option>
								<option value="Grade 4">Grade 4</option>
								<option value="Grade 5">Grade 5</option>
								<option value="Grade 6">Grade 6</option>
								<option value="Grade 7">Grade 7</option>
								<option value="Grade 8">Grade 8</option>
								<option value="Grade 9">Grade 9</option>
								<option value="Grade 10">Grade 10</option>
								<option value="Grade 11">Grade 11</option>
								<option value="Grade 12">Grade 12</option>
								<option value="1st Year PS/NT/TV">1st Year PS/NT/TV</option>
								<option value="2nd Year PS/NT/TV">2nd Year PS/NT/TV</option>
								<option value="3rd Year PS/NT/TV">3rd Year PS/NT/TV</option>
								<option value="1st Year College">1st Year College</option>
								<option value="2nd Year College">2nd Year College</option>
								<option value="3rd Year College">3rd Year College</option>
								<option value="4th Year College">4th Year College</option>
								<option value="5th Year College">5th Year College</option>
								<option value="6th Year College">6th Year College</option>
								<option value="Masters degree undergraduate">Masters degree undergraduate</option>
								<option value="Masters graduate">Masters graduate</option>
								<option value="Doctorate degree undergraduate">Doctorate degree undergraduate</option>
								<option value="Doctorate degree graduate">Doctorate degree graduate</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.10 What is the highest educational level completed?
						</div>
						<div class="col-md-5">
							<select name="highest_level" v-model="highest_level" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="No Grade Completed">No Grade Completed</option>
								<option value="Day Care">Day Care</option>
								<option value="Kindergarten/Preparatory">Kindergarten/Preparatory</option>
								<option value="Grade 1">Grade 1</option>
								<option value="Grade 2">Grade 2</option>
								<option value="Grade 3">Grade 3</option>
								<option value="Grade 4">Grade 4</option>
								<option value="Grade 5">Grade 5</option>
								<option value="Grade 6">Grade 6</option>
								<option value="Grade 7">Grade 7</option>
								<option value="Grade 8">Grade 8</option>
								<option value="Grade 9">Grade 9</option>
								<option value="Grade 10">Grade 10</option>
								<option value="Grade 11">Grade 11</option>
								<option value="Grade 12">Grade 12</option>
								<option value="1st Year PS/NT/TV">1st Year PS/NT/TV</option>
								<option value="2nd Year PS/NT/TV">2nd Year PS/NT/TV</option>
								<option value="3rd Year PS/NT/TV">3rd Year PS/NT/TV</option>
								<option value="1st Year College">1st Year College</option>
								<option value="2nd Year College">2nd Year College</option>
								<option value="3rd Year College">3rd Year College</option>
								<option value="4th Year College">4th Year College</option>
								<option value="5th Year College">5th Year College</option>
								<option value="6th Year College">6th Year College</option>
								<option value="Maste’s degree undergraduate">Masters degree undergraduate</option>
								<option value="Masters graduate">Masters graduate</option>
								<option value="Doctorate degree undergraduate">Doctorate degree undergraduate</option>
								<option value="Doctorate degree graduate">Doctorate degree graduate</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.11 If female, is she pregnant?
						</div>
						<div class="col-md-5">
							<select name="pregnant" v-model="pregnant" class="form-control" @change="validateMember" :disabled="gender != 'Female'">
								<option value="" selected disabled></option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.12 Is the family member have children?
						</div>
						<div class="col-md-5">
							<select name="have_children" v-model="have_children" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.13 Is solo parent?
						</div>
						<div class="col-md-5">
							<select name="solo_parent" v-model="solo_parent" class="form-control" @change="validateMember" :disabled="have_children != 'Yes'">
								<option value="" selected disabled></option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.14 Does the family member have a disability?
						</div>
						<div class="col-md-5">
							<select name="disability" v-model="disability" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.15 (IF YES IN Question C.14).  What type of disability?
						</div>
						<div class="col-md-5">
							<select name="disability_type" v-model="disability_type" class="form-control" @change="validateMember" :disabled="disability != 'Yes'">
								<option value="" selected disabled></option>
								<option value="Total Blindness">Total Blindness</option>
								<option value="Partial Blindness">Partial Blindness</option>
								<option value="Low Vision">Low Vision</option>
								<option value="Totally Deaf">Totally Deaf</option>
								<option value="Partially Deaf">Partially Deaf</option>
								<option value="Oral Defect">Oral Defect</option>
								<option value="One Hand">One Hand</option>
								<option value="No Hands">No Hands</option>
								<option value="One Leg">One Leg</option>
								<option value="No Legs">No Legs</option>
								<option value="Mild Cerebral Palsy">Mild Cerebral Palsy</option>
								<option value="Severe Cerebral Palsy">Severe Cerebral Palsy</option>
								<option value="Retarded, Mentally Ill">Retarded, Mentally Ill</option>
								<option value="Mental Retardation">Mental Retardation</option>
								<option value="Multiple Impairment">Multiple Impairment</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.16 Are there other health problems? Specify
						</div>
						<div class="col-md-5">
							<input class="form-control" name="health_problem" v-model="health_problem" @keyup="validateMember"/>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.17 Is the family member employed?
						</div>
						<div class="col-md-5">
							<select name="employed" v-model="employed" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="Yes">Yes</option>
								<option value="No">No</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.18 What is the occupation of the family member?
						</div>
						<div class="col-md-5">
							<select name="occupation" v-model="occupation" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="Assembler">Assembler</option>
								<option value="Associate Professional">Associate Professional</option>
								<option value="Clerk">Clerk</option>
								<option value="Corporate Executive">Corporate Executive</option>
								<option value="Farmer">Farmer</option>
								<option value="Fisherman">Fisherman</option>
								<option value="Forestry Worker">Forestry Worker</option>
								<option value="Laborer">Laborer</option>
								<option value="Manager">Manager</option>
								<option value="Managing Proprietor">Managing Proprietor</option>
								<option value="Official of the government and Special Interest Organization">Official of the government and Special Interest Organization</option>
								<option value="Plant and Machine Operator">Plant and Machine Operator</option>
								<option value="Professional">Professional</option>
								<option value="Service Worker">Service Worker</option>
								<option value="Special Occupation">Special Occupation</option>
								<option value="Shop and Market Sales Worker">Shop and Market Sales Worker</option>
								<option value="Supervisor">Supervisor</option>
								<option value="Technician">Technician</option>
								<option value="Trades and Related Worker">Trades and Related Worker</option>
								<option value="unskilled Worker">unskilled Worker</option>
								
							</select>
						</div>
					</div>
					<div class="col-md-12 text-left" style="margin-top: 5px">
						<div class="col-md-7" style="font-size: 16px">
							C.19 Where the family member work?
						</div>
						<div class="col-md-5">
							<select name="where_occupation" v-model="where_occupation" class="form-control" @change="validateMember">
								<option value="" selected disabled></option>
								<option value="Employer in Own Family-operated Farm/Business">Employer in Own Family-operated Farm or Business</option>
								<option value="Worked for Government/Non-Government Corporation">Worked for Government/Non-Government Corporation</option>
								<option value="Self-Employed Without Any Paid Employee">Self-Employed Without Any Paid Employee</option>
								<option value="Worked for Private Household">Worked for Private Household</option>
								<option value="Worked for Private Establishment">Worked for Private Establishment</option>
								<option value="Worked with Pay in Own Family-operated Farm/Business">Worked with Pay in Own Family-operated Farm or Business</option>
								<option value="Worked without Pay in Own Family-operated Farm/Business">Worked without Pay in Own Family-operated Farm or Business</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAddMember == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>