<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-8">
        C.1 Is your house secured enough and there is no need for you to evacuate during calamity?
    </div>
    <div class="col-md-4">
         <select name="c1" v-model="c1" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-8">
        C.2 [If No in Question No C.1] In the past 3 years, have you experience to evacuate because of calamity?
    </div>
    <div class="col-md-4">
        <input type="hidden" value="" name="c2">
         <select name="c2" v-model="c2" class="form-control" :disabled="c1 != 'No'" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-8">
        C.3 [If No in Question No C.1] Are you aware of a place where to evacuate?
    </div>
    <div class="col-md-4">
        <input type="hidden" value="" name="c3">
         <select name="c3" v-model="c3" class="form-control" :disabled="c1 != 'No'" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-8">
        C.4 [If No in Question No C.1] Where did you evacuate?
    </div>
    <div class="col-md-4">
        <div class="checkbox text-left">
            <label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
                <input type="hidden" name="c4_1" value="0">
                <input type="checkbox" name="c4_1" v-model="c4_1" value="1" :disabled="c1 != 'No'">
                School
            </label>
            <label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
                <input type="hidden" name="c4_2" value="0">
                <input type="checkbox" name="c4_2" v-model="c4_2" value="1" :disabled="c1 != 'No'">
                Church
            </label>
            <label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
                <input type="hidden" name="c4_3" value="0">
                <input type="checkbox" name="c4_3" v-model="c4_3" value="1" :disabled="c1 != 'No'">
                Covered court/gym
            </label>
            <label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
                <input type="hidden" name="c4_4" value="0">
                <input type="checkbox" name="c4_4" v-model="c4_4" value="1" :disabled="c1 != 'No'">
                Relative’s house
            </label>
            <label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
                <input type="hidden" name="c4_5" value="0">
                <input type="checkbox" name="c4_5" v-model="c4_5" value="1" :disabled="c1 != 'No'">
                Neighbor or friends house
            </label>
            <label style="margin-bottom: 5px;margin-left: 5px;margin-right: 5px">
                <input type="hidden" name="c4_6" value="0">
                <input type="checkbox" name="c4_6" v-model="c4_6" value="1" :disabled="c1 != 'No'">
                Others
            </label>
        </div>
        {{-- <input type="hidden" value="" name="c4">
         <select name="c4" v-model="c4" class="form-control" :disabled="c1 != 'No'" @change="validate">
            <option value="" selected disabled></option>
            <option value="Day Care">Day Care</option>
            <option value="Gym">Gym</option>
            <option value="School">School</option>
            <option value="Residential">Residential</option>
        </select> --}}
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-8">
        C.5 [If YES in Question No C.1] Are you willing to adopt an evacuee?
    </div>
    <div class="col-md-4">
        <input type="hidden" value="" name="c5">
         <select name="c5" v-model="c5" class="form-control" :disabled="c1 != 'Yes'" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-8">
        C.6 [If YES in Question No C.1] How many evacuees can you accommodate?
    </div>
    <div class="col-md-4">
        <input type="hidden" value="" name="c6">
         <input type="number" name="c6" v-model.number="c6" class="form-control" :disabled="c1 != 'Yes'" step="1" min="1" @keyup="validate">
    </div>
</div>