<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        B.1 Have you seen or heard information encouraging people in your community to be prepared for disaster situations in the past 12 months?
    </div>
    <div class="col-md-2">
         <select name="b1" v-model="b1" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-12">
        B.2 [If Yes in Question No. B.1] Where did you receive information, attended meeting or training regarding disaster prevention and preparedness?
    </div>
    <div class="col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11">
         <div class="checkbox">
            <input type="hidden" value="0" name="b2_1">
            <input type="checkbox" name="b2_1" v-model="b2_1" value="1" :disabled="b1 != 'Yes'">
            National Office
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_2">
            <input type="checkbox" name="b2_2" v-model="b2_2" value="1" :disabled="b1 != 'Yes'">
            Governor's Office
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_3">
            <input type="checkbox" name="b2_3" v-model="b2_3" value="1" :disabled="b1 != 'Yes'">
            Municipal or Mayor's Office
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_4">
            <input type="checkbox" name="b2_4" v-model="b2_4" value="1" :disabled="b1 != 'Yes'">
            Barangay Officials
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_5">
            <input type="checkbox" name="b2_5" v-model="b2_5" value="1" :disabled="b1 != 'Yes'">
            Local Law Enforcement
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_6">
            <input type="checkbox" name="b2_6" v-model="b2_6" value="1" :disabled="b1 != 'Yes'">
            Local Emergency Management Office
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_7">
            <input type="checkbox" name="b2_7" v-model="b2_7" value="1" :disabled="b1 != 'Yes'">
            School
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_8">
            <input type="checkbox" name="b2_8" v-model="b2_8" value="1" :disabled="b1 != 'Yes'">
            An online resources
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_9">
            <input type="checkbox" name="b2_9" v-model="b2_9" value="1" :disabled="b1 != 'Yes'">
            Private contractor/agency
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_10">
            <input type="checkbox" name="b2_10" v-model="b2_10" value="1" :disabled="b1 != 'Yes'">
            The military
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_11">
            <input type="checkbox" name="b2_11" v-model="b2_11" value="1" :disabled="b1 != 'Yes'">
            A non-profit organization
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_12">
            <input type="checkbox" name="b2_12" v-model="b2_12" value="1" :disabled="b1 != 'Yes'">
            Some other person or agency
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_13">
            <input type="checkbox" name="b2_13" v-model="b2_13" value="1" :disabled="b1 != 'Yes'">
            I don't remember
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b2_14">
            <input type="checkbox" name="b2_14" v-model="b2_14" value="1" :disabled="b1 != 'Yes'">
            None
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-12">
        B.3 What is your preferred way to receive information regarding disaster prevention and preparedness?
    </div>
    <div class="col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11">
         <div class="checkbox">
            <input type="hidden" value="0" name="b3_1">
            <input type="checkbox" name="b3_1" v-model="b3_1" value="1">
            Television
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_2">
            <input type="checkbox" name="b3_2" v-model="b3_2" value="1">
            Radio
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_3">
            <input type="checkbox" name="b3_3" v-model="b3_3" value="1">
            Internet/Webiste
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_4">
            <input type="checkbox" name="b3_4" v-model="b3_4" value="1">
            Fact sheet/brochure/pamphlet
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_5">
            <input type="checkbox" name="b3_5" v-model="b3_5" value="1">
            By speakerphone
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_6">
            <input type="checkbox" name="b3_6" v-model="b3_6" value="1">
            Phone Call
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_7">
            <input type="checkbox" name="b3_7" v-model="b3_7" value="1">
            Mail/E-mail
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_8">
            <input type="checkbox" name="b3_8" v-model="b3_8" value="1">
            Magazine
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_9">
            <input type="checkbox" name="b3_9" v-model="b3_9" value="1">
            Newspaper
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_10">
            <input type="checkbox" name="b3_10" v-model="b3_10" value="1">
            Facebook
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_11">
            <input type="checkbox" name="b3_11" v-model="b3_11" value="1">
            Twitter
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_12">
            <input type="checkbox" name="b3_12" v-model="b3_12" value="1">
            Relative/friend/neighbor
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_13">
            <input type="checkbox" name="b3_13" v-model="b3_13" value="1">
            From work
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_14">
            <input type="checkbox" name="b3_14" v-model="b3_14" value="1">
            SMS/text messages
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="b3_15">
            <input type="checkbox" name="b3_15" v-model="b3_15" value="1">
            Disaster Related Application Software
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        B.4 Have you or someone in your household attended meetings, received information or training regarding disaster preparedness in the past 12 months?
    </div>
    <div class="col-md-2">
         <select name="b4" v-model="b4" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>