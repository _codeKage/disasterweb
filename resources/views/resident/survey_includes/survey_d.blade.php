<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        D.1 Did the family carefully select the construction site of the residential building?
    </div>
    <div class="col-md-2">
         <select name="d1" v-model="d1" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        D.2 Did the family secure the house to its foundation?
    </div>
    <div class="col-md-2">
         <select name="d2" v-model="d2" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        D.3 Are typhoon guards installed on windows during disaster?
    </div>
    <div class="col-md-2">
         <select name="d3" v-model="d3" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        D.4 Is the house reinforced in preparation for disaster occurrence?
    </div>
    <div class="col-md-2">
         <select name="d4" v-model="d4" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        D.5 Does your family have a “Household/Family Emergency Plan” in case of calamity?
    </div>
    <div class="col-md-2">
         <select name="d5" v-model="d5" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        D.6 If family or loved ones get separated during disaster, does your family have a communication plan?
    </div>
    <div class="col-md-2">
         <select name="d6" v-model="d6" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
       D.7 Are you aware of warning and monitoring activity of the concerned agency regarding calamity?
    </div>
    <div class="col-md-2">
         <select name="d7" v-model="d7" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        D.8 Do you know the person or government agency to contact in case of calamity?
    </div>
    <div class="col-md-2">
         <select name="d8" v-model="d8" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        D.9 Do you have a preparedness kit?
    </div>
    <div class="col-md-2">
         <select name="d9" v-model="d9" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-12">
        D.10 [If YES in Question No D.9]. Which of the following do you have in your preparedness kit?
    </div>
    <div class="col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11">
         <div class="checkbox">
            <input type="hidden" value="0" name="d10_1">
            <input type="checkbox" name="d10_1" v-model="d10_1" value="1" :disabled="d9 != 'Yes'">
            Water
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_2">
            <input type="checkbox" name="d10_2" v-model="d10_2" value="1" :disabled="d9 != 'Yes'">
            Food(canned goods,biscuits, etc.
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_3">
            <input type="checkbox" name="d10_3" v-model="d10_3" value="1" :disabled="d9 != 'Yes'">
            Matches/Ligther
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_4">
            <input type="checkbox" name="d10_4" v-model="d10_4" value="1" :disabled="d9 != 'Yes'">
            Flashlight/Emergency Light
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_5">
            <input type="checkbox" name="d10_5" v-model="d10_5" value="1" :disabled="d9 != 'Yes'">
            Transistor Radio
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_6">
            <input type="checkbox" name="d10_6" v-model="d10_6" value="1" :disabled="d9 != 'Yes'">
            Candle
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_7">
            <input type="checkbox" name="d10_7" v-model="d10_7" value="1" :disabled="d9 != 'Yes'">
            Whistle
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_8">
            <input type="checkbox" name="d10_8" v-model="d10_8" value="1" :disabled="d9 != 'Yes'">
            Medical kit
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_9">
            <input type="checkbox" name="d10_9" v-model="d10_9" value="1" :disabled="d9 != 'Yes'">
            Extra prescription medication
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_10">
            <input type="checkbox" name="d10_10" v-model="d10_10" value="1" :disabled="d9 != 'Yes'">
            Clothes
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_11">
            <input type="checkbox" name="d10_11" v-model="d10_11" value="1" :disabled="d9 != 'Yes'">
            Blanket
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_12">
            <input type="checkbox" name="d10_12" v-model="d10_12" value="1" :disabled="d9 != 'Yes'">
            Important Documents
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_13">
            <input type="checkbox" name="d10_13" v-model="d10_13" value="1" :disabled="d9 != 'Yes'">
            Fuel
        </div>
        <div class="checkbox">
            <input type="hidden" value="0" name="d10_14">
            <input type="checkbox" name="d10_14" v-model="d10_14" value="1" :disabled="d9 != 'Yes'">
            Extra Cash-on-hand
        </div>
    </div>
</div>