<div class="col-md-12" style="margin-top: 10px;padding: 0">
    <div class="col-md-10">
        A. 1 Has the household experienced destructive calamity for the past 12 months?
    </div>
    <div class="col-md-2">
         <select name="a1" v-model="a1" class="form-control" @change="validate">
            <option value="" selected disabled></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;">
    <table class="table-striped table" style="border: 1px solid black; font-size: 12px">
        <tr style="border: 1px solid black;">
            <th>A.2  Destructive calamity/ies experienced?  </th>
            <th>A.3 Number of calamities have occurred for the past 12 months? </th>
            <th>A.4 Did the household receive any assistance?</th>
            <th>A.5 From where did the assistance come?</th>                               
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_1">
                    <input type="checkbox" name="a2_1" v-model="a2_1" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Typhoon
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_1" value="">
                <input type="number" name="a3_1" v-model.number="a3_1" class="form-control" :disabled="a1 != 'Yes' || a2_1 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_1" value="">
                <select name="a4_1" v-model="a4_1" class="form-control" :disabled="a1 != 'Yes' || a2_1 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_1_1" value="0">
                        <input type="checkbox" name="a5_1_1" v-model="a5_1_1" value="1" :disabled="a1 != 'Yes' || a2_1 != 1 || a4_1 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_1_2" value="0">
                        <input type="checkbox" name="a5_1_2" v-model="a5_1_2" value="1" :disabled="a1 != 'Yes' || a2_1 != 1 || a4_1 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_1_3" value="0">
                        <input type="checkbox" name="a5_1_3" v-model="a5_1_3" value="1" :disabled="a1 != 'Yes' || a2_1 != 1 || a4_1 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_1_4" value="0">
                        <input type="checkbox" name="a5_1_4" v-model="a5_1_4" value="1" :disabled="a1 != 'Yes' || a2_1 != 1 || a4_1 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_1_5" value="0">
                        <input type="checkbox" name="a5_1_5" v-model="a5_1_5" value="1" :disabled="a1 != 'Yes' || a2_1 != 1 || a4_1 != 'Yes'">
                        Others
                    </label>
                </div>
                {{-- <select name="a5_1" v-model="a5_1" class="form-control" :disabled="a1 != 'Yes' || a2_1 != 1 || a4_1 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_2">
                    <input type="checkbox" name="a2_2" v-model="a2_2" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Heavy Rains
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_2" value="">
                <input type="number" name="a3_2" v-model.number="a3_2" class="form-control" :disabled="a1 != 'Yes' || a2_2 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_2" value="">
                <select name="a4_2" v-model="a4_2" class="form-control" :disabled="a1 != 'Yes' || a2_2 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_2_1" value="0">
                        <input type="checkbox" name="a5_2_1" v-model="a5_2_1" value="1" :disabled="a1 != 'Yes' || a2_2 != 1 || a4_2 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_2_2" value="0">
                        <input type="checkbox" name="a5_2_2" v-model="a5_2_2" value="1" :disabled="a1 != 'Yes' || a2_2 != 1 || a4_2 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_2_3" value="0">
                        <input type="checkbox" name="a5_2_3" v-model="a5_2_3" value="1" :disabled="a1 != 'Yes' || a2_2 != 1 || a4_2 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_2_4" value="0">
                        <input type="checkbox" name="a5_2_4" v-model="a5_2_4" value="1" :disabled="a1 != 'Yes' || a2_2 != 1 || a4_2 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_2_5" value="0">
                        <input type="checkbox" name="a5_2_5" v-model="a5_2_5" value="1" :disabled="a1 != 'Yes' || a2_2 != 1 || a4_2 != 'Yes'">
                        Others
                    </label>
                </div>
                {{-- <select name="a5_2" v-model="a5_2" class="form-control" :disabled="a1 != 'Yes' || a2_2 != 1 || a4_2 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_3">
                    <input type="checkbox" name="a2_3" v-model="a2_3" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Drought
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_3" value="">
                <input type="number" name="a3_3" v-model.number="a3_3" class="form-control" :disabled="a1 != 'Yes' || a2_3 != 1" step="1" min="1" >
            </td>
            <td>
                <input type="hidden" name="a4_3" value="">
                <select name="a4_3" v-model="a4_3" class="form-control" :disabled="a1 != 'Yes' || a2_3 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_3_1" value="0">
                        <input type="checkbox" name="a5_3_1" v-model="a5_3_1" value="1" :disabled="a1 != 'Yes' || a2_3 != 1 || a4_3 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_3_2" value="0">
                        <input type="checkbox" name="a5_3_2" v-model="a5_3_2" value="1" :disabled="a1 != 'Yes' || a2_3 != 1 || a4_3 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_3_3" value="0">
                        <input type="checkbox" name="a5_3_3" v-model="a5_3_3" value="1" :disabled="a1 != 'Yes' || a2_3 != 1 || a4_3 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_3_4" value="0">
                        <input type="checkbox" name="a5_3_4" v-model="a5_3_4" value="1" :disabled="a1 != 'Yes' || a2_3 != 1 || a4_3 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_3_5" value="0">
                        <input type="checkbox" name="a5_3_5" v-model="a5_3_5" value="1" :disabled="a1 != 'Yes' || a2_3 != 1 || a4_3 != 'Yes'">
                        Others
                    </label>
                </div>
                {{-- <select name="a5_3" v-model="a5_3" class="form-control" :disabled="a1 != 'Yes' || a2_3 != 1 || a4_3 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_4">
                    <input type="checkbox" name="a2_4" v-model="a2_4" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Earthquake
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_4" value="">
                <input type="number" name="a3_4" v-model.number="a3_4" class="form-control" :disabled="a1 != 'Yes' || a2_4 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_4" value="">
                <select name="a4_4" v-model="a4_4" class="form-control" :disabled="a1 != 'Yes' || a2_4 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_4_1" value="0">
                        <input type="checkbox" name="a5_4_1" v-model="a5_4_1" value="1" :disabled="a1 != 'Yes' || a2_4 != 1 || a4_4 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_4_2" value="0">
                        <input type="checkbox" name="a5_4_2" v-model="a5_4_2" value="1" :disabled="a1 != 'Yes' || a2_4 != 1 || a4_4 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_4_3" value="0">
                        <input type="checkbox" name="a5_4_3" v-model="a5_4_3" value="1" :disabled="a1 != 'Yes' || a2_4 != 1 || a4_4 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_4_4" value="0">
                        <input type="checkbox" name="a5_4_4" v-model="a5_4_4" value="1" :disabled="a1 != 'Yes' || a2_4 != 1 || a4_4 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_4_5" value="0">
                        <input type="checkbox" name="a5_4_5" v-model="a5_4_5" value="1" :disabled="a1 != 'Yes' || a2_4 != 1 || a4_4 != 'Yes'">
                        Others
                    </label>
                </div>
                {{-- <select name="a5_4" v-model="a5_4" class="form-control" :disabled="a1 != 'Yes' || a2_4 != 1 || a4_4 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_5">
                    <input type="checkbox" name="a2_5" v-model="a2_5" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Volcanic Eruption
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_5" value="">
                <input type="number" name="a3_5" v-model.number="a3_5" class="form-control" :disabled="a1 != 'Yes' || a2_5 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_5" value="">
                <select name="a4_5" v-model="a4_5" class="form-control" :disabled="a1 != 'Yes' || a2_5 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_5_1" value="0">
                        <input type="checkbox" name="a5_5_1" v-model="a5_5_1" value="1" :disabled="a1 != 'Yes' || a2_5 != 1 || a4_5 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_5_2" value="0">
                        <input type="checkbox" name="a5_5_2" v-model="a5_5_2" value="1" :disabled="a1 != 'Yes' || a2_5 != 1 || a4_5 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_5_3" value="0">
                        <input type="checkbox" name="a5_5_3" v-model="a5_5_3" value="1" :disabled="a1 != 'Yes' || a2_5 != 1 || a4_5 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_5_4" value="0">
                        <input type="checkbox" name="a5_5_4" v-model="a5_5_4" value="1" :disabled="a1 != 'Yes' || a2_5 != 1 || a4_5 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_5_5" value="0">
                        <input type="checkbox" name="a5_5_5" v-model="a5_5_5" value="1" :disabled="a1 != 'Yes' || a2_5 != 1 || a4_5 != 'Yes'">
                        Others
                    </label>
                </div>
                {{-- <select name="a5_5" v-model="a5_5" class="form-control" :disabled="a1 != 'Yes' || a2_5 != 1 || a4_5 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_6">
                    <input type="checkbox" name="a2_6" v-model="a2_6" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Landslide/mudslide
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_6" value="">
                <input type="number" name="a3_6" v-model.number="a3_6" class="form-control" :disabled="a1 != 'Yes' || a2_6 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_6" value="">
                <select name="a4_6" v-model="a4_6" class="form-control" :disabled="a1 != 'Yes' || a2_6 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_6_1" value="0">
                        <input type="checkbox" name="a5_6_1" v-model="a5_6_1" value="1" :disabled="a1 != 'Yes' || a2_6 != 1 || a4_6 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_6_2" value="0">
                        <input type="checkbox" name="a5_6_2" v-model="a5_6_2" value="1" :disabled="a1 != 'Yes' || a2_6 != 1 || a4_6 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_6_3" value="0">
                        <input type="checkbox" name="a5_6_3" v-model="a5_6_3" value="1" :disabled="a1 != 'Yes' || a2_6 != 1 || a4_6 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_6_4" value="0">
                        <input type="checkbox" name="a5_6_4" v-model="a5_6_4" value="1" :disabled="a1 != 'Yes' || a2_6 != 1 || a4_6 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_6_5" value="0">
                        <input type="checkbox" name="a5_6_5" v-model="a5_6_5" value="1" :disabled="a1 != 'Yes' || a2_6 != 1 || a4_6 != 'Yes'">
                        Others
                    </label>
                </div>
               {{--  <select name="a5_6" v-model="a5_6" class="form-control" :disabled="a1 != 'Yes' || a2_6 != 1 || a4_6 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_7">
                    <input type="checkbox" name="a2_7" v-model="a2_7" value="1" :disabled="a1 != 'Yes'">
                    Tsunami
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_7" value="">
                <input type="number" name="a3_7" v-model.number="a3_7" class="form-control" :disabled="a1 != 'Yes' || a2_7 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_7" value="">
                <select name="a4_7" v-model="a4_7" class="form-control" :disabled="a1 != 'Yes' || a2_7 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_7_1" value="0">
                        <input type="checkbox" name="a5_7_1" v-model="a5_7_1" value="1" :disabled="a1 != 'Yes' || a2_7 != 1 || a4_7 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_7_2" value="0">
                        <input type="checkbox" name="a5_7_2" v-model="a5_7_2" value="1" :disabled="a1 != 'Yes' || a2_7 != 1 || a4_7 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_7_3" value="0">
                        <input type="checkbox" name="a5_7_3" v-model="a5_7_3" value="1" :disabled="a1 != 'Yes' || a2_7 != 1 || a4_7 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_7_4" value="0">
                        <input type="checkbox" name="a5_7_4" v-model="a5_7_4" value="1" :disabled="a1 != 'Yes' || a2_7 != 1 || a4_7 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_7_5" value="0">
                        <input type="checkbox" name="a5_7_5" v-model="a5_7_5" value="1" :disabled="a1 != 'Yes' || a2_7 != 1 || a4_7 != 'Yes'">
                        Others
                    </label>
                </div>
               {{--  <select name="a5_7" v-model="a5_7" class="form-control" :disabled="a1 != 'Yes' || a2_7 != 1 || a4_7 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_8">
                    <input type="checkbox" name="a2_8" v-model="a2_8" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Lightning Incidents
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_8" value="">
                <input type="number" name="a3_8" v-model.number="a3_8" class="form-control" :disabled="a1 != 'Yes' || a2_8 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_8" value="">
                <select name="a4_8" v-model="a4_8" class="form-control" :disabled="a1 != 'Yes' || a2_8 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_8_1" value="0">
                        <input type="checkbox" name="a5_8_1" v-model="a5_8_1" value="1" :disabled="a1 != 'Yes' || a2_8 != 1 || a4_8 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_8_2" value="0">
                        <input type="checkbox" name="a5_8_2" v-model="a5_8_2" value="1" :disabled="a1 != 'Yes' || a2_8 != 1 || a4_8 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_8_3" value="0">
                        <input type="checkbox" name="a5_8_3" v-model="a5_8_3" value="1" :disabled="a1 != 'Yes' || a2_8 != 1 || a4_8 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_8_4" value="0">
                        <input type="checkbox" name="a5_8_4" v-model="a5_8_4" value="1" :disabled="a1 != 'Yes' || a2_8 != 1 || a4_8 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_8_5" value="0">
                        <input type="checkbox" name="a5_8_5" v-model="a5_8_5" value="1" :disabled="a1 != 'Yes' || a2_8 != 1 || a4_8 != 'Yes'">
                        Others
                    </label>
                </div>
                {{-- <select name="a5_8" v-model="a5_8" class="form-control" :disabled="a1 != 'Yes' || a2_8 != 1 || a4_8 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_9">
                    <input type="checkbox" name="a2_9" v-model="a2_9" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Storm Surge
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_9" value="">
                <input type="number" name="a3_9" v-model.number="a3_9" class="form-control" :disabled="a1 != 'Yes' || a2_9 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_9" value="">
                <select name="a4_9" v-model="a4_9" class="form-control" :disabled="a1 != 'Yes' || a2_9 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_9_1" value="0">
                        <input type="checkbox" name="a5_9_1" v-model="a5_9_1" value="1" :disabled="a1 != 'Yes' || a2_9 != 1 || a4_9 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_9_2" value="0">
                        <input type="checkbox" name="a5_9_2" v-model="a5_9_2" value="1" :disabled="a1 != 'Yes' || a2_9 != 1 || a4_9 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_9_3" value="0">
                        <input type="checkbox" name="a5_9_3" v-model="a5_9_3" value="1" :disabled="a1 != 'Yes' || a2_9 != 1 || a4_9 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_9_4" value="0">
                        <input type="checkbox" name="a5_9_4" v-model="a5_9_4" value="1" :disabled="a1 != 'Yes' || a2_9 != 1 || a4_9 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_9_5" value="0">
                        <input type="checkbox" name="a5_9_5" v-model="a5_9_5" value="1" :disabled="a1 != 'Yes' || a2_9 != 1 || a4_9 != 'Yes'">
                        Others
                    </label>
                </div>
                {{-- <select name="a5_9" v-model="a5_9" class="form-control" :disabled="a1 != 'Yes' || a2_9 != 1 || a4_9 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_10">
                    <input type="checkbox" name="a2_10" v-model="a2_10" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Tornadoes/Whirlwinds
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_10" value="">
                <input type="number" name="a3_10" v-model.number="a3_10" class="form-control" :disabled="a1 != 'Yes' || a2_10 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_10" value="">
                <select name="a4_10" v-model="a4_10" class="form-control" :disabled="a1 != 'Yes' || a2_10 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_10_1" value="0">
                        <input type="checkbox" name="a5_10_1" v-model="a5_10_1" value="1" :disabled="a1 != 'Yes' || a2_10 != 1 || a4_10 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_10_2" value="0">
                        <input type="checkbox" name="a5_10_2" v-model="a5_10_2" value="1" :disabled="a1 != 'Yes' || a2_10 != 1 || a4_10 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_10_3" value="0">
                        <input type="checkbox" name="a5_10_3" v-model="a5_10_3" value="1" :disabled="a1 != 'Yes' || a2_10 != 1 || a4_10 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_10_4" value="0">
                        <input type="checkbox" name="a5_10_4" v-model="a5_10_4" value="1" :disabled="a1 != 'Yes' || a2_10 != 1 || a4_10 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_10_5" value="0">
                        <input type="checkbox" name="a5_10_5" v-model="a5_10_5" value="1" :disabled="a1 != 'Yes' || a2_10 != 1 || a4_10 != 'Yes'">
                        Others
                    </label>
                </div>
                {{-- <select name="a5_10" v-model="a5_10" class="form-control" :disabled="a1 != 'Yes' || a2_10 != 1 || a4_10 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_11">
                    <input type="checkbox" name="a2_11" v-model="a2_11" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Sinkhole
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_11" value="">
                <input type="number" name="a3_11" v-model.number="a3_11" class="form-control" :disabled="a1 != 'Yes' || a2_11 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_11" value="">
                <select name="a4_11" v-model="a4_11" class="form-control" :disabled="a1 != 'Yes' || a2_11 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_11_1" value="0">
                        <input type="checkbox" name="a5_11_1" v-model="a5_11_1" value="1" :disabled="a1 != 'Yes' || a2_11 != 1 || a4_11 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_11_2" value="0">
                        <input type="checkbox" name="a5_11_2" v-model="a5_11_2" value="1" :disabled="a1 != 'Yes' || a2_11 != 1 || a4_11 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_11_3" value="0">
                        <input type="checkbox" name="a5_11_3" v-model="a5_11_3" value="1" :disabled="a1 != 'Yes' || a2_11 != 1 || a4_11 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_11_4" value="0">
                        <input type="checkbox" name="a5_11_4" v-model="a5_11_4" value="1" :disabled="a1 != 'Yes' || a2_11 != 1 || a4_11 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_11_5" value="0">
                        <input type="checkbox" name="a5_11_5" v-model="a5_11_5" value="1" :disabled="a1 != 'Yes' || a2_11 != 1 || a4_11 != 'Yes'">
                        Others
                    </label>
                </div>
                {{-- <select name="a5_11" v-model="a5_11" class="form-control" :disabled="a1 != 'Yes' || a2_11 != 1 || a4_11 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_12">
                    <input type="checkbox" name="a2_12" v-model="a2_12" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Flood
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_12" value="">
                <input type="number" name="a3_12" v-model.number="a3_12" class="form-control" :disabled="a1 != 'Yes' || a2_12 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_12" value="">
                <select name="a4_12" v-model="a4_12" class="form-control" :disabled="a1 != 'Yes' || a2_12 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_12_1" value="0">
                        <input type="checkbox" name="a5_12_1" v-model="a5_12_1" value="1" :disabled="a1 != 'Yes' || a2_12 != 1 || a4_12 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_12_2" value="0">
                        <input type="checkbox" name="a5_12_2" v-model="a5_12_2" value="1" :disabled="a1 != 'Yes' || a2_12 != 1 || a4_12 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_12_3" value="0">
                        <input type="checkbox" name="a5_12_3" v-model="a5_12_3" value="1" :disabled="a1 != 'Yes' || a2_12 != 1 || a4_12 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_12_4" value="0">
                        <input type="checkbox" name="a5_12_4" v-model="a5_12_4" value="1" :disabled="a1 != 'Yes' || a2_12 != 1 || a4_12 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_12_5" value="0">
                        <input type="checkbox" name="a5_12_5" v-model="a5_12_5" value="1" :disabled="a1 != 'Yes' || a2_12 != 1 || a4_12 != 'Yes'">
                        Others
                    </label>
                </div>
                {{-- <select name="a5_12" v-model="a5_12" class="form-control" :disabled="a1 != 'Yes' || a2_12 != 1 || a4_12 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government">Government</option>
                    <option value="Relative">Relative</option>
                    <option value="NGO">NGO</option>
                    <option value="Friends">Friends</option>
                    <option value="Others">Others</option>
                </select> --}}
            </td>
        </tr>
        <tr style="border: 1px solid black;">
            <td>
                <div class="checkbox text-left" style="margin: 30px">
                    <input type="hidden" value="0" name="a2_13">
                    <input type="checkbox" name="a2_13" v-model="a2_13" value="1" :disabled="a1 != 'Yes'" @change="validate">
                    Wild Fire
                </div>
            </td>
            <td>
                <input type="hidden" name="a3_13" value="">
                <input type="number" name="a3_13" v-model.number="a3_13" class="form-control" :disabled="a1 != 'Yes' || a2_13 != 1" step="1" min="1" @keyup="validate">
            </td>
            <td>
                <input type="hidden" name="a4_13" value="">
                <select name="a4_13" v-model="a4_13" class="form-control" :disabled="a1 != 'Yes' || a2_13 != 1" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
            <td>
                <div class="checkbox text-left">
                    <label class="col-md-12">
                        <input type="hidden" name="a5_13_1" value="0">
                        <input type="checkbox" name="a5_13_1" v-model="a5_13_1" value="1" :disabled="a1 != 'Yes' || a2_13 != 1 || a4_13 != 'Yes'">
                        Government
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_13_2" value="0">
                        <input type="checkbox" name="a5_13_2" v-model="a5_13_2" value="1" :disabled="a1 != 'Yes' || a2_13 != 1 || a4_13 != 'Yes'">
                        Relative
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_13_3" value="0">
                        <input type="checkbox" name="a5_13_3" v-model="a5_13_3" value="1" :disabled="a1 != 'Yes' || a2_13 != 1 || a4_13 != 'Yes'">
                        NGO
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_13_4" value="0">
                        <input type="checkbox" name="a5_13_4" v-model="a5_13_4" value="1" :disabled="a1 != 'Yes' || a2_13 != 1 || a4_13 != 'Yes'">
                        Friends
                    </label>
                    <label class="col-md-12">
                        <input type="hidden" name="a5_13_5" value="0">
                        <input type="checkbox" name="a5_13_5" v-model="a5_13_5" value="1" :disabled="a1 != 'Yes' || a2_13 != 1 || a4_13 != 'Yes'">
                        Others
                    </label>
                </div>
               {{--  <select name="a5_13" v-model="a5_13_" class="form-control" :disabled="a1 != 'Yes' || a2_13 != 1 || a4_13 != 'Yes'" @change="validate">
                    <option value="" selected disabled></option>
                    <option value="Government"></option>
                    <option value="Relative"></option>
                    <option value="NGO"></option>
                    <option value="Friends"></option>
                    <option value="Others"></option>
                </select> --}}
            </td>
        </tr>
    </table>
</div>