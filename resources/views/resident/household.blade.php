@extends('layouts.app')

@section('title')
	Residents Profile Household
@endsection

@section('css')
<!-- internal styles -->
<style>
	@media only screen and (min-width: 800px) {
		.modal-dialog {
			width:60%;
		}
	}
	.panel-heading{
		background-color: #1b3c5b!important;
		color: white !important;
	}
</style>
<!-- Awesomplete -->
{{Html::style('awesomplete/awesomplete.css')}}
@endsection

@section('body')
<div id="household" class="barangay top20">
	 <div class="panel panel-default" >
			<div class="panel-heading" style="color: black;  ">
				<h3 class="panel-title"><b>HOUSEHOLD</b></h3>
			</div>
			<div class="panel-body"  style="height: 65vh;">
			<div class="container1">
				@if(Session::has('flash_message'))
					<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}} </div>
				@endif
				<div class="col-sm-3" style="padding-top:8px;padding-bottom: 8px">
				@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
					<a class="btn btn-primary" data-toggle="modal" href='#addhousehold' data-backdrop="static" style="height:35px">Add Household</a>
					@include('resident.modal.household_add')
			 	@endif
				</div>
				<div class="col-sm-9 text-right">
				{{ Form::open(['route'=>'residentprofile.household.search','method'=>'get', 'class'=>'navbar-form']) }}  
					<div class="form-group" >
						<input type="text" class="form-control" placeholder="Last Name" name="search">
						<span class="form-group-btn">
							<button type="submit" class="btn btn-default">Search</button>
						</span>
						<a href="{{route('residentprofile.household')}}" class="btn btn-primary">View All</a>
					</div>
				{{ Form::close() }}
				</div>
				<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th>Household Identification Number</th>
						<th>Households</th>
						<th>Purok No./Sitio </th>
						<th>Number of Family members</th>
						<th>Last User</th>
						<th>Last Update</th>
						<th>Status</th>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<th>Actions</th> 
						@endif	
					</tr>
					@forelse($resident_households as $rh)
					<tr style="border: 1px solid black; @if($rh->deleted_at != null) background-color: #f87272; @endif">
						<td>{{$rh->house_identification_number}}</td>
						<td class="text-left">
							@if(count($rh->resident_household_members) > 0)
								@foreach($rh->resident_household_members->unique('last_name') as $rhrhm)
									@if($rhrhm->relation == 'Head')
										<strong>{{$rhrhm->last_name}} Family</strong><br>
									@endif
								@endforeach
							@endif
							Address: {{$rh->street}}, {{$rh->municipality}}<br>
							Contact: {{$rh->contact}}
						</td>
						<td>{{$rh->purok}}</td>
						<td>{{$rh->no_members}}</td>
						<td>
							
						</td>
						<td>{{ $rh->updated_at }}</td>
						<td>
							@if($rh->deleted_at == null)
								Active
							@else
								Deleted
							@endif
						</td>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary')
						<td>
 							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
								@if($rh->deleted_at == null)
									<li>
										<a data-toggle="modal" href='#addhouseholdmember{{$rh->id}}' data-backdrop="static" title="Add Member"><i class="fa fa-plus margin-right"></i>&nbsp;&nbsp;Add Household Member</a>
									</li>
									@if(count($rh->survey_b) == 0 && count($rh->survey_c) == 0 && count($rh->survey_d) == 0)
									<li>
										<a href="{{route('resident.household.survey.add',['id'=>$rh->id])}}" title="Add Survey"><i class="fa fa-plus margin-right"></i>&nbsp;&nbsp;Add Survey</a>
									</li>
									@endif
									<li>
										<a data-toggle="modal" href='#viewhousehold{{$rh->id}}' data-backdrop="static" title="View Household" @click="editData('{{$rh->id}}','{{$rh->province}}','{{$rh->municipality}}','{{$rh->barangay}}','{{$rh->purok}}','{{$rh->street}}','{{$rh->house_identification_number}}','{{$rh->latitude}}','{{$rh->longitude}}','{{$rh->name_of_respondent}}','{{$rh->contact}}','{{$rh->no_family}}','{{$rh->no_members}}','{{$rh->disability}}','{{$rh->no_disability}}','{{$rh->building_type}}','{{$rh->water_supply}}','{{$rh->toilet}}','{{$rh->water_filtration}}','{{$rh->electricity}}','{{$rh->power_generator}}')"><i class="fa fa-eye margin-right"></i>&nbsp;&nbsp;View Household</a>
									</li> 
									<li>
										<a href="/resident_profile/household/member/{{$rh->id}}" onclick="document.getElementById('viewMembersSearch').submit();"><i class="fa fa-eye margin-right"></i>&nbsp;&nbsp;View Household Members</a>  
									</li>
									<li>
										<a data-toggle="modal" href='#edithousehold{{$rh->id}}' data-backdrop="static" title="Edit Household" @click="editData('{{$rh->id}}','{{$rh->province}}','{{$rh->municipality}}','{{$rh->barangay}}','{{$rh->purok}}','{{$rh->street}}','{{$rh->house_identification_number}}','{{$rh->latitude}}','{{$rh->longitude}}','{{$rh->name_of_respondent}}','{{$rh->contact}}','{{$rh->no_family}}','{{$rh->no_members}}','{{$rh->disability}}','{{$rh->no_disability}}','{{$rh->building_type}}','{{$rh->water_supply}}','{{$rh->toilet}}','{{$rh->water_filtration}}','{{$rh->electricity}}','{{$rh->power_generator}}')"><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit Household</a>
									</li>
									 @if(count($rh->survey_b) > 0 && count($rh->survey_c) > 0 && count($rh->survey_d) > 0)
									<li>
										<a href="{{route('resident.household.survey.edit',['id'=>$rh->id])}}" title="Edit Survey"><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit Survey</a>
									</li>
									@endif
									<li>
										<a href="{{route('residentprofile.household.delete',['id'=>$rh->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete Household</a>
									</li>
								@else
									<li>
										<a href="{{route('residentprofile.household.restore',['id'=>$rh->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore Household</a>
									</li>
								@endif
								</ul>
							</div>
							@include('resident.modal.household_view')
							@include('resident.modal.household_edit')
							@include('resident.modal.household_member_add')
						</td>
						@endif
					</tr>
					@empty
						<tr><td colspan="@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary') 8 @else 7 @endif"><p style="text-center">No Available Households</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="text-right">{{$resident_households->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJVX1wI8LOsRMUAlVbh1pTrXQvBFJoA4w">
</script>
{{Html::script('js/vue.min.js')}}
{{Html::script('js/axios.min.js')}}
<script>
	var myLatLng = {lat: {{Session::get('barangayAbout')->latitude}}, lng: {{Session::get('barangayAbout')->longitude}}};
	var marker = [];
	var app = new Vue({
		el: '#household',
		delimiters: ["[[","]]"],
		data: {
			province_add: '{{Session::get('barangayAbout')->province}}',
			municipality_add: '{{Session::get('barangayAbout')->municipality}}',
			barangay_add: '{{Session::get('barangayAbout')->barangay}}',
			purok_add: '',
			street_add: '',
			house_identification_number_add: '',
			latitude_add: '',
			longitude_add: '',
			name_of_respondent_add: '',
			contact_add: '',
			no_family_add: '',
			no_members_add: 0,
			disability_add: '',
			no_disability_add: '',
			building_type_add: '',
			water_supply_add: '',
			toilet_add: '',
			water_filtration_add: '',
			electricity_add: '',
			power_generator_add: '',
			id_edit: '',
			province_edit: '',
			municipality_edit: '',
			barangay_edit: '',
			purok_edit: '',
			street_edit: '',
			house_identification_number_edit: '',
			latitude_edit: '',
			longitude_edit: '',
			name_of_respondent_edit: '',
			contact_edit: '',
			no_family_edit: '',
			no_members_edit: '',
			disability_edit: '',
			no_disability_edit: '',
			building_type_edit: '',
			water_supply_edit: '',
			toilet_edit: '',
			water_filtration_edit: '',
			electricity_edit: '',
			power_generator_edit: '',
			enableAdd: false,
			enableEdit: false,
			isLoadingGetCoordinates_add: false,
			unknownCoordinates_add: '',
			isErrorCoordinates_add: false,
			isLoadingGetCoordinates_edit: false,
			unknownCoordinates_edit: '',
			isErrorCoordinates_edit: false,
			first_name: '',
			middle_name: '',
			last_name: '',
			family_belong: '',
			relation: '',
			civil_status: '',
			gender: '',
			birthdate: '',
			age: '',
			same_address: '',
			attending_school: '',
			year_level: '',
			highest_level: '',
			pregnant: '',
			have_children: '',
			solo_parent: '',
			disability: '',
			disability_type: '',
			health_problem: '',
			employed: '',
			occupation: '',
			where_occupation: '',
			enableAddMember: false,
			isHouseholdNumberNotUnique_add: false,
			isUnique_add:'',
			isHouseholdNumberNotUnique_edit: false,
			isUnique_edit:'',
			house_identification_number_old:'',
			gMap_add: '',
			@foreach($resident_households as $rhs)
			gMap_edit{{$rhs->id}}: '',
			@endforeach
		},
		mounted(){
			let vm = this;
			vm.gMap_add = new google.maps.Map(document.getElementById('map_add'), {
			  zoom: 17,
			  center: myLatLng
			});
			google.maps.event.addListener(this.gMap_add, "click", function (event) {
				vm.latitude_add = parseFloat(event.latLng.lat()).toFixed(7);
				vm.longitude_add = parseFloat(event.latLng.lng()).toFixed(7);
				myLatLng = {lat: event.latLng.lat(), lng: event.latLng.lng()};
				vm.addressDetailsCoordsAdd();
			});
			@foreach($resident_households as $rhs)
			vm.gMap_edit{{$rhs->id}} = new google.maps.Map(document.getElementById('map_edit{{$rhs->id}}'), {
			  zoom: 17,
			  center: {lat: {{Session::get('barangayAbout')->latitude}}, lng:  {{Session::get('barangayAbout')->longitude}}}
			});
			marker = new google.maps.Marker({
			  position: {lat: {{$rhs->latitude}}, lng: {{$rhs->longitude}} },
			  map: this.gMap_edit{{$rhs->id}},
			});
			google.maps.event.addListener(this.gMap_edit{{$rhs->id}}, "click", function (event) {
				vm.latitude_edit = parseFloat(event.latLng.lat()).toFixed(7);
				vm.longitude_edit = parseFloat(event.latLng.lng()).toFixed(7);
				myLatLng = {lat: event.latLng.lat(), lng: event.latLng.lng()};
				vm.addressDetailsCoordsEdit();
			});
			@endforeach
		},
		methods: {
			validateAdd(){
				if(this.disability_add != 'Yes'){
					this.no_disability_add = '';
				}
				if(this.purok_add != '' && this.province_add != '' && this.municipality_add != '' && this.barangay_add != '' && this.street_add != '' && this.house_identification_number_add != '' && this.latitude_add != '' && this.longitude_add != '' && this.name_of_respondent_add != '' && this.contact_add != '' && this.no_family_add != '' && this.disability_add != '' && this.building_type_add != '' && this.water_supply_add != '' && this.toilet_add != '' && this.water_filtration_add != '' && this.electricity_add != '' && this.power_generator_add != '' && this.isLoadingGetCoordinates_add == false && this.isHouseholdNumberNotUnique_add == false && this.isUnique_add == ''){
					this.enableAdd = true;
				} else {
					this.enableAdd = false;
				}
			},
			validateEdit(){
				if(this.disability_edit != 'Yes'){
					this.no_disability_edit = '';
				}
				if(this.purok_edit != '' && this.province_edit != '' && this.municipality_edit != '' && this.barangay_edit != '' && this.street_edit != '' && this.house_identification_number_edit != '' && this.latitude_edit != '' && this.longitude_edit != '' && this.name_of_respondent_edit != '' && this.contact_edit != '' && this.no_family_edit != '' && this.disability_edit != '' && this.building_type_edit != '' && this.water_supply_edit != '' && this.toilet_edit != '' && this.water_filtration_edit != '' && this.electricity_edit != '' && this.power_generator_edit != '' && this.isLoadingGetCoordinates_edit == false && this.isHouseholdNumberNotUnique_edit == false && this.isUnique_edit == ''){
					this.enableEdit = true;
				} else {
					this.enableEdit = false;
				}
			},
			validateMember(){
				if(this.attending_school != 'Yes'){
					this.year_level = '';
				}
				if(this.gender != 'Female'){
					this.pregnant = '';
				}
				if(this.disability != 'Yes'){
					this.disability_type = '';
				}
				if(this.have_children != 'Yes'){
					this.solo_parent = '';
				}
				if(this.first_name != '' && this.last_name != '' && this.family_belong != '' && this.relation != '' && this.civil_status != '' && this.gender != '' && this.birthdate != '' && this.same_address != '' && this.attending_school != '' && this.highest_level != '' && this.have_children != '' && this.disability != '' && this.employed != ''){
					if((this.gender == 'Female' && this.pregnant == '') || (this.disablity == 'Yes' && this.disability_type == '')  || (this.attending_school == 'Yes' && this.year_level == '') || (this.employed == 'Yes' && (this.occupation == '' || this.where_occupation == ''))) {
						this.enableAddMember = false;
					} else {
						this.enableAddMember = true;

					}
				} else {
					this.enableAddMember = false;
				}
			},
			editData(id,province,municipality,barangay,purok,street,house_identification_number,latitude,longitude,name_of_respondent,contact,no_family,no_members,disability,no_disability,building_type,water_supply,toilet,water_filtration,electricity,power_generator){
				this.id_edit = id;
				this.province_edit = province;
				this.municipality_edit = municipality;
				this.barangay_edit = barangay;
				this.purok_edit = purok;
				this.street_edit = street;
				this.house_identification_number_edit = house_identification_number;
				this.latitude_edit = latitude;
				this.longitude_edit = longitude;
				this.name_of_respondent_edit = name_of_respondent;
				this.contact_edit = contact;
				this.no_family_edit = no_family;
				this.no_members_edit = no_members;
				this.disability_edit = disability;
				this.no_disability_edit = no_disability;
				this.building_type_edit = building_type;
				this.water_supply_edit = water_supply;
				this.toilet_edit = toilet;
				this.water_filtration_edit = water_filtration;
				this.electricity_edit = electricity;
				this.power_generator_edit = power_generator;
				this.house_identification_number_old = house_identification_number;
				this.validateEdit();
			},
			getAge(){
				this.age = moment().diff(this.birthdate, 'years');
				this.validateMember();
			},
			async getCoordinatesAdd() {
				vm = this;
				try{
					vm.isLoadingGetCoordinates_add = true;
					vm.isErrorCoordinate_add = false;
					vm.unknownCoordinates_add = '';
					/*getting the longitude and latitude from the address*/
					const responseLatLong = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?address='+vm.street_add+','+vm.municipality_add+','+vm.province_add+'&key=AIzaSyAIOnshF_fHAIQAYl1tvNKqcTlXTlW-l4o');
					responseLatLong.data.results.forEach((values) =>{
						vm.latitude_add = values.geometry.location.lat;
						vm.longitude_add = values.geometry.location.lng;
					});
					if (marker && marker.setMap) {
						marker.setMap(null);
					}
					marker = new google.maps.Marker({
					  position: {lat: vm.latitude_add, lng: vm.longitude_add},
					  map: vm.gMap_add,
					});
					vm.isLoadingGetCoordinates_add = false;
					vm.validateAdd();
				} catch (error) {
					vm.isLoadingGetCoordinates_add = false;
					vm.isErrorCoordinates_add = true;
					vm.latitude_add = null;
					vm.longitude_add = null;
					vm.unknownCoordinates_add = '<span style="color:red">Cannot find the Coordinates.</span>';
				}
			},
			async getCoordinatesEdit() {
				vm = this;
				try{
					vm.isLoadingGetCoordinates_edit = true;
					vm.isErrorCoordinate_edit = false;
					vm.unknownCoordinates_edit = '';
					/*getting the longitude and latitude from the address*/
					const responseLatLong = await axios.get('https://maps.googleapis.com/maps/api/geocode/json?address='+vm.street_edit+','+vm.municipality_edit+','+vm.province_edit+'&key=AIzaSyAIOnshF_fHAIQAYl1tvNKqcTlXTlW-l4o');
					responseLatLong.data.results.forEach((values) =>{
						vm.latitude_edit = values.geometry.location.lat;
						vm.longitude_edit = values.geometry.location.lng;
					});
					if (marker && marker.setMap) {
						marker.setMap(null);
					}
					@foreach($resident_households as $rhsem)
						if({{$rhsem->id}} == vm.id_edit){
							marker = new google.maps.Marker({
							  position: {lat: vm.latitude_edit, lng: vm.longitude_edit},
							  map: vm.gMap_edit{{$rhsem->id}},
							});
						}
					@endforeach
					vm.isLoadingGetCoordinates_edit = false;
					vm.validateEdit();
				} catch (error) {
					vm.isLoadingGetCoordinates_edit = false;
					vm.isErrorCoordinates_edit = true;
					vm.latitude_edit = null;
					vm.longitude_edit = null;
					vm.unknownCoordinates_edit = '<span style="color:red">Cannot find the Coordinates.</span>';
				}
			},
			checkHouseholdNumberAdd(){
				let vm = this;
				if(vm.house_identification_number_add != ''){
					axios.get('/api/houseIdentificationNumberAdd/'+this.house_identification_number_add+'/'+{{ Session::get('barangayAbout')->id }}, {
					})
					.then(function (response) {
						if(response.data.unique == 1){
							vm.isHouseholdNumberNotUnique_add = false;
							vm.isUnique_add = '';
						} else {
							vm.isHouseholdNumberNotUnique_add = true;
							vm.isUnique_add = '<span style="color:red">House Identification Number Already Used</span>';
						}
						vm.validateAdd();
					})
					.catch(function (error) {
						vm.checkHouseholdNumberAdd();
					});
				} else {
					vm.isHouseholdNumberNotUnique_add = false;
					vm.isUnique_add = '';
				}
			},
			checkHouseholdNumberEdit(){
				let vm = this;
				if(vm.house_identification_number_edit != ''){
					axios.get('/api/houseIdentificationNumberEdit/'+vm.house_identification_number_edit+'/'+vm.house_identification_number_old+'/'+{{ Session::get('barangayAbout')->id }}, {
					})
					.then(function (response) {
						if(response.data.unique == 1){
							vm.isHouseholdNumberNotUnique_edit = false;
							vm.isUnique_edit = '';
						} else {
							vm.isHouseholdNumberNotUnique_edit = true;
							vm.isUnique_edit = '<span style="color:red">House Identification Number Already Used</span>';
						}
						vm.validateEdit();
					})
					.catch(function (error) {
						vm.checkHouseholdNumberEdit();
					});
				} else {
					vm.isHouseholdNumberNotUnique_edit = false;
					vm.isUnique_edit = '';
				}
			},
			addressDetailsCoordsAdd() {
				vm = this;
				try{
					vm.unknownCoordinates_add = '';
					vm.isErrorCoordinates_add = false;
					if (marker && marker.setMap) {
						marker.setMap(null);
					}
					marker = new google.maps.Marker({
					  position: myLatLng,
					  map: vm.gMap_add,
					});
					vm.longitude_add = parseFloat(vm.longitude_add).toFixed(7);
					vm.latitude_add = parseFloat(vm.latitude_add).toFixed(7);
					vm.validateAdd();
				} catch (error) {
					vm.isErrorCoordinates_add = false;
					vm.latitude_add = null;
					vm.longitude_add = null;
					vm.unknownCoordinates_add = '<span style="color:red">Cannot find the coordinates.</span>';
				}
			},
			addressDetailsCoordsEdit() {
				vm = this;
				try{
					vm.unknownCoordinates_edit = '';
					vm.isErrorCoordinates_edit = false;
					if (marker && marker.setMap) {
						marker.setMap(null);
					}
					@foreach($resident_households as $rhse)
						if({{$rhse->id}} == vm.id_edit){
							marker = new google.maps.Marker({
							  position: myLatLng,
							  map: vm.gMap_edit{{$rhse->id}},
							});
						}
					@endforeach
					vm.longitude_edit = parseFloat(vm.longitude_edit).toFixed(7);
					vm.latitude_edit = parseFloat(vm.latitude_edit).toFixed(7);
					vm.validateEdit();
				} catch (error) {
					vm.isErrorCoordinates_edit = false;
					vm.latitude_edit = null;
					vm.longitude_edit = null;
					vm.unknownCoordinates_edit = '<span style="color:red">Cannot find the coordinates.</span>';
				}
			},
		}
	});
</script>
@endsection