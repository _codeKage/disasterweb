@extends('layouts.app')

@section('title')
	Residents Profile Household Member
@endsection

@section('css')
<!-- internal styles -->
<style>
	@media only screen and (min-width: 800px) {
		.modal-dialog {
			width:60%;
		}
	}
	.panel-heading{
		background-color: #1b3c5b!important;
		color: white !important;
	}
</style>
<!-- Awesomplete -->
{{Html::style('awesomplete/awesomplete.css')}}
@endsection

@section('body')
<div id="householdmember" class="barangay top20">
	@if(Session::has('id'))
		@include('resident.modal.household_member_add_loop')
	@endif
	<div class="panel panel-default" >
			<div class="panel-heading" style="color: black; ">
				<h3 class="panel-title" ><b>HOUSEHOLD MEMBER</b></h3>
			</div>
			<div class="panel-body" style="height: 65vh;">
				<div class="container1">
					@if(Session::has('flash_message'))
						<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
					@endif				

					<div class="col-md-offset-3 col-md-9 text-right">
					{{ Form::open(['route'=>'residentprofile.household.member','method'=>'get', 'class'=>'navbar-form']) }}  
						<div class="form-group" >
							<input type="text" class="form-control" placeholder="Search" name="search">
							<span class="form-group-btn">
								<button type="submit" class="btn btn-default">Search</button>
							</span>
							<a href="{{route('residentprofile.household.member')}}" class="btn btn-primary">View All</a>
						</div>
					{{ Form::close() }}
					</div>
					<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
						<tr style="border: 1px solid black;">
							<th>Household Identification Number</th>
							<th>Name of Resident</th>
							<th>Age</th>
							<th>Gender</th>
							<th>Status</th>
							<th>Actions</th> 	
						</tr>
						@forelse($resident_household_members as $rhm)
						<tr style="border: 1px solid black; @if($rhm->deleted_at != null) background-color: #f87272; @endif">
							<td>{{$rhm->resident_households->house_identification_number}}</td>
							<td class="text-left">
								{{$rhm->last_name}}, {{$rhm->first_name}} {{$rhm->middle_name}}
							</td>
							<td>{{$rhm->age}}</td>
							<td>{{$rhm->gender}}</td>
							<td>
								@if($rhm->deleted_at == null)
									Active
								@else
									Deleted
								@endif
							</td>
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
									@if($rhm->deleted_at == null)
										<li>
											<a data-toggle="modal" href='#viewhouseholdmember{{$rhm->id}}' data-backdrop="static" @click="editData('{{$rhm->id}}','{{$rhm->first_name}}','{{$rhm->middle_name}}','{{$rhm->last_name}}','{{$rhm->family_belong}}','{{$rhm->relation}}','{{$rhm->civil_status}}','{{$rhm->gender}}','{{$rhm->birthdate}}','{{$rhm->age}}','{{$rhm->same_address}}','{{$rhm->attending_school}}','{{$rhm->year_level}}','{{$rhm->highest_level}}','{{$rhm->pregnant}}','{{$rhm->have_children}}','{{$rhm->solo_parent}}','{{$rhm->disability}}','{{$rhm->disability_type}}','{{$rhm->health_problem}}','{{$rhm->employed}}','{{$rhm->occupation}}','{{$rhm->where_occupation}}')"><i class="fa fa-eye margin-right"></i>&nbsp;&nbsp;View Household Member</a>
										</li>
										<li>
											<a data-toggle="modal" href='#edithouseholdmember{{$rhm->id}}' data-backdrop="static" title="Edit" @click="editData('{{$rhm->id}}','{{$rhm->first_name}}','{{$rhm->middle_name}}','{{$rhm->last_name}}','{{$rhm->family_belong}}','{{$rhm->relation}}','{{$rhm->civil_status}}','{{$rhm->gender}}','{{$rhm->birthdate}}','{{$rhm->age}}','{{$rhm->same_address}}','{{$rhm->attending_school}}','{{$rhm->year_level}}','{{$rhm->highest_level}}','{{$rhm->pregnant}}','{{$rhm->have_children}}','{{$rhm->solo_parent}}','{{$rhm->disability}}','{{$rhm->disability_type}}','{{$rhm->health_problem}}','{{$rhm->employed}}','{{$rhm->occupation}}','{{$rhm->where_occupation}}')"><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit Household Member</a>
										</li>
										<li>
											<a href="{{route('residentprofile.household.member.delete',['id'=>$rhm->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
										</li>
									@else
										<li>
											<a href="{{route('residentprofile.household.member.restore',['id'=>$rhm->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
										</li>
									@endif
									</ul>
								</div>
								@include('resident.modal.household_member_edit')
								@include('resident.modal.household_member_view')
							</td>
						</tr>
						@empty
							<tr><td colspan="6"><p style="text-center">No Available Household Members</p></td></tr>
						@endforelse
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="text-right">{{$resident_household_members->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
<script>

	$(window).on('load',function(){
        $('#addhouseholdmember').modal({
		    backdrop: 'static',
		    keyboard: false
		});
    });

	var app = new Vue({
		el: '#householdmember',
		delimiters: ["[[","]]"],
		data: {
			first_name: '',
			middle_name: '',
			last_name: '',
			family_belong: '',
			relation: '',
			civil_status: '',
			gender: '',
			birthdate: '',
			age: '',
			same_address: '',
			attending_school: '',
			year_level: '',
			highest_level: '',
			pregnant: '',
			have_children: '',
			solo_parent: '',
			disability: '',
			disability_type: '',
			health_problem: '',
			employed: '',
			occupation: '',
			where_occupation: '',
			first_name_add: '',
			middle_name_add: '',
			last_name_add: '',
			family_belong_add: '',
			relation_add: '',
			civil_status_add: '',
			gender_add: '',
			birthdate_add: '',
			age_add: '',
			same_address_add: '',
			attending_school_add: '',
			year_level_add: '',
			highest_level_add: '',
			pregnant_add: '',
			have_children_add: '',
			solo_parent_add: '',
			disability_add: '',
			disability_type_add: '',
			health_problem_add: '',
			employed_add: '',
			occupation_add: '',
			where_occupation_add: '',
			enableEditMember: false,
			enableAddMember: false,
		},
		mounted(){
			this.validateEditMember();
		},
		methods: {
			validateEditMember(){
				if(this.attending_school != 'Yes'){
					this.year_level = '';
				}
				if(this.gender != 'Female'){
					this.pregnant = '';
				}
				if(this.disability != 'Yes'){
					this.disability_type = '';
				}
				if(this.have_children != 'Yes'){
					this.solo_parent = '';
				}
				if(this.first_name != '' && this.last_name != '' && this.family_belong != '' && this.relation != '' && this.civil_status != '' && this.gender != '' && this.birthdate != '' && this.same_address != '' && this.attending_school != '' && this.highest_level != '' && this.have_children != '' && this.disability != '' && this.employed != ''){
					if((this.gender == 'Female' && this.pregnant == '') || (this.disablity == 'Yes' && this.disability_type == '')  || (this.attending_school == 'Yes' && this.year_level == '') || (this.employed == 'Yes' && (this.occupation == '' || this.where_occupation == ''))) {
						this.enableEditMember = false;
					} else {
						this.enableEditMember = true;

					}
				} else {
					this.enableEditMember = false;
				}
			},
			validateAddMember(){
				if(this.attending_school_add != 'Yes'){
					this.year_level = '';
				}
				if(this.gender_add != 'Female'){
					this.pregnant = '';
				}
				if(this.disability_add != 'Yes'){
					this.disability_type = '';
				}
				if(this.have_children_add != 'Yes'){
					this.solo_parent = '';
				}
				if(this.first_name_add != '' && this.last_name_add != '' && this.family_belong_add != '' && this.relation_add != '' && this.civil_status_add != '' && this.gender_add != '' && this.birthdate_add != '' && this.same_address_add != '' && this.attending_school_add != '' && this.highest_level_add != '' && this.have_children_add != '' && this.disability_add != '' && this.employed_add != ''){
					if((this.gender_add == 'Female' && this.pregnant_add == '') || (this.disablity_add == 'Yes' && this.disability_type_add == '')  || (this.attending_school_add == 'Yes' && this.year_level_add == '') || (this.employed_add == 'Yes' && (this.occupation_add == '' || this.where_occupation_add == ''))) {
						this.enableAddMember = false;
					} else {
						this.enableAddMember = true;

					}
				} else {
					this.enableAddMember = false;
				}
			},
			editData(id,first_name,middle_name,last_name,family_belong,relation,civil_status,gender,birthdate,age,same_address,attending_school,year_level,highest_level,pregnant,have_children,solo_parent,disability,disability_type,health_problem,employed,occupation,where_occupation){
				this.first_name = first_name;
				this.middle_name = middle_name;
				this.last_name = last_name;
				this.family_belong = family_belong;
				this.relation = relation;
				this.civil_status = civil_status;
				this.gender = gender;
				this.birthdate = birthdate;
				this.age = age;
				this.same_address = same_address;
				this.attending_school = attending_school;
				this.year_level = year_level;
				this.highest_level = highest_level;
				this.pregnant = pregnant;
				this.have_children = have_children;
				this.solo_parent = solo_parent;
				this.disability = disability;
				this.disability_type = disability_type;
				this.health_problem = health_problem;
				this.employed = employed;
				this.occupation = occupation;
				this.where_occupation = where_occupation;
				this.validateEditMember();
			},
			getAgeAdd(){
				this.age = moment().diff(this.birthdate_add, 'years');
				this.validateAddMember();
			},
			getAgeEdit(){
				this.age = moment().diff(this.birthdate, 'years');
				this.validateEditMember();
			}
		}
	});
</script>
@endsection