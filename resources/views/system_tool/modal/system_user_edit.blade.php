<div class="modal fade" id="editsystemuser{{$su->id}}">

	<div class="modal-dialog">

		<div class="modal-content">

		{!! Form::open(['route'=>'systemtool.system_user.update']) !!}

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title">Edit User</h4>

			</div>

			<div class="modal-body">

				<input type="hidden" name="id" value="{{$su->id}}">


				<div class="row">

					<div class="col-md-12 text-left" style="margin-top: 5px" :class="{ 'has-error': isUsernameNotUnique_edit }">

						<label>Username</label>

						<input type="text" class="form-control" name="username" v-model="username_edit" style="margin-bottom: 5px;" @keyup="checkUsernameEdit">

						<span v-html="isUnique_edit"></span>

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>First Name</label>

						<input type="text" class="form-control" name="first_name" v-model="first_name_edit" style="margin-bottom: 5px;" @keyup="validateEdit">

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>Middle Initial</label>

						<input type="text" class="form-control" name="middle_initial" v-model="middle_initial_edit" style="margin-bottom: 5px;" @keyup="validateEdit" maxlength="1">

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>Last Name</label>

						<input type="text" class="form-control" name="last_name" v-model="last_name_edit" style="margin-bottom: 5px;" @keyup="validateEdit">

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>Email</label>

						<input type="email" class="form-control" name="email" v-model="email_edit" style="margin-bottom: 5px;" @keyup="validateEdit">

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>Usertype</label>

						<select name="usertype" v-model="usertype_edit" class="form-control" @change="validateEdit">

							<option value="" selected disabled>Select User Type</option>
							<option value="Chairman">Chairman</option>
							<option value="Secretary">Secretary</option>
							<option value="Treasurer">Treasurer</option>
							<option value="Teacher">Teacher</option>
							<option value="Resident">Resident</option>
						</select>

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>Access</label>

						<select name="access" v-model="access_edit" class="form-control" @change="validateEdit">

							<option value="" selected disabled>Select Access</option>

							<option value="1">Enabled</option>

							<option value="0">Disabled</option>

						</select>

					</div>

				</div>

			</div>

			<div class="modal-footer text-center">

				<span v-if="enableEdit == true"  v-cloak>

					<button type="submit" class="btn btn-success">Update</button>

				</span>

				<span v-else>

					<button type="submit" class="btn btn-success" disabled>Update</button>

				</span>

				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

			</div>

		{!! Form::close() !!}

		</div>

	</div>

</div>