<div class="modal fade" id="addsystemusers">

	<div class="modal-dialog">

		<div class="modal-content">

		{!! Form::open(['route'=>'systemtool.system_user.store']) !!}

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

				<h4 class="modal-title">Add User</h4>

			</div>

			<div class="modal-body">

				<div class="row">

					<div class="col-md-12 text-left" style="margin-top: 5px" :class="{ 'has-error': isUsernameNotUnique_add }">

						<label>Username</label>

						<input type="text" class="form-control" name="username" v-model="username_add" style="margin-bottom: 5px;" @keyup="checkUsernameAdd">

						<span v-html="isUnique_add"></span>

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>First Name</label>

						<input type="text" class="form-control" name="first_name" v-model="first_name_add" style="margin-bottom: 5px;" @keyup="validateAdd">

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>Middle Initial</label>

						<input type="text" class="form-control" name="middle_initial" v-model="middle_initial_add" style="margin-bottom: 5px;" @keyup="validateAdd" maxlength="1">

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>Last Name</label>

						<input type="text" class="form-control" name="last_name" v-model="last_name_add" style="margin-bottom: 5px;" @keyup="validateAdd">

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>Email</label>

						<input type="email" class="form-control" name="email" v-model="email_add" style="margin-bottom: 5px;" @keyup="validateAdd">

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>Usertype</label>

						<select name="usertype" v-model="usertype_add" class="form-control" @change="validateAdd">

							<option value="" selected disabled>Select User Type</option>

							<option value="Chairman">Chairman</option>

							<option value="Secretary">Secretary</option>
							<option value="Treasurer">Treasurer</option>
							<option value="Teacher">Teacher</option>
							<option value="Resident">Resident</option>
						</select>

					</div>

					<div class="col-md-12 text-left" style="margin-top: 5px">

						<label>Access</label>

						<select name="access" v-model="access_add" class="form-control" @change="validateAdd">

							<option value="" selected disabled>Select Access</option>

							<option value="1">Enabled</option>

							<option value="0">Disabled</option>

						</select>

					</div>

				</div>

			</div>

			<div class="modal-footer text-center">

				<span v-if="enableAdd == true"  v-cloak>

					<button type="submit" class="btn btn-success">Add</button>

				</span>

				<span v-else>

					<button type="submit" class="btn btn-success" disabled>Add</button>

				</span>

				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

			</div>

		{!! Form::close() !!}

		</div>

	</div>

</div>