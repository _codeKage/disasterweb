@extends('layouts.app')

@section('title')
	System Tools Change Password
@endsection

@section('css')
<!-- internal styles -->
<style type="text/css">
	.content
	{
		height: 90%;
	}
	.panel-heading{
		background-color: #1b3c5b!important;
		color: white !important;
	}
</style>
@endsection

@section('body')
<div class="top20">
    
	<div class="panel panel-default" >
        <div class="panel-heading" style="color: black;  ">
			<h3 class="panel-title"><b>Change Password</b></h3>
        </div>
        <div class="panel-body" style="height: 65vh;">
			<div class="container1">
				{!! Form::open(['route'=>'systemtool.update_password']) !!}
				<div class="col-md-12" style="margin-top: 10px">
					@if(Session::has('flash_message'))
						<div class="alert alert-success">{{Session::get('flash_message')}}</div>
					@endif
					@if (count($errors) > 0)
			            <div class="alert alert-danger">
			                <ul>
			                    @foreach ($errors->all() as $error)
			                        <li>{{ $error }}</li>
			                    @endforeach
			                </ul>
			            </div>
			        @endif
			        @if(Session::has('error'))
		                <div class="alert alert-danger" role="alert" style="margin-top: 10px;">
		                    <span class="sr-only">Error:</span> &nbsp;{{Session::get('error')}}
		                </div>
		            @endif
				</div>
				<div class="col-md-12">
					<h4 class="text-center"> Enter User Password</h4>
					<div class="form-group row">
					    <label for="inputPassword" class="col-sm-3">Old Password</label>
					   	<div class="col-sm-9">
					      <input type="password" name="old_password" class="form-control">
					    </div>
					</div>

					<div class="form-group row">
					    <label  class="col-sm-3">New Password</label>
					   	<div class="col-sm-9">
					      <input type="password" name="password" class="form-control">
					    </div>
					</div>

					<div class="form-group row">
					    <label  class="col-sm-3">Confirm Password</label>
					   	<div class="col-sm-9">
					      <input type="password" name="confirm_password" class="form-control">
					    </div>
					</div>

					<div class="form-group row">
					   	<div class="col-sm-offset-3 col-sm-9">
							<input type="submit" class="btn btn-success text-center" value="Update">
					    </div>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>				
@endsection

@section('js')
<!-- internal scripts -->
@endsection