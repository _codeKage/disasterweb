@extends('layouts.app')

@section('title')
	System Tools Request Change Password
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div id="system_user" class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #09548e; color: white;  ">
            <h3 class="panel-title" >Request Change Password</h3>     
        </div>
        <div class="panel-body" style="">
		    <div class="container1">
		    @if(Session::has('flash_error'))
				<div class="alert alert-danger" style="margin-top: 10px">{{Session::get('flash_error')}}</div>
			@endif
			{{ Form::open(['route'=>'systemtool.request_change_password.request','method'=>'post', 'class'=>'navbar-form']) }}  
				<div style="width:50%;margin:auto">
					<div class="col-md-4">
						Username:
					</div>
					<div class="col-md-8">
						<input type="text" name="username" class="form-control" required="required">
						<button type="submit" class="btn btn-success" style="margin-top: 10px">Change Password</button>
					</div>
					<div class="col-md-12" style="margin-top: 30px">
						@if(Session::has('flash_message'))
							<p><strong>{{Session::get('flash_message')}}</strong></p>
						@endif
					</div>
				</div>
			{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
@endsection