@extends('layouts.app')

@section('title')
	System Tools Database Backup
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div id="system_user" class="barangay top20" >
	<div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #09548e; color: white;  ">
            <h3 class="panel-title" >Database Backup</h3>     
        </div>
        <div class="panel-body" style="">
		    <div class="container1">
		    @if(Session::has('flash_message'))
				<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
			@endif 
			 @if(Session::has('error_message'))
				<div class="alert alert-danger" style="margin-top: 10px">{{Session::get('error_message')}}</div>
			@endif 
				<div style="width:50%;margin:auto">
					<div class="col-md-12 row text-center">
						<h2 style="color: blue;">Attention</h2>
					</div>
					<div class="col-md-12 row text-justify" style="margin-top: 10px">
						<p>This module is used to BACKUP your database automatically once the backup process is already finished the backup data will be avalable at the storage folder that is in the form of zip file.</p>
					</div>
					<div class="col-md-12 text-center">
						<a href="{{route('systemtool.database_backup.dump')}}" class="btn btn-primary" id="backupDatabase" onclick="document.getElementById('backupDatabase').style.display="none";">Backup Data</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
@endsection