@extends('layouts.app')

@section('title')
	System Tools System User
@endsection

@section('css')
<!-- internal styles -->
	<style>
		.panel-heading{
			background-color: #1b3c5b!important;
			color: white !important;
		}
	</style>
@endsection

@section('body')
<div id="system_user" class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="color: black;  ">
			<h3 class="panel-title"><b>SYSTEM USER</b></h3>
        </div>
        <div class="panel-body" style="height: 100vh;">
    	    <div class="container1">
    	    @if(Session::has('flash_message'))
    			<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
    		@endif
    	    	<div class="col-sm-3" style="padding-top:8px;padding-bottom: 8px">
    				<a class="btn btn-primary" data-toggle="modal" href='#addsystemusers' data-backdrop="static" style="height:35px">Add User</a>
    				@include('system_tool.modal.system_user_add')
    	    	</div>
    	    	<div class="col-sm-9 text-right">
    		    {{ Form::open(['route'=>'systemtool.system_user','method'=>'get', 'class'=>'navbar-form']) }}  
    		        <div class="form-group" >
    		            <input type="text" class="form-control" placeholder="Search" name="search">
    		            <span class="form-group-btn">
    		            	<button type="submit" class="btn btn-default">Search</button>
    		            </span>
                         <button type="submit" class="btn btn-primary">View All</button>
    		        </div>
    		    {{ Form::close() }}
    			</div>
    			<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
    				<tr style="border: 1px solid black;">
    					<th>Username</th>
    					<th>Full Name</th>
    					<th>Usertype</th>
    					<th>Access</th>
    					<th>Status</th>
    			 		<th>Actions</th> 	
    				</tr>
    				@forelse($system_users as $su)
    				<tr style="border: 1px solid black;">
    					<td>{{$su->username}}</td>
    					<td>{{$su->last_name}}, {{$su->first_name}} {{$su->middle_initial}}</td>
    					<td>{{$su->usertype}}</td>
    					<td>
    						@if($su->access == 0)
    							Disabled
    						@else
    							Enabled
    						@endif
    					</td>
    					<td>
    						@if($su->deleted_at == null)
    							Active
    						@else
    							Deleted
    						@endif
    					</td>
    					<td>
    						<div class="dropdown">
    							<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
    							<span class="caret"></span></button>
    							<ul class="dropdown-menu">
    							@if($su->deleted_at == null)
    								<li>
    									<a data-toggle="modal" href='#editsystemuser{{$su->id}}' data-backdrop="static" title="Edit" @click="editData('{{$su->username}}','{{$su->first_name}}','{{$su->middle_initial}}','{{$su->last_name}}','{{$su->email}}','{{$su->usertype}}','{{$su->access}}')"><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
    								</li>
    								<li>
    									@if($su->access == 0)
    										<a href="{{route('systemtool.system_user.enable_access',['id'=>$su->id])}}" title="Enable Access" class="confirmation"><i class="fa fa-unlock margin-right"></i>&nbsp;&nbsp;Enable Access</a>
    									@else
    										<a href="{{route('systemtool.system_user.disable_access',['id'=>$su->id])}}" title="Disable Access" class="confirmation"><i class="fa fa-lock margin-right"></i>&nbsp;&nbsp;Disable Access</a>
    									@endif
    								</li>
    								<li>
    									<a href="{{route('systemtool.system_user.delete',['id'=>$su->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
    								</li>
    							@else
    								<li>
    									<a href="{{route('systemtool.system_user.restore',['id'=>$su->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
    								</li>
    							@endif
    							</ul>
    						</div>
    						@include('system_tool.modal.system_user_edit')		
    					</td>
    				</tr>
    				@empty
    					<tr><td colspan="6"><p style="text-center">No Available Users</p></td></tr>
    				@endforelse
    			</table>
    		</div>
        </div>
	</div>
	<div class="text-right">{{$system_users->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
{{Html::script('js/axios.min.js')}}
<script>
    var app = new Vue({
        el: '#system_user',
       	delimiters: ["[[","]]"],
        data: {
        	username_add: '',
        	first_name_add: '',
            middle_initial_add: '',
            last_name_add: '',
            usertype_add: '',
            access_add: '',
            email_add: '',
            username_edit: '',
        	first_name_edit: '',
            middle_initial_edit: '',
            last_name_edit: '',
            usertype_edit: '',
            access_edit: '',
            email_edit: '',
            enableAdd: false,
            enableEdit: false,
            isUsernameNotUnique_add: false,
            isUnique_add:'',
            isUsernameNotUnique_edit: false,
            isUnique_edit:'',
            Username_old:'',

        },
        methods: {
        	validateAdd(){
        		if(this.username_add != '' && this.first_name_add != '' && this.middle_initial_add != '' && this.last_name_add != '' && this.usertype_add != '' && this.access_add != '' && this.isUsernameNotUnique_add == false && this.isUnique_add == ''){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.username_edit != '' && this.first_name_edit != '' && this.middle_initial_edit != '' && this.last_name_edit != '' && this.usertype_edit != '' && this.access_edit != '' && this.isUsernameNotUnique_edit == false && this.isUnique_edit == ''){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	editData(username,first_name,middle_initial,last_name,email,usertype,access){
        		this.username_edit = username;
	            this.first_name_edit = first_name;
	            this.middle_initial_edit = middle_initial;
	            this.last_name_edit = last_name;
	            this.email_edit = email;
	            this.usertype_edit = usertype;
	            this.access_edit = access;

	            this.Username_old = username;
	            this.validateEdit();
        	},
   			checkUsernameAdd(){
                let vm = this;
                if(vm.username_add != ''){
                    axios.get('/api/usernameAdd/'+this.username_add, {
                    })
                    .then(function (response) {
                        console.log
                        console.log(response.data);
                        if(response.data.unique == 1){
                            vm.isUsernameNotUnique_add = false;
                            vm.isUnique_add = '';
                        } else {
                            vm.isUsernameNotUnique_add = true;
                            vm.isUnique_add = '<span style="color:red">Username Already Used</span>';
                        }
                        vm.validateAdd();
                    })
                    .catch(function (error) {
                        vm.checkUsernameAdd();
                    });
                } else {
                    vm.isUsernameNotUnique_add = false;
                    vm.isUnique_add = '';
                }
            },
            checkUsernameEdit(){
                let vm = this;
                if(vm.username_edit != ''){
                    axios.get('/api/usernameEdit/'+this.username_edit+'/'+this.Username_old, {
                    })
                    .then(function (response) {
                        console.log
                        console.log(response.data);
                        if(response.data.unique == 1){
                            vm.isUsernameNotUnique_edit = false;
                            vm.isUnique_edit = '';
                        } else {
                            vm.isUsernameNotUnique_edit = true;
                            vm.isUnique_edit = '<span style="color:red">Username Already Used</span>';
                        }
                        vm.validateEdit();
                    })
                    .catch(function (error) {
                        vm.checkUsernameEdit();
                    });
                } else {
                    vm.isUsernameNotUnique_edit = false;
                    vm.isUnique_add = '';
                }
            },
        }
    });
</script>
@endsection