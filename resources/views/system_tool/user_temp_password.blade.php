@extends('layouts.app')

@section('title')
	System Tools User with Temporary Password
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div id="system_user" class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #09548e; color: white;  ">
            <h3 class="panel-title" >User with Temporary Password</h3>     
        </div>
        <div class="panel-body" style="">
		    <div class="container1">
		    @if(Session::has('flash_message'))
				<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
			@endif
		    	<div class="col-sm-12 text-right">
			    {{ Form::open(['route'=>'systemtool.user_temp_password','method'=>'get', 'class'=>'navbar-form']) }}  
			        <div class="form-group" >
			            <input type="text" class="form-control" placeholder="Search" name="search">
			            <span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
			            <button type="submit" class="btn btn-primary">View All</button>
			        </div>
			    {{ Form::close() }}
				</div>
				<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th>Username</th>
						<th>Full Name</th>
						<th>Usertype</th>
						<th>Access</th>
						<th>Password</th>	
					</tr>
					@forelse($users as $user)
					<tr style="border: 1px solid black;">
						<td>{{$user->username}}</td>
						<td>{{$user->last_name}}, {{$user->first_name}} {{$user->middle_initial}}</td>
						<td>{{$user->usertype}}</td>
						<td>
							@if($user->access == 0)
								Disabled
							@else
								Enabled
							@endif
						</td>
						<td>{{$user->temporary_password}}</td>
					</tr>
					@empty
						<tr><td colspan="6"><p style="text-center">No Available Users</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="text-right">{{$users->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
@endsection