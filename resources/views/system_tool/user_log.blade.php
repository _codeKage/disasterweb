@extends('layouts.app')

@section('title')
	System Tools Activity Logs
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div id="system_user" class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #09548e; color: white;  ">
            <h3 class="panel-title" >Activity Logs</h3>     
        </div>
        <div class="panel-body" style="">
		    <div class="container1">
		    @if(Session::has('flash_message'))
				<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
			@endif
		    	<div class="col-sm-12 text-right">
			    {{ Form::open(['route'=>'systemtool.user_log','method'=>'get', 'class'=>'navbar-form']) }}  
			        <div class="form-group" >
			            <input type="text" class="form-control" placeholder="Search" name="search">
			            <span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
			             <button type="submit" class="btn btn-primary">View All</button>
			        </div>
			    {{ Form::close() }}
				</div>
				<table class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th>Date</th>
						<th>Username</th>
						<th>Description</th>
						<th>Activity</th>	
					</tr>
					@forelse($activity_logs as $al)
					<tr style="border: 1px solid black;">
						<td>{{$al->created_at}}</td>
						<td>{{$al->username}}</td>
						<td class="text-left">{{$al->description}}</td>
						<td class="text-left">{{$al->activity}}</td>
					</tr>
					@empty
						<tr><td colspan="4"><p style="text-center">No Available Activity Logs</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="text-right">{{$activity_logs->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
@endsection