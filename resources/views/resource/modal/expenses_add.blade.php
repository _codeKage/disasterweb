<div class="modal fade" id="addexpenses">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['url'=>'/financial/budget/'.$resource_budget->id.'/expenses/store','action'=>'POST']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Expenses</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 top10">
						<label>Date</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="date" v-model="date_add" style="margin-bottom: 5px;width:100% !important" @change="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Particular</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="particular" class="form-control" rows="3" v-model="particular_add" style="margin-bottom: 5px;width:100% !important" @change="validateAdd">
							<option value="" selected disabled></option>
							<option value="Response">Response</option>
							<option value="Disaster prevention and mitigation, preparedness, rehabilitation and recovery">Disaster prevention and mitigation, preparedness, rehabilitation and recovery</option>
						</select>
					</div>
					<div class="col-md-4 top10">
						<label>Amount</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px" :class="{ 'has-error': exceed_expense_add }">
						<input type="number" class="form-control" name="amount" v-model.number="amount_add" step="0.01" @keyup="checkExpenseAdd" @change="checkExpenseAdd" required>
						<span v-html="exceed_expense_text_add"></span>
					</div>
					<div class="col-md-4 top10">
						<label>Remarks</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px" >
						<input type="test" class="form-control" name="remarks">
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAdd == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		