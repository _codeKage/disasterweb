<div class="modal fade" id="editbudget{{$rb->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'financial.budget.update']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit Budget</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$rb->id}}">
				<div class="row">
					<div class="col-md-4 text-left top10">
						<label>Fiscal Year</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="fiscal_year" v-model="fiscal_year_edit" class="form-control" @change="validateEdit">
							<option value="" selected disabled></option>
							@for($year = (int)date('Y');$year > (int)date('Y')-30;$year--)
								<option value="{{$year}}">{{$year}}</option>
							@endfor
						</select>
					</div>
					<div class="col-md-4 text-left top10">
						<label>Account Code</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						
						<input type="text" class="form-control" name="code" v-model="code_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Account Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="account_name" v-model="account_name_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Funding Amount</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						
						<input type="number" class="form-control" name="funding_amount" v-model.number="funding_amount_edit" step="0.01" @keyup="validateEdit" required>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Update</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Update</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		