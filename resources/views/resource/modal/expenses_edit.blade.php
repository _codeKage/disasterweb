<div class="modal fade" id="editexpenses{{$re->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['url'=>'/financial/budget/'.$resource_budget->id.'/expenses/update','action'=>'POST']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit Expenses</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$re->id}}">
				<div class="row">
					<div class="col-md-4 text-left top10">
						<label>Date</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="date" v-model="date_edit" style="margin-bottom: 5px;width:100% !important" @change="validateEdit">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Particular</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="particular" class="form-control" rows="3" v-model="particular_edit" style="margin-bottom: 5px;width:100% !important" @change="validateEdit"></textarea>
					</div>
					<div class="col-md-4 text-left top10">
						<label>Amount</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px" :class="{ 'has-error': exceed_expense_edit }">

						<input type="number" class="form-control" name="amount" v-model.number="amount_edit" step="0.01" @keyup="checkExpenseEdit" @change="checkExpenseEdit" required>
						<span v-html="exceed_expense_text_edit"></span>
					</div>

					<div class="col-md-4 text-left top10">
						<label>Remarks</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px" :class="{ 'has-error': exceed_expense_edit }">

						<input type="text" class="form-control" name="remarks" value="{{$re->remarks}}">
					</div>


				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Update</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Update</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		