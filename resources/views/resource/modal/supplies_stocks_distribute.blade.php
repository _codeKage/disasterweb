<div class="modal fade" id="distributeStock{{$rs->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'resources.supplies.distributes.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Distribute {{$rs->name}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="resources_supply_id" value="{{$rs->id}}">
				<div class="row">
					<div class="col-md-4 text-left top10">
						<label>Date</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="date" v-model="date" style="margin-bottom: 5px;" @keyup="validateStocksDistributed">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Particular</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="particular" class="form-control" rows="3" v-model="particular" style="margin-bottom: 5px;width:100% !important" @keyup="validateStocksDistributed"></textarea>
					</div>
					<div class="text-left">
						<div class="col-md-4 text-left" style="margin-top: 5px">
							<label>Stocks</label>
							<input type="number"  class="form-control" value="{{$rs->resources_supply_histories->sum('added') - $rs->resources_supply_histories->sum('distributed')}}" disabled>
						</div>
						<div class="col-md-4 text-left" style="margin-top: 5px">
							<label>Distribute</label>
							<input type="number"  class="form-control" step="1" name="distributed" v-model.number="distributed" max="{{$rs->resources_supply_histories->sum('added') - $rs->resources_supply_histories->sum('distributed')}}" @keyup="stockDValue({{$rs->resources_supply_histories->sum('added') - $rs->resources_supply_histories->sum('distributed')}})" @change="stockDValue({{$rs->resources_supply_histories->sum('added') - $rs->resources_supply_histories->sum('distributed')}})">
						</div>
						<div class="col-md-4 text-left" style="margin-top: 5px">
							<label> </label>
							<input type="number"  class="form-control" name="balance" :value="new_balance" readonly>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableDistributeStocks == true"  v-cloak>
					<button type="submit" class="btn btn-success">Distribute</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Distribute</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		