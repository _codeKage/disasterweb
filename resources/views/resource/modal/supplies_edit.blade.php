<div class="modal fade" id="editSupplies{{$rs->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'resources.supplies.update']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit {{$rs->name}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$rs->id}}">
				<div class="row">
					
					<div class="col-md-4 text-left top10">
						<label>Supply Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name" v-model="name_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Supply Description</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="description" class="form-control" rows="3" v-model="description_edit" style="margin-bottom: 5px;width:100% !important" @change="validateEdit"></textarea>
					</div>
					<div class="col-md-4 text-left top10">
						<label>Category</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="category" v-model="category_edit" class="form-control" @change="validateEdit">
							<option value="" selected disabled>Select Category</option>
							<option value="Water">Water</option>
							<option value="Medical Kit">Medical Kit</option>
							<option value="Biscuits">Biscuits</option>
							<option value="Canned Goods">Canned Goods</option>
							<option value="Instant Noodles">Instant Noodles</option>
							<option value="Others">Others</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Update</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Update</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		