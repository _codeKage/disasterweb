<div class="modal fade" id="addbudget">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'financial.budget.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Budget</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 top10">
						<label>Fiscal Year</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="fiscal_year" v-model="fiscal_year_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled></option>
							@for($year = (int)date('Y');$year > (int)date('Y')-30;$year--)
								<option value="{{$year}}">{{$year}}</option>
							@endfor
						</select>
					</div>
					<div class="col-md-4 top10">
						<label>Account Code</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="code" v-model="code_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Account Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="account_name" v-model="account_name_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Funding Amount</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="number" class="form-control" name="funding_amount" v-model.number="funding_amount_add" step="0.01" @keyup="validateAdd" required>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAdd == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		