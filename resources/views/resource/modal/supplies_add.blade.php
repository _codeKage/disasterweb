<div class="modal fade" id="addSupplies">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'resources.supplies.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Supplies</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4 top10">
						<label>Supply Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name" v-model="name_add" style="margin-bottom: 5px;" @keyup="validateAdd">
					</div>
					<div class="col-md-4 top10">
						<label>Supply Description</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="description" class="form-control" rows="3" v-model="description_add" style="margin-bottom: 5px;width:100% !important" @change="validateAdd"></textarea>
					</div>
					<div class="col-md-4 top10">
						<label>Category</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="category" v-model="category_add" class="form-control" @change="validateAdd">
							<option value="" selected disabled>Select Category</option>
							<option value="Water">Water</option>
							<option value="Medical Kit">Medical Kit</option>
							<option value="Biscuits">Biscuits</option>
							<option value="Canned Goods">Canned Goods</option>
							<option value="Instant Noodles">Instant Noodles</option>
							<option value="Others">Others</option>
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAdd == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		