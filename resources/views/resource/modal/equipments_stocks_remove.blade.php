<div class="modal fade" id="removeStock{{$re->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'resources.equipments.remove.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Remove {{$re->name}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="resources_equipment_id" value="{{$re->id}}">
				<div class="row">
					<div class="col-md-4 text-left top10">
						<label>Date</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="date" v-model="date" style="margin-bottom: 5px;" @keyup="validateStocksRemoved">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Particular</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="particular" class="form-control" rows="3" v-model="particular" style="margin-bottom: 5px;width:100% !important" @keyup="validateStocksRemoved"></textarea>
					</div>
					<div class="text-left">
						<div class="col-md-4" style="margin-top: 5px">
							<label>Bad Condition Stocks</label>
							<input type="number"  class="form-control" value="{{$re->bad_condition}}" disabled>
						</div>
						<div class="col-md-4" style="margin-top: 5px">
							<label>Remove</label>
							<input type="number"  class="form-control" step="1" name="removed" v-model.number="removed" max="{{$re->bad_condition}}" @keyup="stockDValue({{$re->bad_condition}})" @change="stockDValue({{$re->bad_condition}})">
						</div>
						<div class="col-md-4" style="margin-top: 5px">
							<label> </label>
							<input type="number"  class="form-control" :value="balance" name="balance" readonly>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableRemoveStocks == true"  v-cloak>
					<button type="submit" class="btn btn-success">Remove</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Remove</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		