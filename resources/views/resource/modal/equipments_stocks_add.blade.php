<div class="modal fade" id="addEquipmentStock{{$re->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'resources.equipments.stocks.store']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add {{$re->name}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="resources_equipment_id" value="{{$re->id}}">
				<div class="row">
					<div class="col-md-4 text-left top10">
						<label>Date</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="date" class="form-control" name="date" v-model="date" style="margin-bottom: 5px;" @keyup="validateStocksAdd">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Source</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name_source" v-model="name_source" style="margin-bottom: 5px;" @keyup="validateStocksAdd">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Particular</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="particular" class="form-control" rows="3" v-model="particular" style="margin-bottom: 5px;width:100% !important" @keyup="validateStocksAdd"></textarea>
					</div>
					<div class="text-left">
						<div class="col-md-4 text-left" style="margin-top: 5px">
							<label>Stocks</label>
							<input type="number"  class="form-control" value="{{$re->good_condition + $re->bad_condition}}" disabled>
						</div>
						<div class="col-md-4 text-left" style="margin-top: 5px">
							<label>New Stocks</label>
							<input type="number"  class="form-control" step="1" name="added" v-model.number="added" @keyup="stockValue({{$re->good_condition + $re->bad_condition}})" @change="stockValue({{$re->good_condition + $re->bad_condition}})">
						</div>
						<div class="col-md-4 text-left" style="margin-top: 5px">
							<label> </label>
							<input type="number"  class="form-control" :value="stocks" disabled>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableAddStocks == true"  v-cloak>
					<button type="submit" class="btn btn-success">Add</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Add</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		