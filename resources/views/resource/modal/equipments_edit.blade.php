<div class="modal fade" id="editEquipment{{$re->id}}">
	<div class="modal-dialog">
		<div class="modal-content">
		{!! Form::open(['route'=>'resources.equipments.update']) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Edit {{$re->name}}</h4>
			</div>
			<div class="modal-body">
				<input type="hidden" name="id" value="{{$re->id}}">
				<div class="row">
					<div class="col-md-4 text-left top10">
						<label>Equipment Name</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<input type="text" class="form-control" name="name" v-model="name_edit" style="margin-bottom: 5px;" @keyup="validateEdit">
					</div>
					<div class="col-md-4 text-left top10">
						<label>Equipment Description</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<textarea name="description" class="form-control" rows="3" v-model="description_edit" style="margin-bottom: 5px;width:100% !important" @change="validateEdit"></textarea>
					</div>
					<div class="col-md-4 text-left top10">
						<label>Category</label>
					</div>
					<div class="col-md-8 text-left" style="margin-top: 5px">
						<select name="type" v-model="type_edit" class="form-control" @change="validateEdit">
							<option value="" selected disabled>Select Type</option>
							<option value="Cooking Equipment">Cooking Equipment</option>
							<option value="Sleeping Equipment">Sleeping Equipment</option>
							<option value="Others">Others</option>
						</select>
					</div>
				
						<div class="col-md-4 text-left top10">
							<label>Good Condition</label>
						</div>
						<div class="col-md-8">
							<input type="number" class="form-control" name="good_condition" v-model.number="good_condition" min="0" step="1" style="margin-bottom: 5px;" @keyup="changeGoodCondition" @change="changeGoodCondition">
						</div>
						<div class="col-md-4 text-left top10">
							<label>Bad Condition</label>
						</div>
						<div class="col-md-8">
							<input type="number" class="form-control" name="bad_condition" v-model.number="bad_condition" min="0" step="1" style="margin-bottom: 5px;" @keyup="changeBadCondition" @change="changeBadCondition">
						</div>
			
				</div>
			</div>
			<div class="modal-footer text-center">
				<span v-if="enableEdit == true"  v-cloak>
					<button type="submit" class="btn btn-success">Update</button>
				</span>
				<span v-else>
					<button type="submit" class="btn btn-success" disabled>Update</button>
				</span>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>		