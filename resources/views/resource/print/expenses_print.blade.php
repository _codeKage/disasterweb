<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3 style="color: blue;">Expenses of {{$resource_budget->account_name}} for {{$resource_budget->fiscal_year}}</h3>
<div class="content">
	<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
		<tr style="border: 1px solid black;">
			<th>Date</th>
			<th>Particular</th>
			<th>Amount</th>
			<th>Status</th>
		</tr>

		@forelse($resources_expenses as $re)
		<tr style="border: 1px solid black;">
			<td>{{$re->date}}</td>
			<td class="text-left">{{$re->particular}}</td>
			<td class="text-right">{{number_format($re->amount)}}</td>
			<td>
				@if($re->deleted_at == null)
					Active
				@else
					Deleted
				@endif
			</td>
		</tr>
		@empty
			<tr><td colspan="4"><p style="text-center">No Available Expenses</p></td></tr>
		@endforelse
	</table>
</div>