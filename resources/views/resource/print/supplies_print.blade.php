<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3 style="color: blue;">Supplies</h3>
<div class="content">
	<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
		<tr style="border: 1px solid black;">
			<th>Supplies Name</th>
			<th>Description</th>
			<th>Category</th>
			<th>Stocks</th>
			<th>Status</th>
		</tr>

		@forelse($resources_supplies as $rs)
		<tr style="border: 1px solid black;">
			<td>{{$rs->name}}</td>
			<td>{{$rs->description}}</td>
			<td>{{$rs->category}}</td>
			<td>{{$rs->resources_supply_histories()->sum('added') - $rs->resources_supply_histories()->sum('distributed')}}</td>
			<td>
				@if($rs->deleted_at == null)
					Active
				@else
					Deleted
				@endif
			</td>
		</tr>
		@empty
			<tr><td colspan="6"><p style="text-center">No Available Supplies</p></td></tr>
		@endforelse
	</table>
</div>