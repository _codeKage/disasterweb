<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3 style="color: blue;">Equipment</h3>
<div class="content">
	<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
		<tr style="border: 1px solid black;">
			<th>Equipment Name</th>
			<th>Description</th>
			<th>Type</th>
			<th>Good Condition</th>
			<th>Bad Condition</th>
			<th>Status</th>
		</tr>

		@forelse($resources_equipments as $re)
		<tr style="border: 1px solid black;">
			<td>{{$re->name}}</td>
			<td>{{$re->description}}</td>
			<td>{{$re->type}}</td>
			<td>{{$re->good_condition}}</td>
			<td>{{$re->bad_condition}}</td>
			<td>
				@if($re->deleted_at == null)
					Active
				@else
					Deleted
				@endif
			</td>
		</tr>
		@empty
			<tr><td colspan="6"><p style="text-center">No Available Equipment</p></td></tr>
		@endforelse
	</table>
</div>