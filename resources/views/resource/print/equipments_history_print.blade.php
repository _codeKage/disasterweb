<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3 style="color: blue;">Equipment History of {{$re->name}}</h3>
<div class="content">
	<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
		<tr style="border: 1px solid black;">
			<th>Date</th>
			<th>Source</th>
			<th>Particular</th>
			<th>Added</th>
			<th>Removed</th>	
		</tr>

		@forelse($resources_equipment_histories as $reh)
		<tr style="border: 1px solid black;">
			<td>{{$reh->date}}</td>
			<td class="text-left">{{$reh->name_source}}</td>
			<td class="text-left">{{$reh->particular}}</td>
			<td>{{$reh->added}}</td>
			<td>{{$reh->removed}}</td>
		</tr>
		@empty
			<tr><td colspan="5"><p style="text-center">No Available Equipment History</p></td></tr>
		@endforelse
	</table>
</div>