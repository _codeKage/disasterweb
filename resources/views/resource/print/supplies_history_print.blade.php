<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3 style="color: blue;">Supplies History of {{$rs->name}}</h3>
<div class="content">
	<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
		<tr style="border: 1px solid black;">
			<th>Date</th>
			<th>Source</th>
			<th>Particular</th>
			<th>Added</th>
			<th>Distributed</th>
			<th>Balance</th>	
		</tr>

		@forelse($resources_supply_histories as $rsh)
		<tr style="border: 1px solid black;">
			<td>{{$rsh->date}}</td>
			<td class="text-left">{{$rsh->name_source}}</td>
			<td class="text-left">{{$rsh->particular}}</td>
			<td>{{$rsh->added}}</td>
			<td>{{$rsh->distributed}}</td>
			<td>{{$rsh->balance}}</td>
		</tr>
		@empty
			<tr><td colspan="6"><p style="text-center">No Available Supply History</p></td></tr>
		@endforelse
	</table>
</div>