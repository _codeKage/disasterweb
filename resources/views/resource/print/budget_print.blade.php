<script>window.onload = function() { window.print(); }</script>
<style type="text/css">
@page{
        size:  auto;   /* auto is the initial value */
        margin: 25mm;  /* this affects the margin in the printer settings */
}
</style>
{{Html::style('css/bootstrap.min.css')}}
<h2>Disaster Get-Ready Application (Barangay {{Session::get('barangayAbout')->barangay}})</h2>
<h3 style="color: blue;">Budget</h3>
<div class="content">
	<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
		<tr style="border: 1px solid black;">
			<th>Fiscal Year</th>
			<th>Account Code</th>
			<th>Account Name</th>
			<th>Budget</th>
			<th>Expenses</th>
			<th>Balance</th>
			<th>Status</th>
		</tr>

		@forelse($resources_budgets as $rb)
		<tr style="border: 1px solid black;">
			<td>{{$rb->fiscal_year}}</td>
			<td class="text-left">{{$rb->code}}</td>
			<td class="text-left">{{$rb->account_name}}</td>
			<td class="text-right">{{number_format($rb->funding_amount)}}</td>
			<td class="text-right">{{number_format($rb->resources_expenses()->sum('amount'))}}</td>
			<td class="text-right">{{number_format($rb->funding_amount - $rb->resources_expenses()->sum('amount'))}}</td>
			<td>
				@if($rb->deleted_at == null)
					Active
				@else
					Deleted
				@endif
			</td>
		</tr>
		@empty
			<tr><td colspan="8"><p style="text-center">No Available Budgets</p></td></tr>
		@endforelse
	</table>
</div>