@extends('layouts.app')

@section('title')
	Resources Equipment History
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div id="equipments" class="barangay top20">
		<div class="panel panel-default" >
            <div class="panel-heading" style="background-color: #09548e; color: white;  ">
                <h3 class="panel-title">Equipment History of {{$re->name}}</h3>     
            </div>
            <div class="panel-body" style="height: 65vh;">
			    <div class="container1">
			    	@if(Session::has('flash_message'))
						<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
					@endif
			    	<div class="col-sm-12 text-right">
				    {{ Form::open(['url'=>'/resource/equipments/history/'.$re->id,'method'=>'get', 'class'=>'navbar-form']) }}  
				        <div class="form-group" >
				            <input type="text" class="form-control" placeholder="Search" name="search">
				            <span class="form-group-btn">
				            	<button type="submit" class="btn btn-default">Search</button>
				            </span>
				            <button type="submit" class="btn btn-primary">View All</button>
				        </div>
				    {{ Form::close() }}
					</div>
					<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
						<tr style="border: 1px solid black;">
							<th>Date</th>
							<th>Source</th>
							<th>Particular</th>
							<th>Added</th>
							<th>Removed</th>	
						</tr>

						@forelse($resources_equipment_histories as $reh)
						<tr style="border: 1px solid black;">
							<td>{{$reh->date}}</td>
							<td class="text-left">{{$reh->name_source}}</td>
							<td class="text-left">{{$reh->particular}}</td>
							<td>{{$reh->added}}</td>
							<td>{{$reh->removed}}</td>
						</tr>
						@empty
							<tr><td colspan="5"><p style="text-center">No Available Equipment History</p></td></tr>
						@endforelse
					</table>
				</div>
			</div>
		</div>
	<div class="row">
		<div class="col-md-3" style="margin-bottom: 5px">
			@if(Session::get('logUser')->usertype != 'Secretary')
			{{ Form::open(['url'=>'/resource/equipments/history/'.$re->id.'/print','method'=>'get', 'class'=>'navbar-form','target'=>'_blank']) }}  
		        <input type="hidden" name="print" value="{{(isset($_GET["search"]) ? $_GET["search"]:"" )}}">
		        <button type="submit" class="btn btn-primary">Print List</button>
		    {{ Form::close() }}
		    @endif
		</div>
		<div class="col-md-9" style="margin-bottom: 5px">
			<div class="text-right">{{$resources_equipment_histories->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
@endsection