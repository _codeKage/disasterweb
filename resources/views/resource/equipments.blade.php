@extends('layouts.app')

@section('title')
	Resources Equipment
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div id="equipments" class="barangay top20" >
	<div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #09548e; color: white;  ">
            <h3 class="panel-title">Equipment</h3>     
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
		    	@if(Session::has('flash_message'))
					<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
				@endif
		    	<div class="col-sm-6">
		    		@if(Session::get('logUser')->usertype != 'Secretary')
					<a class="btn btn-primary" data-toggle="modal" href='#addEquipment' data-backdrop="static" style="height:35px;margin-top:8px;margin-bottom:8px">Add Equipment</a>
					@include('resource.modal.equipments_add')
					@endif
		    	</div>
		    	<div class="col-sm-6 text-right">
			    {{ Form::open(['route'=>'resources.equipments','method'=>'get', 'class'=>'navbar-form']) }}  
			        <div class="form-group" >
			            <input type="text" class="form-control" placeholder="Search" name="search">
			            <span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
			            <a href="{{route('resources.equipments')}}" class="btn btn-primary">View All</a>
			        </div>
			    {{ Form::close() }}
				</div>
				<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th>Equipment Name</th>
						<th>Description</th>
						<th>Type</th>
						<th>Good Condition</th>
						<th>Bad Condition</th>
						<th>Status</th>
						<th>Action</th>	
					</tr>

					@forelse($resources_equipments as $re)
					<tr style="border: 1px solid black; @if($re->deleted_at != null) background-color: #f87272; @endif">
						<td class="text-left">{{$re->name}}</td>
						<td class="text-left">{{$re->description}}</td>
						<td class="text-left">{{$re->type}}</td>
						<td>{{$re->good_condition}}</td>
						<td>{{$re->bad_condition}}</td>
						<td>
							@if($re->deleted_at == null)
								Active
							@else
								Deleted
							@endif
						</td>
						<td>
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
							@if($re->deleted_at == null)
								@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a data-toggle="modal" href='#editEquipment{{$re->id}}' data-backdrop="static" title="Edit" @click='editData("{{$re->name}}","{{$re->description}}","{{$re->type}}",{{$re->good_condition}},{{$re->bad_condition}})'><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
									</li>
									<li>
										<a data-toggle="modal" href='#addEquipmentStock{{$re->id}}' data-backdrop="static" title="add stock" @click="stocksIValue({{$re->resources_equipments_histories->sum('added') + $re->bad_condition}})"><i class="fa fa-plus margin-right"></i>&nbsp;&nbsp;Add Stock</a>
									</li>
								@endif
									<li>
										<a href='/resource/equipments/history/{{$re->id}}' title="Equipment History"><i class="fa fa-eye margin-right"></i>&nbsp;&nbsp;Equipment History</a>
									</li>
								@if(Session::get('logUser')->usertype != 'Secretary')
									@if($re->bad_condition > 0)
									<li>
										<a data-toggle="modal" href='#removeStock{{$re->id}}' data-backdrop="static" title="remove stock"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Remove Bad Stock</a>
									</li>
									@endif
									<li>
										<a href="{{route('resources.equipments.delete',['id'=>$re->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
									</li>
								@endif
							@else
								@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a href="{{route('resources.equipments.restore',['id'=>$re->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
									</li>
								@endif
							@endif
								</ul>
							</div>
							@include('resource.modal.equipments_edit')
							@include('resource.modal.equipments_stocks_add')
							@include('resource.modal.equipments_stocks_remove')
						</td>
					</tr>
					@empty
						<tr><td colspan="7"><p style="text-center">No Available Equipment</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3" style="margin-bottom: 5px">
			{{ Form::open(['route'=>'resources.equipments.print','method'=>'get', 'class'=>'navbar-form','target'=>'_blank']) }}  
		        <input type="hidden" name="print" value="{{(isset($_GET["search"]) ? $_GET["search"]:"" )}}">
		        <button type="submit" class="btn btn-primary">Print List</button>
		    {{ Form::close() }}
		</div>
		<div class="col-md-9" style="margin-bottom: 5px">
			<div class="text-right">{{$resources_equipments->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
<script>
    var app = new Vue({
        el: '#equipments',
       	delimiters: ["[[","]]"],
        data: {
        	name_add: '',
	        type_add: '',
	        description_add: '',
	        name_edit: '',
	        type_edit: '',
	        date: '',
	        name_source: '',
	        particular: '',
	        added: '',
	        removed: '',
	        balance: '',
	        good_condition: '',
	        bad_condition: '',
	        stocks: 0,
	        description_edit: '',
            enableAdd: false,
            enableEdit: false,
            enableAddStocks: false,
            enableRemoveStocks: false,
        },
        methods: {
        	validateAdd(){
        		if(this.name_add != '' && this.type_add != '' && this.description_add != ''){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.name_edit != '' && this.type_edit != '' && this.description_edit != '' && this.good_condition >= 0 && this.bad_condition >= 0){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	validateStocksAdd(){
        		if(this.date != '' && this.name_source != '' && this.particular != '' && this.added != ''){
        			this.enableAddStocks = true;
        		} else {
        			this.enableAddStocks = false;
        		}
        	},
        	validateStocksRemoved(){
        		if(this.date != '' && this.particular != '' && this.removed != '' && this.balance >= 0){
        			this.enableRemoveStocks = true;
        		} else {
        			this.enableRemoveStocks = false;
        		}
        	},
        	stockValue(stocks){
        		this.stocks = parseInt(this.added) + parseInt(stocks);
        		this.validateStocksAdd();
        	},
        	stocksIValue(good_condition,bad_condition){
        		this.stocks = good_condition+bad_condition;
        	},
        	stockDValue(new_balance){
        		this.balance = parseInt(new_balance) - parseInt(this.removed);
        		this.validateStocksRemoved();
        	},
        	changeGoodCondition(){
        		this.bad_condition = this.stocks - this.good_condition;
        		this.validateEdit();
        	},
        	changeBadCondition(){
        		this.good_condition = this.stocks - this.bad_condition;
        		this.validateEdit();
        	},
        	editData(name,description,type,good_condition,bad_condition){
        		this.name_edit = name;
	            this.description_edit = description;
	            this.type_edit = type;
	            this.good_condition = good_condition;
	            this.bad_condition = bad_condition;
	            this.stocks = good_condition + bad_condition;
	            this.validateEdit();
        	}
        }
    });
</script>
@endsection