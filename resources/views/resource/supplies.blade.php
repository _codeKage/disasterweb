@extends('layouts.app')

@section('title')
	Resources Supplies
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div id="supplies" class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="background-color: #09548e; color: white;  ">
            <h3 class="panel-title">Supplies</h3>     
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
		    	@if(Session::has('flash_message'))
					<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
				@endif
		    	<div class="col-sm-6">
		    		@if(Session::get('logUser')->usertype != 'Secretary')
					<a class="btn btn-primary" data-toggle="modal" href='#addSupplies' data-backdrop="static" style="height:35px;margin-top:8px;margin-bottom:8px">Add Supplies</a>
					@include('resource.modal.supplies_add')
					@endif
		    	</div>
		    	<div class="col-sm-6 text-right">
			    {{ Form::open(['route'=>'resources.supplies','method'=>'get', 'class'=>'navbar-form']) }}  
			        <div class="form-group" >
			            <input type="text" class="form-control" placeholder="Search" name="search">
			            <span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
			            <a href="{{route('resources.supplies')}}" class="btn btn-primary">View All</a>
			        </div>
			    {{ Form::close() }}
				</div>
				<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th>Supplies Name</th>
						<th>Description</th>
						<th>Category</th>
						<th>Stocks</th>
						<th>Status</th>
						<th>Action</th>	
					</tr>

					@forelse($resources_supplies as $rs)
					<tr style="border: 1px solid black; @if($rs->deleted_at != null) background-color: #f87272; @endif">
						<td class="text-left">{{$rs->name}}</td>
						<td class="text-left">{{$rs->description}}</td>
						<td class="text-left">{{$rs->category}}</td>
						<td>{{$rs->resources_supply_histories()->sum('added') - $rs->resources_supply_histories()->sum('distributed')}}</td>
						<td>
							@if($rs->deleted_at == null)
								Active
							@else
								Deleted
							@endif
						</td>
						<td>
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
							@if($rs->deleted_at == null)
								@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a data-toggle="modal" href='#editSupplies{{$rs->id}}' data-backdrop="static" title="Edit" @click='editData("{{$rs->name}}","{{$rs->description}}","{{$rs->category}}")'><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
									</li>
									<li>
										<a data-toggle="modal" href='#addSuppliesStock{{$rs->id}}' data-backdrop="static" title="add stock" @click="stocksIValue({{$rs->resources_supply_histories->sum('added') - $rs->resources_supply_histories->sum('distributed')}})"><i class="fa fa-plus margin-right"></i>&nbsp;&nbsp;Add Stock</a>
									</li>
									<li>
										<a data-toggle="modal" href='#distributeStock{{$rs->id}}' data-backdrop="static" title="distribute stock" @click="stocksIValue({{$rs->resources_supply_histories->sum('added') - $rs->resources_supply_histories->sum('distributed')}})"><i class="fa fa-share margin-right"></i>&nbsp;&nbsp;Distribute Stock</a>
									</li>
								@endif
									<li>
										<a href='/resource/supplies/history/{{$rs->id}}' title="Supply History"><i class="fa fa-eye margin-right"></i>&nbsp;&nbsp;Supply History</a>
									</li>
								@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a href="{{route('resources.supplies.delete',['id'=>$rs->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
									</li>
								@endif
							@else
								@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a href="{{route('resources.supplies.restore',['id'=>$rs->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
									</li>
								@endif
							@endif
								</ul>
							</div>
							@include('resource.modal.supplies_edit')
							@include('resource.modal.supplies_stocks_add')
							@include('resource.modal.supplies_stocks_distribute')
						</td>
					</tr>
					@empty
						<tr><td colspan="6"><p style="text-center">No Available Supplies</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3" style="margin-bottom: 5px">
			@if(Session::get('logUser')->usertype != 'Secretary')
			{{ Form::open(['route'=>'resources.supplies.print','method'=>'get', 'class'=>'navbar-form','target'=>'_blank']) }}  
		        <input type="hidden" name="print" value="{{(isset($_GET["search"]) ? $_GET["search"]:"" )}}">
		        <button type="submit" class="btn btn-primary">Print List</button>
		    {{ Form::close() }}
		    @endif
		</div>
		<div class="col-md-9" style="margin-bottom: 5px">
			<div class="text-right">{{$resources_supplies->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
<script>
    var app = new Vue({
        el: '#supplies',
       	delimiters: ["[[","]]"],
        data: {
        	name_add: '',
	        category_add: '',
	        description_add: '',
	        name_edit: '',
	        category_edit: '',
	        date: '',
	        name_source: '',
	        particular: '',
	        added: '',
	        distributed: '',
	        balance: '',
	        new_balance: 0,
	        description_edit: '',
            enableAdd: false,
            enableEdit: false,
            enableAddStocks: false,
            enableDistributeStocks: false,
        },
        methods: {
        	validateAdd(){
        		if(this.name_add != '' && this.category_add != '' && this.description_add != ''){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.name_edit != '' && this.category_edit != '' && this.description_edit != ''){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	validateStocksAdd(){
        		if(this.date != '' && this.name_source != '' && this.particular != '' && this.added != ''){
        			this.enableAddStocks = true;
        		} else {
        			this.enableAddStocks = false;
        		}
        	},
        	validateStocksDistributed(){
        		if(this.date != '' && this.particular != '' && this.distributed != ''){
        			this.enableDistributeStocks = true;
        		} else {
        			this.enableDistributeStocks = false;
        		}
        	},
        	stockValue(new_balance){
        		this.new_balance = parseInt(this.added) + parseInt(new_balance);
        		this.validateStocksAdd();
        	},
        	stocksIValue(value){
        		this.new_balance = value;
        	},
        	stockDValue(new_balance){
        		this.new_balance = parseInt(new_balance) - parseInt(this.distributed);
        		this.validateStocksDistributed();
        	},
        	editData(name,description,category){
        		this.name_edit = name;
	            this.description_edit = description;
	            this.category_edit = category;
	            this.validateEdit();
        	}
        }
    });
</script>
@endsection