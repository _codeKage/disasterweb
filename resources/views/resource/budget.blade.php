@extends('layouts.app')

@section('title')
	Resources Budget
@endsection

@section('css')
<!-- internal styles -->
<style type="text/css">

	.panel-heading{
		background-color: #1b3c5b!important;
		color: white !important;
	}
</style>
@endsection

@section('body')
<div id="budget" class="barangay top20">
	<div class="panel panel-default" >
        <div class="panel-heading" style="color: black;  ">
            <h3 class="panel-title" ><b>Budgets</b></h3>
        </div>
        <div class="panel-body" style="height: 65vh;">
		    <div class="container1">
		    	@if(Session::has('flash_message'))
					<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
				@endif
		    	<div class="col-sm-6">
		    		@if(Session::get('logUser')->usertype != 'Secretary')
					<a class="btn btn-primary" data-toggle="modal" href='#addbudget' data-backdrop="static" style="height:35px;margin-top:8px;margin-bottom:8px">Add Budget</a>
					@include('resource.modal.budget_add')
					@endif
		    	</div>
		    	<div class="col-sm-6 text-right">
			    {{ Form::open(['route'=>'financial.budget','method'=>'get', 'class'=>'navbar-form']) }}  
			        <div class="form-group" >
			            <input type="text" class="form-control" placeholder="Search" name="search">
			            <span class="form-group-btn">
			            	<button type="submit" class="btn btn-default">Search</button>
			            </span>
			            <a href="{{route('financial.budget')}}" class="btn btn-primary">View All</a>
			        </div>
			    {{ Form::close() }}
				</div>
				<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th>Fiscal Year</th>
						<th>Account Code</th>
						<th>Account Name</th>
						<th>Budget</th>
						<th>Expenses</th>
						<th>Balance</th>
						<th>Status</th>
						<th>Action</th>	
					</tr>

					@forelse($resources_budgets as $rb)
					@if($rb->deleted_at == null)
					<tr style="border: 1px solid black;">
						<td>{{$rb->fiscal_year}}</td>
						<td class="text-left">{{$rb->code}}</td>
						<td class="text-left">{{$rb->account_name}}</td>
						<td class="text-right">{{number_format($rb->funding_amount)}}</td>
						<td class="text-right">{{number_format($rb->resources_expenses()->sum('amount'))}}</td>
						<td class="text-right">{{number_format($rb->funding_amount - $rb->resources_expenses()->sum('amount'))}}</td>
						<td>
							@if($rb->deleted_at == null)
								Active
							@else
								Deleted
							@endif
						</td>
						<td>
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
								@if($rb->deleted_at == null)
									@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a data-toggle="modal" href='#editbudget{{$rb->id}}' data-backdrop="static" title="Edit" @click='editData("{{$rb->fiscal_year}}","{{$rb->code}}","{{$rb->account_name}}","{{$rb->funding_amount}}")'><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
									</li>
									@endif
									<li>
										<a href='/financial/budget/{{$rb->id}}/expenses' title="Expenses"><i class="fa fa-eye margin-right"></i>&nbsp;&nbsp;Add Expenses</a>
									</li>
									@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a href="{{route('financial.budget.delete',['id'=>$rb->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
									</li>
									@endif
								@else
									@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a href="{{route('financial.budget.restore',['id'=>$rb->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
									</li>
									@endif
								@endif
								</ul>
							</div>
							@include('resource.modal.budget_edit')
						</td>
					</tr>
					@else
					<tr style="border: 1px solid black;  background-color: #f87272;">
						<td>{{$rb->fiscal_year}}</td>
						<td class="text-left">{{$rb->code}}</td>
						<td class="text-left">{{$rb->account_name}}</td>
						<td class="text-right">{{number_format($rb->funding_amount)}}</td>
						<td class="text-right">{{number_format($rb->resources_expenses()->sum('amount'))}}</td>
						<td class="text-right">{{number_format($rb->funding_amount - $rb->resources_expenses()->sum('amount'))}}</td>
						<td>
							@if($rb->deleted_at == null)
								Active
							@else
								Deleted
							@endif
						</td>
						<td>
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
								@if($rb->deleted_at == null)
									@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a data-toggle="modal" href='#editbudget{{$rb->id}}' data-backdrop="static" title="Edit" @click='editData("{{$rb->fiscal_year}}","{{$rb->code}}","{{$rb->account_name}}","{{$rb->funding_amount}}")'><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
									</li>
									@endif
									<li>
										<a href='/financial/budget/{{$rb->id}}/expenses' title="Expenses"><i class="fa fa-eye margin-right"></i>&nbsp;&nbsp;Expenses</a>
									</li>
									@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a href="{{route('financial.budget.delete',['id'=>$rb->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
									</li>
									@endif
								@else
									@if(Session::get('logUser')->usertype != 'Secretary')
									<li>
										<a href="{{route('financial.budget.restore',['id'=>$rb->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
									</li>
									@endif
								@endif
								</ul>
							</div>
							@include('resource.modal.budget_edit')
						</td>
					</tr>
					@endif
					@empty
						<tr><td colspan="8"><p style="text-center">No Available Budgets</p></td></tr>
					@endforelse
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3" style="margin-bottom: 5px">
			@if(Session::get('logUser')->usertype != 'Secretary')
			{{ Form::open(['route'=>'financial.budget.print','method'=>'get', 'class'=>'navbar-form','target'=>'_blank']) }}  
		        <input type="hidden" name="print" value="{{(isset($_GET["search"]) ? $_GET["search"]:"" )}}">
		        <button type="submit" class="btn btn-primary">Print List</button>
		    {{ Form::close() }}
		    @endif
		</div>
		<div class="col-md-9" style="margin-bottom: 5px">
			<div class="text-right">{{$resources_budgets->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
<script>
    var app = new Vue({
        el: '#budget',
       	delimiters: ["[[","]]"],
        data: {
        	fiscal_year_add: '',
	        code_add: '',
	        account_name_add: '',
	        funding_amount_add: '',
	        fiscal_year_edit: '',
	        code_edit: '',
	        account_name_edit: '',
	        funding_amount_edit: '',
            enableAdd: false,
            enableEdit: false,
        },
        methods: {
        	validateAdd(){
        		if(this.fiscal_year_add != '' && this.code_add != '' && this.account_name_add != '' && this.funding_amount_add != ''){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.fiscal_year_edit != '' && this.code_edit != '' && this.account_name_edit != '' && this.funding_amount_edit != ''){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	editData(fiscal_year,code,account_name,funding_amount){
        		this.fiscal_year_edit = fiscal_year;
	            this.code_edit = code;
	            this.account_name_edit = account_name;
	            this.funding_amount_edit = funding_amount;
	            this.validateEdit();
        	}
        }
    });
</script>
@endsection