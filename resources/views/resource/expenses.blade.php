@extends('layouts.app')

@section('title')
	Resources Expenses
@endsection

@section('css')
<!-- internal styles -->
@endsection

@section('body')
<div id="expenses" class="barangay top20">
		<div class="panel panel-default" >
            <div class="panel-heading" style="background-color: #09548e; color: white;  ">
                <h3 class="panel-title">Expenses of {{$resource_budget->account_name}} for {{$resource_budget->fiscal_year}}</h3>     
            </div>
            <div class="panel-body" style="height: 65vh;">
			    <div class="container1">
			    	@if(Session::has('flash_message'))
						<div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}}</div>
					@endif
			    	<div class="col-sm-6">
			    		@if(Session::get('logUser')->usertype != 'Secretary')
						<a class="btn btn-primary" data-toggle="modal" href='#addexpenses' data-backdrop="static" style="height:35px;margin-top:8px;margin-bottom:8px">Add expenses</a>
						@include('resource.modal.expenses_add')
						@endif
			    	</div>
			    	<div class="col-sm-6 text-right">
				    {{ Form::open(['url'=>'/financial/budget/'.$resource_budget->id.'/expenses','method'=>'get', 'class'=>'navbar-form']) }}  
				        <div class="form-group" >
				            <input type="text" class="form-control" placeholder="Search" name="search">
				            <span class="form-group-btn">
				            	<button type="submit" class="btn btn-default">Search</button>
				            </span>
				            <button type="submit" class="btn btn-primary">View All</button>
				        </div>
				    {{ Form::close() }}
					</div>
					<table id="tablePrint" class="table-striped table" style="border: 1px solid black; font-size: 12px;margin-top: 10px">
						<tr style="border: 1px solid black;">
							<th>Date</th>
							<th>Particular</th>
							<th>Amount</th>
							<th>Status</th>
							<th>Remarks</th>
							@if(Session::get('logUser')->usertype != 'Secretary')
							<th>Action</th>	
							@endif
						</tr>

						@forelse($resources_expenses as $re)
						<tr style="border: 1px solid black;">
							<td>{{$re->date}}</td>
							<td class="text-left">{{$re->particular}}</td>
							<td class="text-right">{{number_format($re->amount)}}</td>
							<td>
								@if($re->deleted_at == null)
									Active
								@else
									Deleted
								@endif
							</td>

							<td>
								{{$re->remarks}}
							</td>
							@if(Session::get('logUser')->usertype != 'Secretary')
							<td>
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
									@if($re->deleted_at == null)
										<li>
											<a data-toggle="modal" href='#editexpenses{{$re->id}}' data-backdrop="static" title="Edit" @click='editData("{{$re->date}}","{{$re->particular}}","{{$re->amount}}")'><i class="fa fa-pencil margin-right"></i>&nbsp;&nbsp;Edit</a>
										</li>
										<li>
											<a href="{{route('financial.budget.expenses.delete',['resources_budget_id'=>$resource_budget->id,'id'=>$re->id])}}" title="Delete" class="confirmation"><i class="fa fa-trash margin-right"></i>&nbsp;&nbsp;Delete</a>
										</li>
									@else
										<li>
											<a href="{{route('financial.budget.expenses.restore',['resources_budget_id'=>$resource_budget->id,'id'=>$re->id])}}" title="Restore" class="confirmation"><i class="fa fa-refresh margin-right"></i>&nbsp;&nbsp;Restore</a>
										</li>
									@endif
									</ul>
								</div>
								@include('resource.modal.expenses_edit')
							</td>
							@endif
						</tr>
						@empty
							<tr><td colspan="@if(Session::get('logUser')->usertype != 'Secretary') 6 @else 5 @endif"><p style="text-center">No Available Expenses</p></td></tr>
						@endforelse
					</table>
				</div>
			</div>
		</div>
	<div class="row">
		<div class="col-md-3" style="margin-bottom: 5px">
			@if(Session::get('logUser')->usertype != 'Secretary')
			{{ Form::open(['url'=>'/financial/budget/'.$resource_budget->id.'/expenses/print','method'=>'get', 'class'=>'navbar-form','target'=>'_blank']) }}  
		        <input type="hidden" name="print" value="{{(isset($_GET["search"]) ? $_GET["search"]:"" )}}">
		        <button type="submit" class="btn btn-primary">Print List</button>
		    {{ Form::close() }}
		    @endif
		</div>
		<div class="col-md-9" style="margin-bottom: 5px">
			<div class="text-right">{{$resources_expenses->appends(['search' =>  (isset($_GET["search"]) ? $_GET["search"]:"" )])->links()}}</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<!-- internal scripts -->
{{Html::script('js/vue.min.js')}}
<script>
    var app = new Vue({
        el: '#expenses',
       	delimiters: ["[[","]]"],
        data: {
        	date_add: '',
	        particular_add: '',
	        amount_add: '',
	        date_edit: '',
	        particular_edit: '',
	        amount_edit: '',
	        exceed_expense_add: false,
	        exceed_expense_text_add: '',
	        exceed_expense_edit: false,
	        exceed_expense_text_edit: '',
            enableAdd: false,
            enableEdit: false,
            budget: '',
        },
        mounted(){
        	this.budget = {{$resource_budget->funding_amount - $resource_budget->resources_expenses()->sum('amount')}};
        },
        methods: {
        	validateAdd(){
        		if(this.date_add != '' && this.particular_add != '' && this.amount_add != '' && this.exceed_expense_add == false){
        			this.enableAdd = true;
        		} else {
        			this.enableAdd = false;
        		}
        	},
        	validateEdit(){
        		if(this.date_edit != '' && this.particular_edit != '' && this.amount_edit != ''  && this.exceed_expense_edit == false){
        			this.enableEdit = true;
        		} else {
        			this.enableEdit = false;
        		}
        	},
        	editData(date,particular,amount){
        		this.date_edit = date;
	            this.particular_edit = particular;
	            this.amount_edit = amount;
	            this.validateEdit();
        	},
        	checkExpenseAdd(){
				let computedAmount = 0.0;
				const amountFactor = this.particular_add == 'Response'? 0.30 : 0.70;
				computedAmount = this.budget * amountFactor;
				if(this.amount_add > computedAmount){
        			this.exceed_expense_add = true;
        			this.exceed_expense_text_add = '<span style="color:red">Expense Cannot Exceed Budget.</span>';
        		} else {
					this.exceed_expense_add = false;
        			this.exceed_expense_text_add = '';
        		}
        		this.validateAdd();
        	},
        	checkExpenseEdit(){
				let computedAmount = 0.0;
				const amountFactor = this.particular_edit == 'Response'? 0.30 : 0.70;
				computedAmount = this.budget * amountFactor;
        		if(this.amount_edit > computedAmount){
        			this.exceed_expense_edit = true;
        			this.exceed_expense_text_edit = '<span style="color:red">Expense Cannot Exceed Budget.</span>';
        		} else {
					this.exceed_expense_edit = false;
        			this.exceed_expense_text_edit = '';
        		}
        		this.validateEdit();
        	}
        }
    });
</script>
@endsection