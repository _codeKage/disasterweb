@extends('layouts.app')

@section('title')
    Education
@endsection

@section('css')
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
@endsection

@section('body')
    <!--Container-->
    <!--Form-->
    <div id="school" class="barangay top20">
        {!! Form::open(['route'=>'education.school.store','files'=>true])!!}
        <div class="panel panel-default" >
            <div  class="panel-heading" style="background-color: #1b3c5b; color: white;">
                <h3 class="panel-title">SCHOOL PROFILE</h3>
            </div>
            <div class="panel-body" style="height: 65vh;">
                @if(Session::has('flash_message'))
                    <div class="alert alert-success" style="margin-top: 10px">{{Session::get('flash_message')}} </div>
                @endif
                <div class="form-horizontal">
                    <div class="col-lg-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label col-lg-2 col-md-2 col-sm-2 col-xs-2" for="name">
                                Name:
                            </label>
                            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                <div class="input-group col-lg-8 col-md-8 col-sm-9 col-xs-12">
                                    <div class="input-group input-group-btn">
                                        <input type="text" class="form-control col-xs-push-1 col-sm-push-0" placeholder="Enter name" v-model="school_name" id="school_name" name="school_name"  @change="validateAdd">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label col-lg-2 col-md-2 col-sm-2 col-xs-2" for="barangay">
                                Barangay:
                            </label>
                            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6">
                                <div class="input-group col-lg-8 col-md-8 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control col-xs-push-1 col-sm-push-0" id="barangay" placeholder="Enter barangay" v-model="barangay" id="barangay" name="barangay" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label col-lg-2 col-md-2 col-sm-2 col-xs-2" for="purok">Purok:</label>
                            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6 ">
                                <div class="input-group col-lg-8 col-md-8 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control col-xs-push-1 col-sm-push-0" v-model="purok" id="purok" name="purok" placeholder="Enter purok" @change="validateAdd">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label col-lg-2 col-md-2 col-sm-2 col-xs-2" for="schoolyear">School Year:</label>
                            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-6 ">
                                <div class="input-group col-lg-8 col-md-8 col-sm-9 col-xs-12">
                                    <input type="number" class="form-control col-xs-push-1 col-sm-push-0" v-model="school_year" id="school_year" name="school_year" placeholder="Enter School Year" @change="validateAdd">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label col-lg-2 col-md-2 col-sm-2 col-xs-2" for="category">Category:</label>
                            <div class="input-group">
                                <div class="col-sm-10 col-xs-push-2 col-sm-push-0">
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" id="elem" name="elem" value="0">
                                            <input id="elem" value="1" v-model="elem" type="checkbox" name="elem" @change="validateAdd" checked>Elementary
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" id="junior" name="junior" value="0">
                                            <input id="junior" value="1" v-model="junior" type="checkbox" id="junior" value="1" name="junior" @change="validateAdd">Junior High School
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="hidden" id="senior" name="senior" value="0">
                                            <input id="senior" value="1" v-model="senior" type="checkbox" id="senior" value="1" name="senior" @change="validateAdd">Senior High School
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                </div>
                <div id="elemForm" class="container1" v-if="elem=='1'">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ELEMENTARY</th>
                            <th class="text-center">Maximum Capacity</th>
                            <th class="text-center">Number of Enrolled</th>
                            <th class="text-center">Available Slots</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input id="elemGrades.grade1.label" name="elemGrades.grade1.label" v-model="elemGrades.grade1.label" hidden>
                                    Grade 1
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade1.capacity" name="elemGrades.grade1.capacity" v-model="elemGrades.grade1.capacity" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade1,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade1.enrollees" name="elemGrades.grade1.enrollees" v-model="elemGrades.grade1.enrollees" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade1,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade1.availSlot" name="elemGrades.grade1.availSlot" v-model="elemGrades.grade1.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="elemGrades.grade2.label" name="elemGrades.grade2.label" v-model="elemGrades.grade2.label" hidden>
                                    Grade 2
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade2.capacity" name="elemGrades.grade2.capacity" v-model="elemGrades.grade2.capacity" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade2,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade2.enrollees" name="elemGrades.grade2.enrollees" v-model="elemGrades.grade2.enrollees" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade2,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="slots"
                                        id="elemGrades.grade2.availSlot" name="elemGrades.grade2.availSlot" v-model="elemGrades.grade2.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="elemGrades.grade3.label" name="elemGrades.grade3.label" v-model="elemGrades.grade3.label" hidden>
                                    Grade 3
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade3.capacity" name="elemGrades.grade3.capacity" v-model="elemGrades.grade3.capacity" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade3,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade3.enrollees" name="elemGrades.grade3.enrollees" v-model="elemGrades.grade3.enrollees" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade3,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade3.availSlot" name="elemGrades.grade3.availSlot" v-model="elemGrades.grade3.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="elemGrades.grade4.label" name="elemGrades.grade4.label" v-model="elemGrades.grade4.label" hidden>
                                    Grade 4
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade4.capacity" name="elemGrades.grade4.capacity" v-model="elemGrades.grade4.capacity" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade4,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade4.enrollees" name="elemGrades.grade4.enrollees" v-model="elemGrades.grade4.enrollees" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade4,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade4.availSlot" name="elemGrades.grade4.availSlot" v-model="elemGrades.grade4.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="elemGrades.grade5.label" name="elemGrades.grade5.label" v-model="elemGrades.grade5.label" hidden>
                                    Grade 5
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade5.capacity" name="elemGrades.grade5.capacity" v-model="elemGrades.grade5.capacity" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade5,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade5.enrollees" name="elemGrades.grade5.enrollees" v-model="elemGrades.grade5.enrollees" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade5,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade5.availSlot" name="elemGrades.grade5.availSlot" v-model="elemGrades.grade5.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="elemGrades.grade6.label" name="elemGrades.grade6.label" v-model="elemGrades.grade6.label" hidden>
                                    Grade 6
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade6.capacity" name="elemGrades.grade6.capacity" v-model="elemGrades.grade6.capacity" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade6,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade6.enrollees" name="elemGrades.grade6.enrollees" v-model="elemGrades.grade6.enrollees" style="text-align:center;" @input="calculateSlots($event,elemGrades.grade6,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade6.availSlot" name="elemGrades.grade6.availSlot" v-model="elemGrades.grade6.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
                <div id="juniorForm" class="container1" v-if="junior=='1'">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>JUNIOR HIGH</th>
                            <th class="text-center">Maximum Capacity</th>
                            <th class="text-center">Number of Enrolled</th>
                            <th class="text-center">Available Slots</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input id="juniorGrades.grade7.label" name="juniorGrades.grade7.label" v-model="juniorGrades.grade7.label" hidden>
                                    Grade 7
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade7.capacity" name="juniorGrades.grade7.capacity" v-model="juniorGrades.grade7.capacity" style="text-align:center;" @input="calculateSlots($event,juniorGrades.grade7,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade7.enrollees" name="juniorGrades.grade7.enrollees" v-model="juniorGrades.grade7.enrollees" style="text-align:center;" @input="calculateSlots($event,juniorGrades.grade7,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="elemGrades.grade7.availSlot" name="elemGrades.grade7.availSlot" v-model="juniorGrades.grade7.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="juniorGrades.grade8.label" name="juniorGrades.grade8.label" v-model="juniorGrades.grade8.label" hidden>
                                    Grade 8
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade8.capacity" name="juniorGrades.grade8.capacity" v-model="juniorGrades.grade8.capacity" style="text-align:center;" @input="calculateSlots($event,juniorGrades.grade8,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade8.enrollees" name="juniorGrades.grade8.enrollees" v-model="juniorGrades.grade8.enrollees" style="text-align:center;" @input="calculateSlots($event,juniorGrades.grade8,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade8.availSlot" name="juniorGrades.grade8.availSlot" v-model="juniorGrades.grade8.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="juniorGrades.grade9.label" name="juniorGrades.grade9.label" v-model="juniorGrades.grade9.label" hidden>
                                    Grade 9
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade9.capacity" name="juniorGrades.grade9.capacity" v-model="juniorGrades.grade9.capacity" style="text-align:center;" @input="calculateSlots($event,juniorGrades.grade9,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade9.enrollees" name="juniorGrades.grade9.enrollees" v-model="juniorGrades.grade9.enrollees" style="text-align:center;" @input="calculateSlots($event,juniorGrades.grade9,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade9.availSlot" name="juniorGrades.grade9.availSlot" v-model="juniorGrades.grade9.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="juniorGrades.grade10.label" name="juniorGrades.grade10.label" v-model="juniorGrades.grade10.label" hidden>
                                    Grade 10
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade10.capacity" name="juniorGrades.grade10.capacity" v-model="juniorGrades.grade10.capacity" style="text-align:center;" @input="calculateSlots($event,juniorGrades.grade10,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade10.enrollees" name="juniorGrades.grade10.enrollees" v-model="juniorGrades.grade10.enrollees" style="text-align:center;" @input="calculateSlots($event,juniorGrades.grade10,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="juniorGrades.grade10.availSlot" name="juniorGrades.grade10.availSlot" v-model="juniorGrades.grade10.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="seniorForm" class="container1" v-if="senior=='1'">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>SENIOR HIGH</th>
                            <th class="text-center">Maximum Capacity</th>
                            <th class="text-center">Number of Enrolled</th>
                            <th class="text-center">Available Slots</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input id="seniorGrades.stem11.label" name="seniorGrades.stem11.label" v-model="seniorGrades.stem11.label" hidden>
                                    STEM-11
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.stem11.capacity" name="seniorGrades.stem11.capacity" v-model="seniorGrades.stem11.capacity" style="text-align:center;" @input="calculateSlots($event,seniorGrades.stem11,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.stem11.enrollees" name="seniorGrades.stem11.enrollees" v-model="seniorGrades.stem11.enrollees" style="text-align:center;" @input="calculateSlots($event,seniorGrades.stem11,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.stem11.availSlot" name="seniorGrades.stem11.availSlot" v-model="seniorGrades.stem11.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="seniorGrades.ga11.label" name="seniorGrades.ga11.label" v-model="seniorGrades.ga11.label" hidden>
                                    GA-11
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.ga11.capacity" name="seniorGrades.ga11.capacity" v-model="seniorGrades.ga11.capacity" style="text-align:center;" @input="calculateSlots($event,seniorGrades.ga11,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.ga11.enrollees" name="seniorGrades.ga11.enrollees" v-model="seniorGrades.ga11.enrollees" style="text-align:center;" @input="calculateSlots($event,seniorGrades.ga11,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.ga11.availSlot" name="seniorGrades.ga11.aailSlot" v-model="seniorGrades.ga11.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="seniorGrades.humms11.label" name="seniorGrades.humms11.label" v-model="seniorGrades.humms11.label" hidden>
                                    HUMM-11
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.humms11.capacity" name="seniorGrades.humms11.capacity" v-model="seniorGrades.humms11.capacity" style="text-align:center;" @input="calculateSlots($event,seniorGrades.humms11,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.humms11.enrollees" name="seniorGrades.humms11.enrollees" v-model="seniorGrades.humms11.enrollees" style="text-align:center;" @input="calculateSlots($event,seniorGrades.humms11,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.humms11.availSlot" name="seniorGrades.humms11.availSlot" v-model="seniorGrades.humms11.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="seniorGrades.abm11.label" name="seniorGrades.abm11.label" v-model="seniorGrades.abm11.label" hidden>
                                    ABM-11
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.abm11.capacity" name="seniorGrades.abm11.capacity" v-model="seniorGrades.abm11.capacity" style="text-align:center;" @input="calculateSlots($event,seniorGrades.abm11,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.abm11.enrollees" name="seniorGrades.abm11.enrollees" v-model="seniorGrades.abm11.enrollees" style="text-align:center;" @input="calculateSlots($event,seniorGrades.abm11,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.abm11.availSlot" name="seniorGrades.abm11.availSlot" v-model="seniorGrades.abm11.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="seniorGrades.stem12.label" name="seniorGrades.stem12.label" v-model="seniorGrades.stem12.label" hidden>
                                    STEM-12
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.stem12.capacity" name="seniorGrades.stem12.capacity" v-model="seniorGrades.stem12.capacity" style="text-align:center;" @input="calculateSlots($event,seniorGrades.stem12,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.stem12.enrollees" name="seniorGrades.stem12.enrollees" v-model="seniorGrades.stem12.enrollees" style="text-align:center;" @input="calculateSlots($event,seniorGrades.stem12,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.stem12.availSlot" name="seniorGrades.stem12.availSlot" v-model="seniorGrades.stem12.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="seniorGrades.ga12.label" name="seniorGrades.ga12.label" v-model="seniorGrades.ga12.label" hidden>
                                    GA-12
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.ga12.capacity" name="seniorGrades.ga12.capacity" v-model="seniorGrades.ga12.capacity" style="text-align:center;" @input="calculateSlots($event,seniorGrades.ga12,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.ga12.enrollees" name="seniorGrades.ga12.enrollees" v-model="seniorGrades.ga12.enrollees" style="text-align:center;" @input="calculateSlots($event,seniorGrades.ga12,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.ga12.availSlot" name="seniorGrades.ga12.availSlot" v-model="seniorGrades.ga12.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="seniorGrades.humms12.label" name="seniorGrades.humms12.label" v-model="seniorGrades.humms12.label" hidden>
                                    HUMM-12
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.humms12.capacity" name="seniorGrades.humms12.capacity" v-model="seniorGrades.humms12.capacity" style="text-align:center;" @input="calculateSlots($event,seniorGrades.humms12,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.humms12.enrollees" name="seniorGrades.humms12.enrollees" v-model="seniorGrades.humms12.enrollees" style="text-align:center;" @input="calculateSlots($event,seniorGrades.humms12,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.humms12.availSlot" name="seniorGrades.humms12.availSlot" v-model="seniorGrades.humms12.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="seniorGrades.abm12.label" name="seniorGrades.abm12.label" v-model="seniorGrades.abm12.label" hidden>
                                    ABM-12
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.abm12.capacity" name="seniorGrades.abm12.capacity" v-model="seniorGrades.abm12.capacity" style="text-align:center;" @input="calculateSlots($event,seniorGrades.abm12,'enrollees')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.abm12.enrollees" name="seniorGrades.abm12.enrollees" v-model="seniorGrades.abm12.enrollees" style="text-align:center;" @input="calculateSlots($event,seniorGrades.abm12,'capacity')">
                                </td>
                                <td>
                                    <input type="number" class="form-control" id="seniorGrades.abm12.availSlot" name="seniorGrades.abm12.availSlot" v-model="seniorGrades.abm12.availSlot" style="text-align:center;" disabled>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> <!--End Panel Body -->
        </div>
        <div class="container" style="float: right; margin-top: 3px; margin-right: 5%;">
            <div class="inline-group inline-group-btn" style="float: right;">
                <button type="submit" class="btn btn-primary" style="margin-right: 5px;" v-model="submit" value="update" :disabled="!isButtonEnabled">Save</button>
            </div>
        </div>
    </div> <!--End Container-->

    <!-- Modal Save -->
    <div class="modal fade" id="modalSave" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p class="text-center">Data has been saved.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Update -->
    <div class="modal fade" id="modalUpdate" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <p class="text-center">Data has been updated.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <!-- internal scripts -->
    {{Html::script('js/vue.min.js')}}
    {{Html::script('js/axios.min.js')}}

    <script>
        var app = new Vue({
            el: '#school',
            delimiters: ["[[","]]"],
            data: {
                school_name: '',
                barangay:'{{Session::get('barangayAbout')->barangay}}',
                purok: '',
                school_year: '',
                elem:'',
                elemGrades:{
                    grade1:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'Grade 1'
                    },
                    grade2:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'Grade 2'
                    },
                    grade3:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'Grade 3'
                    },
                    grade4:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'Grade 4'
                    },
                    grade5:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'Grade 5'
                    },
                    grade6:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'Grade 6'
                    }
                },
                junior:'',
                juniorGrades:{
                    grade7:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'Grade 7'
                    },
                    grade8:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'Grade 8'
                    },
                    grade9:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'Grade 9'
                    },
                    grade10:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'Grade 10'
                    }
                },
                senior:'',
                seniorGrades:{
                    stem11:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'STEM-11'
                    },
                    ga11:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'GA-11'
                    },
                    humms11:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'HUMMS-11'
                    },
                    abm11:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'ABM-11'
                    },
                    stem12:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'STEM-12'
                    },
                    ga12:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'GA-12'
                    },
                    humms12:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'HUMMS-12'
                    },
                    abm12:{
                        capacity: '',
                        enrollees: '',
                        availSlot: 0,
                        label: 'ABM-12'
                    }
                },
                submit:'',
                isButtonEnabled: false
            },
            methods: {
                validateAdd(){
                    console.log(this.school_name)
                    this.isButtonEnabled = this.school_name 
                        && this.purok && this.school_year && this.barangay
                        && (this.elem!='0' || this.junior!='0' || this.senior!='0');
                },
                calculateSlots(currentInputevent,currentData,subtrahend){
                    console.log(currentInputevent);
                    console.log(currentData[subtrahend]);
                    //TODO: do not allow negative slots value
                    let difference = 0;
                    if(subtrahend==='enrollees'){
                        difference = currentInputevent.target.value - currentData[subtrahend];
                    }else{
                        difference =  currentData[subtrahend] - currentInputevent.target.value;
                    }
                    currentData.availSlot = difference;
                }
            }
        });
    </script>
@endsection