@extends('layouts.app')

@section('title')
	Education
@endsection

@section('css')
	<!-- jQuery library -->
	<script src="https://aj	ax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style>
		.panel-heading{
			background-color: #1b3c5b!important;
			color: white !important;
		}
	</style>
	{{Html::style('awesomplete/awesomplete.css')}}
@endsection

@section('body')
<div id="household" class="barangay top20">
    <div class="panel panel-default" >
        <div class="panel-heading" style="color: black;  ">
            <h3 class="panel-title" ><b>SCHOOL</b></h3>
        </div>
        <div class="panel-body"  style="height: 65vh;">
            <div class="container1">
                @if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary' || Session::get('logUser')->usertype == 'Teacher')
					<!-- <button type="button" class="btn btn-primary" style="height:35px"> -->
                    <a href="{{route('education.school.add')}}" class="btn btn-primary">Add School</a>
					<!-- </button> -->
				@endif
                <table class="table-striped table" style="border: 1px solid black; font-size: 14px;margin-top: 10px">
					<tr style="border: 1px solid black;">
						<th>School Name</th>
						<th>Purok</th>
						@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary' || Session::get('logUser')->usertype == 'Teacher')
						<th>Actions</th> 
						@endif	
					</tr>
					@forelse($school as $profile)
						@if($profile->deleted_at == null)
					<tr style="border: 1px solid black;">
						<td>{{$profile->school_name}}</td>
						<td>{{$profile->purok}}</td>

						<td>
							<div class="dropdown" style="display: flex; text-align: center;">
								<a href="/education/school/search/{{$profile->school_name}}" class="btn btn-primary" style="margin-right: 2rem; margin-left: 37%">Edit</a>
								{!! Form::open(['route'=>'education.school.remove']) !!}
								<button class="btn btn-danger" type="submit">Delete</button>
								<input type="hidden" name="id" value="{{$profile->id}}">
								{!! Form::close() !!}
							</div>
						</td>
					</tr>
						@endif
					@empty
						<tr><td colspan="@if(Session::get('logUser')->usertype == 'Chairman' || Session::get('logUser')->usertype == 'Secretary') 8 @else 7 @endif"><p style="text-center">No Available School</p></td></tr>
					@endforelse
				</table>
            </div>
        </div>
    </div>
</div>

@endsection

