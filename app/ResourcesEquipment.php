<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourcesEquipment extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'resources_equipments';

    public function resources_equipments_histories(){
    	return $this->hasMany('App\ResourcesEquipmentHistory','resources_equipment_id');
    }
}
