<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});


Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});


Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});


Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Route::get('/', function () {
	return redirect()->route('login');
});

//login
Route::get('login','AdminController@login')->name('login');
Route::post('login/attempt','AdminController@loginAttempt')->name('login.attempt');

//logout
Route::get('logout','AdminController@logout')->name('logout');

//Barangay Profile
Route::group(['middleware' => 'checkAllowAccess','prefix' => 'barangay_profile'],function(){
	//about
	Route::get('/about','BarangayController@about')->name('barangayprofile.about');
	Route::post('/about/update', 'BarangayController@aboutUpdate')->name('barangayprofile.about.update');

	//calendar_events
	Route::get('/calendar_events','BarangayController@calendarEvents')->name('barangayprofile.calendar_events');
	Route::post('/calendar_events/store', 'BarangayController@calendarEventsStore')->name('barangayprofile.calendar_events.store');
	Route::post('/calendar_events/update','BarangayController@calendarEventsUpdate')->name('barangayprofile.calendar_events.update');
	Route::get('/calendar_events/{id}/delete','BarangayController@calendarEventsDelete')->name('barangayprofile.calendar_events.delete');
	Route::get('/calendar_events/{id}/restore','BarangayController@calendarEventsRestore')->name('barangayprofile.calendar_events.restore');

	//achievements
	Route::get('/achievements','BarangayController@achievement')->name('barangayprofile.achievement');
	Route::post('/achievements/store', 'BarangayController@achievementStore')->name('barangayprofile.achievement.store');
	Route::post('/achievements/update','BarangayController@achievementUpdate')->name('barangayprofile.achievement.update');
	Route::get('/achievements/{id}/delete','BarangayController@achievementDelete')->name('barangayprofile.achievement.delete');
	Route::get('/achievements/{id}/restore','BarangayController@achievementRestore')->name('barangayprofile.achievement.restore');

	//plans
	Route::get('/plans','BarangayController@plans')->name('barangayprofile.plans');
	Route::post('/plans/store', 'BarangayController@plansStore')->name('barangayprofile.plans.store');
	Route::post('/plans/update','BarangayController@plansUpdate')->name('barangayprofile.plans.update');
	Route::get('/plans/{id}/delete','BarangayController@plansDelete')->name('barangayprofile.plans.delete');
	Route::get('/plans/{id}/restore','BarangayController@plansRestore')->name('barangayprofile.plans.restore');
});

//Resident Profile
Route::group(['middleware' => 'checkAllowAccess','prefix' => 'resident_profile'],function(){
	//household
	Route::get('/household','ResidentProfileController@household')->name('residentprofile.household');
	Route::get('/household/search','ResidentProfileController@householdSearch')->name('residentprofile.household.search');
	Route::post('/household/store', 'ResidentProfileController@householdStore')->name('residentprofile.household.store');
	Route::post('/household/update','ResidentProfileController@householdUpdate')->name('residentprofile.household.update');
	Route::get('/household/{id}/delete','ResidentProfileController@householdDelete')->name('residentprofile.household.delete');
	Route::get('/household/{id}/restore','ResidentProfileController@householdRestore')->name('residentprofile.household.restore');
	Route::get('household/survey/add/{id}','ResidentProfileController@householdSurveyAdd')->name('resident.household.survey.add');
	Route::get('household/survey/edit/{id}','ResidentProfileController@householdSurveyEdit')->name('resident.household.survey.edit');
	Route::post('/household/survey/store','ResidentProfileController@householdSurveyStore')->name('residentprofile.household.survey.store');
	Route::post('/household/survey/update','ResidentProfileController@householdSurveyUpdate')->name('residentprofile.household.survey.update');

	// Route:post('/household/survey','');

	//household_member
	Route::get('/household/member','ResidentProfileController@householdMember')->name('residentprofile.household.member');
	Route::get('/household/member/{id}','ResidentProfileController@getMembers')->name('residentprofile.household.getmembers');
	Route::post('/household/member/store', 'ResidentProfileController@householdMemberStore')->name('residentprofile.household.member.store');
	Route::post('/household/member/storeloop', 'ResidentProfileController@householdMemberStoreLoop')->name('residentprofile.household.member.storeloop');
	Route::post('/household/member/update','ResidentProfileController@householdMemberUpdate')->name('residentprofile.household.member.update');
	Route::get('/household/member/{id}/delete','ResidentProfileController@householdMemberDelete')->name('residentprofile.household.member.delete');
	Route::get('/household/member/{id}/restore','ResidentProfileController@householdMemberRestore')->name('residentprofile.household.member.restore');
});

//Disaster Admin
Route::group(['middleware' => 'checkAllowAccess','prefix' => 'disaster_admin'],function(){
	//advisory
	Route::get('/advisory','DisasterAdminController@advisory')->name('disasteradmin.advisory');
	Route::post('/advisory/store', 'DisasterAdminController@advisoryStore')->name('disasteradmin.advisory.store');
	Route::post('/advisory/update','DisasterAdminController@advisoryUpdate')->name('disasteradmin.advisory.update');
	Route::get('/advisory/{id}/delete','DisasterAdminController@advisoryDelete')->name('disasteradmin.advisory.delete');
	Route::post('/advisory/send','DisasterAdminController@advisorySend')->name('disasteradmin.advisory.send');
	Route::post('/advisory/resend','DisasterAdminController@resendMessage')->name('disasteradmin.advisory.resend');
	Route::get('/advisory/delete','DisasterAdminController@advisoryDeletePerYear')->name('disasteradmin.advisory.batchDelete');
	
	//safety_tips
	Route::get('/safety_tips','DisasterAdminController@safetyTips')->name('disasteradmin.safety_tips');
	Route::post('/safety_tips/store', 'DisasterAdminController@safetyTipsStore')->name('disasteradmin.safety_tips.store');
	Route::post('/safety_tips/update','DisasterAdminController@safetyTipsUpdate')->name('disasteradmin.safety_tips.update');
	Route::get('/safety_tips/{id}/delete','DisasterAdminController@safetyTipsDelete')->name('disasteradmin.safety_tips.delete');
	Route::get('/safety_tips/{id}/restore','DisasterAdminController@safetyTipsRestore')->name('disasteradmin.safety_tips.restore');

	//make_plan
	Route::get('/make_plan','DisasterAdminController@makePlan')->name('disasteradmin.make_plan');
	Route::post('/make_plan/identify_escape_routes','DisasterAdminController@identifyEscapeRoutes')->name('disasteradmin.make_plan.identify_escape_routes');
	Route::post('/make_plan/establish_meeting_places','DisasterAdminController@establishMeetingPlaces')->name('disasteradmin.make_plan.establish_meeting_places');
	Route::post('/make_plan/plan_for_children','DisasterAdminController@planForChildren')->name('disasteradmin.make_plan.plan_for_children');
	Route::post('/make_plan/address_any_special_health_needs','DisasterAdminController@addressAnySpecialHealthNeeds')->name('disasteradmin.make_plan.address_any_special_health_needs');
	Route::post('/make_plan/plan_for_pets','DisasterAdminController@planForPets')->name('disasteradmin.make_plan.plan_for_pets');
	Route::post('/make_plan/plan_for_specific_risks','DisasterAdminController@planForSpecificRisks')->name('disasteradmin.make_plan.plan_for_specific_risks');
	Route::post('/make_plan/record_emergency_contact_information','DisasterAdminController@recordEmergencyContactInformation')->name('disasteradmin.make_plan.record_emergency_contact_information');
	Route::post('/make_plan/complete_safe_home_instructions','DisasterAdminController@completeSafeHomeInstructions')->name('disasteradmin.make_plan.complete_safe_home_instructions');
	Route::post('/make_plan/summary','DisasterAdminController@summary')->name('disasteradmin.make_plan.summary');


	//preparedness_kit
	Route::get('/preparedness_kit','DisasterAdminController@preparednessKit')->name('disasteradmin.preparedness_kit');
	Route::post('/preparedness_kit/update','DisasterAdminController@preparednessKitUpdate')->name('disasteradmin.preparedness_kit.update');

	//bdrrmc_team
	Route::get('/bdrrmc_team','DisasterAdminController@bdrrmcTeam')->name('disasteradmin.bdrrmc_team');
	Route::post('/bdrrmc_team/store', 'DisasterAdminController@bdrrmcTeamStore')->name('disasteradmin.bdrrmc_team.store');
	Route::post('/bdrrmc_team/update','DisasterAdminController@bdrrmcTeamUpdate')->name('disasteradmin.bdrrmc_team.update');
	Route::get('/bdrrmc_team/{id}/delete','DisasterAdminController@bdrrmcTeamDelete')->name('disasteradmin.bdrrmc_team.delete');
	Route::get('/bdrrmc_team/{id}/restore','DisasterAdminController@bdrrmcTeamRestore')->name('disasteradmin.bdrrmc_team.restore');

	//bdrrmc_team_members
	Route::get('/bdrrmc_team_member','DisasterAdminController@bdrrmcTeamMember')->name('disasteradmin.bdrrmc_team_member');
	Route::post('/bdrrmc_team_member/store', 'DisasterAdminController@bdrrmcTeamMemberStore')->name('disasteradmin.bdrrmc_team_member.store');
	Route::post('/bdrrmc_team_member/update','DisasterAdminController@bdrrmcTeamMemberUpdate')->name('disasteradmin.bdrrmc_team_member.update');
	Route::get('/bdrrmc_team_member/{id}/delete','DisasterAdminController@bdrrmcTeamMemberDelete')->name('disasteradmin.bdrrmc_team_member.delete');
	Route::get('/bdrrmc_team_member/{id}/restore','DisasterAdminController@bdrrmcTeamMemberRestore')->name('disasteradmin.bdrrmc_team_member.restore');
	Route::get('bdrrmc_team_member/print', 'DisasterAdminController@bdrrmcTeamMemberPrint')->name('disasteradmin.bdrrmc_team_member.print');

	//evacuation_center
	Route::get('/evacuation_center','DisasterAdminController@evacuationCenter')->name('disasteradmin.evacuation_center');
	Route::post('/evacuation_center/store', 'DisasterAdminController@evacuationCenterStore')->name('disasteradmin.evacuation_center.store');
	Route::post('/evacuation_center/update','DisasterAdminController@evacuationCenterUpdate')->name('disasteradmin.evacuation_center.update');
	Route::get('/evacuation_center/{id}/delete','DisasterAdminController@evacuationCenterDelete')->name('disasteradmin.evacuation_center.delete');
	Route::get('/evacuation_center/{id}/restore','DisasterAdminController@evacuationCenterRestore')->name('disasteradmin.evacuation_center.restore');
});

//resources
Route::group(['middleware' => 'checkAllowAccess','prefix' => 'financial'],function(){
	//supplies
	Route::get('/supplies','ResourcesController@supplies')->name('resources.supplies');
	Route::post('/supplies/store', 'ResourcesController@suppliesStore')->name('resources.supplies.store');
	Route::post('/supplies/stocks/store', 'ResourcesController@suppliesStocksStore')->name('resources.supplies.stocks.store');
	Route::post('/supplies/distributes/store', 'ResourcesController@suppliesDistributesStore')->name('resources.supplies.distributes.store');
	Route::post('/supplies/update','ResourcesController@suppliesUpdate')->name('resources.supplies.update');
	Route::get('/supplies/{id}/delete','ResourcesController@suppliesDelete')->name('resources.supplies.delete');
	Route::get('/supplies/{id}/restore','ResourcesController@suppliesRestore')->name('resources.supplies.restore');
	Route::get('/supplies/print', 'ResourcesController@suppliesPrint')->name('resources.supplies.print');
	Route::get('/supplies/history/{id}','ResourcesController@suppliesHistory')->name('resources.supplies.history');
	Route::get('/supplies/history/{id}/print', 'ResourcesController@suppliesHistoryPrint')->name('resources.supplies.history.print');

	//equipment
	Route::get('/equipments','ResourcesController@equipments')->name('resources.equipments');
	Route::post('/equipments/store', 'ResourcesController@equipmentsStore')->name('resources.equipments.store');
	Route::post('/equipments/stocks/store', 'ResourcesController@equipmentsStocksStore')->name('resources.equipments.stocks.store');
	Route::post('/equipments/remove/store', 'ResourcesController@equipmentsRemoveStore')->name('resources.equipments.remove.store');
	Route::post('/equipments/update','ResourcesController@equipmentsUpdate')->name('resources.equipments.update');
	Route::get('/equipments/{id}/delete','ResourcesController@equipmentsDelete')->name('resources.equipments.delete');
	Route::get('/equipments/{id}/restore','ResourcesController@equipmentsRestore')->name('resources.equipments.restore');
	Route::get('/equipments/print', 'ResourcesController@equipmentsPrint')->name('resources.equipments.print');
	Route::get('/equipments/history/{id}','ResourcesController@equipmentsHistory')->name('resources.equipments.history');
	Route::get('/equipments/history/{id}/print', 'ResourcesController@equipmentsHistoryPrint')->name('resources.equipments.history.print');

	//budget
	Route::get('/budget','ResourcesController@budget')->name('financial.budget');
	Route::post('/budget/store', 'ResourcesController@budgetStore')->name('financial.budget.store');
	Route::post('/budget/update','ResourcesController@budgetUpdate')->name('financial.budget.update');
	Route::get('/budget/{id}/delete','ResourcesController@budgetDelete')->name('financial.budget.delete');
	Route::get('/budget/{id}/restore','ResourcesController@budgetRestore')->name('financial.budget.restore');
	Route::get('/budget/print', 'ResourcesController@budgetPrint')->name('financial.budget.print');
	Route::get('/budget/{resources_budget_id}/expenses','ResourcesController@budgetExpenses')->name('financial.budget.expenses');
	Route::post('/budget/{resources_budget_id}/expenses/store', 'ResourcesController@budgetExpensesStore')->name('financial.budget.expenses.store');
	Route::post('/budget/{resources_budget_id}/expenses/update','ResourcesController@budgetExpensesUpdate')->name('financial.budget.expenses.update');
	Route::get('/budget/{resources_budget_id}/expenses/{id}/delete','ResourcesController@budgetExpensesDelete')->name('financial.budget.expenses.delete');
	Route::get('/budget/{resources_budget_id}/expenses/{id}/restore','ResourcesController@budgetExpensesRestore')->name('financial.budget.expenses.restore');
	Route::get('/budget/{resources_budget_id}/expenses/print', 'ResourcesController@budgetExpensesPrint')->name('financial.budget.expenses.print');
});


//BARANGAY CRUD
Route::group(['middleware' => 'checkAllowAccess','prefix' => 'admin'],function(){
	Route::get('/barangay/{id}/delete','AdminController@deleteBarangay')->name('admin.delete.brgy');
	Route::get('/barangay/{id}/add_chairman','AdminController@createChairman')->name('admin.create.brgy.chairman');
	Route::post('/barangay/{id}/store_chairman','AdminController@storeChairman')->name('admin.store.brgy.chairman');
	Route::get('/barangay','AdminController@listBrgy')->name('admin.brgy');
	Route::get('/barangay/create','AdminController@createBrgy')->name('admin.create.brgy');
	Route::post('/barangay/store','AdminController@storeBrgy')->name('admin.store.brgy');

	Route::get('/logout','AdminController@adminLogout')->name('admin.logout');
});

//Reports
Route::group(['middleware' => 'checkAllowAccess','prefix' => 'reports'],function(){
	//household_purok
	Route::get('/household_purok','ReportsController@householdPurok')->name('reports.household_purok');
	Route::get('/household_purok/print', 'ReportsController@householdPurokPrint')->name('reports.household_purok.print');

	//household_member_status
	Route::get('/household_member_status','ReportsController@householdMemberStatus')->name('reports.household_member_status');
	Route::get('/household_member_status/print', 'ReportsController@householdMemberStatusPrint')->name('reports.household_member_status.print');

	//resident_purok
	Route::get('/resident_purok','ReportsController@residentPurok')->name('reports.resident_purok');
	Route::get('/resident_purok/print', 'ReportsController@residentPurokPrint')->name('reports.resident_purok.print');

	//household_nuclear_family
	Route::get('/household_nuclear_family','ReportsController@householdNuclearFamily')->name('reports.household_nuclear_family');
	Route::get('/household_nuclear_family/print', 'ReportsController@householdNuclearFamilyPrint')->name('reports.household_nuclear_family.print');

	//barangay_profile
	Route::get('/barangay_profile','ReportsController@barangayProfile')->name('reports.barangay_profile');
	Route::get('/barangay_profile/general_information/print', 'ReportsController@barangayProfileGeneralInformationPrint')->name('reports.barangay_profile.general_information.print');
	Route::get('/barangay_profile/demography/print', 'ReportsController@barangayProfileDemographyPrint')->name('reports.barangay_profile.demography.print');
	Route::get('/barangay_profile/socio-economic_profile/print', 'ReportsController@barangayProfileSocioEconomicProfilePrint')->name('reports.barangay_profile.economic_profile.print');
	Route::get('/barangay_profile/health/print', 'ReportsController@barangayProfileHealthPrint')->name('reports.barangay_profile.health.print');
	Route::get('/barangay_profile/water_sanitation/print', 'ReportsController@barangayProfileWaterSanitationPrint')->name('reports.barangay_profile.water_sanitation.print');
	Route::get('/barangay_profile/shelter/print', 'ReportsController@barangayProfileShelterPrint')->name('reports.barangay_profile.shelter.print');
	Route::get('/barangay_profile/basic_education/print', 'ReportsController@barangayProfileBasicEducationPrint')->name('reports.barangay_profile.basic_education.print');

	//supplies_equipments
	Route::get('/supplies_equipment','ReportsController@suppliesEquipment')->name('reports.supplies_equipment');

	//financial_reports
	Route::get('/financial_reports','ReportsController@financialReports')->name('reports.financial_reports');

	//other_reports
	Route::get('/other_reports','ReportsController@otherReports')->name('reports.other_reports');


	//Livelihood
    Route::get('/livelihood','ReportsController@livelihood')->name('reports.livelihood');
});

//Forecast
Route::group(['middleware' => 'checkAllowAccess','prefix' => 'forecast'],function(){
	//forecast
	// Route::get('/forecast','ReportsController@forecast')->name('reports.forecast');
	Route::get('/riskarea','ReportsController@forecastRiskArea')->name('forecast.riskarea');
	Route::get('/budget','ReportsController@forecastBudget')->name('forecast.budget');
	Route::get('/relief','ReportsController@reliefBudget')->name('forecast.relief');
    Route::get('/elementary', 'ReportsController@elementary')->name('forecast.elementary');
    Route::get('/hs', 'ReportsController@highSchool')->name('forecast.hs');
    Route::get('/shs', 'ReportsController@seniorHigh')->name('forecast.shs');
    Route::get('/evacuation-forecast', 'ReportsController@evacuationForecast')->name('forecast.evacuation-forecast');
});


//System Tools
Route::group(['middleware' => 'checkAllowAccess','prefix' => 'system_tool'],function(){
	//change_password
	Route::get('/change_password','SystemToolsController@changePassword')->name('systemtool.change_password');
	Route::post('/change_password/update','SystemToolsController@updatePassword')->name('systemtool.update_password');

	//system_user
	Route::get('/system_user','SystemToolsController@systemUser')->name('systemtool.system_user');
	Route::post('/system_user/store', 'SystemToolsController@systemUserStore')->name('systemtool.system_user.store');
	Route::post('/system_user/update','SystemToolsController@systemUserUpdate')->name('systemtool.system_user.update');
	Route::get('/system_user/{id}/enable_access','SystemToolsController@systemUserEnableAccess')->name('systemtool.system_user.enable_access');
	Route::get('/system_user/{id}/disable_access','SystemToolsController@systemUserDisableAccess')->name('systemtool.system_user.disable_access');
	Route::get('/system_user/{id}/delete','SystemToolsController@systemUserDelete')->name('systemtool.system_user.delete');
	Route::get('/system_user/{id}/restore','SystemToolsController@systemUserRestore')->name('systemtool.system_user.restore');

	//user_log
	Route::get('/user_log','SystemToolsController@userLog')->name('systemtool.user_log');

	//request_change_password
	Route::get('/request_change_password','SystemToolsController@requestChangePassword')->name('systemtool.request_change_password');
	Route::post('/request_change_password/request','SystemToolsController@requestChangePasswordRequest')->name('systemtool.request_change_password.request');

	//user_temp_password
	Route::get('/user_temp_password','SystemToolsController@userTempPassword')->name('systemtool.user_temp_password');

	//module_control
	Route::get('/module_control','SystemToolsController@moduleControl')->name('systemtool.module_control');

	//database_backup
	Route::get('/database_backup','SystemToolsController@databaseBackup')->name('systemtool.database_backup.index');
	Route::get('/database_backup/dump', 'SystemToolsController@databaseBackupDump')->name('systemtool.database_backup.dump');
});

//API
Route::group(['prefix' => 'api'],function(){
	//mobile barangay residents
	Route::get('/residentsMobile/{barangay_id}','JSONController@residentsMobile');
	//api/secretaryMobile
	//mobile secretary

	Route::get('/secretaryMobile/{barangay_id}','JSONController@secretaryMobile');
	Route::post('/login','JSONController@login');
	Route::post('/householdAdd','JSONController@householdAdd');
	Route::post('/householdMemberAdd','JSONController@householdMemberAdd');
	Route::post('/householdSurveyAdd','JSONController@householdSurveyAdd');
	Route::post('/evacuationCenterAdd','JSONController@evacuationCenterAdd');
	Route::post('/householdUpdate','JSONController@householdUpdate');
	Route::post('/householdMemberUpdate','JSONController@householdMemberUpdate');
	Route::post('/householdSurveyUpdate','JSONController@householdSurveyUpdate');
	Route::post('/evacuationCenterUpdate','JSONController@evacuationCenterUpdate');

	//check if unique
	Route::get('/houseIdentificationNumberAdd/{house_identification_number}/{barangay_id}','JSONController@houseIdentificationNumber');
	Route::get('/houseIdentificationNumberEdit/{house_identification_number}/{house_identification_number_old}/{barangay_id}','JSONController@houseIdentificationNumberEdit');
	Route::get('/usernameAdd/{username}','JSONController@usernameAdd');
	Route::get('/usernameEdit/{username}/{username_old}','JSONController@usernameEdit');
	Route::get('/usernameEdit/{username}/{username_old}','JSONController@usernameEdit');
	Route::get('/barangayDetails','JSONController@barangayDetails')->name('barangayDetails');
	
	// Route::get('/advisory/crud','DisasterAdminController@advisoryDeletePerYear');

});

Route::group(['middleware' => 'checkAllowAccess','prefix' => 'education'],function(){
	Route::get('/school','SchoolController@getSchool')->name('education.school');
    Route::get('/school/add','SchoolController@add')->name('education.school.add');
	Route::post('/school/store','SchoolController@store')->name('education.school.store');
    Route::get('/school/search/{name}','SchoolController@search')->name('education.school.search');
    Route::post('/school/edit','SchoolController@update')->name('education.school.edit');
	Route::get('/school/report','SchoolController@report')->name('education.school.report');
	Route::post('/school/remove','SchoolController@remove')->name('education.school.remove');
});
