<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use DB;

use App\Http\Requests;

use App\BarangayAbout;
use App\BarangayCalendarEvent;
use App\BarangayAchievement;
use App\BarangayPlan;
use App\ActivityLog;
use Mockery\Exception;

class BarangayController extends Controller
{
    function about(){
        $barangay_id = Session::get('barangayAbout')->id;
        $barangayAbout = BarangayAbout::find($barangay_id);
        return view('barangay.about',compact('barangayAbout'));    
    }

    function aboutUpdate(Request $request){
        $barangayAbout = BarangayAbout::find($request->id);
        $barangayAbout->address = $request->address;
        $barangayAbout->latitude = $request->latitude;
        $barangayAbout->longitude = $request->longitude;
        $barangayAbout->barangay = $request->barangay;
        $barangayAbout->municipality = $request->municipality;
        $barangayAbout->province = $request->province;
        $barangayAbout->region = $request->region;
        $barangayAbout->barangay_category = $request->barangay_category;
        $barangayAbout->land_classification = $request->land_classification;
        $barangayAbout->major_economic_source = $request->major_economic_source;
        $barangayAbout->vision = $request->vision;
        $barangayAbout->mission = $request->mission;
//        $barangayAbout->history = $request->history;
//        $barangayAbout->brief_description = $request->brief_description;
        $barangayAbout->total_land_area = $request->total_land_area;
        if($request->logo != null){
            $image = $request->logo;
            $barangayName = Session::get('barangayAbout')->barangay;

            $extension = $image->getClientOriginalExtension();
            $filename = $barangayName."_logo.".$extension;
            if($image){
                    Storage::disk('logo')->put( $filename, File::get($image));
            }
            $barangayAbout->logo = $filename;
        }
        $barangayAbout->save();
        Session::put('barangayAbout',$barangayAbout);
        $al = new ActivityLog();
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated About Barangay';
        $al->activity = 'update';
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->save();
        return redirect()->route('barangayprofile.about')->with('flash_message', 'About Barangay Updated!!');    
    }

    function calendarEvents(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $barangay_calendars = DB::table('barangay_calendar_events')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('when','like', '%'.$request->search.'%')
                ->orWhere('where','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $barangay_calendars = DB::table('barangay_calendar_events')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('when','like', '%'.$request->search.'%')
                ->orWhere('where','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('updated_at','desc')
            ->paginate(25);
        }
    	return view('barangay.calendar_event',compact('barangay_calendars'));
    }

    function calendarEventsStore(Request $request){
        $barangay_calendar = new BarangayCalendarEvent();
        $barangay_calendar->barangay_about_id = Session::get('barangayAbout')->id;
        $barangay_calendar->name = $request->name;
        $barangay_calendar->when = $request->when;
        $barangay_calendar->where = $request->where;
        $barangay_calendar->type = $request->type;
        $barangay_calendar->save();

        $al = new ActivityLog();
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$request->name.' on Barangay Calenday Event';
        $al->activity = 'add';
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->save();

        return redirect()->route('barangayprofile.calendar_events')->with('flash_message', $request->name.' was Added!!');
    }

    function calendarEventsUpdate(Request $request){
        $barangay_calendar = BarangayCalendarEvent::find($request->id);
        $barangay_calendar->name = $request->name;
        $barangay_calendar->when = $request->when;
        $barangay_calendar->where = $request->where;
        $barangay_calendar->type = $request->type;
        $barangay_calendar->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$request->name.' on Barangay Calenday Event';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('barangayprofile.calendar_events')->with('flash_message',  $request->name.' was Updated!!');
    }

    function calendarEventsDelete($id){
        $barangay_calendar = BarangayCalendarEvent::find($id);
        $barangay_calendar->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$barangay_calendar->name.' on Barangay Calenday Event';
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('barangayprofile.calendar_events')->with('flash_message', $barangay_calendar->name.' was Deleted');
    }

    function calendarEventsRestore($id){
        $barangay_calendar = BarangayCalendarEvent::onlyTrashed()->find($id);
        $name = $barangay_calendar->name;
        $barangay_calendar->restore();
 
        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$barangay_calendar->name.' on Barangay Calenday Event';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('barangayprofile.calendar_events')->with('flash_message', $barangay_calendar->name.' was Restored');
    }

    function achievement(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $barangay_achievements = DB::table('barangay_achievements')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('date','like', '%'.$request->search.'%')
                ->orWhere('detail','like', '%'.$request->search.'%')
                ->orWhere('other_detail','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $barangay_achievements = DB::table('barangay_achievements')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('date','like', '%'.$request->search.'%')
                ->orWhere('detail','like', '%'.$request->search.'%')
                ->orWhere('other_detail','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('updated_at','desc')
            ->paginate(25);
        }
        return view('barangay.achievement',compact('barangay_achievements'));
    }

    function achievementStore(Request $request){
        $barangay_achievement = new BarangayAchievement();
        $barangay_achievement->barangay_about_id = Session::get('barangayAbout')->id;
        $barangay_achievement->name = $request->name;
        $barangay_achievement->date = $request->date;
        $barangay_achievement->detail = $request->detail;
        $barangay_achievement->other_detail = $request->other_detail;
        $barangay_achievement->type = $request->type;
        $barangay_achievement->save();
        if($request->image != null){
            $image = $request->image;

            $extension = $image->getClientOriginalExtension();

            $filename = $barangay_achievement->id.".".$extension;

            if($image){
                Storage::disk('achievement')->put($filename, File::get($image));
            }
            $barangayImage = BarangayAchievement::find($barangay_achievement->id);
            $barangayImage->image = $filename;
            $barangayImage->update();
        }

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$request->name.' on Barangay Achievement';
        $al->activity = 'add';
        $al->save();
        
        return redirect()->route('barangayprofile.achievement')->with('flash_message', $request->name.' was Added!!');
    }

    function achievementUpdate(Request $request){
        $barangay_achievement = BarangayAchievement::find($request->id);
        $barangay_achievement->name = $request->name;
        $barangay_achievement->date = $request->date;
        $barangay_achievement->detail = $request->detail;
        $barangay_achievement->other_detail = $request->other_detail;
        $barangay_achievement->type = $request->type;
        if($request->image != null){
            $image = $request->image;

            $extension = $image->getClientOriginalExtension();

            $filename = $request->id.".".$extension;

            if($image){
                Storage::disk('achievement')->put($filename, File::get($image));
            }
            $barangay_achievement->image = $filename;
        }
        $barangay_achievement->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$request->name.' on Barangay Achievement';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('barangayprofile.achievement')->with('flash_message',  $request->name.' was Updated!!');
    }

    function achievementDelete($id){
        $barangay_achievement = BarangayAchievement::find($id);
        $barangay_achievement->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$barangay_achievement->name.' on Barangay Achievement';
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('barangayprofile.achievement')->with('flash_message', $barangay_achievement->name.' was Deleted');
    }

    function achievementRestore($id){
        $barangay_achievement = BarangayAchievement::onlyTrashed()->find($id);
        $name = $barangay_achievement->name;
        $barangay_achievement->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$barangay_achievement->name.' on Barangay Achievement';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('barangayprofile.achievement')->with('flash_message', $barangay_achievement->name.' was Restored');
    }

    function plans(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $barangay_plans = DB::table('barangay_plans')
            ->join('users','users.id', '=', 'barangay_plans.user_id')
            ->select(
                'barangay_plans.*',
                'users.first_name',
                'users.middle_initial',
                'users.last_name'
            )
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('date','like', '%'.$request->search.'%')
                ->orWhere('detail','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%')
                ->orWhere('first_name','like', '%'.$request->search.'%')
                ->orWhere('last_name','like', '%'.$request->search.'%');
            })
            ->where('barangay_plans.barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $barangay_plans = DB::table('barangay_plans')
            ->join('users','users.id', '=', 'barangay_plans.user_id')
            ->select(
                'barangay_plans.*',
                'users.first_name',
                'users.middle_initial',
                'users.last_name'
            )
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('date','like', '%'.$request->search.'%')
                ->orWhere('detail','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%')
                ->orWhere('first_name','like', '%'.$request->search.'%')
                ->orWhere('last_name','like', '%'.$request->search.'%');
            })
            ->where('barangay_plans.barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('updated_at','desc')
            ->paginate(25);
        }
        return view('barangay.plan',compact('barangay_plans'));
    }

    function plansStore(Request $request){
        $plans = new BarangayPlan();
        $plans->barangay_about_id = Session::get('barangayAbout')->id;
        $plans->name = $request->name;
        $plans->date = $request->date;
        $plans->detail = $request->detail;
        $plans->user_id = $request->user_id;
        $plans->type = $request->type;
        $plans->save();
        if($request->file != null){
            $file = $request->file;

            $extension = $file->getClientOriginalExtension();

            $filename = $plans->id.".".$extension;

            if($file){
                Storage::disk('plans')->put($filename, File::get($file));
            }
            $plansFile = BarangayPlan::find($plans->id);
            $plansFile->file = $filename;
            $plansFile->update();
        }

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$request->name.' on Barangay Plan';
        $al->activity = 'add';
        $al->save();
        
        return redirect()->route('barangayprofile.plans')->with('flash_message', $request->name.' was Added!!');
    }

    function plansUpdate(Request $request){
        $plans = BarangayPlan::find($request->id);
        $plans->name = $request->name;
        $plans->date = $request->date;
        $plans->detail = $request->detail;
        $plans->user_id = $request->user_id;
        $plans->type = $request->type;
        if($request->file != null){
            $file = $request->file;

            $extension = $file->getClientOriginalExtension();

            $filename = $plans->name.".".$extension;

            if($file){
                Storage::disk('plans')->put($filename, File::get($image));
            }
            $plans->file = $filename;
        }
        $plans->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$request->name.' on Barangay Plan';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('barangayprofile.plans')->with('flash_message',  $request->name.' was Updated!!');
    }

    function plansDelete($id){
        $plans = BarangayPlan::find($id);
        $plans->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$plans->name.' on Barangay Plan';
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('barangayprofile.plans')->with('flash_message', $plans->name.' was Deleted');
    }

    function plansRestore($id){
        $plans = BarangayPlan::onlyTrashed()->find($id);  
        $plans->restore();
        $name = $plans->name;

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$plans->name.' on Barangay Plan';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('barangayprofile.plans')->with('flash_message', $plans->name.' was Restored');
    }
}
