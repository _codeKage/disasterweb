<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use DB;
use Artisan;
use App\User;
use App\ActivityLog;
use Carbon\Carbon;
use App\TemporaryPassword;

use App\Http\Requests;

class SystemToolsController extends Controller

{
    function changePassword(){
    	return view('system_tool.change_password');
    }

    function updatePassword(Request $request){
        $this->validate($request, [
            'old_password'=>'required',
            'password'=>'required',
            'confirm_password'=>'required | same:password',
        ]);

        $user = User::find(Session::get('logUser')->id);
        if(password_verify($request->old_password,$user->password)){
            $user->password = bcrypt($request->password);
            $user->update();
            Session::forget('logUser');
            Session::put('logUser',$user);

            $al = new ActivityLog();
            $al->barangay_about_id = Session::get('barangayAbout')->id;
            $al->username = $user->username;
            $al->description = 'Change password';
            $al->activity = 'change password';
            $al->save();

            return redirect()->back()->with('flash_message','Successfully changed password!!');
        } else {
            Session::flash('error','Incorrect old Password!');
            return redirect()->back();
        }
    }

    function systemUser(Request $request){
        if(Input::get('sortby')!=null){
            $val = explode(".",Input::get('sortby'));
            $system_users = DB::table('users')
                ->Where(function ($query) use ($request) {
                    $query->where('username','like', '%'.$request->search.'%')
                    ->orWhere('first_name','like', '%'.$request->search.'%')
                    ->orWhere('middle_name','like', '%'.$request->search.'%')
                    ->orWhere('last_name','like', '%'.$request->search.'%')
                    ->orWhere('usertype','like', '%'.$request->search.'%');
                })
            ->where('id','!=',1)
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else{
           $system_users = DB::table('users')
                ->Where(function ($query) use ($request) {
                    $query->where('username','like', '%'.$request->search.'%')
                    ->orWhere('first_name','like', '%'.$request->search.'%')
                    ->orWhere('middle_initial','like', '%'.$request->search.'%')
                    ->orWhere('last_name','like', '%'.$request->search.'%')
                    ->orWhere('usertype','like', '%'.$request->search.'%');
                })
            ->where('id','!=',1)
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('last_name','asc')
            ->paginate(25);
        }
        return view('system_tool.system_user',compact('system_users'));
    }

    function systemUserStore(Request $request){
        $system_users = new User();
        $system_users->barangay_about_id = Session::get('barangayAbout')->id;
        $system_users->username = $request->username;
        $system_users->password = bcrypt($request->username);
        $system_users->first_name = $request->first_name;
        $system_users->middle_initial = $request->middle_initial;
        $system_users->last_name = $request->last_name;
        $system_users->email = $request->email;
        $system_users->usertype = $request->usertype;
        $system_users->access = $request->access;
        $system_users->save();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$system_users->username.' account as '.$system_users->usertype;
        $al->activity = 'add';
        $al->save();

        return redirect()->route('systemtool.system_user')->with('flash_message', $system_users->username.' was Added!!');

    }

    function systemUserUpdate(Request $request){
        $system_users = User::find($request->id);
        $system_users->first_name = $request->first_name;
        $system_users->middle_initial = $request->middle_initial;
        $system_users->last_name = $request->last_name;
        $system_users->email = $request->email;
        $system_users->usertype = $request->usertype;
        $system_users->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$request->username.' account';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('systemtool.system_user')->with('flash_message',  $request->username.' was Updated!!');
    }

    function systemUserEnableAccess($id){
        $system_users = User::find($id);
        $system_users->access = 1;
        $system_users->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Enabled '.$system_users->username.' account access';
        $al->activity = 'enable access';
        $al->save();

        return redirect()->route('systemtool.system_user')->with('flash_message', $system_users->username.' access was Enabled');
    }

    function systemUserDisableAccess($id){
        $system_users = User::find($id);
        $system_users->access = 0;
        $system_users->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Disabled '.$system_users->username.' account access';
        $al->activity = 'disable access';
        $al->save();

        return redirect()->route('systemtool.system_user')->with('flash_message', $system_users->username.' access was Disabled');

    }

    function systemUserDelete($id){
        $system_users = User::find($id);
        $system_users->access = 0;
        $system_users->update();
        $system_users->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$system_users->username.' account';
        $al->activity = 'delete';
        $al->save();

        return redirect()->route('systemtool.system_user')->with('flash_message', $system_users->username.' was Deleted');

    }

    function systemUserRestore($id){
        $system_users = User::onlyTrashed()->find($id);
        $name = $system_users->name;
        $system_users->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$system_users->username.' account';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('systemtool.system_user')->with('flash_message', $system_users->username.' was Restored');
    }

    function userLog(Request $request){
        if(Input::get('sortby')!=null){
            $val = explode(".",Input::get('sortby'));
            $activity_logs = DB::table('activity_logs')
                ->Where(function ($query) use ($request) {
                    $query->where('username','like', '%'.$request->search.'%')
                    ->orWhere('description','like', '%'.$request->search.'%')
                    ->orWhere('activity','like', '%'.$request->search.'%')
                    ->orWhere('created_at','like', '%'.$request->search.'%');
                })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }else{
           $activity_logs = DB::table('activity_logs')
            ->Where(function ($query) use ($request) {
                $query->where('username','like', '%'.$request->search.'%')
                ->orWhere('description','like', '%'.$request->search.'%')
                ->orWhere('activity','like', '%'.$request->search.'%')
                ->orWhere('created_at','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('created_at','desc')
            ->paginate(25);
        }
        return view('system_tool.user_log',compact('activity_logs'));
    }

    function requestChangePassword(){
        return view('system_tool.request_change_password');
    }

    function requestChangePasswordRequest(Request $request){
        $user = User::where('username',$request->username)->first();
        if(count($user) < 1){
            return redirect()->back()->with('flash_error','Userame does not exist.');
        }
        $temp_password = str_random(8);
        $user->password = bcrypt($temp_password);
        $user->update();

        $checkTemp = TemporaryPassword::where('user_id',$user->id)->first();
        if(count($checkTemp) < 1){
            $temporary_password = new TemporaryPassword();
            $temporary_password->user_id = $user->id;
            $temporary_password->temporary_password = $temp_password;
            $temporary_password->save();
        } else {
            $checkTemp->user_id = $user->id;
            $checkTemp->temporary_password = $temp_password;
            $checkTemp->update();
        }

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Change the password of '.$user->username.' account';
        $al->activity = 'change password';
        $al->save();

        return redirect()->back()->with('flash_message', 'Please write your temporary password in a piece of paper. Your Temporary Password is '.$temp_password);
    }

    function userTempPassword(Request $request){
        if(Input::get('sortby')!=null){
            $val = explode(".",Input::get('sortby'));
            $users = DB::table('temporary_passwords')
                ->join('users','users.id','=','temporary_passwords.user_id')
                ->select(
                    'temporary_passwords.*',
                    'users.username',
                    'users.first_name',
                    'users.middle_initial',
                    'users.last_name',
                    'users.usertype',
                    'users.access'
                )
                ->Where(function ($query) use ($request) {
                    $query->where('username','like', '%'.$request->search.'%')
                    ->orWhere('first_name','like', '%'.$request->search.'%')
                    ->orWhere('middle_initial','like', '%'.$request->search.'%')
                    ->orWhere('last_name','like', '%'.$request->search.'%')
                    ->orWhere('usertype','like', '%'.$request->search.'%')
                    ->orWhere('access','like', '%'.$request->search.'%');
                })
                ->where('temporary_passwords.barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->paginate(25);
        }else{
            $users = DB::table('temporary_passwords')
                ->join('users','users.id','=','temporary_passwords.user_id')
                ->select(
                    'temporary_passwords.*',
                    'users.username',
                    'users.first_name',
                    'users.middle_initial',
                    'users.last_name',
                    'users.usertype',
                    'users.access'
                )
                ->Where(function ($query) use ($request) {
                    $query->where('username','like', '%'.$request->search.'%')
                    ->orWhere('first_name','like', '%'.$request->search.'%')
                    ->orWhere('middle_initial','like', '%'.$request->search.'%')
                    ->orWhere('last_name','like', '%'.$request->search.'%')
                    ->orWhere('usertype','like', '%'.$request->search.'%')
                    ->orWhere('access','like', '%'.$request->search.'%');
                })
                ->where('temporary_passwords.barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('created_at','desc')
                ->paginate(25);
        }
        return view('system_tool.user_temp_password',compact('users'));
    }

    function databaseBackup(){
    	return view('system_tool.database_backup');
    }

    function databaseBackupDump(){
        try{
            $exitCode = Artisan::call('backup:run');
        } catch(Exception $e) {
            return redirect()->back()->with('error_message',$e)->name('systemtool.database_backup.dump');
        }
        return redirect()->back()->with('flash_message','Database has been backup on your system storage.');
    }
}

