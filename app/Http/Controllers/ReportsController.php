<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use DB;
use Log;

use App\Http\Requests;

use App\BarangayAbout;
use App\BarangayCalendarEvent;
use App\BarangayAchievement;
use App\BarangayPlan;
use App\DisasterAdvisory;
use App\DisasterSafetyTip;
use App\DisasterEvacuationCenter;
use App\DisasterKit;
use App\DisasterPlan;
use App\DisasterTeam;
use App\DisasterTeamMember;
use App\ResidentHousehold;
use App\ResidentHouseholdMember;
use App\School;
use App\SchoolCategory;
use App\GradeCapacity;
use App\ResidentHouseholdSurveyA;
use App\ResidentHouseholdSurveyB;
use App\ResidentHouseholdSurveyC;
use App\ResidentHouseholdSurveyD;
use App\ResourcesSupply;
use App\ResourcesSupplyHistory;
use App\ResourcesEquipment;
use App\ResourcesEquipmentHistory;
use App\ResourcesBudget;
use App\ResourcesExpense;
use Carbon\Carbon;

class ReportsController extends Controller
{
    function reliefBudget()
    {
        $brgy_id =Session::get("brgy_id");
        $currentYear = date("Y");

        $yearlyPreparednessKit = DB::select(
            "	SELECT
					sum(
						CASE
							WHEN d10_1 = 0
							THEN 1
							ELSE 0
						END 
					) as water,
					sum(
						CASE
							WHEN d10_2 = 0
							THEN 1
							ELSE 0
						END
					) as food,
					sum(
						CASE
							WHEN d10_3 = 0
							THEN 1
							ELSE 0
						END
					) as matches,
					sum(
						CASE
							WHEN d10_4 = 0
							THEN 1
							ELSE 0
						END
					) as flashlight,
					sum(
						CASE
							WHEN d10_5 = 0
							THEN 1
							ELSE 0
						END
					) as radio,
					sum(
						CASE
							WHEN d10_6 = 0
							THEN 1
							ELSE 0
						END
					) as candle,
					sum(
						CASE
							WHEN d10_7 = 0
							THEN 1
							ELSE 0
						END
					) as whistle,
					sum(
						CASE
							WHEN d10_8 = 0
							THEN 1
							ELSE 0
						END
					) as med_kit,
					sum(
						CASE
							WHEN d10_9 = 0
							THEN 1
							ELSE 0
						END
					) as medicine,
					sum(
						CASE
							WHEN d10_10 = 0
							THEN 1
							ELSE 0
						END
					) as clothes,
					sum(
						CASE
							WHEN d10_11 = 0
							THEN 1
							ELSE 0
						END
					) as blanket,
					sum(
						CASE
							WHEN d10_12 = 0
							THEN 1
							ELSE 0
						END
					) as documents,
					sum(
						CASE
							WHEN d10_13 = 0
							THEN 1
							ELSE 0
						END
					) as fuel,
					sum(
						CASE
							WHEN d10_14 = 0
							THEN 1
							ELSE 0
						END
					) as cash,
					survey.year
				FROM yearly_survey survey
				LEFT JOIN resident_household_survey_ds survey_d
				ON survey.survey_d_id = survey_d.id
				WHERE 
					survey.barangay_id = $brgy_id AND survey.year = $currentYear
				GROUP BY survey.year
				LIMIT 1
			"
        );

        // return view('report.forecast.relief',[
        // 	'datenow'=>$datenow,
        // 	'dateminus1'=>$dateminus1,
        // 	'yearfuture'=>$yearfuture,
        // ]);

        return view('report.forecast.relief',
            ['yearlyPreparednessKit'=>$yearlyPreparednessKit]);

        // return response()->json($yearlyPreparednessKit,200, array(),JSON_PRETTY_PRINT);

    }

    function forecastBudget()
    {
        $brgy_id =Session::get("brgy_id");
        //get all yearly data
        $yearlyBudgetExpense = DB::select(
            "	SELECT 
					budget.id,
					budget.barangay_about_id,
					budget.fiscal_year,
					budget.funding_amount,
					SUM(expense.amount) AS expenses,
					(
						budget.funding_amount 
						- (
							SELECT 
								SUM(amount) 
							FROM resources_expenses 
							WHERE resources_budget_id = budget.id 
							GROUP BY budget.id
						)
					) AS balance
              	FROM resources_budgets budget
				LEFT JOIN resources_expenses expense
				ON expense.resources_budget_id = budget.id
			  	WHERE 
					budget.barangay_about_id = $brgy_id 
					AND budget.deleted_at IS NULL
				GROUP BY expense.resources_budget_id
				ORDER BY budget.fiscal_year
				ASC
			"
        );

        $year = "";

        //add additional year to forecast
        if(count($yearlyBudgetExpense)>0){
            $year = ((int) end($yearlyBudgetExpense)->fiscal_year) + 1;
            $forecastYear = new \stdClass();
            $forecastYear->id = 0;
            $forecastYear->barangay_about_id = $brgy_id;
            $forecastYear->fiscal_year = ''.$year;
            $forecastYear->funding_amount = 0;
            $forecastYear->expenses = 0;
            $forecastYear->balance = 0;
            $forecastYear->forecastBudget = 0;

            array_push($yearlyBudgetExpense,$forecastYear);

            //forecasting
            $alpha = 0.2;
            $delta = 1 - $alpha;
            forEach($yearlyBudgetExpense as $index=>$budgetExpense){
                $prevIndex = ($index-1);
                $forecast = 0;
                Log::info('budget expense current', ['index'=>$index, 'year'=>$budgetExpense->fiscal_year]);
                if($prevIndex > -1) {
                    $prevBudgetExpense = $yearlyBudgetExpense[$prevIndex];
                    if($index > 1) {
                        $prevExpense = $alpha * $prevBudgetExpense->expenses;
                        $prevForecast = $delta * $prevBudgetExpense->forecastBudget;
                        $forecast = $prevExpense + $prevForecast;
                    } else {
                        $forecast = $prevBudgetExpense->expenses;
                    }
                    Log::info('budget expense prev', ['index'=>($prevIndex), 'year'=>$yearlyBudgetExpense[$prevIndex]->fiscal_year]);
                }
                $budgetExpense->forecastBudget = $forecast;
            }

        }

        // return response()->json(
        //     [
        //         'yearlyBudgetExpense'=>$yearlyBudgetExpense,
        //         'forecastYear'=>$year,
        //         'population'=>$this->populationPerBarangay()
        //     ]
        //     ,200, array(),JSON_PRETTY_PRINT);

        return view('report.forecast.budget',[
            'yearlyBudgetExpense'=>$yearlyBudgetExpense,
            'forecastYear'=>$year,
            'population'=>$this->populationPerBarangay()
        ]);

    }

    function forecastRiskArea()
    {
        $brgy_id =Session::get("brgy_id");
        $yearlyRiskArea = DB::select(
            "	SELECT
					(
						SUM(survey_a.a3_1) 
						+ SUM(survey_a.a3_2)
						+ SUM(survey_a.a3_3)
						+ SUM(survey_a.a3_4)
						+ SUM(survey_a.a3_5)
						+ SUM(survey_a.a3_6)
						+ SUM(survey_a.a3_7)
						+ SUM(survey_a.a3_8)
						+ SUM(survey_a.a3_9)
						+ SUM(survey_a.a3_10)
						+ SUM(survey_a.a3_11)
						+ SUM(survey_a.a3_12)
						+ SUM(survey_a.a3_13)
					) AS timesExperienced,
                    SUM(household.no_members) AS populationPerPurok,
					household.purok,
					survey.year
				FROM yearly_survey survey
				LEFT JOIN resident_households household
				ON survey.resident_household_id = household.id
				LEFT JOIN resident_household_survey_as survey_a
				ON survey.survey_a_id = survey_a.id
				WHERE survey.barangay_id = $brgy_id
				GROUP BY survey.year, household.purok
				ORDER BY survey.year
				ASC
			"
        );

        $riskAreaPerPurok = array();
        $purok = '';

        forEach($yearlyRiskArea as $riskArea){
            if($riskArea->purok != $purok && empty($riskAreaPerPurok[$riskArea->purok])){
                Log::info('here');
                $purok = $riskArea->purok;
                $riskAreaPerPurok[$purok] = array();
            }
            array_push($riskAreaPerPurok[$riskArea->purok], $riskArea);
        }

        forEach($riskAreaPerPurok as $key=>$riskArea){
            $purokArea = $riskAreaPerPurok[$key];

            $year = ((int) end($riskArea)->year) + 1;
            $forecastYear = new \stdClass();
            $forecastYear->timesExperienced = 0;
            $forecastYear->purok = $key;
            $forecastYear->year = ''.$year;
            $forecastYear->forecastOccurence = 0;

            Log::info('forecastYear',[
                'forecastYear'=>$forecastYear
            ]);

            array_push($riskAreaPerPurok[$key],$forecastYear);
            // forecasting
            $alpha = 0.2;
            $delta = 1 - $alpha;

            forEach($riskAreaPerPurok[$key] as $index=>$area){
                $prevIndex = ($index-1);
                $forecast = 0;
                if($prevIndex > -1) {
                    $prevOccurence = $riskAreaPerPurok[$key][$prevIndex];
                    if($index > 1) {
                        $prevActual = $alpha * $prevOccurence->timesExperienced;
                        $prevForecast = $delta * $prevOccurence->forecastOccurence;
                        $forecast = $prevActual + $prevForecast;
                    } else {
                        $forecast = $prevOccurence->timesExperienced;
                    }
                }
                $area->forecastOccurence = $forecast;
            }

        }

        // return response()->json([
        // 	'riskPerPurok'=>$riskAreaPerPurok
        // ],200, array(),JSON_PRETTY_PRINT);

        return view('report.forecast.riskarea',[
            'yearlyRiskArea'=>$riskAreaPerPurok
        ]);

    }
    function barangayProfile(){
        $barangayId = Session::get('barangayAbout')->id;
        $barangay_about = BarangayAbout::find($barangayId);
        $household = ResidentHousehold::where('barangay_about_id',Session::get('barangayAbout')->id)->get();
        $household_member = ResidentHouseholdMember::where('barangay_about_id',Session::get('barangayAbout')->id)->get();

        $health_problem = ResidentHouseholdMember::select('health_problem')
            ->where('health_problem','!=','')
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->get();
        $health_problem_count = count(array_map('trim',explode(',',$health_problem->implode('health_problem',','))));

        $disability_type = ResidentHouseholdMember::select('disability_type')
            ->where('disability_type','!=','')
            ->whereNotNull('disability_type')
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->get();
        $disability_type_count = count(array_diff(array_map('trim',explode(',',$disability_type->implode('disability_type',','))),['Total Blindness','Partial Blindness','Low Vision','Totally Deaf','Partially Deaf','Oral Defect','One Hand','No Hands','One Leg','No Legs','Mild Cerebral Palsy','Severe Cerebral Palsy','Retarded','Mentally Ill','Mental Retardation','Multiple Impairment']));
        $health_others_count = $disability_type_count + $health_problem_count;

        return view('report.barangay_profile',compact('barangay_about','household_member','household','health_others_count'));
    }

    function barangayProfileGeneralInformationPrint(){
        $barangayId = Session::get('barangayAbout')->id;
        $barangay_about = BarangayAbout::find($barangayId);
        return view('report.print.barangay_profile_general_information_print',compact('barangay_about'));
    }

    function barangayProfileDemographyPrint(){
        $household_member = ResidentHouseholdMember::where('barangay_about_id',Session::get('barangayAbout')->id)->get();
        $household = ResidentHousehold::where('barangay_about_id',Session::get('barangayAbout')->id)->get();
        return view('report.print.barangay_profile_demography_print',compact('household_member','household'));
    }

    function barangayProfileSocioEconomicProfilePrint(){
        $household_member = ResidentHouseholdMember::where('barangay_about_id',Session::get('barangayAbout')->id)->get();
        return view('report.print.barangay_profile_socio-economic_profile_print',compact('household_member'));
    }

    function barangayProfileWaterSanitationPrint(){
        $household = ResidentHousehold::where('barangay_about_id',Session::get('barangayAbout')->id)->get();
        return view('report.print.barangay_profile_water_sanitation_print',compact('household'));
    }

    function barangayProfileShelterPrint(){
        $household = ResidentHousehold::where('barangay_about_id',Session::get('barangayAbout')->id)->get();
        return view('report.print.barangay_profile_shelter_print',compact('household'));
    }

    function barangayProfileBasicEducationPrint(){
        $household_member = ResidentHouseholdMember::where('barangay_about_id',Session::get('barangayAbout')->id)->get();
        return view('report.print.barangay_profile_basic_education_print',compact('household_member'));
    }

    function barangayProfileHealthPrint(){
        $household_member = ResidentHouseholdMember::where('barangay_about_id',Session::get('barangayAbout')->id)->get();

        $health_problem = ResidentHouseholdMember::select('health_problem')
            ->where('health_problem','!=','')
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->get();
        $health_problem_count = count(array_map('trim',explode(',',$health_problem->implode('health_problem',','))));

        $disability_type = ResidentHouseholdMember::select('disability_type')
            ->where('disability_type','!=','')
            ->whereNotNull('disability_type')
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->get();
        $disability_type_count = count(array_diff(array_map('trim',explode(',',$disability_type->implode('disability_type',','))),['Total Blindness','Partial Blindness','Low Vision','Totally Deaf','Partially Deaf','Oral Defect','One Hand','No Hands','One Leg','No Legs','Mild Cerebral Palsy','Severe Cerebral Palsy','Retarded','Mentally Ill','Mental Retardation','Multiple Impairment']));
        $health_others_count = $disability_type_count + $health_problem_count;
        return view('report.print.barangay_profile_health_print',compact('household_member','household','health_others_count'));
    }

    function householdMemberStatus(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->paginate(25);
        }
        else
        {
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('purok','asc')
                ->paginate(25);
        }
        return view('report.household_member_status',compact('resident_households'));
    }

    function householdMemberStatusPrint(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->print.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->paginate(25);
        }
        else
        {
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->print.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('purok','asc')
                ->paginate(25);
        }
        return view('report.print.household_member_status_print',compact('resident_households'));
    }

    function householdNuclearFamily(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->paginate(25);
        }
        else
        {
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('purok','asc')
                ->paginate(25);
        }
        return view('report.household_nuclear_family',compact('resident_households'));
    }

    function householdNuclearFamilyPrint(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->print.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->paginate(25);
        }
        else
        {
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->print.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('purok','asc')
                ->paginate(25);
        }
        return view('report.print.household_nuclear_family_print',compact('resident_households'));
    }

    function householdPurok(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->paginate(25);
        }
        else
        {
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('purok','asc')
                ->paginate(25);
        }
        return view('report.household_purok',compact('resident_households'));
    }

    function householdPurokPrint(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->print.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->paginate(25);
        }
        else
        {
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->print.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('purok','asc')
                ->paginate(25);
        }
        return view('report.print.household_purok_print',compact('resident_households'));
    }

    function residentPurok(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->paginate(25);
        }
        else
        {
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('purok','asc')
                ->paginate(25);
        }
        return view('report.resident_purok',compact('resident_households'));
    }

    function residentPurokPrint(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->print.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->paginate(25);
        }
        else
        {
            $resident_households = ResidentHousehold::with('resident_household_members')
                ->Where(function ($query) use ($request) {
                    $query->where('purok','like', '%'.$request->print.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('purok','asc')
                ->paginate(25);
        }
        return view('report.print.resident_purok_print',compact('resident_households'));
    }

    function suppliesEquipment(){
        $most_distributed_supplies = ResourcesSupply::orderBy('total_distributed','desc')->limit(5)->get();
        $supplies = ResourcesSupply::orderBy('total_added','desc')->get();

        $most_good_equipments = ResourcesEquipment::where('barangay_about_id',Session::get('barangayAbout')->id)->orderBy('good_condition','desc')->limit(5)->get();
        $most_bad_equipments = ResourcesEquipment::where('barangay_about_id',Session::get('barangayAbout')->id)->orderBy('bad_condition','desc')->limit(5)->get();
        $equipments = ResourcesEquipment::where('barangay_about_id',Session::get('barangayAbout')->id)->orderBy('good_condition','desc')->get();
        return view('report.supplies_equipment',compact('most_distributed_supplies','supplies','most_good_equipments','most_bad_equipments','equipments'));
    }

    function financialReports(){
        $budgets = ResourcesBudget::with('resources_expenses')
            ->orderBy('fiscal_year','desc')
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->get();
        return view('report.financial_reports',compact('budgets'));
    }

    function otherReports(){
        $households = ResidentHousehold::with('resident_household_survey_ds')->where('barangay_about_id',Session::get('barangayAbout')->id)->get();
        $checkKit = ResidentHouseholdSurveyD::select('d9')->where('barangay_about_id',Session::get('barangayAbout')->id)->get();
        $household_at_risk = ResidentHousehold::where('building_type','Light Materials')->where('barangay_about_id',Session::get('barangayAbout')->id)->orWhere('building_type','Semi-Concrete')->get();
        $household_members_at_risk = ResidentHouseholdMember::with('resident_households')
            ->orWhereHas('resident_households', function ($query){
                $query->where('building_type','Light Materials')
                    ->orWhere('building_type','Semi-Concrete');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->get();
        $puroks = ResidentHousehold::select('purok')->distinct()->where('barangay_about_id',Session::get('barangayAbout')->id)->get();
        return view('report.other_reports',compact('households','checkKit','household_at_risk','puroks','household_members_at_risk'));
    }

    function livelihood() {
        $householdMembers = ResidentHouseholdMember::where('barangay_about_id', Session::get('brgy_id'))->get();
        $most = array();
        $classification1=0;
        $classification2=0;
        $classification3=0;
        $classification4=0;
        foreach ($householdMembers as $member) {
            if($member->occupation == 'Farmer' || $member->occupation == 'Forestry Worker') {
                $classification3++;
            }
            else if ($member->occupation == 'Fisherman') {
                $classification4++;
            }
            else if($member->occupation == 'Assembler' || $member->occupation == 'Plant and Machine Operator' || $member->occupation == 'Service Worker' || $member->occupation == 'Special Occupation' || $member->occupation == 'Shop and Market Sales Worker' || $member->occupation == 'Technician') {
                $classification2++;
            }
            else if($member->occupation == 'Associate Professional' || $member->occupation == 'Clerk' || $member->occupation == 'Corporate Executive' || $member->occupation == 'Manager' || $member->occupation == 'Managing Proprietor' || $member->occupation == 'Official of the government and special interest' || $member->occupation == 'Organization' || $member->occupation == 'Assembler' || $member->occupation == 'Professional' || $member->occupation == 'Supervisor' || $member->occupation == 'Trade Related Worker') {
                $classification1++;
            }
        }

        $arr = ["class1" => $classification1, "class2" => $classification2, "class3" => $classification3, "class4" => $classification4];
        arsort($arr);
        reset($arr);
        if($classification1 == 0 && $classification2 == 0 && $classification3 == 0 && $classification4 == 0) {
            return view('report.livelihood', compact('most'));
        }
        switch (key($arr)) {
            case "class1" :
                array_push($most, "Sari-Sari Store Negosyo program");
                array_push($most, "Abaca Paper Making");
                array_push($most, "Abaca Handicrafts Production");
                array_push($most, "Banana Chips Production");
                array_push($most, "Pilinuts Production");
                array_push($most, "Other Food Processing");
                array_push($most, "Latik Production");
                array_push($most, "Bread and Pastry Production");
                break;
            case "class2" :
                array_push($most, "Rattan Weaving");
                array_push($most, "Welding");
                array_push($most, "Cosmetology");
                array_push($most, "Baking");
                array_push($most, "Softbroom Making");
                array_push($most, "Massage Program");

                break;
            case "class3" :
                array_push($most, "Organic Fertilizer Making");
                array_push($most, "Livestock Raising Project");
                array_push($most, "Home Gardening");
                array_push($most, "Organic Vegetable Farming");
                array_push($most, "Farming Equipment");
                array_push($most, "Fertilizing Micro Financing");
                array_push($most, "Carpentry Equipment");

                break;
            case "class4" :
                array_push($most, "Animal Loan Program");
                array_push($most, "Tilapia Culture Production");
                array_push($most, "Crab Production");
                array_push($most, "Shrimp Production");
                array_push($most, "Fishing Equipment");
                array_push($most, "Smoked Fish Processing");
                break;
        }
        return view('report.livelihood', compact('most'));
    }

    function populationPerBarangay(){
        $brgy_id =Session::get("brgy_id");
        return $yearlyBudgetExpense = DB::select(
            "	SELECT 
					SUM(no_members) AS populationPerBrgy
              	FROM resident_households
				WHERE 
					barangay_about_id = $brgy_id 
				GROUP BY barangay_about_id
			"
        );

    }

    function elementary() {
        $forecastYear = date('Y');
        $schools = School::where('barangay_id','=', Session::get('brgy_id'))->get();
        $totalCapacity = 0;
        $totalEnrolled = 0;
        $availableSlots = 0;
        foreach ($schools as $school) {

            $cap = GradeCapacity::where('school_id','=', $school->id)
            ->where('school_year', '=', $forecastYear)->get();
            foreach ($cap as $gradeCap) {
                if (($gradeCap->grade == "Grade 1" || $gradeCap->grade == "Grade 2" || $gradeCap->grade == "Grade 3" || $gradeCap->grade == "Grade 4" || $gradeCap->grade == "Grade 5" || $gradeCap->grade == "Grade 6")) {
                    $totalCapacity += $gradeCap->max_capacity;
                    $totalEnrolled += $gradeCap->no_enrolled;
                    $availableSlots += $gradeCap->available_slot;
                }
            }
        }

        return view('report/forecast/elementary', ['forecastYear' => $forecastYear, 'totalCapacity' => $totalCapacity, 'totalEnrolled' => $totalEnrolled, 'availableSlots' => $availableSlots]);
    }
    function highSchool() {
        $forecastYear = date('Y');
        $schools = School::where('barangay_id','=', Session::get('brgy_id'))->get();
        $totalCapacity = 0;
        $totalEnrolled = 0;
        $availableSlots = 0;
        foreach ($schools as $school) {

            $cap = GradeCapacity::where('school_id','=', $school->id)
                ->where('school_year', '=', $forecastYear)->get();
            foreach ($cap as $gradeCap) {
                if (($gradeCap->grade == "Grade 7" || $gradeCap->grade == "Grade 8" || $gradeCap->grade == "Grade 9" || $gradeCap->grade == "Grade 10" )) {
                    $totalCapacity += $gradeCap->max_capacity;
                    $totalEnrolled += $gradeCap->no_enrolled;
                    $availableSlots += $gradeCap->available_slot;
                }
            }
        }

        return view('report/forecast/hs', ['forecastYear' => $forecastYear, 'totalCapacity' => $totalCapacity, 'totalEnrolled' => $totalEnrolled, 'availableSlots' => $availableSlots]);
    }

    function seniorHigh() {
        $forecastYear = date('Y');
        $schools = School::where('barangay_id','=', Session::get('brgy_id'))->get();
        $totalCapacity = 0;
        $totalEnrolled = 0;
        $availableSlots = 0;
        foreach ($schools as $school) {

            $cap = GradeCapacity::where('school_id','=', $school->id)
                ->where('school_year', '=', $forecastYear)->get();
            foreach ($cap as $gradeCap) {
                if (($gradeCap->grade == "STEM-11" || $gradeCap->grade == "GA-11" || $gradeCap->grade == "HUMM-11" || $gradeCap->grade == "ABM-11"  || $gradeCap->grade == "STEM-12" || $gradeCap->grade == "GA-12" || $gradeCap->grade == "HUMM-12" || $gradeCap->grade == "ABM-12")) {
                    $totalCapacity += $gradeCap->max_capacity;
                    $totalEnrolled += $gradeCap->no_enrolled;
                    $availableSlots += $gradeCap->available_slot;
                }
            }
        }

        return view('report/forecast/shs', ['forecastYear' => $forecastYear, 'totalCapacity' => $totalCapacity, 'totalEnrolled' => $totalEnrolled, 'availableSlots' => $availableSlots]);
    }

    function evacuationForecast() {
        $forecastYear = date('Y');
        $totalCapacity = 0;
        $numPeople=0;
        $evacs = DisasterEvacuationCenter::where('barangay_about_id', '=', Session::get('brgy_id'))->get();
        foreach ($evacs as $evac) {
            $totalCapacity += $evac->capacity;
            $numPeople += $evac->num_people;
        }
        $availableSlots = $totalCapacity - $numPeople;

        return view('report/forecast/evaucation-forecast', ['forecastYear' => $forecastYear, 'totalCapacity' => $totalCapacity, 'numPeople' => $numPeople, 'availableSlots' => $availableSlots]);
    }
 
}

