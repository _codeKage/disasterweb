<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;
use Response;
use App\BarangayAbout;
use App\BarangayCalendarEvent;
use App\BarangayPlan;
use App\BarangayAchievement;
use App\DisasterAdvisory;
use App\DisasterSafetyTip;
use App\DisasterPlan;
use App\DisasterKit;
use App\DisasterEvacuationCenter;
use App\ResidentHousehold;
use App\ResidentHouseholdMember;
use App\ResidentHouseholdSurveyA;
use App\ResidentHouseholdSurveyB;
use App\ResidentHouseholdSurveyC;
use App\ResidentHouseholdSurveyD;
use App\User;
use App\ActivityLog;

use App\Http\Requests;
use Carbon\Carbon;

class JSONController extends Controller
{
    function houseIdentificationNumber($house_identification_number,$barangay_id){
        $household = ResidentHousehold::where('house_identification_number',$house_identification_number)->where('barangay_about_id',$barangay_id)->count();
        if($household > 0){
            $unique = 0;
        } else {
            $unique = 1;
        }
        return Response::json(['result'=>'okay','unique'=>$unique],200, array(),JSON_PRETTY_PRINT);
    }
    function houseIdentificationNumberEdit($house_identification_number,$house_identification_number_old,$barangay_id){
        $household = ResidentHousehold::where('house_identification_number', '!=', $house_identification_number_old)->where('house_identification_number',$house_identification_number)->where('barangay_about_id',$barangay_id)->count();
        if($household > 0){
            $unique = 0;
        } else {
            $unique = 1;
        }
        return Response::json(['result'=>'okay','unique'=>$unique],200, array(),JSON_PRETTY_PRINT);
    }

    function usernameAdd($username){
        $user = User::where('username',$username)->count();
        if($user > 0){
            $unique = 0;
        } else {
            $unique = 1;
        }
        return Response::json(['result'=>'okay','unique'=>$unique],200, array(),JSON_PRETTY_PRINT);
    }
    
    function usernameEdit($username,$username_old){
        $user = User::where('username', '!=', $username_old)->where('username',$username)->count();
        if($user > 0){
            $unique = 0;
        } else {
            $unique = 1;
        }
        return Response::json(['result'=>'okay','unique'=>$unique],200, array(),JSON_PRETTY_PRINT);
    }

    function residentsMobile($barangay_id){
        $barangay_about = BarangayAbout::where('id',$barangay_id)->get();
        $barangay_calendar_event = BarangayCalendarEvent::where('barangay_about_id',$barangay_id)->get();
        $barangay_plans = BarangayPlan::where('barangay_about_id',$barangay_id)->get();
        $barangay_achievements = BarangayAchievement::where('barangay_about_id',$barangay_id)->get();
        $disaster_advisories = DisasterAdvisory::where('barangay_about_id',$barangay_id)->get();

        $know_the_risk = DisasterSafetyTip::select(
                        'hazard',
                        'description',
                        'facts',
                        'tips',
                        'created_at',
                        'updated_at'
                    )
                    ->orderBy('updated_at','desc')
                    ->get();

        $what_to_do = DisasterSafetyTip::select(
                        'hazard',
                        'before',
                        'during',
                        'after',
                        'created_at',
                        'updated_at'
                    )
                    ->orderBy('updated_at','desc')
                    ->get();

        $make_a_plan = DisasterPlan::where('barangay_about_id',$barangay_id)->get();
        $disaster_kit = DisasterKit::where('barangay_about_id',$barangay_id)->get();
        $evacuation_center = DisasterEvacuationCenter::where('barangay_about_id',$barangay_id)->get();

        return Response::json([
            'result'=>'okay',
            'barangay_about'=>$barangay_about,
            'barangay_calendar_event'=>$barangay_calendar_event,
            'barangay_plans'=>$barangay_plans,
            'barangay_achievements'=>$barangay_achievements,
            'disaster_advisories'=>$disaster_advisories,
            'know_the_risk'=>$know_the_risk,
            'what_to_do'=>$what_to_do,
            'make_a_plan'=>$make_a_plan,
            'disaster_kit'=>$disaster_kit,
            'evacuation_center'=>$evacuation_center
        ],200, array(),JSON_PRETTY_PRINT);
    }

    function secretaryMobile($barangay_id){
        $evacuation_centers = DisasterEvacuationCenter::where('barangay_about_id',$barangay_id)->get();
        $households = ResidentHousehold::with('resident_household_members','resident_household_survey_as','resident_household_survey_bs','resident_household_survey_cs','resident_household_survey_ds')->where('barangay_about_id',$barangay_id)->get();
        // $households = ResidentHousehold::with('resident_household_members')->where('barangay_about_id',$barangay_id)->get();

        return Response::json([
            'result'=>'okay',
            'households'=>$households,
            'evacuation_centers'=>$evacuation_centers
        ],200, array(),JSON_PRETTY_PRINT);
    }

    function login(Request $request){
        $user = User::where('username',$request->username)->where('access',1)->first();
    	if($user != null || $user != ''){
                if($user->usertype == 'Chairman' || $user->usertype == 'Secretary'){
                    if(password_verify($request->password,$user->password)){
                        $user_info = User::select('username','first_name','middle_initial','last_name','usertype')->find($user->id);
                        $barangay_info = BarangayAbout::find($user->barangay_about_id);
                        return Response::json(['result'=>'okay','user_info'=>$user_info,'barangay_info'=>$barangay_info],200, array(),JSON_PRETTY_PRINT);
                    }
                }
    	}
       	return Response::json(['result'=>'error','message'=>'You entered an invalid username/password.'],200, array(),JSON_PRETTY_PRINT);
    }

    function householdAdd(Request $request){
        $regexInt = '/^([+]?[1-9]\d*|0)$/';
        try{
            $household = ResidentHousehold::where('house_identification_number',$request->house_identification_number)->where('barangay_about_id',$request->barangay_id)->count();
            
            if($household > 0){
                return Response::json(['result'=>'error','message'=>'household identification number must be unique.'],200, array(),JSON_PRETTY_PRINT);
            }

            if(!preg_match($regexInt,$request->no_family)){
                return Response::json(['result'=>'error','message'=>'B1 must be a number.'],200, array(),JSON_PRETTY_PRINT);
            }

            if(!preg_match($regexInt,$request->no_members)){
                return Response::json(['result'=>'error','message'=>'B2 must be a number.'],200, array(),JSON_PRETTY_PRINT);
            }

            if(!preg_match($regexInt,$request->no_disability)){
                return Response::json(['result'=>'error','message'=>'B4 must be a number.'],200, array(),JSON_PRETTY_PRINT);
            }
            $resident_household = new ResidentHousehold();
            $resident_household->barangay_about_id = $request->barangay_id;
            $resident_household->province = $request->province;
            $resident_household->municipality = $request->municipality;
            $resident_household->province = $request->province;
            $resident_household->municipality = $request->municipality;
            $resident_household->barangay = $request->barangay;
            $resident_household->purok = $request->purok;
            $resident_household->street = $request->street;
            $resident_household->house_identification_number = $request->house_identification_number;
            $resident_household->latitude = $request->latitude;
            $resident_household->longitude = $request->longitude;
            $resident_household->name_of_respondent = $request->name_of_respondent;
            $resident_household->contact = $request->contact;
            $resident_household->no_family = $request->no_family;
            $resident_household->no_members = $request->no_members;
            $resident_household->disability = $request->disability;
            $resident_household->no_disability = $request->no_disability;
            $resident_household->building_type = $request->building_type;
            $resident_household->water_supply = $request->water_supply;
            $resident_household->toilet = $request->toilet;
            $resident_household->water_filtration = $request->water_filtration;
            $resident_household->electricity = $request->electricity;
            $resident_household->power_generator = $request->power_generator;
            $resident_household->save();

            $al = new ActivityLog();
            $al->barangay_about_id = $request->barangay_id;
            $al->username = $request->username;
            $al->description = 'Added '.$resident_household->house_identification_number.' on Resident Profile Household';
            $al->activity = 'add';
            $al->save();

            return Response::json(['result'=>'okay','message'=>'Successfully added '.$resident_household->house_identification_number,'household_id'=>$resident_household->id],200, array(),JSON_PRETTY_PRINT);
        } catch(\Illuminate\Database\QueryException $ex) {
            return Response::json(['result'=>'error','message'=>$ex],200, array(),JSON_PRETTY_PRINT);
        }
    }

    function householdMemberAdd(Request $request){
        try{
            $currentDate = Carbon::now();
            $birthdate = Carbon::parse($request->birthdate);
            $age = $currentDate->diffInYears($birthdate);
        } catch(\Exception $e){
            return Response::json(['result'=>'error','message'=>'Invalid birthdate format.'],200, array(),JSON_PRETTY_PRINT);
        }
        try{
            $resident_household_member = new ResidentHouseholdMember();
            $resident_household_member->resident_household_id = $request->resident_household_id;
            $resident_household_member->barangay_about_id = $request->barangay_about_id;
            
            $resident_household_member->first_name = $request->first_name;
            $resident_household_member->middle_name = $request->middle_name;
            $resident_household_member->last_name = $request->last_name;
            $resident_household_member->family_belong = $request->family_belong;
            $resident_household_member->relation = $request->relation;
            $resident_household_member->civil_status = $request->civil_status;
            $resident_household_member->gender = $request->gender;
            $resident_household_member->birthdate = $request->birthdate;
            $resident_household_member->age = $age;
            $resident_household_member->same_address = $request->same_address;
            $resident_household_member->attending_school = $request->attending_school;
            $resident_household_member->year_level = $request->year_level;
            $resident_household_member->highest_level = $request->highest_level;
            $resident_household_member->pregnant = $request->pregnant;
            $resident_household_member->have_children = $request->have_children;
            $resident_household_member->solo_parent = $request->solo_parent;
            $resident_household_member->disability = $request->disability;
            $resident_household_member->disability_type = $request->disability_type;
            $resident_household_member->health_problem = $request->health_problem;
            $resident_household_member->employed = $request->employed;
            $resident_household_member->occupation = $request->occupation;
            $resident_household_member->where_occupation = $request->where_occupation;
            $resident_household_member->save();

        $al = new ActivityLog();
        $al->barangay_about_id = $request->barangay_about_id;
        // $al->barangay_about_id = $request->barangay_id;
        $al->username = $request->username;
        $al->description = 'Added '.$resident_household_member->first_name.' '.$resident_household_member->last_name.' on Resident Profile Household Member';
        $al->activity = 'add';
        $al->save();

            return Response::json(['result'=>'okay','message'=>'Successfully added '.$resident_household_member->first_name.' '.$resident_household_member->last_name],200, array(),JSON_PRETTY_PRINT);
        } catch(\Illuminate\Database\QueryException $ex) {
            return Response::json(['result'=>'error','message'=>$ex],200, array(),JSON_PRETTY_PRINT);
        }
    }

    function householdSurveyAdd(Request $request){
        try{
            $resident_household = ResidentHousehold::find($request->resident_household_id);

            $survey_a = new ResidentHouseholdSurveyA();
            $survey_a->resident_household_id = $resident_household->id;
            $survey_a->barangay_about_id = $request->barangay_about_id;
            $survey_a->a1 = $request->a1;
            $survey_a->a2_1 = $request->a2_1;
            $survey_a->a3_1 = $request->a3_1;
            $survey_a->a4_1 = $request->a4_1;
            $survey_a->a5_1_1 = $request->a5_1_1;
            $survey_a->a5_1_2 = $request->a5_1_2;
            $survey_a->a5_1_3 = $request->a5_1_3;
            $survey_a->a5_1_4 = $request->a5_1_4;
            $survey_a->a5_1_5 = $request->a5_1_5;
            $survey_a->a2_2 = $request->a2_2;
            $survey_a->a3_2 = $request->a3_2;
            $survey_a->a4_2 = $request->a4_2;
            $survey_a->a5_2_1 = $request->a5_2_1;
            $survey_a->a5_2_2 = $request->a5_2_2;
            $survey_a->a5_2_3 = $request->a5_2_3;
            $survey_a->a5_2_4 = $request->a5_2_4;
            $survey_a->a5_2_5 = $request->a5_2_5;
            $survey_a->a2_3 = $request->a2_3;
            $survey_a->a3_3 = $request->a3_3;
            $survey_a->a4_3 = $request->a4_3;
            $survey_a->a5_3_1 = $request->a5_3_1;
            $survey_a->a5_3_2 = $request->a5_3_2;
            $survey_a->a5_3_3 = $request->a5_3_3;
            $survey_a->a5_3_4 = $request->a5_3_4;
            $survey_a->a5_3_5 = $request->a5_3_5;
            $survey_a->a2_4 = $request->a2_4;
            $survey_a->a3_4 = $request->a3_4;
            $survey_a->a4_4 = $request->a4_4;
            $survey_a->a5_4_1 = $request->a5_4_1;
            $survey_a->a5_4_2 = $request->a5_4_2;
            $survey_a->a5_4_3 = $request->a5_4_3;
            $survey_a->a5_4_4 = $request->a5_4_4;
            $survey_a->a5_4_5 = $request->a5_4_5;
            $survey_a->a2_5 = $request->a2_5;
            $survey_a->a3_5 = $request->a3_5;
            $survey_a->a4_5 = $request->a4_5;
            $survey_a->a5_5_1 = $request->a5_5_1;
            $survey_a->a5_5_2 = $request->a5_5_2;
            $survey_a->a5_5_3 = $request->a5_5_3;
            $survey_a->a5_5_4 = $request->a5_5_4;
            $survey_a->a5_5_5 = $request->a5_5_5;
            $survey_a->a2_6 = $request->a2_6;
            $survey_a->a3_6 = $request->a3_6;
            $survey_a->a4_6 = $request->a4_6;
            $survey_a->a5_6_1 = $request->a5_6_1;
            $survey_a->a5_6_2 = $request->a5_6_2;
            $survey_a->a5_6_3 = $request->a5_6_3;
            $survey_a->a5_6_4 = $request->a5_6_4;
            $survey_a->a5_6_5 = $request->a5_6_5;
            $survey_a->a2_7 = $request->a2_7;
            $survey_a->a3_7 = $request->a3_7;
            $survey_a->a4_7 = $request->a4_7;
            $survey_a->a5_7_1 = $request->a5_7_1;
            $survey_a->a5_7_2 = $request->a5_7_2;
            $survey_a->a5_7_3 = $request->a5_7_3;
            $survey_a->a5_7_4 = $request->a5_7_4;
            $survey_a->a5_7_5 = $request->a5_7_5;
            $survey_a->a2_8 = $request->a2_8;
            $survey_a->a3_8 = $request->a3_8;
            $survey_a->a4_8 = $request->a4_8;
            $survey_a->a5_8_1 = $request->a5_8_1;
            $survey_a->a5_8_2 = $request->a5_8_2;
            $survey_a->a5_8_3 = $request->a5_8_3;
            $survey_a->a5_8_4 = $request->a5_8_4;
            $survey_a->a5_8_5 = $request->a5_8_5;
            $survey_a->a2_9 = $request->a2_9;
            $survey_a->a3_9 = $request->a3_9;
            $survey_a->a4_9 = $request->a4_9;
            $survey_a->a5_9_1 = $request->a5_9_1;
            $survey_a->a5_9_2 = $request->a5_9_2;
            $survey_a->a5_9_3 = $request->a5_9_3;
            $survey_a->a5_9_4 = $request->a5_9_4;
            $survey_a->a5_9_5 = $request->a5_9_5;
            $survey_a->a2_10 = $request->a2_10;
            $survey_a->a3_10 = $request->a3_10;
            $survey_a->a4_10 = $request->a4_10;
            $survey_a->a5_10_1 = $request->a5_10_1;
            $survey_a->a5_10_2 = $request->a5_10_2;
            $survey_a->a5_10_3 = $request->a5_10_3;
            $survey_a->a5_10_4 = $request->a5_10_4;
            $survey_a->a5_10_5 = $request->a5_10_5;
            $survey_a->a2_11 = $request->a2_11;
            $survey_a->a3_11 = $request->a3_11;
            $survey_a->a4_11 = $request->a4_11;
            $survey_a->a5_11_1 = $request->a5_11_1;
            $survey_a->a5_11_2 = $request->a5_11_2;
            $survey_a->a5_11_3 = $request->a5_11_3;
            $survey_a->a5_11_4 = $request->a5_11_4;
            $survey_a->a5_11_5 = $request->a5_11_5;
            $survey_a->a2_12 = $request->a2_12;
            $survey_a->a3_12 = $request->a3_12;
            $survey_a->a4_12 = $request->a4_12;
            $survey_a->a5_12_1 = $request->a5_12_1;
            $survey_a->a5_12_2 = $request->a5_12_2;
            $survey_a->a5_12_3 = $request->a5_12_3;
            $survey_a->a5_12_4 = $request->a5_12_4;
            $survey_a->a5_12_5 = $request->a5_12_5;
            $survey_a->a2_13 = $request->a2_13;
            $survey_a->a3_13 = $request->a3_13;
            $survey_a->a4_13 = $request->a4_13;
            $survey_a->a5_13_1 = $request->a5_13_1;
            $survey_a->a5_13_2 = $request->a5_13_2;
            $survey_a->a5_13_3 = $request->a5_13_3;
            $survey_a->a5_13_4 = $request->a5_13_4;
            $survey_a->a5_13_5 = $request->a5_13_5;
            $survey_a->save();

            $survey_b = new ResidentHouseholdSurveyB();
            
            $survey_b->resident_household_id = $resident_household->id;
            $survey_b->barangay_about_id = $request->barangay_about_id;
            $survey_b->b1 = $request->b1;
            $survey_b->b2_1 = $request->b2_1;
            $survey_b->b2_2 = $request->b2_2;
            $survey_b->b2_3 = $request->b2_3;
            $survey_b->b2_4 = $request->b2_4;
            $survey_b->b2_5 = $request->b2_5;
            $survey_b->b2_6 = $request->b2_6;
            $survey_b->b2_7 = $request->b2_7;
            $survey_b->b2_8 = $request->b2_8;
            $survey_b->b2_9 = $request->b2_9;
            $survey_b->b2_10 = $request->b2_10;
            $survey_b->b2_11 = $request->b2_11;
            $survey_b->b2_12 = $request->b2_12;
            $survey_b->b2_13 = $request->b2_13;
            $survey_b->b2_14 = $request->b2_14;
            $survey_b->b3_1 = $request->b3_1;
            $survey_b->b3_2 = $request->b3_2;
            $survey_b->b3_3 = $request->b3_3;
            $survey_b->b3_4 = $request->b3_4;
            $survey_b->b3_5 = $request->b3_5;
            $survey_b->b3_6 = $request->b3_6;
            $survey_b->b3_7 = $request->b3_7;
            $survey_b->b3_8 = $request->b3_8;
            $survey_b->b3_9 = $request->b3_9;
            $survey_b->b3_10 = $request->b3_10;
            $survey_b->b3_11 = $request->b3_11;
            $survey_b->b3_12 = $request->b3_12;
            $survey_b->b3_13 = $request->b3_13;
            $survey_b->b3_14 = $request->b3_14;
            $survey_b->b3_15 = $request->b3_15;
            $survey_b->b4 = $request->b4;
            $survey_b->save();

            $survey_c = new ResidentHouseholdSurveyC();
            $survey_c->resident_household_id = $resident_household->id;
            $survey_c->barangay_about_id = $request->barangay_about_id;
            $survey_c->c1 = $request->c1;
            $survey_c->c2 = $request->c2;
            $survey_c->c3 = $request->c3;
            $survey_c->c4_1 = $request->c4_1;
            $survey_c->c4_2 = $request->c4_2;
            $survey_c->c4_3 = $request->c4_3;
            $survey_c->c4_4 = $request->c4_4;
            $survey_c->c4_5 = $request->c4_5;
            $survey_c->c4_6 = $request->c4_6;
            $survey_c->c5 = $request->c5;
            $survey_c->c6 = $request->c6;
            $survey_c->save();

            $survey_d = new ResidentHouseholdSurveyD();
            $survey_d->resident_household_id = $resident_household->id;
            $survey_d->barangay_about_id = $request->barangay_about_id;
            $survey_d->d1 = $request->d1;
            $survey_d->d2 = $request->d2;
            $survey_d->d3 = $request->d3;
            $survey_d->d4 = $request->d4;
            $survey_d->d5 = $request->d5;
            $survey_d->d6 = $request->d6;
            $survey_d->d7 = $request->d7;
            $survey_d->d8 = $request->d8;
            $survey_d->d9 = $request->d9;
            $survey_d->d10_1 = $request->d10_1;
            $survey_d->d10_2 = $request->d10_2;
            $survey_d->d10_3 = $request->d10_3;
            $survey_d->d10_4 = $request->d10_4;
            $survey_d->d10_5 = $request->d10_5;
            $survey_d->d10_6 = $request->d10_6;
            $survey_d->d10_7 = $request->d10_7;
            $survey_d->d10_8 = $request->d10_8;
            $survey_d->d10_9 = $request->d10_9;
            $survey_d->d10_10 = $request->d10_10;
            $survey_d->d10_11 = $request->d10_11;
            $survey_d->d10_12 = $request->d10_12;
            $survey_d->d10_13 = $request->d10_13;
            $survey_d->d10_14 = $request->d10_14;
            $survey_d->save();

            $al = new ActivityLog();
            $al->barangay_about_id = $request->barangay_about_id;
            // $al->barangay_about_id = $request->barangay_id;
            $al->username = $request->username;
            $al->description = 'Added a survey on Resident Profile Household that has house identification number of '.$resident_household->house_identification_number;
            $al->activity = 'add';
            $al->save();

            return Response::json(['result'=>'okay','message'=>'Added survey to household with identification number of '.$resident_household->house_identification_number],200, array(),JSON_PRETTY_PRINT);
        } catch(\Illuminate\Database\QueryException $ex) {
            return Response::json(['result'=>'error','message'=>$ex],200, array(),JSON_PRETTY_PRINT);
        }
    }

    function evacuationCenterAdd(Request $request){
	    try{
            $disaster_evacuation_center = new DisasterEvacuationCenter();
            $disaster_evacuation_center->address = $request->address;
            $disaster_evacuation_center->barangay_about_id = $request->barangay_about_id;

            $disaster_evacuation_center->latitude = $request->latitude;
            $disaster_evacuation_center->longitude = $request->longitude;
            $disaster_evacuation_center->purok = $request->purok;
            $disaster_evacuation_center->street = $request->street;
            $disaster_evacuation_center->barangay = $request->barangay;
            $disaster_evacuation_center->municipality = $request->municipality;
            $disaster_evacuation_center->province = $request->province;
            $disaster_evacuation_center->name = $request->name;
            $disaster_evacuation_center->type = $request->type;
            $disaster_evacuation_center->condition = $request->condition;
            $disaster_evacuation_center->incharge = $request->incharge;
            $disaster_evacuation_center->contact = $request->contact;
            $disaster_evacuation_center->capacity = $request->capacity;
            $disaster_evacuation_center->facility_toilet = $request->facility_toilet;
            $disaster_evacuation_center->facility_access_to_water = $request->facility_access_to_water;
            $disaster_evacuation_center->facility_with_electricity = $request->facility_with_electricity;
            $disaster_evacuation_center->facility_working_generator = $request->facility_working_generator;
            $disaster_evacuation_center->facility_with_private_rooms = $request->facility_with_private_rooms;
            $disaster_evacuation_center->facility_with_lactating_room = $request->facility_with_lactating_room;
            $disaster_evacuation_center->facility_kitchen = $request->facility_kitchen;
            $disaster_evacuation_center->applicable_typhoon = $request->applicable_typhoon;
            $disaster_evacuation_center->applicable_flood = $request->applicable_flood;
            $disaster_evacuation_center->applicable_storm_surge = $request->applicable_storm_surge;
            $disaster_evacuation_center->applicable_landslide = $request->applicable_landslide;
            $disaster_evacuation_center->applicable_drought = $request->applicable_drought;
            $disaster_evacuation_center->applicable_volcanic_eruption = $request->applicable_volcanic_eruption;
            $disaster_evacuation_center->applicable_sinkhole = $request->applicable_sinkhole;
            $disaster_evacuation_center->save();

            $al = new ActivityLog();
            $al->barangay_about_id = $request->barangay_id;
            $al->username = $request->username;
            $al->description = 'Added '.$request->name.' on Disaster Admin Evacuation Center';
            $al->activity = 'add';
            $al->save();

	       return Response::json(['result'=>'okay','message'=>$disaster_evacuation_center->name.' was Added!!'],200, array(),JSON_PRETTY_PRINT);
	    } catch(\Illuminate\Database\QueryException $ex) {
            return Response::json(['result'=>'error','message'=>$ex],200, array(),JSON_PRETTY_PRINT);
        }
    }

    function householdUpdate(Request $request){
        $regexInt = '/^([+]?[1-9]\d*|0)$/';
        try{
            $household = ResidentHousehold::where('house_identification_number', '!=', $house_identification_number_old)->where('house_identification_number',$house_identification_number)->count();
            
            if($household > 0){
                return Response::json(['result'=>'error','message'=>'household identification number must be unique.'],200, array(),JSON_PRETTY_PRINT);
            }

            if(!preg_match($regexInt,$request->no_family)){
                return Response::json(['result'=>'error','message'=>'B1 must be a number.'],200, array(),JSON_PRETTY_PRINT);
            }

            if(!preg_match($regexInt,$request->no_members)){
                return Response::json(['result'=>'error','message'=>'B2 must be a number.'],200, array(),JSON_PRETTY_PRINT);
            }

            if(!preg_match($regexInt,$request->no_disability)){
                return Response::json(['result'=>'error','message'=>'B4 must be a number.'],200, array(),JSON_PRETTY_PRINT);
            }
            $resident_household = ResidentHousehold::find($request->id);
            $resident_household->province = $request->province;
            $resident_household->municipality = $request->municipality;
            $resident_household->province = $request->province;
            $resident_household->municipality = $request->municipality;
            $resident_household->barangay = $request->barangay;
            $resident_household->purok = $request->purok;
            $resident_household->street = $request->street;
            $resident_household->house_identification_number = $request->house_identification_number;
            $resident_household->latitude = $request->latitude;
            $resident_household->longitude = $request->longitude;
            $resident_household->name_of_respondent = $request->name_of_respondent;
            $resident_household->contact = $request->contact;
            $resident_household->no_family = $request->no_family;
            $resident_household->no_members = $request->no_members;
            $resident_household->disability = $request->disability;
            $resident_household->no_disability = $request->no_disability;
            $resident_household->building_type = $request->building_type;
            $resident_household->water_supply = $request->water_supply;
            $resident_household->toilet = $request->toilet;
            $resident_household->water_filtration = $request->water_filtration;
            $resident_household->electricity = $request->electricity;
            $resident_household->power_generator = $request->power_generator;
            $resident_household->update();

            $al = new ActivityLog();
            $al->barangay_about_id = $request->barangay_id;
            $al->username = $request->username;
            $al->description = 'Updated '.$resident_household->house_identification_number.' on Resident Profile Household';
            $al->activity = 'update';
            $al->save();

            return Response::json(['result'=>'okay','message'=>'Successfully Updated '.$resident_household->house_identification_number],200, array(),JSON_PRETTY_PRINT);
        } catch(\Illuminate\Database\QueryException $ex) {
            return Response::json(['result'=>'error','message'=>$ex],200, array(),JSON_PRETTY_PRINT);
        }
    }

    function householdMemberUpdate(Request $request){
        try{
            $currentDate = Carbon::now();
            $birthdate = Carbon::parse($request->birthdate);
            $age = $currentDate->diffInYears($birthdate);
        } catch(\Exception $e){
            return Response::json(['result'=>'error','message'=>'Invalid birthdate format.'],200, array(),JSON_PRETTY_PRINT);
        }
        try{
            $resident_household_member = ResidentHouseholdMember::find($request->id);

            $resident_household_member->resident_household_id = $request->resident_household_id;
            $resident_household_member->barangay_about_id = $request->barangay_about_id;


            $resident_household_member->first_name = $request->first_name;
            $resident_household_member->middle_name = $request->middle_name;
            $resident_household_member->last_name = $request->last_name;
            $resident_household_member->family_belong = $request->family_belong;
            $resident_household_member->relation = $request->relation;
            $resident_household_member->civil_status = $request->civil_status;
            $resident_household_member->gender = $request->gender;
            $resident_household_member->birthdate = $request->birthdate;
            $resident_household_member->age = $age;
            // $resident_household_member->age = $request->age;
            $resident_household_member->same_address = $request->same_address;
            $resident_household_member->attending_school = $request->attending_school;
            $resident_household_member->year_level = $request->year_level;
            $resident_household_member->highest_level = $request->highest_level;
            $resident_household_member->pregnant = $request->pregnant;
            $resident_household_member->have_children = $request->have_children;
            $resident_household_member->solo_parent = $request->solo_parent;
            $resident_household_member->disability = $request->disability;
            $resident_household_member->disability_type = $request->disability_type;
            $resident_household_member->health_problem = $request->health_problem;
            $resident_household_member->employed = $request->employed;
            $resident_household_member->occupation = $request->occupation;
            $resident_household_member->where_occupation = $request->where_occupation;
            $resident_household_member->update();

        $al = new ActivityLog();
        $al->barangay_about_id = $request->barangay_about_id;
        // $al->barangay_about_id = $request->barangay_id;
        $al->username = $request->username;
        $al->description = 'Updated '.$resident_household_member->first_name.' '.$resident_household_member->last_name.' on Resident Profile Household Member';
        $al->activity = 'update';
        $al->save();

            return Response::json(['result'=>'okay','message'=>'Successfully Updated '.$resident_household_member->first_name.' '.$resident_household_member->last_name],200, array(),JSON_PRETTY_PRINT);
        } catch(\Illuminate\Database\QueryException $ex) {
            return Response::json(['result'=>'error','message'=>$ex],200, array(),JSON_PRETTY_PRINT);
        }
    }

    function householdSurveyUpdate(Request $request){
        try{
            $resident_household = ResidentHousehold::find($request->resident_household_id);

            $survey_a = ResidentHouseholdSurveyA::where('resident_household_id',$resident_household->id)->first();
            $survey_a->a1 = $request->a1;
            $survey_a->a2_1 = $request->a2_1;
            $survey_a->a3_1 = $request->a3_1;
            $survey_a->a4_1 = $request->a4_1;
            $survey_a->a5_1_1 = $request->a5_1_1;
            $survey_a->a5_1_2 = $request->a5_1_2;
            $survey_a->a5_1_3 = $request->a5_1_3;
            $survey_a->a5_1_4 = $request->a5_1_4;
            $survey_a->a5_1_5 = $request->a5_1_5;
            $survey_a->a2_2 = $request->a2_2;
            $survey_a->a3_2 = $request->a3_2;
            $survey_a->a4_2 = $request->a4_2;
            $survey_a->a5_2_1 = $request->a5_2_1;
            $survey_a->a5_2_2 = $request->a5_2_2;
            $survey_a->a5_2_3 = $request->a5_2_3;
            $survey_a->a5_2_4 = $request->a5_2_4;
            $survey_a->a5_2_5 = $request->a5_2_5;
            $survey_a->a2_3 = $request->a2_3;
            $survey_a->a3_3 = $request->a3_3;
            $survey_a->a4_3 = $request->a4_3;
            $survey_a->a5_3_1 = $request->a5_3_1;
            $survey_a->a5_3_2 = $request->a5_3_2;
            $survey_a->a5_3_3 = $request->a5_3_3;
            $survey_a->a5_3_4 = $request->a5_3_4;
            $survey_a->a5_3_5 = $request->a5_3_5;
            $survey_a->a2_4 = $request->a2_4;
            $survey_a->a3_4 = $request->a3_4;
            $survey_a->a4_4 = $request->a4_4;
            $survey_a->a5_4_1 = $request->a5_4_1;
            $survey_a->a5_4_2 = $request->a5_4_2;
            $survey_a->a5_4_3 = $request->a5_4_3;
            $survey_a->a5_4_4 = $request->a5_4_4;
            $survey_a->a5_4_5 = $request->a5_4_5;
            $survey_a->a2_5 = $request->a2_5;
            $survey_a->a3_5 = $request->a3_5;
            $survey_a->a4_5 = $request->a4_5;
            $survey_a->a5_5_1 = $request->a5_5_1;
            $survey_a->a5_5_2 = $request->a5_5_2;
            $survey_a->a5_5_3 = $request->a5_5_3;
            $survey_a->a5_5_4 = $request->a5_5_4;
            $survey_a->a5_5_5 = $request->a5_5_5;
            $survey_a->a2_6 = $request->a2_6;
            $survey_a->a3_6 = $request->a3_6;
            $survey_a->a4_6 = $request->a4_6;
            $survey_a->a5_6_1 = $request->a5_6_1;
            $survey_a->a5_6_2 = $request->a5_6_2;
            $survey_a->a5_6_3 = $request->a5_6_3;
            $survey_a->a5_6_4 = $request->a5_6_4;
            $survey_a->a5_6_5 = $request->a5_6_5;
            $survey_a->a2_7 = $request->a2_7;
            $survey_a->a3_7 = $request->a3_7;
            $survey_a->a4_7 = $request->a4_7;
            $survey_a->a5_7_1 = $request->a5_7_1;
            $survey_a->a5_7_2 = $request->a5_7_2;
            $survey_a->a5_7_3 = $request->a5_7_3;
            $survey_a->a5_7_4 = $request->a5_7_4;
            $survey_a->a5_7_5 = $request->a5_7_5;
            $survey_a->a2_8 = $request->a2_8;
            $survey_a->a3_8 = $request->a3_8;
            $survey_a->a4_8 = $request->a4_8;
            $survey_a->a5_8_1 = $request->a5_8_1;
            $survey_a->a5_8_2 = $request->a5_8_2;
            $survey_a->a5_8_3 = $request->a5_8_3;
            $survey_a->a5_8_4 = $request->a5_8_4;
            $survey_a->a5_8_5 = $request->a5_8_5;
            $survey_a->a2_9 = $request->a2_9;
            $survey_a->a3_9 = $request->a3_9;
            $survey_a->a4_9 = $request->a4_9;
            $survey_a->a5_9_1 = $request->a5_9_1;
            $survey_a->a5_9_2 = $request->a5_9_2;
            $survey_a->a5_9_3 = $request->a5_9_3;
            $survey_a->a5_9_4 = $request->a5_9_4;
            $survey_a->a5_9_5 = $request->a5_9_5;
            $survey_a->a2_10 = $request->a2_10;
            $survey_a->a3_10 = $request->a3_10;
            $survey_a->a4_10 = $request->a4_10;
            $survey_a->a5_10_1 = $request->a5_10_1;
            $survey_a->a5_10_2 = $request->a5_10_2;
            $survey_a->a5_10_3 = $request->a5_10_3;
            $survey_a->a5_10_4 = $request->a5_10_4;
            $survey_a->a5_10_5 = $request->a5_10_5;
            $survey_a->a2_11 = $request->a2_11;
            $survey_a->a3_11 = $request->a3_11;
            $survey_a->a4_11 = $request->a4_11;
            $survey_a->a5_11_1 = $request->a5_11_1;
            $survey_a->a5_11_2 = $request->a5_11_2;
            $survey_a->a5_11_3 = $request->a5_11_3;
            $survey_a->a5_11_4 = $request->a5_11_4;
            $survey_a->a5_11_5 = $request->a5_11_5;
            $survey_a->a2_12 = $request->a2_12;
            $survey_a->a3_12 = $request->a3_12;
            $survey_a->a4_12 = $request->a4_12;
            $survey_a->a5_12_1 = $request->a5_12_1;
            $survey_a->a5_12_2 = $request->a5_12_2;
            $survey_a->a5_12_3 = $request->a5_12_3;
            $survey_a->a5_12_4 = $request->a5_12_4;
            $survey_a->a5_12_5 = $request->a5_12_5;
            $survey_a->a2_13 = $request->a2_13;
            $survey_a->a3_13 = $request->a3_13;
            $survey_a->a4_13 = $request->a4_13;
            $survey_a->a5_13_1 = $request->a5_13_1;
            $survey_a->a5_13_2 = $request->a5_13_2;
            $survey_a->a5_13_3 = $request->a5_13_3;
            $survey_a->a5_13_4 = $request->a5_13_4;
            $survey_a->a5_13_5 = $request->a5_13_5;
            $survey_a->update();

            $survey_b = ResidentHouseholdSurveyB::where('resident_household_id',$resident_household->id)->first();
            $survey_b->resident_household_id = $resident_household->id;
            $survey_b->b1 = $request->b1;
            $survey_b->b2_1 = $request->b2_1;
            $survey_b->b2_2 = $request->b2_2;
            $survey_b->b2_3 = $request->b2_3;
            $survey_b->b2_4 = $request->b2_4;
            $survey_b->b2_5 = $request->b2_5;
            $survey_b->b2_6 = $request->b2_6;
            $survey_b->b2_7 = $request->b2_7;
            $survey_b->b2_8 = $request->b2_8;
            $survey_b->b2_9 = $request->b2_9;
            $survey_b->b2_10 = $request->b2_10;
            $survey_b->b2_11 = $request->b2_11;
            $survey_b->b2_12 = $request->b2_12;
            $survey_b->b2_13 = $request->b2_13;
            $survey_b->b2_14 = $request->b2_14;
            $survey_b->b3_1 = $request->b3_1;
            $survey_b->b3_2 = $request->b3_2;
            $survey_b->b3_3 = $request->b3_3;
            $survey_b->b3_4 = $request->b3_4;
            $survey_b->b3_5 = $request->b3_5;
            $survey_b->b3_6 = $request->b3_6;
            $survey_b->b3_7 = $request->b3_7;
            $survey_b->b3_8 = $request->b3_8;
            $survey_b->b3_9 = $request->b3_9;
            $survey_b->b3_10 = $request->b3_10;
            $survey_b->b3_11 = $request->b3_11;
            $survey_b->b3_12 = $request->b3_12;
            $survey_b->b3_13 = $request->b3_13;
            $survey_b->b3_14 = $request->b3_14;
            $survey_b->b3_15 = $request->b3_15;
            $survey_b->b4 = $request->b4;
            $survey_b->update();

            $survey_c = ResidentHouseholdSurveyC::where('resident_household_id',$resident_household->id)->first();
            $survey_c->resident_household_id = $resident_household->id;
            $survey_c->c1 = $request->c1;
            $survey_c->c2 = $request->c2;
            $survey_c->c3 = $request->c3;
            $survey_c->c4_1 = $request->c4_1;
            $survey_c->c4_2 = $request->c4_2;
            $survey_c->c4_3 = $request->c4_3;
            $survey_c->c4_4 = $request->c4_4;
            $survey_c->c4_5 = $request->c4_5;
            $survey_c->c4_6 = $request->c4_6;
            $survey_c->c5 = $request->c5;
            $survey_c->c6 = $request->c6;
            $survey_c->update();

            $survey_d = ResidentHouseholdSurveyD::where('resident_household_id',$resident_household->id)->first();
            $survey_d->resident_household_id = $resident_household->id;
            $survey_d->d1 = $request->d1;
            $survey_d->d2 = $request->d2;
            $survey_d->d3 = $request->d3;
            $survey_d->d4 = $request->d4;
            $survey_d->d5 = $request->d5;
            $survey_d->d6 = $request->d6;
            $survey_d->d7 = $request->d7;
            $survey_d->d8 = $request->d8;
            $survey_d->d9 = $request->d9;
            $survey_d->d10_1 = $request->d10_1;
            $survey_d->d10_2 = $request->d10_2;
            $survey_d->d10_3 = $request->d10_3;
            $survey_d->d10_4 = $request->d10_4;
            $survey_d->d10_5 = $request->d10_5;
            $survey_d->d10_6 = $request->d10_6;
            $survey_d->d10_7 = $request->d10_7;
            $survey_d->d10_8 = $request->d10_8;
            $survey_d->d10_9 = $request->d10_9;
            $survey_d->d10_10 = $request->d10_10;
            $survey_d->d10_11 = $request->d10_11;
            $survey_d->d10_12 = $request->d10_12;
            $survey_d->d10_13 = $request->d10_13;
            $survey_d->d10_14 = $request->d10_14;
            $survey_d->update();

            $al = new ActivityLog();
            $al->barangay_about_id = $request->barangay_id;
            $al->username = $request->username;
            $al->description = 'Updated a survey on Resident Profile Household that has house identification number of '.$resident_household->house_identification_number;
            $al->activity = 'update';
            $al->save();

            return Response::json(['result'=>'okay','message'=>'Updated survey to household with identification number of '.$resident_household->house_identification_number],200, array(),JSON_PRETTY_PRINT);
        } catch(\Illuminate\Database\QueryException $ex) {
            return Response::json(['result'=>'error','message'=>$ex],200, array(),JSON_PRETTY_PRINT);
        }
    }

    function evacuationCenterUpdate(Request $request){
    try{
            $disaster_evacuation_center = DisasterEvacuationCenter::find($request->id);
            $disaster_evacuation_center->address = $request->address;
            $disaster_evacuation_center->latitude = $request->latitude;
            $disaster_evacuation_center->longitude = $request->longitude;
            $disaster_evacuation_center->purok = $request->purok;
            $disaster_evacuation_center->street = $request->street;
            $disaster_evacuation_center->barangay = $request->barangay;
            $disaster_evacuation_center->municipality = $request->municipality;
            $disaster_evacuation_center->province = $request->province;
            $disaster_evacuation_center->name = $request->name;
            $disaster_evacuation_center->type = $request->type;
            $disaster_evacuation_center->condition = $request->condition;
            $disaster_evacuation_center->incharge = $request->incharge;
            $disaster_evacuation_center->contact = $request->contact;
            $disaster_evacuation_center->capacity = $request->capacity;
            $disaster_evacuation_center->no_people = $request->no_people;
            $disaster_evacuation_center->facility_toilet = $request->facility_toilet;
            $disaster_evacuation_center->facility_access_to_water = $request->facility_access_to_water;
            $disaster_evacuation_center->facility_with_electricity = $request->facility_with_electricity;
            $disaster_evacuation_center->facility_working_generator = $request->facility_working_generator;
            $disaster_evacuation_center->facility_with_private_rooms = $request->facility_with_private_rooms;
            $disaster_evacuation_center->facility_with_lactating_room = $request->facility_with_lactating_room;
            $disaster_evacuation_center->facility_kitchen = $request->facility_kitchen;
            $disaster_evacuation_center->applicable_typhoon = $request->applicable_typhoon;
            $disaster_evacuation_center->applicable_flood = $request->applicable_flood;
            $disaster_evacuation_center->applicable_storm_surge = $request->applicable_storm_surge;
            $disaster_evacuation_center->applicable_landslide = $request->applicable_landslide;
            $disaster_evacuation_center->applicable_drought = $request->applicable_drought;
            $disaster_evacuation_center->applicable_volcanic_eruption = $request->applicable_volcanic_eruption;
            $disaster_evacuation_center->applicable_sinkhole = $request->applicable_sinkhole;
            $disaster_evacuation_center->update();

            $al = new ActivityLog();
            $al->barangay_about_id = $request->barangay_id;
            $al->username = $request->username;
            $al->description = 'Updated '.$request->name.' on Disaster Admin Evacuation Center';
            $al->activity = 'update';
            $al->save();

        return Response::json(['result'=>'okay','message'=>$disaster_evacuation_center->name.' was Updated!!'],200, array(),JSON_PRETTY_PRINT);
    } catch(\Illuminate\Database\QueryException $ex) {
            return Response::json(['result'=>'error','message'=>$ex],200, array(),JSON_PRETTY_PRINT);
        }
    }

    public function barangayDetails(){
        $brgyId = $brgy_id =Session::get("brgy_id");
        $barangayDetails = BarangayAbout::find($brgyId);

        return Response::json(['result'=>'okay','barangayDetails'=>$barangayDetails],200, array(),JSON_PRETTY_PRINT);
    }

}
