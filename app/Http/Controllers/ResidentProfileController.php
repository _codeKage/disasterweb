<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use DB;

use App\Http\Requests;

use App\ResidentHousehold;
use App\ResidentHouseholdMember;
use App\ResidentHouseholdSurveyA;
use App\ResidentHouseholdSurveyB;
use App\ResidentHouseholdSurveyC;
use App\ResidentHouseholdSurveyD;
use App\ActivityLog;
use App\YearlySurvey;
use Carbon\Carbon;
use Log;
class ResidentProfileController extends Controller
{
    function getMembers($id)
    {
        // Log::info('get members'.$id);
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resident_household_members = ResidentHouseholdMember::where('resident_household_id',$id)
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->withTrashed()
                ->paginate(25);
        }
        else
        {
            $resident_household_members = ResidentHouseholdMember::where('resident_household_id',$id)
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('last_name','desc')
                ->withTrashed()
                ->paginate(25);
        }


        // session()->flash('id',$id);
        return view('resident.household_member',compact('resident_household_members'));
    }

    function householdSearch(Request $request){
        if(Input::get('sortby')!=null)
        {
            // Log::info('1'.Session::get('barangayAbout')->id);
            $val = explode(".",Input::get('sortby'));
            $resident_households = ResidentHousehold::with('resident_household_members','yearly_survey')
                ->Where(function ($query) use ($request) {
                    $query->where('house_identification_number','like', '%'.$request->search.'%')
                        ->orWhere('street','like', '%'.$request->search.'%')
                        ->orWhere('contact','like', '%'.$request->search.'%')
                        ->orWhere('purok','like', '%'.$request->search.'%');
                })
                ->orWhereHas('resident_household_members', function ($query) use ($request){
                    $query->where('last_name', 'like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->withTrashed()
                ->paginate(25);
        }
        else
        {
            // Log::info('2'.Session::get('barangayAbout')->id);
            $resident_households = ResidentHousehold::with('resident_household_members','yearly_survey')
                ->Where(function ($query) use ($request) {
                    $query->where('house_identification_number','like', '%'.$request->search.'%')
                        ->orWhere('street','like', '%'.$request->search.'%')
                        ->orWhere('contact','like', '%'.$request->search.'%')
                        ->orWhere('purok','like', '%'.$request->search.'%');
                })
                ->orWhereHas('resident_household_members', function ($query) use ($request){
                    $query->where('last_name', 'like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('updated_at','desc')
                ->withTrashed()
                ->paginate(25);
        }
        return view('resident.household',compact('resident_households'));
    }


    function household(Request $request){
        $orderParam1 = 'updated_at';
        $orderParam2 = 'desc';
        $currentYear = date("Y");

        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $orderParam1 = $val[0];
            $prderParam2 = $val[1];
        }

        $resident_households = ResidentHousehold::with(['resident_household_members'])

            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($orderParam1,$orderParam2)
            ->withTrashed()
            ->paginate(10);

        foreach($resident_households as $household){

            $yearly_survey = DB::table('yearly_survey')
                ->where('yearly_survey.resident_household_id', '=',$household->id)
                ->where('yearly_survey.barangay_id','=',$household->barangay_about_id)
                ->where('yearly_survey.year','=',$currentYear);

            $survey_a = $yearly_survey
                ->leftjoin('resident_household_survey_as','yearly_survey.survey_a_id','=','resident_household_survey_as.id')
                ->select('resident_household_survey_as.*','yearly_survey.year AS survey_year')
                ->first();

            $survey_b = $yearly_survey
                ->leftjoin('resident_household_survey_bs','yearly_survey.survey_b_id','=','resident_household_survey_bs.id')
                ->select('resident_household_survey_bs.*','yearly_survey.year AS survey_year')
                ->first();

            $survey_c = $yearly_survey
                ->leftjoin('resident_household_survey_cs','yearly_survey.survey_c_id','=','resident_household_survey_cs.id')
                ->select('resident_household_survey_cs.*','yearly_survey.year AS survey_year')
                ->first();

            $survey_d = $yearly_survey
                ->leftjoin('resident_household_survey_ds','yearly_survey.survey_d_id','=','resident_household_survey_ds.id')
                ->select('resident_household_survey_ds.*','yearly_survey.year AS survey_year')
                ->first();

            $household->survey_a = $survey_a;
            $household->survey_b = $survey_b;
            $household->survey_c = $survey_c;
            $household->survey_d = $survey_d;

            // $household->survey = $survey;
            Log::info('survey', ['household'=>$household]);

        }

        Log::info('$resident_households '.$resident_households);
        // return response()->json($resident_households,200, array(),JSON_PRETTY_PRINT);

        return view('resident.household',compact('resident_households'));
    }

    function householdStore(Request $request){
        Log::info($request);
        $resident_household = new ResidentHousehold();
        $resident_household->barangay_about_id = Session::get('barangayAbout')->id;
        $resident_household->province = $request->province;
        $resident_household->municipality = $request->municipality;
        $resident_household->province = $request->province;
        $resident_household->municipality = $request->municipality;
        $resident_household->barangay = $request->barangay;
        $resident_household->purok = $request->purok;
        $resident_household->street = $request->street;
        $resident_household->house_identification_number = $request->house_identification_number;
        $resident_household->latitude = $request->latitude;
        $resident_household->longitude = $request->longitude;
        $resident_household->name_of_respondent = $request->name_of_respondent;
        $resident_household->contact = $request->contact;
        $resident_household->no_family = $request->no_family;
        $resident_household->no_members = $request->no_members;
        $resident_household->disability = $request->disability;
        $resident_household->no_disability = $request->no_disability;
        $resident_household->building_type = $request->building_type;
        $resident_household->water_supply = $request->water_supply;
        $resident_household->toilet = $request->toilet;
        $resident_household->water_filtration = $request->water_filtration;
        $resident_household->electricity = $request->electricity;
        $resident_household->power_generator = $request->power_generator;
        $resident_household->save();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$resident_household->house_identification_number.' on Resident Profile Household';
        $al->activity = 'add';
        $al->save();

        // return redirect()->route('residentprofile.household')->with('flash_message', $resident_household->house_identification_number.' was Added!!');

        return redirect('resident_profile/household/member/'.$resident_household->id)
            ->with('flash_message', $resident_household->house_identification_number.' was Added!!')
            ->with('id', $resident_household->id)
            ->with('no_family', $resident_household->no_family);
    }

    function householdUpdate(Request $request){
        $resident_household = ResidentHousehold::find($request->id);
        $resident_household->province = $request->province;
        $resident_household->municipality = $request->municipality;
        $resident_household->province = $request->province;
        $resident_household->municipality = $request->municipality;
        $resident_household->barangay = $request->barangay;
        $resident_household->purok = $request->purok;
        $resident_household->street = $request->street;
        $resident_household->house_identification_number = $request->house_identification_number;
        $resident_household->latitude = $request->latitude;
        $resident_household->longitude = $request->longitude;
        $resident_household->name_of_respondent = $request->name_of_respondent;
        $resident_household->contact = $request->contact;
        $resident_household->no_family = $request->no_family;
        $resident_household->no_members = $request->no_members;
        $resident_household->disability = $request->disability;
        $resident_household->no_disability = $request->no_disability;
        $resident_household->building_type = $request->building_type;
        $resident_household->water_supply = $request->water_supply;
        $resident_household->toilet = $request->toilet;
        $resident_household->water_filtration = $request->water_filtration;
        $resident_household->electricity = $request->electricity;
        $resident_household->power_generator = $request->power_generator;
        $resident_household->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$resident_household->house_identification_number.' on Resident Profile Household';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('residentprofile.household')->with('flash_message',  $resident_household->house_identification_number.' was Updated!!');
    }

    function householdDelete($id){
        $resident_household = ResidentHousehold::find($id);
        $resident_household->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$resident_household->house_identification_number.' on Resident Profile Household';
        $al->activity = 'delete';
        $al->save();

        return redirect()->route('residentprofile.household')->with('flash_message', $resident_household->house_identification_number.' was Deleted');
    }

    function householdRestore($id){
        $resident_household = ResidentHousehold::onlyTrashed()->find($id);
        $resident_household->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$resident_household->house_identification_number.' oon Resident Profile Household';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('residentprofile.household')->with('flash_message', $resident_household->house_identification_number.' was Restored');
    }

    function householdMember(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resident_household_members = ResidentHouseholdMember::with(['resident_households'=> function ($query) {
                $query->withTrashed();
            }])
                ->Where(function ($query) use ($request) {
                    $query->where('first_name','like', '%'.$request->search.'%')
                        ->orWhere('middle_name','like', '%'.$request->search.'%')
                        ->orWhere('last_name','like', '%'.$request->search.'%')
                        ->orWhere('age','like', '%'.$request->search.'%')
                        ->orWhere('gender','like', '%'.$request->search.'%');
                })
                ->orWhereHas('resident_households', function ($query) use ($request){
                    $query->where('resident_household_id', 'like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy($val[0],$val[1])
                ->withTrashed()
                ->paginate(25);
        }
        else
        {
            $resident_household_members = ResidentHouseholdMember::with(['resident_households'=> function ($query) {
                $query->withTrashed();
            }])
                ->Where(function ($query) use ($request) {
                    $query->where('first_name','like', '%'.$request->search.'%')
                        ->orWhere('middle_name','like', '%'.$request->search.'%')
                        ->orWhere('last_name','like', '%'.$request->search.'%')
                        ->orWhere('age','like', '%'.$request->search.'%')
                        ->orWhere('gender','like', '%'.$request->search.'%');
                })
                ->orWhereHas('resident_households', function ($query) use ($request){
                    $query->where('house_identification_number', 'like', '%'.$request->search.'%');
                })
                ->where('barangay_about_id',Session::get('barangayAbout')->id)
                ->orderBy('last_name','desc')
                ->withTrashed()
                ->paginate(25);
        }
        return view('resident.household_member',compact('resident_household_members'));
    }

    function householdMemberStore(Request $request){
        $resident_household_member = new ResidentHouseholdMember();
        $resident_household_member->barangay_about_id = Session::get('barangayAbout')->id;
        $resident_household_member->resident_household_id = $request->resident_household_id;
        $resident_household_member->first_name = $request->first_name;
        $resident_household_member->middle_name = $request->middle_name;
        $resident_household_member->last_name = $request->last_name;
        $resident_household_member->family_belong = $request->family_belong;
        $resident_household_member->relation = $request->relation;
        $resident_household_member->civil_status = $request->civil_status;
        $resident_household_member->gender = $request->gender;
        $resident_household_member->birthdate = $request->birthdate;
        $resident_household_member->age = $request->age;
        $resident_household_member->same_address = $request->same_address;
        $resident_household_member->attending_school = $request->attending_school;
        $resident_household_member->year_level = $request->year_level;
        $resident_household_member->highest_level = $request->highest_level;
        $resident_household_member->pregnant = $request->pregnant;
        $resident_household_member->have_children = $request->have_children;
        $resident_household_member->solo_parent = $request->solo_parent;
        $resident_household_member->disability = $request->disability;
        $resident_household_member->disability_type = $request->disability_type;
        $resident_household_member->health_problem = $request->health_problem;
        $resident_household_member->employed = $request->employed;
        $resident_household_member->occupation = $request->occupation;
        $resident_household_member->where_occupation = $request->where_occupation;
        $resident_household_member->save();

        $resident_household = ResidentHousehold::find($request->resident_household_id);
        $resident_household->no_members = ($request->no_members)+1;
        $resident_household->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$resident_household_member->first_name.' '.$resident_household_member->last_name.' on Resident Profile Household Member';
        $al->activity = 'add';
        $al->save();

        return redirect()->route('residentprofile.household')->with('flash_message', $resident_household_member->first_name.' '.$resident_household_member->last_name.' was Added as a Member!!');
    }

    function householdMemberStoreLoop(Request $request){
        $resident_household_member = new ResidentHouseholdMember();
        $resident_household_member->barangay_about_id = Session::get('barangayAbout')->id;
        $resident_household_member->resident_household_id = $request->resident_household_id;
        $resident_household_member->first_name = $request->first_name;
        $resident_household_member->middle_name = $request->middle_name;
        $resident_household_member->last_name = $request->last_name;
        $resident_household_member->family_belong = $request->family_belong;
        $resident_household_member->relation = $request->relation;
        $resident_household_member->civil_status = $request->civil_status;
        $resident_household_member->gender = $request->gender;
        $resident_household_member->birthdate = $request->birthdate;
        $resident_household_member->age = $request->age;
        $resident_household_member->same_address = $request->same_address;
        $resident_household_member->attending_school = $request->attending_school;
        $resident_household_member->year_level = $request->year_level;
        $resident_household_member->highest_level = $request->highest_level;
        $resident_household_member->pregnant = $request->pregnant;
        $resident_household_member->have_children = $request->have_children;
        $resident_household_member->solo_parent = $request->solo_parent;
        $resident_household_member->disability = $request->disability;
        $resident_household_member->disability_type = $request->disability_type;
        $resident_household_member->health_problem = $request->health_problem;
        $resident_household_member->employed = $request->employed;
        $resident_household_member->occupation = $request->occupation;
        $resident_household_member->where_occupation = $request->where_occupation;
        $resident_household_member->save();

        $resident_household = ResidentHousehold::find($request->resident_household_id);
        $resident_household->no_members = ($request->no_members)+1;
        $resident_household->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$resident_household_member->first_name.' '.$resident_household_member->last_name.' on Resident Profile Household Member';
        $al->activity = 'add';
        $al->save();

        // session()->flash('id',$request->resident_household_id);

        // return redirect()->route('residentprofile.household')->with('flash_message', $resident_household_member->first_name.' '.$resident_household_member->last_name.' was Added as a Member!!');

        return redirect('resident_profile/household/member/'.$request->resident_household_id)
            ->with('id', $request->resident_household_id)
            ->with('no_family', $resident_household->no_family);
    }

    function householdMemberUpdate(Request $request){
        $resident_household_member = ResidentHouseholdMember::find($request->id);
        $resident_household_member->first_name = $request->first_name;
        $resident_household_member->middle_name = $request->middle_name;
        $resident_household_member->last_name = $request->last_name;
        $resident_household_member->family_belong = $request->family_belong;
        $resident_household_member->relation = $request->relation;
        $resident_household_member->civil_status = $request->civil_status;
        $resident_household_member->gender = $request->gender;
        $resident_household_member->birthdate = $request->birthdate;
        $resident_household_member->age = $request->age;
        $resident_household_member->same_address = $request->same_address;
        $resident_household_member->attending_school = $request->attending_school;
        $resident_household_member->year_level = $request->year_level;
        $resident_household_member->highest_level = $request->highest_level;
        $resident_household_member->pregnant = $request->pregnant;
        $resident_household_member->have_children = $request->have_children;
        $resident_household_member->solo_parent = $request->solo_parent;
        $resident_household_member->disability = $request->disability;
        $resident_household_member->disability_type = $request->disability_type;
        $resident_household_member->health_problem = $request->health_problem;
        $resident_household_member->employed = $request->employed;
        $resident_household_member->occupation = $request->occupation;
        $resident_household_member->where_occupation = $request->where_occupation;
        $resident_household_member->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$resident_household_member->first_name.' '.$resident_household_member->last_name.' on Resident Profile Household Member';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('residentprofile.household.member')->with('flash_message', $resident_household_member->first_name.' '.$resident_household_member->last_name.' was Updated!!');
    }

    function householdMemberDelete($id){
        $resident_household_member = ResidentHouseholdMember::find($id);
        $resident_household_member->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$resident_household_member->first_name.' '.$resident_household_member->last_name.' on Resident Profile Household Member';
        $al->activity = 'delete';
        $al->save();

        return redirect()->route('residentprofile.household.member')->with('flash_message',$resident_household_member->first_name.' '.$resident_household_member->last_name.' was Deleted');
    }

    function householdMemberRestore($id){
        $resident_household_member = ResidentHouseholdMember::onlyTrashed()->find($id);
        $resident_household_member->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$resident_household_member->first_name.' '.$resident_household_member->last_name.' on Resident Profile Household Member';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('residentprofile.household.member')->with('flash_message', $resident_household_member->first_name.' '.$resident_household_member->last_name.' was Restored');
    }

    function householdSurveyAdd($id){
        $resident_household = ResidentHousehold::find($id);
        return view('resident.household_survey_add',compact('resident_household'));
    }

    function householdSurveyStore(Request $request){
        $resident_household = ResidentHousehold::find($request->resident_household_id);

        $survey_a = new ResidentHouseholdSurveyA();
        $survey_a->a1 = $request->a1;
        $survey_a->a2_1 = $request->a2_1;
        $survey_a->a3_1 = $request->a3_1;
        $survey_a->a4_1 = $request->a4_1;
        $survey_a->a5_1_1 = $request->a5_1_1;
        $survey_a->a5_1_2 = $request->a5_1_2;
        $survey_a->a5_1_3 = $request->a5_1_3;
        $survey_a->a5_1_4 = $request->a5_1_4;
        $survey_a->a5_1_5 = $request->a5_1_5;
        $survey_a->a2_2 = $request->a2_2;
        $survey_a->a3_2 = $request->a3_2;
        $survey_a->a4_2 = $request->a4_2;
        $survey_a->a5_2_1 = $request->a5_2_1;
        $survey_a->a5_2_2 = $request->a5_2_2;
        $survey_a->a5_2_3 = $request->a5_2_3;
        $survey_a->a5_2_4 = $request->a5_2_4;
        $survey_a->a5_2_5 = $request->a5_2_5;
        $survey_a->a2_3 = $request->a2_3;
        $survey_a->a3_3 = $request->a3_3;
        $survey_a->a4_3 = $request->a4_3;
        $survey_a->a5_3_1 = $request->a5_3_1;
        $survey_a->a5_3_2 = $request->a5_3_2;
        $survey_a->a5_3_3 = $request->a5_3_3;
        $survey_a->a5_3_4 = $request->a5_3_4;
        $survey_a->a5_3_5 = $request->a5_3_5;
        $survey_a->a2_4 = $request->a2_4;
        $survey_a->a3_4 = $request->a3_4;
        $survey_a->a4_4 = $request->a4_4;
        $survey_a->a5_4_1 = $request->a5_4_1;
        $survey_a->a5_4_2 = $request->a5_4_2;
        $survey_a->a5_4_3 = $request->a5_4_3;
        $survey_a->a5_4_4 = $request->a5_4_4;
        $survey_a->a5_4_5 = $request->a5_4_5;
        $survey_a->a2_5 = $request->a2_5;
        $survey_a->a3_5 = $request->a3_5;
        $survey_a->a4_5 = $request->a4_5;
        $survey_a->a5_5_1 = $request->a5_5_1;
        $survey_a->a5_5_2 = $request->a5_5_2;
        $survey_a->a5_5_3 = $request->a5_5_3;
        $survey_a->a5_5_4 = $request->a5_5_4;
        $survey_a->a5_5_5 = $request->a5_5_5;
        $survey_a->a2_6 = $request->a2_6;
        $survey_a->a3_6 = $request->a3_6;
        $survey_a->a4_6 = $request->a4_6;
        $survey_a->a5_6_1 = $request->a5_6_1;
        $survey_a->a5_6_2 = $request->a5_6_2;
        $survey_a->a5_6_3 = $request->a5_6_3;
        $survey_a->a5_6_4 = $request->a5_6_4;
        $survey_a->a5_6_5 = $request->a5_6_5;
        $survey_a->a2_7 = $request->a2_7;
        $survey_a->a3_7 = $request->a3_7;
        $survey_a->a4_7 = $request->a4_7;
        $survey_a->a5_7_1 = $request->a5_7_1;
        $survey_a->a5_7_2 = $request->a5_7_2;
        $survey_a->a5_7_3 = $request->a5_7_3;
        $survey_a->a5_7_4 = $request->a5_7_4;
        $survey_a->a5_7_5 = $request->a5_7_5;
        $survey_a->a2_8 = $request->a2_8;
        $survey_a->a3_8 = $request->a3_8;
        $survey_a->a4_8 = $request->a4_8;
        $survey_a->a5_8_1 = $request->a5_8_1;
        $survey_a->a5_8_2 = $request->a5_8_2;
        $survey_a->a5_8_3 = $request->a5_8_3;
        $survey_a->a5_8_4 = $request->a5_8_4;
        $survey_a->a5_8_5 = $request->a5_8_5;
        $survey_a->a2_9 = $request->a2_9;
        $survey_a->a3_9 = $request->a3_9;
        $survey_a->a4_9 = $request->a4_9;
        $survey_a->a5_9_1 = $request->a5_9_1;
        $survey_a->a5_9_2 = $request->a5_9_2;
        $survey_a->a5_9_3 = $request->a5_9_3;
        $survey_a->a5_9_4 = $request->a5_9_4;
        $survey_a->a5_9_5 = $request->a5_9_5;
        $survey_a->a2_10 = $request->a2_10;
        $survey_a->a3_10 = $request->a3_10;
        $survey_a->a4_10 = $request->a4_10;
        $survey_a->a5_10_1 = $request->a5_10_1;
        $survey_a->a5_10_2 = $request->a5_10_2;
        $survey_a->a5_10_3 = $request->a5_10_3;
        $survey_a->a5_10_4 = $request->a5_10_4;
        $survey_a->a5_10_5 = $request->a5_10_5;
        $survey_a->a2_11 = $request->a2_11;
        $survey_a->a3_11 = $request->a3_11;
        $survey_a->a4_11 = $request->a4_11;
        $survey_a->a5_11_1 = $request->a5_11_1;
        $survey_a->a5_11_2 = $request->a5_11_2;
        $survey_a->a5_11_3 = $request->a5_11_3;
        $survey_a->a5_11_4 = $request->a5_11_4;
        $survey_a->a5_11_5 = $request->a5_11_5;
        $survey_a->a2_12 = $request->a2_12;
        $survey_a->a3_12 = $request->a3_12;
        $survey_a->a4_12 = $request->a4_12;
        $survey_a->a5_12_1 = $request->a5_12_1;
        $survey_a->a5_12_2 = $request->a5_12_2;
        $survey_a->a5_12_3 = $request->a5_12_3;
        $survey_a->a5_12_4 = $request->a5_12_4;
        $survey_a->a5_12_5 = $request->a5_12_5;
        $survey_a->a2_13 = $request->a2_13;
        $survey_a->a3_13 = $request->a3_13;
        $survey_a->a4_13 = $request->a4_13;
        $survey_a->a5_13_1 = $request->a5_13_1;
        $survey_a->a5_13_2 = $request->a5_13_2;
        $survey_a->a5_13_3 = $request->a5_13_3;
        $survey_a->a5_13_4 = $request->a5_13_4;
        $survey_a->a5_13_5 = $request->a5_13_5;
        $survey_a->save();

        $survey_b = new ResidentHouseholdSurveyB();
        $survey_b->b1 = $request->b1;
        $survey_b->b2_1 = $request->b2_1;
        $survey_b->b2_2 = $request->b2_2;
        $survey_b->b2_3 = $request->b2_3;
        $survey_b->b2_4 = $request->b2_4;
        $survey_b->b2_5 = $request->b2_5;
        $survey_b->b2_6 = $request->b2_6;
        $survey_b->b2_7 = $request->b2_7;
        $survey_b->b2_8 = $request->b2_8;
        $survey_b->b2_9 = $request->b2_9;
        $survey_b->b2_10 = $request->b2_10;
        $survey_b->b2_11 = $request->b2_11;
        $survey_b->b2_12 = $request->b2_12;
        $survey_b->b2_13 = $request->b2_13;
        $survey_b->b2_14 = $request->b2_14;
        $survey_b->b3_1 = $request->b3_1;
        $survey_b->b3_2 = $request->b3_2;
        $survey_b->b3_3 = $request->b3_3;
        $survey_b->b3_4 = $request->b3_4;
        $survey_b->b3_5 = $request->b3_5;
        $survey_b->b3_6 = $request->b3_6;
        $survey_b->b3_7 = $request->b3_7;
        $survey_b->b3_8 = $request->b3_8;
        $survey_b->b3_9 = $request->b3_9;
        $survey_b->b3_10 = $request->b3_10;
        $survey_b->b3_11 = $request->b3_11;
        $survey_b->b3_12 = $request->b3_12;
        $survey_b->b3_13 = $request->b3_13;
        $survey_b->b3_14 = $request->b3_14;
        $survey_b->b3_15 = $request->b3_15;
        $survey_b->b4 = $request->b4;
        $survey_b->save();

        $survey_c = new ResidentHouseholdSurveyC();
        $survey_c->c1 = $request->c1;
        $survey_c->c2 = $request->c2;
        $survey_c->c3 = $request->c3;
        $survey_c->c4_1 = $request->c4_1;
        $survey_c->c4_2 = $request->c4_2;
        $survey_c->c4_3 = $request->c4_3;
        $survey_c->c4_4 = $request->c4_4;
        $survey_c->c4_5 = $request->c4_5;
        $survey_c->c4_6 = $request->c4_6;
        $survey_c->c5 = $request->c5;
        $survey_c->c6 = $request->c6;
        $survey_c->save();

        $survey_d = new ResidentHouseholdSurveyD();
        $survey_d->d1 = $request->d1;
        $survey_d->d2 = $request->d2;
        $survey_d->d3 = $request->d3;
        $survey_d->d4 = $request->d4;
        $survey_d->d5 = $request->d5;
        $survey_d->d6 = $request->d6;
        $survey_d->d7 = $request->d7;
        $survey_d->d8 = $request->d8;
        $survey_d->d9 = $request->d9;
        $survey_d->d10_1 = $request->d10_1;
        $survey_d->d10_2 = $request->d10_2;
        $survey_d->d10_3 = $request->d10_3;
        $survey_d->d10_4 = $request->d10_4;
        $survey_d->d10_5 = $request->d10_5;
        $survey_d->d10_6 = $request->d10_6;
        $survey_d->d10_7 = $request->d10_7;
        $survey_d->d10_8 = $request->d10_8;
        $survey_d->d10_9 = $request->d10_9;
        $survey_d->d10_10 = $request->d10_10;
        $survey_d->d10_11 = $request->d10_11;
        $survey_d->d10_12 = $request->d10_12;
        $survey_d->d10_13 = $request->d10_13;
        $survey_d->d10_14 = $request->d10_14;
        $survey_d->save();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added a survey on Resident Profile Household that has house identification number of '.$resident_household->house_identification_number;
        $al->activity = 'add';
        $al->save();

        LOG::info('survey_a', ["request"=>$request]);
        // $currentYear = date("Y");
        $currentYear = $request->year;
        $yearlySurvey = new YearlySurvey();
        $yearlySurvey->barangay_id = Session::get('barangayAbout')->id;
        $yearlySurvey->resident_household_id = $resident_household->id;
        $yearlySurvey->survey_a_id = $survey_a->id;
        $yearlySurvey->survey_b_id = $survey_b->id;
        $yearlySurvey->survey_c_id = $survey_c->id;
        $yearlySurvey->survey_d_id = $survey_d->id;
        $yearlySurvey->year = $currentYear;
        $yearlySurvey->save();

        LOG::info('yearly_survey: '.$yearlySurvey);

        return redirect()->route('residentprofile.household')->with('flash_message', 'Added survey to household with identification number of '.$resident_household->house_identification_number);

        // return response()->json(['result'=> $yearlySurvey],200, array(),JSON_PRETTY_PRINT);
    }

    function householdSurveyEdit($id){
        $currentYear = date("Y");
        $resident_household = ResidentHousehold::find($id);

        $yearly_survey = DB::table('yearly_survey')
            ->where('yearly_survey.resident_household_id', '=',$resident_household->id)
            ->where('yearly_survey.barangay_id','=',$resident_household->barangay_about_id)
            ->where('yearly_survey.year','=',$currentYear);

        $survey_a = $yearly_survey
            ->leftjoin('resident_household_survey_as','yearly_survey.survey_a_id','=','resident_household_survey_as.id')
            ->select('resident_household_survey_as.*','yearly_survey.year AS survey_year')
            ->first();

        $survey_b = $yearly_survey
            ->leftjoin('resident_household_survey_bs','yearly_survey.survey_b_id','=','resident_household_survey_bs.id')
            ->select('resident_household_survey_bs.*','yearly_survey.year AS survey_year')
            ->first();

        $survey_c = $yearly_survey
            ->leftjoin('resident_household_survey_cs','yearly_survey.survey_c_id','=','resident_household_survey_cs.id')
            ->select('resident_household_survey_cs.*','yearly_survey.year AS survey_year')
            ->first();

        $survey_d = $yearly_survey
            ->leftjoin('resident_household_survey_ds','yearly_survey.survey_d_id','=','resident_household_survey_ds.id')
            ->select('resident_household_survey_ds.*','yearly_survey.year AS survey_year')
            ->first();

        $resident_household->survey_a = $survey_a;
        $resident_household->survey_b = $survey_b;
        $resident_household->survey_c = $survey_c;
        $resident_household->survey_d = $survey_d;

        Log::info('edit survey');
        Log::info($resident_household);
        return view('resident.household_survey_edit',compact('resident_household'));
    }

    function householdSurveyUpdate(Request $request){
        $resident_household = ResidentHousehold::find($request->resident_household_id);

        $yearly_survey = YearlySurvey::find($request->resident_household_id);

        Log::info($yearly_survey);

        $survey_a = ResidentHouseholdSurveyA::where('id',$yearly_survey->survey_a_id)->first();
        $survey_a->a1 = $request->a1;
        $survey_a->a2_1 = $request->a2_1;
        $survey_a->a3_1 = $request->a3_1;
        $survey_a->a4_1 = $request->a4_1;
        $survey_a->a5_1_1 = $request->a5_1_1;
        $survey_a->a5_1_2 = $request->a5_1_2;
        $survey_a->a5_1_3 = $request->a5_1_3;
        $survey_a->a5_1_4 = $request->a5_1_4;
        $survey_a->a5_1_5 = $request->a5_1_5;
        $survey_a->a2_2 = $request->a2_2;
        $survey_a->a3_2 = $request->a3_2;
        $survey_a->a4_2 = $request->a4_2;
        $survey_a->a5_2_1 = $request->a5_2_1;
        $survey_a->a5_2_2 = $request->a5_2_2;
        $survey_a->a5_2_3 = $request->a5_2_3;
        $survey_a->a5_2_4 = $request->a5_2_4;
        $survey_a->a5_2_5 = $request->a5_2_5;
        $survey_a->a2_3 = $request->a2_3;
        $survey_a->a3_3 = $request->a3_3;
        $survey_a->a4_3 = $request->a4_3;
        $survey_a->a5_3_1 = $request->a5_3_1;
        $survey_a->a5_3_2 = $request->a5_3_2;
        $survey_a->a5_3_3 = $request->a5_3_3;
        $survey_a->a5_3_4 = $request->a5_3_4;
        $survey_a->a5_3_5 = $request->a5_3_5;
        $survey_a->a2_4 = $request->a2_4;
        $survey_a->a3_4 = $request->a3_4;
        $survey_a->a4_4 = $request->a4_4;
        $survey_a->a5_4_1 = $request->a5_4_1;
        $survey_a->a5_4_2 = $request->a5_4_2;
        $survey_a->a5_4_3 = $request->a5_4_3;
        $survey_a->a5_4_4 = $request->a5_4_4;
        $survey_a->a5_4_5 = $request->a5_4_5;
        $survey_a->a2_5 = $request->a2_5;
        $survey_a->a3_5 = $request->a3_5;
        $survey_a->a4_5 = $request->a4_5;
        $survey_a->a5_5_1 = $request->a5_5_1;
        $survey_a->a5_5_2 = $request->a5_5_2;
        $survey_a->a5_5_3 = $request->a5_5_3;
        $survey_a->a5_5_4 = $request->a5_5_4;
        $survey_a->a5_5_5 = $request->a5_5_5;
        $survey_a->a2_6 = $request->a2_6;
        $survey_a->a3_6 = $request->a3_6;
        $survey_a->a4_6 = $request->a4_6;
        $survey_a->a5_6_1 = $request->a5_6_1;
        $survey_a->a5_6_2 = $request->a5_6_2;
        $survey_a->a5_6_3 = $request->a5_6_3;
        $survey_a->a5_6_4 = $request->a5_6_4;
        $survey_a->a5_6_5 = $request->a5_6_5;
        $survey_a->a2_7 = $request->a2_7;
        $survey_a->a3_7 = $request->a3_7;
        $survey_a->a4_7 = $request->a4_7;
        $survey_a->a5_7_1 = $request->a5_7_1;
        $survey_a->a5_7_2 = $request->a5_7_2;
        $survey_a->a5_7_3 = $request->a5_7_3;
        $survey_a->a5_7_4 = $request->a5_7_4;
        $survey_a->a5_7_5 = $request->a5_7_5;
        $survey_a->a2_8 = $request->a2_8;
        $survey_a->a3_8 = $request->a3_8;
        $survey_a->a4_8 = $request->a4_8;
        $survey_a->a5_8_1 = $request->a5_8_1;
        $survey_a->a5_8_2 = $request->a5_8_2;
        $survey_a->a5_8_3 = $request->a5_8_3;
        $survey_a->a5_8_4 = $request->a5_8_4;
        $survey_a->a5_8_5 = $request->a5_8_5;
        $survey_a->a2_9 = $request->a2_9;
        $survey_a->a3_9 = $request->a3_9;
        $survey_a->a4_9 = $request->a4_9;
        $survey_a->a5_9_1 = $request->a5_9_1;
        $survey_a->a5_9_2 = $request->a5_9_2;
        $survey_a->a5_9_3 = $request->a5_9_3;
        $survey_a->a5_9_4 = $request->a5_9_4;
        $survey_a->a5_9_5 = $request->a5_9_5;
        $survey_a->a2_10 = $request->a2_10;
        $survey_a->a3_10 = $request->a3_10;
        $survey_a->a4_10 = $request->a4_10;
        $survey_a->a5_10_1 = $request->a5_10_1;
        $survey_a->a5_10_2 = $request->a5_10_2;
        $survey_a->a5_10_3 = $request->a5_10_3;
        $survey_a->a5_10_4 = $request->a5_10_4;
        $survey_a->a5_10_5 = $request->a5_10_5;
        $survey_a->a2_11 = $request->a2_11;
        $survey_a->a3_11 = $request->a3_11;
        $survey_a->a4_11 = $request->a4_11;
        $survey_a->a5_11_1 = $request->a5_11_1;
        $survey_a->a5_11_2 = $request->a5_11_2;
        $survey_a->a5_11_3 = $request->a5_11_3;
        $survey_a->a5_11_4 = $request->a5_11_4;
        $survey_a->a5_11_5 = $request->a5_11_5;
        $survey_a->a2_12 = $request->a2_12;
        $survey_a->a3_12 = $request->a3_12;
        $survey_a->a4_12 = $request->a4_12;
        $survey_a->a5_12_1 = $request->a5_12_1;
        $survey_a->a5_12_2 = $request->a5_12_2;
        $survey_a->a5_12_3 = $request->a5_12_3;
        $survey_a->a5_12_4 = $request->a5_12_4;
        $survey_a->a5_12_5 = $request->a5_12_5;
        $survey_a->a2_13 = $request->a2_13;
        $survey_a->a3_13 = $request->a3_13;
        $survey_a->a4_13 = $request->a4_13;
        $survey_a->a5_13_1 = $request->a5_13_1;
        $survey_a->a5_13_2 = $request->a5_13_2;
        $survey_a->a5_13_3 = $request->a5_13_3;
        $survey_a->a5_13_4 = $request->a5_13_4;
        $survey_a->a5_13_5 = $request->a5_13_5;
        $survey_a->update();

        $survey_b= ResidentHouseholdSurveyB::where('id',$yearly_survey->survey_b_id)->first();
        $survey_b->b1 = $request->b1;
        $survey_b->b2_1 = $request->b2_1;
        $survey_b->b2_2 = $request->b2_2;
        $survey_b->b2_3 = $request->b2_3;
        $survey_b->b2_4 = $request->b2_4;
        $survey_b->b2_5 = $request->b2_5;
        $survey_b->b2_6 = $request->b2_6;
        $survey_b->b2_7 = $request->b2_7;
        $survey_b->b2_8 = $request->b2_8;
        $survey_b->b2_9 = $request->b2_9;
        $survey_b->b2_10 = $request->b2_10;
        $survey_b->b2_11 = $request->b2_11;
        $survey_b->b2_12 = $request->b2_12;
        $survey_b->b2_13 = $request->b2_13;
        $survey_b->b2_14 = $request->b2_14;
        $survey_b->b3_1 = $request->b3_1;
        $survey_b->b3_2 = $request->b3_2;
        $survey_b->b3_3 = $request->b3_3;
        $survey_b->b3_4 = $request->b3_4;
        $survey_b->b3_5 = $request->b3_5;
        $survey_b->b3_6 = $request->b3_6;
        $survey_b->b3_7 = $request->b3_7;
        $survey_b->b3_8 = $request->b3_8;
        $survey_b->b3_9 = $request->b3_9;
        $survey_b->b3_10 = $request->b3_10;
        $survey_b->b3_11 = $request->b3_11;
        $survey_b->b3_12 = $request->b3_12;
        $survey_b->b3_13 = $request->b3_13;
        $survey_b->b3_14 = $request->b3_14;
        $survey_b->b3_15 = $request->b3_15;
        $survey_b->b4 = $request->b4;
        $survey_b->update();

        $survey_c = ResidentHouseholdSurveyC::where('id',$yearly_survey->survey_c_id)->first();
        $survey_c->c1 = $request->c1;
        $survey_c->c2 = $request->c2;
        $survey_c->c3 = $request->c3;
        $survey_c->c4_1 = $request->c4_1;
        $survey_c->c4_2 = $request->c4_2;
        $survey_c->c4_3 = $request->c4_3;
        $survey_c->c4_4 = $request->c4_4;
        $survey_c->c4_5 = $request->c4_5;
        $survey_c->c4_6 = $request->c4_6;
        $survey_c->c5 = $request->c5;
        $survey_c->c6 = $request->c6;
        $survey_c->update();

        $survey_d = ResidentHouseholdSurveyD::where('id',$yearly_survey->survey_d_id)->first();
        $survey_d->d1 = $request->d1;
        $survey_d->d2 = $request->d2;
        $survey_d->d3 = $request->d3;
        $survey_d->d4 = $request->d4;
        $survey_d->d5 = $request->d5;
        $survey_d->d6 = $request->d6;
        $survey_d->d7 = $request->d7;
        $survey_d->d8 = $request->d8;
        $survey_d->d9 = $request->d9;
        $survey_d->d10_1 = $request->d10_1;
        $survey_d->d10_2 = $request->d10_2;
        $survey_d->d10_3 = $request->d10_3;
        $survey_d->d10_4 = $request->d10_4;
        $survey_d->d10_5 = $request->d10_5;
        $survey_d->d10_6 = $request->d10_6;
        $survey_d->d10_7 = $request->d10_7;
        $survey_d->d10_8 = $request->d10_8;
        $survey_d->d10_9 = $request->d10_9;
        $survey_d->d10_10 = $request->d10_10;
        $survey_d->d10_11 = $request->d10_11;
        $survey_d->d10_12 = $request->d10_12;
        $survey_d->d10_13 = $request->d10_13;
        $survey_d->d10_14 = $request->d10_14;
        $survey_d->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated a survey on Resident Profile Household that has house identification number of '.$resident_household->house_identification_number;
        $al->activity = 'update';
        $al->save();

        return redirect()->route('residentprofile.household')->with('flash_message', 'Updated survey of household with identification number of '.$resident_household->house_identification_number);
    }
}