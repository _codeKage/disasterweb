<?php

namespace App\Http\Controllers;


use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use DB;
use Response;
use App\Http\Requests;
use App\DisasterAdvisory;
use App\DisasterSafetyTip;
use App\DisasterEvacuationCenter;
use App\DisasterKit;
use App\DisasterPlan;
use App\DisasterTeam;
use App\DisasterTeamMember;
use App\ResidentHousehold;
use Carbon\Carbon;
use Chikka;
use Mail;
use App\ActivityLog;
use Log;

class DisasterAdminController extends Controller
{
    function advisory(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $disaster_advisories = DB::table('disaster_advisories')
            ->join('users','users.id', '=', 'disaster_advisories.user_id')
            ->select(
                'disaster_advisories.*',
                'users.first_name',
                'users.middle_initial',
                'users.last_name'
            )
            ->Where(function ($query) use ($request) {
                $query->where('subject','like', '%'.$request->search.'%')
                ->orWhere('date','like', '%'.$request->search.'%')
                ->orWhere('message','like', '%'.$request->search.'%')
                 ->orWhere('status','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%')
                ->orWhere('first_name','like', '%'.$request->search.'%')
                ->orWhere('last_name','like', '%'.$request->search.'%');
            })
            ->where('disaster_advisories.barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $disaster_advisories = DB::table('disaster_advisories')
            ->join('users','users.id', '=', 'disaster_advisories.user_id')
            ->select(
                'disaster_advisories.*',
                'users.first_name',
                'users.middle_initial',
                'users.last_name'
            )
            ->Where(function ($query) use ($request) {
                $query->where('subject','like', '%'.$request->search.'%')
                ->orWhere('date','like', '%'.$request->search.'%')
                ->orWhere('message','like', '%'.$request->search.'%')
                 ->orWhere('status','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%')
                ->orWhere('first_name','like', '%'.$request->search.'%')
                ->orWhere('last_name','like', '%'.$request->search.'%');
            })
            ->where('disaster_advisories.barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('updated_at','desc')
            ->paginate(25);
        }
        return view('disaster_admin.advisory',compact('disaster_advisories'));
        // return Response::json(['result'=> $disaster_advisories],200, array(),JSON_PRETTY_PRINT);
    }

    function advisoryDeletePerYear(){
        $currentYear = date("Y");
        $disaster_advisories = DB::table('disaster_advisories')
            ->where('disaster_advisories.barangay_about_id',Session::get('barangayAbout')->id)
            ->whereYear('created_at', '<', date('Y'));
            // ->whereNull('deleted_at');

            
        $advisoriesToBeDeleted = collect($disaster_advisories
                    ->pluck('subject'))
                    ->implode(', ');
        
        $disaster_advisories->update(['status'=>'Deleted','deleted_at' => Carbon::now()->format('Y-m-d H:i:s')]);
      
        // foreach($disaster_advisories as $advisory){
         
        //     $al = new ActivityLog();
        //     $al->barangay_about_id = Session::get('barangayAbout')->id;
        //     $al->username = Session::get('logUser')->username;
        //     $al->description = 'Deleted '.$advisory->subject.' on Disaster Admin Advisory';
        //     $al->activity = 'delete';
        //     $al->save();
    
        // }

        return redirect()->route('disasteradmin.advisory')->with('flash_message', 'The following advisories are deleted:  '.$advisoriesToBeDeleted);

        // return Response::json(['result'=> 'The following advisories are deleted:\n '.$advisoriesToBeDeleted],200, array(),JSON_PRETTY_PRINT);
    }

    function advisoryStore(Request $request){
        Log::info('advisory request');
        Log::info($request);
        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$request->subject.' on Disaster Admin Advisory';
        $al->activity = 'add';
        $al->save();

        $disaster_advisories = new DisasterAdvisory();
        $disaster_advisories->barangay_about_id = Session::get('barangayAbout')->id;
        $disaster_advisories->date = Carbon::now()->toDateString();
        $disaster_advisories->subject = $request->subject;
        $disaster_advisories->message = $request->message;
        $disaster_advisories->type = $request->type;
        $disaster_advisories->user_id = $request->user_id;
        if($request->submit == 'Send'){
            $resident_household = ResidentHousehold::select('contact')->whereNull('deleted_at')->get();
            // Mobile number of receiver and message to send
            foreach($resident_household as $rh){
                $mobile = $rh->contact;
                $message = 'Subject:'.$disaster_advisories->subject.'
                '.$disaster_advisories->message.'';
            // Send SMS
            $this->sendSms($mobile, $message);
            }
            $disaster_advisories->status = 'Sent';
            $disaster_advisories->save();

            return redirect()->route('disasteradmin.advisory')->with('flash_message', $request->subject.' was Added and Sent!!');
        } else {
            $disaster_advisories->status = 'Saved';
            $disaster_advisories->save();
            return redirect()->route('disasteradmin.advisory')->with('flash_message', $request->subject.' was Added!!');
        }
    }

    function resendMessage(Request $request) {
        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Sent an SMS message to people titled '.$request->subject;
        $al->activity = 'sent';
        $al->save();

        $disaster_advisories = DisasterAdvisory::find($request->id);
        $disaster_advisories->status = 'Sent';
        $disaster_advisories->user_id = $request->user_id;
        $disaster_advisories->update();

        $resident_household = ResidentHousehold::select('contact')->whereNull('deleted_at')->get();
        // Mobile number of receiver and message to send
        foreach($resident_household as $rh){
            $mobile = $rh->contact;
            $message = 'Subject: '.$disaster_advisories->subject.'
            '.$disaster_advisories->message;
            // Send SMS
            $this->sendSms($mobile, $message);
        }
        return redirect()->route('disasteradmin.advisory')->with('flash_message', $disaster_advisories->subject.' was Sent');
    }

    function advisoryUpdate(Request $request){
        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$request->subject.' on Disaster Admin Advisory';
        $al->activity = 'update';
        $al->save();

        $disaster_advisories = DisasterAdvisory::find($request->id);
        $disaster_advisories->subject = $request->subject;
        $disaster_advisories->message = $request->message;
        $disaster_advisories->type = $request->type;
        $disaster_advisories->user_id = $request->user_id;

        if($request->submit == 'Send'){
            $resident_household = ResidentHousehold::select('contact')->whereNull('deleted_at')->get();
            // Mobile number of receiver and message to send
            foreach($resident_household as $rh){
                $mobile = $rh->contact;
                $message = 'Subject:'.$disaster_advisories->subject.'
                '.$disaster_advisories->message.'';
            // Send SMS
                $this->sendSms($mobile, $message);
            }
            $disaster_advisories->status = 'Sent';
            $disaster_advisories->update();

            return redirect()->route('disasteradmin.advisory')->with('flash_message', $request->subject.' was Updated and Sent!!');
        }
        $disaster_advisories->status = 'Saved';
        $disaster_advisories->update();
        return redirect()->route('disasteradmin.advisory')->with('flash_message',  $request->subject.' was Updated!!');
    }

    function advisoryDelete($id){
        $disaster_advisories = DisasterAdvisory::find($id);
        $disaster_advisories->status = 'Deleted';
        $disaster_advisories->update();
        $disaster_advisories->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$disaster_advisories->subject.' on Disaster Admin Advisory';
        $al->activity = 'delete';
        $al->save();

        return redirect()->route('disasteradmin.advisory')->with('flash_message', $disaster_advisories->subject.' was Deleted');
    }


    function advisorySend(Request $request){
        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Sent an SMS message to people titled '.$request->subject;
        $al->activity = 'sent';
        $al->save();

        $disaster_advisories = DisasterAdvisory::find($request->id);
        $disaster_advisories->subject = $request->subject;
        $disaster_advisories->message = $request->message;
        $disaster_advisories->type = $request->type;
        $disaster_advisories->status = 'Sent';
        $disaster_advisories->user_id = $request->user_id;
        $disaster_advisories->update();

        $resident_household = ResidentHousehold::select('contact')->whereNull('deleted_at')->get();
            // Mobile number of receiver and message to send
        foreach($resident_household as $rh){
            $mobile = $rh->contact;
            $message = 'Subject:'.$disaster_advisories->subject.'
            '.$disaster_advisories->message.'

';
            // Send SMS
            $this->sendSms($mobile, $message);
        }
        return redirect()->route('disasteradmin.advisory')->with('flash_message', $disaster_advisories->subject.' was Sent');
    }

    function safetyTips(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $disaster_safety_tips = DB::table('disaster_safety_tips')
            ->Where(function ($query) use ($request) {
                $query->where('hazard','like', '%'.$request->search.'%')
                ->orWhere('description','like', '%'.$request->search.'%')
                ->orWhere('facts','like', '%'.$request->search.'%')
                ->orWhere('tips','like', '%'.$request->search.'%')
                ->orWhere('before','like', '%'.$request->search.'%')
                ->orWhere('during','like', '%'.$request->search.'%')
                ->orWhere('after','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $disaster_safety_tips = DB::table('disaster_safety_tips')
            ->Where(function ($query) use ($request) {
                $query->where('hazard','like', '%'.$request->search.'%')
                ->orWhere('description','like', '%'.$request->search.'%')
                ->orWhere('facts','like', '%'.$request->search.'%')
                ->orWhere('tips','like', '%'.$request->search.'%')
                ->orWhere('before','like', '%'.$request->search.'%')
                ->orWhere('during','like', '%'.$request->search.'%')
                ->orWhere('after','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('updated_at','desc')
            ->paginate(25);
        }
        return view('disaster_admin.safety_tips',compact('disaster_safety_tips'));
    }

    function safetyTipsStore(Request $request){
        $disaster_safety_tips = new DisasterSafetyTip();
        if($request->hazard != 'Others'){
            $disaster_safety_tips->hazard = $request->hazard;
        } else {
            $disaster_safety_tips->hazard = $request->hazard_others;
        }
        $disaster_safety_tips->barangay_about_id = Session::get('barangayAbout')->id;
        $disaster_safety_tips->description = $request->description;
        $disaster_safety_tips->facts = $request->facts;
        $disaster_safety_tips->tips = $request->tips;
        $disaster_safety_tips->before = $request->before;
        $disaster_safety_tips->during = $request->during;
        $disaster_safety_tips->after = $request->after;
        $disaster_safety_tips->save();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$request->hazard.' on Disaster Admin Safety Tips';
        $al->activity = 'add';
        $al->save();

        return redirect()->route('disasteradmin.safety_tips')->with('flash_message', $disaster_safety_tips->hazard.' was Added!!');
    }

    function safetyTipsUpdate(Request $request){
        $disaster_safety_tips = DisasterSafetyTip::find($request->id);
        if($request->hazard != 'Others'){
            $disaster_safety_tips->hazard = $request->hazard;
        } else {
            $disaster_safety_tips->hazard = $request->hazard_others;
        }
        $disaster_safety_tips->description = $request->description;
        $disaster_safety_tips->facts = $request->facts;
        $disaster_safety_tips->tips = $request->tips;
        $disaster_safety_tips->before = $request->before;
        $disaster_safety_tips->during = $request->during;
        $disaster_safety_tips->after = $request->after;
        $disaster_safety_tips->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$request->hazard.' on Disaster Admin Safety Tips';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('disasteradmin.safety_tips')->with('flash_message',  $disaster_safety_tips->hazard.' was Updated!!');
    }

    function safetyTipsDelete($id){
        $disaster_safety_tips = DisasterSafetyTip::find($id);
        $disaster_safety_tips->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$disaster_safety_tips.' on Disaster Admin Safety Tips';
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('disasteradmin.safety_tips')->with('flash_message', $disaster_safety_tips->hazard.' was Deleted');
    }

    function safetyTipsRestore($id){
        $disaster_safety_tips = DisasterSafetyTip::onlyTrashed()->find($id);
        $disaster_safety_tips->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$disaster_safety_tips.' on Disaster Admin Safety Tips';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('disasteradmin.safety_tips')->with('flash_message', $disaster_safety_tips->hazard.' was Restored');
    }

    function evacuationCenter(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $disaster_evacuation_centers = DB::table('disaster_evacuation_centers')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('address','like', '%'.$request->search.'%')
                ->orWhere('capacity','like', '%'.$request->search.'%')
                ->orWhere('condition','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $disaster_evacuation_centers = DB::table('disaster_evacuation_centers')
            ->Where(function ($query) use ($request) {
               $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('address','like', '%'.$request->search.'%')
                ->orWhere('capacity','like', '%'.$request->search.'%')
                ->orWhere('condition','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('updated_at','desc')
            ->paginate(25);
        }
        return view('disaster_admin.evacuation_center',compact('disaster_evacuation_centers'));
    }

    function evacuationCenterStore(Request $request){
        $disaster_evacuation_center = new DisasterEvacuationCenter();
        $disaster_evacuation_center->barangay_about_id = Session::get('barangayAbout')->id;
        $disaster_evacuation_center->address = $request->address;
        $disaster_evacuation_center->latitude = $request->latitude;
        $disaster_evacuation_center->longitude = $request->longitude;
        $disaster_evacuation_center->purok = $request->purok;
        $disaster_evacuation_center->street = $request->street;
        $disaster_evacuation_center->barangay = $request->barangay;
        $disaster_evacuation_center->municipality = $request->municipality;
        $disaster_evacuation_center->province = $request->province;
        $disaster_evacuation_center->name = $request->name;
        $disaster_evacuation_center->type = $request->type;
        $disaster_evacuation_center->condition = $request->condition;
        $disaster_evacuation_center->incharge = $request->incharge;
        $disaster_evacuation_center->contact = $request->contact;
        $disaster_evacuation_center->capacity = $request->capacity;
        $disaster_evacuation_center->facility_toilet = $request->facility_toilet;
        $disaster_evacuation_center->facility_access_to_water = $request->facility_access_to_water;
        $disaster_evacuation_center->facility_with_electricity = $request->facility_with_electricity;
        $disaster_evacuation_center->facility_working_generator = $request->facility_working_generator;
        $disaster_evacuation_center->facility_with_private_rooms = $request->facility_with_private_rooms;
        $disaster_evacuation_center->facility_with_lactating_room = $request->facility_with_lactating_room;
        $disaster_evacuation_center->facility_kitchen = $request->facility_kitchen;
        $disaster_evacuation_center->applicable_typhoon = $request->applicable_typhoon;
        $disaster_evacuation_center->applicable_flood = $request->applicable_flood;
        $disaster_evacuation_center->applicable_storm_surge = $request->applicable_storm_surge;
        $disaster_evacuation_center->applicable_landslide = $request->applicable_landslide;
        $disaster_evacuation_center->applicable_drought = $request->applicable_drought;
        $disaster_evacuation_center->applicable_volcanic_eruption = $request->applicable_volcanic_eruption;
        $disaster_evacuation_center->applicable_sinkhole = $request->applicable_sinkhole;
        $disaster_evacuation_center->save();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$request->name.' on Disaster Admin Evacuation Center';
        $al->activity = 'add';
        $al->save();

        return redirect()->route('disasteradmin.evacuation_center')->with('flash_message', $disaster_evacuation_center->name.' was Added!!');
    }

    function evacuationCenterUpdate(Request $request){
        $disaster_evacuation_center = DisasterEvacuationCenter::find($request->id);
        $disaster_evacuation_center->address = $request->address;
        $disaster_evacuation_center->latitude = $request->latitude;
        $disaster_evacuation_center->longitude = $request->longitude;
        $disaster_evacuation_center->purok = $request->purok;
        $disaster_evacuation_center->street = $request->street;
        $disaster_evacuation_center->barangay = $request->barangay;
        $disaster_evacuation_center->municipality = $request->municipality;
        $disaster_evacuation_center->province = $request->province;
        $disaster_evacuation_center->name = $request->name;
        $disaster_evacuation_center->type = $request->type;
        $disaster_evacuation_center->condition = $request->condition;
        $disaster_evacuation_center->incharge = $request->incharge;
        $disaster_evacuation_center->contact = $request->contact;
        $disaster_evacuation_center->capacity = $request->capacity;
        $disaster_evacuation_center->no_people = $request->no_people;
        $disaster_evacuation_center->facility_toilet = $request->facility_toilet;
        $disaster_evacuation_center->facility_access_to_water = $request->facility_access_to_water;
        $disaster_evacuation_center->facility_with_electricity = $request->facility_with_electricity;
        $disaster_evacuation_center->facility_working_generator = $request->facility_working_generator;
        $disaster_evacuation_center->facility_with_private_rooms = $request->facility_with_private_rooms;
        $disaster_evacuation_center->facility_with_lactating_room = $request->facility_with_lactating_room;
        $disaster_evacuation_center->facility_kitchen = $request->facility_kitchen;
        $disaster_evacuation_center->applicable_typhoon = $request->applicable_typhoon;
        $disaster_evacuation_center->applicable_flood = $request->applicable_flood;
        $disaster_evacuation_center->applicable_storm_surge = $request->applicable_storm_surge;
        $disaster_evacuation_center->applicable_landslide = $request->applicable_landslide;
        $disaster_evacuation_center->applicable_drought = $request->applicable_drought;
        $disaster_evacuation_center->applicable_volcanic_eruption = $request->applicable_volcanic_eruption;
        $disaster_evacuation_center->applicable_sinkhole = $request->applicable_sinkhole;
        $disaster_evacuation_center->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$request->name.' on Disaster Admin Evacuation Center';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('disasteradmin.evacuation_center')->with('flash_message',  $disaster_evacuation_center->name.' was Updated!!');
    }

    function evacuationCenterDelete($id){
        $disaster_evacuation_center = DisasterEvacuationCenter::find($id);
        $disaster_evacuation_center->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$disaster_evacuation_center->name.' on Disaster Admin Evacuation Center';
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('disasteradmin.evacuation_center')->with('flash_message', $disaster_evacuation_center->name.' was Deleted');
    }

    function evacuationCenterRestore($id){
        $disaster_evacuation_center = DisasterEvacuationCenter::onlyTrashed()->find($id);
        $disaster_evacuation_center->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$disaster_evacuation_center->name.' on Disaster Admin Evacuation Center';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('disasteradmin.evacuation_center')->with('flash_message', $disaster_evacuation_center->name.' was Restored');
    }

	function makePlan(){
        $plan = DisasterPlan::first();
    	return view('disaster_admin.make_plan',compact('plan'));
    }

    function identifyEscapeRoutes(Request $request){
        $file = $request->identify_escape_routes;
        $extension = $file->getClientOriginalExtension();
        $filename = "identify_escape_routes.".$extension;
        if($file){
            Storage::disk('disaster_plans')->put($filename, File::get($file));
        }
        $disaster_plans = DisasterPlan::find($request->id);
        $disaster_plans->identify_escape_routes = $filename;
        $disaster_plans->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated Identify Escape Routes File on Disaster Plan';
        $al->activity = 'update';
        $al->save();
        
        return redirect()->back()->with('flash_message', 'Identify Escape Routes File was Updated');
    }

    function establishMeetingPlaces(Request $request){
        $file = $request->establish_meeting_places;
        $extension = $file->getClientOriginalExtension();
        $filename = "establish_meeting_places.".$extension;
        if($file){
            Storage::disk('disaster_plans')->put($filename, File::get($file));
        }
        $disaster_plans = DisasterPlan::find($request->id);
        $disaster_plans->establish_meeting_places = $filename;
        $disaster_plans->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated Establish Meeting Places File on Disaster Plan';
        $al->activity = 'update';
        $al->save();
        
        return redirect()->back()->with('flash_message', 'Establish Meeting Places File was Updated');
    }

    function planForChildren(Request $request){
        $file = $request->plan_for_children;
        $extension = $file->getClientOriginalExtension();
        $filename = "plan_for_children.".$extension;
        if($file){
            Storage::disk('disaster_plans')->put($filename, File::get($file));
        }
        $disaster_plans = DisasterPlan::find($request->id);
        $disaster_plans->plan_for_children = $filename;
        $disaster_plans->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated Plan For Children File on Disaster Plan';
        $al->activity = 'update';
        $al->save();
        
        return redirect()->back()->with('flash_message', 'Plan For Children File was Updated');
    }

    function addressAnySpecialHealthNeeds(Request $request){
        $file = $request->address_any_special_health_needs;
        $extension = $file->getClientOriginalExtension();
        $filename = "address_any_special_health_needs.".$extension;
        if($file){
            Storage::disk('disaster_plans')->put($filename, File::get($file));
        }
        $disaster_plans = DisasterPlan::find($request->id);
        $disaster_plans->address_any_special_health_needs = $filename;
        $disaster_plans->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated Address Any Special Health Needs File on Disaster Plan';
        $al->activity = 'update';
        $al->save();
        
        return redirect()->back()->with('flash_message', 'Address Any Special Health Needs File was Updated');
    }

    function planForPets(Request $request){
        $file = $request->plan_for_pets;
        $extension = $file->getClientOriginalExtension();
        $filename = "plan_for_pets.".$extension;
        if($file){
            Storage::disk('disaster_plans')->put($filename, File::get($file));
        }
        $disaster_plans = DisasterPlan::find($request->id);
        $disaster_plans->plan_for_pets = $filename;
        $disaster_plans->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated Plan For Pets File on Disaster Plan';
        $al->activity = 'update';
        $al->save();
        
        return redirect()->back()->with('flash_message', 'Plan For Pets File was Updated');
    }

    function planForSpecificRisks(Request $request){
        $file = $request->plan_for_specific_risks;
        $extension = $file->getClientOriginalExtension();
        $filename = "plan_for_specific_risks.".$extension;
        if($file){
            Storage::disk('disaster_plans')->put($filename, File::get($file));
        }
        $disaster_plans = DisasterPlan::find($request->id);
        $disaster_plans->plan_for_specific_risks = $filename;
        $disaster_plans->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated Plan For Specific Risks File on Disaster Plan';
        $al->activity = 'update';
        $al->save();
        
        return redirect()->back()->with('flash_message', 'Plan For Specific Risks File was Updated');
    }

    function recordEmergencyContactInformation(Request $request){
        $file = $request->record_emergency_contact_information;
        $extension = $file->getClientOriginalExtension();
        $filename = "record_emergency_contact_information.".$extension;
        if($file){
            Storage::disk('disaster_plans')->put($filename, File::get($file));
        }
        $disaster_plans = DisasterPlan::find($request->id);
        $disaster_plans->record_emergency_contact_information = $filename;
        $disaster_plans->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated Record Rmergency Contact Information File on Disaster Plan';
        $al->activity = 'update';
        $al->save();
        
        return redirect()->back()->with('flash_message', 'Identify Record Rmergency Contact Information File was Updated');
    }

    function completeSafeHomeInstructions(Request $request){
        $file = $request->complete_safe_home_instructions;
        $extension = $file->getClientOriginalExtension();
        $filename = "complete_safe_home_instructions.".$extension;
        if($file){
            Storage::disk('disaster_plans')->put($filename, File::get($file));
        }
        $disaster_plans = DisasterPlan::find($request->id);
        $disaster_plans->complete_safe_home_instructions = $filename;
        $disaster_plans->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated Complete Safe Home Instructions File on Disaster Plan';
        $al->activity = 'update';
        $al->save();
        
        return redirect()->back()->with('flash_message', 'Complete Safe Home Instructions File was Updated');
    }

    function summary(Request $request){
        $file = $request->summary;
        $extension = $file->getClientOriginalExtension();
        $filename = "summary.".$extension;
        if($file){
            Storage::disk('disaster_plans')->put($filename, File::get($file));
        }
        $disaster_plans = DisasterPlan::find($request->id);
        $disaster_plans->summary = $filename;
        $disaster_plans->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated Summary File on Disaster Plan';
        $al->activity = 'update';
        $al->save();
        
        return redirect()->back()->with('flash_message', 'Summary was Updated');
    }

    function preparednessKit(){
        $pk = DisasterKit::first();
    	return view('disaster_admin.preparedness_kit',compact('pk'));
    }

    function preparednessKitUpdate(Request $request){
        if($request->submit == 'Update'){
            $disaster_kit = DisasterKit::find($request->id);
            $disaster_kit->kit = $request->kit;
            $disaster_kit->update();

            $al = new ActivityLog();
            $al->barangay_about_id = Session::get('barangayAbout')->id;
            $al->username = Session::get('logUser')->username;
            $al->description = 'Updated Preparedness Kit on Disaster Preparedness Kit';
            $al->activity = 'update';
            $al->save();

            return redirect()->back()->with('flash_message', 'Updated Preparedness Kit');
        } else {
            $name = Session::get('logUser')->first_name." ".Session::get('logUser')->middle_name." ".Session::get('logUser')->last_name;
            $data = [
              'name'=>$name,
              'bodyMessage'=>$request->kit,
              'subject'=>'Preparedness Kit(Tips)',
              'email'=>Session::get('logUser')->email,
              'emailSend'=>$request->email
            ];

            Mail::send(['html'=>'email'], $data, function($message) use ($data) {
              $message->to($data['emailSend'])->subject($data['subject']);
              $message->from('support@aguora.com','support@aguora.com');
            });

            $al = new ActivityLog();
            $al->barangay_about_id = Session::get('barangayAbout')->id;
            $al->username = Session::get('logUser')->username;
            $al->description = 'Sent Preparedness Kit Tips to '.$request->email;
            $al->activity = 'sent';
            $al->save();

            return redirect()->back()->with('flash_message', 'Preparedness Kit was successfully sent to '.$request->email);;
        }
    }

    function bdrrmcTeam(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $disaster_bdrrmc_teams = DB::table('disaster_teams')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('code','like', '%'.$request->search.'%')
                ->orWhere('roles','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $disaster_bdrrmc_teams = DB::table('disaster_teams')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('code','like', '%'.$request->search.'%')
                ->orWhere('roles','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('updated_at','desc')
            ->paginate(25);
        }
        return view('disaster_admin.bdrrmc_team',compact('disaster_bdrrmc_teams'));
    }

    function bdrrmcTeamStore(Request $request){
        $disaster_bdrrmc_team = new DisasterTeam();
        $disaster_bdrrmc_team->barangay_about_id = Session::get('barangayAbout')->id;
        $disaster_bdrrmc_team->code = $request->code;
        $disaster_bdrrmc_team->name = $request->name;
        $disaster_bdrrmc_team->roles = $request->roles;
        $disaster_bdrrmc_team->save();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$request->name.' on Disaster Admin BDRRMC Team';
        $al->activity = 'add';
        $al->save();

        return redirect()->route('disasteradmin.bdrrmc_team')->with('flash_message', $disaster_bdrrmc_team->name.' was Added!!');
    }

    function bdrrmcTeamUpdate(Request $request){
        $disaster_bdrrmc_team = DisasterTeam::find($request->id);
        $disaster_bdrrmc_team->code = $request->code;
        $disaster_bdrrmc_team->name = $request->name;
        $disaster_bdrrmc_team->roles = $request->roles;
        $disaster_bdrrmc_team->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$request->name.' on Disaster Admin BDRRMC Team';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('disasteradmin.bdrrmc_team')->with('flash_message',  $disaster_bdrrmc_team->name.' was Updated!!');
    }

    function bdrrmcTeamDelete($id){
        $disaster_bdrrmc_team = DisasterTeam::find($id);
        $disaster_bdrrmc_team->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$disaster_bdrrmc_team->name.' on Disaster Admin BDRRMC Team';
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('disasteradmin.bdrrmc_team')->with('flash_message', $disaster_bdrrmc_team->name.' was Deleted');
    }

    function bdrrmcTeamRestore($id){
        $disaster_bdrrmc_team = DisasterTeam::onlyTrashed()->find($id);
        $disaster_bdrrmc_team->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$disaster_bdrrmc_team->name.' on Disaster Admin BDRRMC Team';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('disasteradmin.bdrrmc_team')->with('flash_message', $disaster_bdrrmc_team->name.' was Restored');
    }

    function bdrrmcTeamMember(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $disaster_bdrrmc_team_members = DB::table('disaster_team_members')
            ->join('disaster_teams','disaster_teams.id','=','disaster_team_members.disaster_team_id')
            ->select(
                'disaster_team_members.*',
                'disaster_teams.name'
            )
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('first_name','like', '%'.$request->search.'%')
                ->orWhere('middle_name','like', '%'.$request->search.'%')
                ->orWhere('last_name','like', '%'.$request->search.'%')
                ->orWhere('gender','like', '%'.$request->search.'%')
                ->orWhere('birthdate','like', '%'.$request->search.'%')
                ->orWhere('period_from','like', '%'.$request->search.'%')
                ->orWhere('period_to','like', '%'.$request->search.'%')
                ->orWhere('contact','like', '%'.$request->search.'%');
            })
            ->where('disaster_team_members.barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $disaster_bdrrmc_team_members = DB::table('disaster_team_members')
            ->join('disaster_teams','disaster_teams.id','=','disaster_team_members.disaster_team_id')
            ->select(
                'disaster_team_members.*',
                'disaster_teams.name'
            )
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('first_name','like', '%'.$request->search.'%')
                ->orWhere('middle_name','like', '%'.$request->search.'%')
                ->orWhere('last_name','like', '%'.$request->search.'%')
                ->orWhere('gender','like', '%'.$request->search.'%')
                ->orWhere('birthdate','like', '%'.$request->search.'%')
                ->orWhere('period_from','like', '%'.$request->search.'%')
                ->orWhere('period_to','like', '%'.$request->search.'%')
                ->orWhere('contact','like', '%'.$request->search.'%');
            })
            ->where('disaster_team_members.barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('updated_at','desc')
            ->paginate(25);
        }
        $disaster_bdrrmc_teams = DisasterTeam::get();
        return view('disaster_admin.bdrrmc_team_member',compact('disaster_bdrrmc_team_members','disaster_bdrrmc_teams'));
    }

    function bdrrmcTeamMemberStore(Request $request){
        $disaster_bdrrmc_team_member = new DisasterTeamMember();
        $disaster_bdrrmc_team_member->barangay_about_id = Session::get('barangayAbout')->id;
        $disaster_bdrrmc_team_member->first_name = $request->first_name;
        $disaster_bdrrmc_team_member->middle_name = $request->middle_name;
        $disaster_bdrrmc_team_member->last_name = $request->last_name;
        $disaster_bdrrmc_team_member->birthdate = $request->birthdate;
        $disaster_bdrrmc_team_member->disaster_team_id = $request->disaster_team_id;
        $disaster_bdrrmc_team_member->gender = $request->gender;
        $disaster_bdrrmc_team_member->period_from = $request->period_from;
        $disaster_bdrrmc_team_member->period_to = $request->period_to;
        $disaster_bdrrmc_team_member->contact = $request->contact;
        $disaster_bdrrmc_team_member->save();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$request->last_name.', '.$request->first_name.' '.$request->middle_name.' on Disaster Admin BDRRMC Team';
        $al->activity = 'add';
        $al->save();

        return redirect()->route('disasteradmin.bdrrmc_team_member')->with('flash_message', $request->last_name.', '.$request->first_name.' '.$request->middle_name.' was Added!!');
    }

    function bdrrmcTeamMemberUpdate(Request $request){
        $disaster_bdrrmc_team_member = DisasterTeamMember::find($request->id);
        $disaster_bdrrmc_team_member->first_name = $request->first_name;
        $disaster_bdrrmc_team_member->middle_name = $request->middle_name;
        $disaster_bdrrmc_team_member->last_name = $request->last_name;
        $disaster_bdrrmc_team_member->birthdate = $request->birthdate;
        $disaster_bdrrmc_team_member->disaster_team_id = $request->disaster_team_id;
        $disaster_bdrrmc_team_member->gender = $request->gender;
        $disaster_bdrrmc_team_member->period_from = $request->period_from;
        $disaster_bdrrmc_team_member->period_to = $request->period_to;
        $disaster_bdrrmc_team_member->contact = $request->contact;
        $disaster_bdrrmc_team_member->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$request->last_name.', '.$request->first_name.' '.$request->middle_name.' on Disaster Admin BDRRMC Team';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('disasteradmin.bdrrmc_team_member')->with('flash_message',  $request->last_name.', '.$request->first_name.' '.$request->middle_name.' was Updated!!');
    }

    function bdrrmcTeamMemberDelete($id){
        $disaster_bdrrmc_team_member = DisasterTeamMember::find($id);
        $disaster_bdrrmc_team_member->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$disaster_bdrrmc_team_member->last_name.', '.$disaster_bdrrmc_team_member->first_name.' '.$disaster_bdrrmc_team_member->middle_name.' on Disaster Admin BDRRMC Team';
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('disasteradmin.bdrrmc_team_member')->with('flash_message', $disaster_bdrrmc_team_member->last_name.', '.$disaster_bdrrmc_team_member->first_name.' '.$disaster_bdrrmc_team_member->middle_name.' was Deleted');
    }

    function bdrrmcTeamMemberRestore($id){
        $disaster_bdrrmc_team_member = DisasterTeamMember::onlyTrashed()->find($id);
        $disaster_bdrrmc_team_member->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$disaster_bdrrmc_team_member->last_name.', '.$disaster_bdrrmc_team_member->first_name.' '.$disaster_bdrrmc_team_member->middle_name.' on Disaster Admin BDRRMC Team';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('disasteradmin.bdrrmc_team_member')->with('flash_message', $disaster_bdrrmc_team_member->last_name.', '.$disaster_bdrrmc_team_member->first_name.' '.$disaster_bdrrmc_team_member->middle_name.' was Restored');
    }

    function bdrrmcTeamMemberPrint(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $disaster_bdrrmc_team_members = DB::table('disaster_team_members')
            ->join('disaster_teams','disaster_teams.id','=','disaster_team_members.disaster_team_id')
            ->select(
                'disaster_team_members.*',
                'disaster_teams.name'
            )
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->print.'%')
                ->orWhere('first_name','like', '%'.$request->print.'%')
                ->orWhere('middle_name','like', '%'.$request->print.'%')
                ->orWhere('last_name','like', '%'.$request->print.'%')
                ->orWhere('gender','like', '%'.$request->print.'%')
                ->orWhere('birthdate','like', '%'.$request->print.'%')
                ->orWhere('period_from','like', '%'.$request->print.'%')
                ->orWhere('period_to','like', '%'.$request->print.'%')
                ->orWhere('contact','like', '%'.$request->print.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->get();
        }
        else
        {
            $disaster_bdrrmc_team_members = DB::table('disaster_team_members')
            ->join('disaster_teams','disaster_teams.id','=','disaster_team_members.disaster_team_id')
            ->select(
                'disaster_team_members.*',
                'disaster_teams.name'
            )
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->print.'%')
                ->orWhere('first_name','like', '%'.$request->print.'%')
                ->orWhere('middle_name','like', '%'.$request->print.'%')
                ->orWhere('last_name','like', '%'.$request->print.'%')
                ->orWhere('gender','like', '%'.$request->print.'%')
                ->orWhere('birthdate','like', '%'.$request->print.'%')
                ->orWhere('period_from','like', '%'.$request->print.'%')
                ->orWhere('period_to','like', '%'.$request->print.'%')
                ->orWhere('contact','like', '%'.$request->print.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('name','asc')
            ->get();
        }
        return view('disaster_admin.bdrrmc_team_member_print',compact('disaster_bdrrmc_team_members'));
    }

    private function sendSms($mobileNumber, $message) {
        try {
            $client = new Client();
            $credentials  = [
                "form_params" => [
                    "apikey" => '38260a91c0e2cd9738cd5dec15400e80',
                    "message" => $message,
                    "number" => $mobileNumber
                ]
            ];
            $res = $client->post('https://api.semaphore.co/api/v4/messages', $credentials);
            echo $res->getBody();
        } catch (GuzzleException $e) {
        }
    }
}
