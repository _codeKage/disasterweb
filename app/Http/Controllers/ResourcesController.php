<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use DB;
use App\Http\Requests;
use App\ResourcesBudget;
use App\ResourcesEquipment;
use App\ResourcesEquipmentHistory;
use App\ResourcesExpense;
use App\ResourcesSupply;
use App\ResourcesSupplyHistory;
use Carbon\Carbon;
use App\ActivityLog;
use Alert;

class ResourcesController extends Controller
{
    function supplies(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_supplies = ResourcesSupply::with('resources_supply_histories')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('description','like', '%'.$request->search.'%')
                ->orWhere('category','like', '%'.$request->search.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $resources_supplies = ResourcesSupply::with('resources_supply_histories')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('description','like', '%'.$request->search.'%')
                ->orWhere('category','like', '%'.$request->search.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('updated_at','desc')
            ->paginate(25);
        }
        return view('resource.supplies',compact('resources_supplies'));
    }

    function suppliesStore(Request $request){
        $resources_supplies = new ResourcesSupply();
        $resources_supplies->barangay_about_id = Session::get('barangayAbout')->id;
        $resources_supplies->name = $request->name;
        $resources_supplies->description = $request->description;
        $resources_supplies->category = $request->category;
        $resources_supplies->save();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$resources_supplies->name.' on Resources Supplies';
        $al->activity = 'add';
        $al->save();

        return redirect()->route('resources.supplies')->with('flash_message', $resources_supplies->name.' was Added!!');
    }

    function suppliesUpdate(Request $request){
        $resources_supplies = ResourcesSupply::find($request->id);
        $resources_supplies->name = $request->name;
        $resources_supplies->description = $request->description;
        $resources_supplies->category = $request->category;
        $resources_supplies->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$resources_supplies->name.' on Resources Supplies';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('resources.supplies')->with('flash_message',  $resources_supplies->name.' was Updated!!');
    }

    function suppliesDelete($id){
        $resources_supplies = ResourcesSupply::find($id);
        $resources_supplies->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$resources_supplies->name.' on Resources Supplies';
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('resources.supplies')->with('flash_message', $resources_supplies->name.' was Deleted');
    }

    function suppliesRestore($id){
        $resources_supplies = ResourcesSupply::onlyTrashed()->find($id);
        $resources_supplies->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$resources_supplies->name.' on Resources Supplies';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('resources.supplies')->with('flash_message', $resources_supplies->name.' was Restored');
    }

    function suppliesPrint(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_supplies = ResourcesSupply::with('resources_supply_histories')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->print.'%')
                ->orWhere('description','like', '%'.$request->print.'%')
                ->orWhere('category','like', '%'.$request->print.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->get();
        }
        else
        {
            $resources_supplies = ResourcesSupply::with('resources_supply_histories')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->print.'%')
                ->orWhere('description','like', '%'.$request->print.'%')
                ->orWhere('category','like', '%'.$request->print.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('name','asc')
            ->get();
        }
        return view('resource.print.supplies_print',compact('resources_supplies'));
    }

    function suppliesHistory($id,Request $request){
        $rs = ResourcesSupply::select('id','name')->find($id);
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_supply_histories = ResourcesSupplyHistory::where('resources_supply_id',$id)
            ->Where(function ($query) use ($request) {
                $query->where('name_source','like', '%'.$request->search.'%')
                ->orWhere('date','like', '%'.$request->search.'%')
                ->orWhere('added','like', '%'.$request->search.'%')
                ->orWhere('distributed','like', '%'.$request->search.'%')
                ->orWhere('particular','like', '%'.$request->search.'%')
                ->orWhere('balance','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $resources_supply_histories = ResourcesSupplyHistory::where('resources_supply_id',$id)
            ->Where(function ($query) use ($request) {
                $query->where('name_source','like', '%'.$request->search.'%')
                ->orWhere('date','like', '%'.$request->search.'%')
                ->orWhere('added','like', '%'.$request->search.'%')
                ->orWhere('distributed','like', '%'.$request->search.'%')
                ->orWhere('particular','like', '%'.$request->search.'%')
                ->orWhere('balance','like', '%'.$request->search.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('date','asc')
            ->paginate(25);
        }
        return view('resource.supplies_history',compact('resources_supply_histories','rs'));
    }

    function suppliesHistoryPrint($id,Request $request){
        $rs = ResourcesSupply::select('id','name')->find($id);
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_supply_histories = ResourcesSupplyHistory::where('resources_supply_id',$id)
            ->Where(function ($query) use ($request) {
                $query->where('name_source','like', '%'.$request->print.'%')
                ->orWhere('date','like', '%'.$request->print.'%')
                ->orWhere('added','like', '%'.$request->print.'%')
                ->orWhere('distributed','like', '%'.$request->print.'%')
                ->orWhere('particular','like', '%'.$request->print.'%')
                ->orWhere('balance','like', '%'.$request->print.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->get();
        }
        else
        {
            $resources_supply_histories = ResourcesSupplyHistory::where('resources_supply_id',$id)
            ->Where(function ($query) use ($request) {
                $query->where('name_source','like', '%'.$request->print.'%')
                ->orWhere('date','like', '%'.$request->print.'%')
                ->orWhere('added','like', '%'.$request->print.'%')
                ->orWhere('distributed','like', '%'.$request->print.'%')
                ->orWhere('particular','like', '%'.$request->print.'%')
                ->orWhere('balance','like', '%'.$request->print.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('date','asc')
            ->get();
        }
        return view('resource.print.supplies_history_print',compact('resources_supply_histories','rs'));
    }

    function suppliesStocksStore(Request $request){
        $resources_supplies = new ResourcesSupplyHistory();
        $resources_supplies->barangay_about_id = Session::get('barangayAbout')->id;
        $resources_supplies->resources_supply_id = $request->resources_supply_id;
        $resources_supplies->date = $request->date;
        $resources_supplies->name_source = $request->name_source;
        $resources_supplies->particular = $request->particular;
        $resources_supplies->added = $request->added;
        $resources_supplies->balance = $request->balance;
        $resources_supplies->save();

        $rs = ResourcesSupply::find($resources_supplies->resources_supply_id);
        $rs->total_added = $rs->total_added + $request->added;
        $rs->update();

        $name = ResourcesSupply::select('name')->find($request->resources_supply_id);

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$resources_supplies->added.' stocks on '.$name->name.' in Resources Supplies';
        $al->activity = 'add';
        $al->save();

        return redirect()->route('resources.supplies')->with('flash_message', $resources_supplies->added.' stocks was Added to '.$name->name.'!!');
    }

    function suppliesDistributesStore(Request $request){
        $resources_supplies = new ResourcesSupplyHistory();
        $resources_supplies->barangay_about_id = Session::get('barangayAbout')->id;
        $resources_supplies->resources_supply_id = $request->resources_supply_id;
        $resources_supplies->date = $request->date;
        $resources_supplies->name_source = $request->name_source;
        $resources_supplies->particular = $request->particular;
        $resources_supplies->distributed = $request->distributed;
        $resources_supplies->balance = $request->balance;
        $resources_supplies->save();

        $rs = ResourcesSupply::find($resources_supplies->resources_supply_id);
        $rs->total_distributed = $rs->total_distributed + $request->distributed;
        $rs->update();

        $name = ResourcesSupply::select('name')->find($request->resources_supply_id);

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Distributed '.$resources_supplies->distributed.' stocks from '.$name->name.' in Resources Supplies';
        $al->activity = 'distributed';
        $al->save();

        return redirect()->route('resources.supplies')->with('flash_message', $resources_supplies->distributed.' stocks was Distributed from '.$name->name.'!!');
    }

    function equipments(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_equipments = ResourcesEquipment::with('resources_equipments_histories')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('description','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $resources_equipments = ResourcesEquipment::with('resources_equipments_histories')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->search.'%')
                ->orWhere('description','like', '%'.$request->search.'%')
                ->orWhere('type','like', '%'.$request->search.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('updated_at','desc')
            ->paginate(25);
        }
        return view('resource.equipments',compact('resources_equipments'));
    }

    function equipmentsStore(Request $request){
        // dd($request);
        $resources_equipments = new ResourcesEquipment();
        $resources_equipments->barangay_about_id = Session::get('barangayAbout')->id;
        $resources_equipments->name = $request->name;
        $resources_equipments->description = $request->description;
        $resources_equipments->type = $request->type;
        $resources_equipments->save();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$resources_equipments->name.' on Resources equipments';
        $al->activity = 'add';
        $al->save();

        return redirect()->route('resources.equipments')->with('flash_message', $resources_equipments->name.' was Added!!');
    }

    function equipmentsUpdate(Request $request){
        $resources_equipments = ResourcesEquipment::find($request->id);
        $resources_equipments->name = $request->name;
        $resources_equipments->description = $request->description;
        $resources_equipments->type = $request->type;
        $resources_equipments->good_condition = $request->good_condition;
        $resources_equipments->bad_condition = $request->bad_condition;
        $resources_equipments->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$resources_equipments->name.' on Resources equipments';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('resources.equipments')->with('flash_message',  $resources_equipments->name.' was Updated!!');
    }

    function equipmentsDelete($id){
        $resources_equipments = ResourcesEquipment::find($id);
        $resources_equipments->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$resources_equipments->name.' on Resources equipments';
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('resources.equipments')->with('flash_message', $resources_equipments->name.' was Deleted');
    }

    function equipmentsRestore($id){
        $resources_equipments = ResourcesEquipment::onlyTrashed()->find($id);
        $resources_equipments->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$resources_equipments->name.' on Resources equipments';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('resources.equipments')->with('flash_message', $resources_equipments->name.' was Restored');
    }

    function equipmentsPrint(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_equipments = ResourcesEquipment::with('resources_equipments_histories')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->print.'%')
                ->orWhere('description','like', '%'.$request->print.'%')
                ->orWhere('type','like', '%'.$request->print.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->get();
        }
        else
        {
            $resources_equipments = ResourcesEquipment::with('resources_equipments_histories')
            ->Where(function ($query) use ($request) {
                $query->where('name','like', '%'.$request->print.'%')
                ->orWhere('description','like', '%'.$request->print.'%')
                ->orWhere('type','like', '%'.$request->print.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('name','asc')
            ->get();
        }
        return view('resource.print.equipments_print',compact('resources_equipments'));
    }

    function equipmentsHistory($id,Request $request){
        $re = ResourcesEquipment::select('id','name')->find($id);
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_equipment_histories = ResourcesEquipmentHistory::where('resources_equipment_id',$id)
            ->Where(function ($query) use ($request) {
                $query->where('name_source','like', '%'.$request->search.'%')
                ->orWhere('date','like', '%'.$request->search.'%')
                ->orWhere('added','like', '%'.$request->search.'%')
                ->orWhere('removed','like', '%'.$request->search.'%')
                ->orWhere('particular','like', '%'.$request->search.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $resources_equipment_histories = ResourcesEquipmentHistory::where('resources_equipment_id',$id)
            ->Where(function ($query) use ($request) {
                $query->where('name_source','like', '%'.$request->search.'%')
                ->orWhere('date','like', '%'.$request->search.'%')
                ->orWhere('added','like', '%'.$request->search.'%')
                ->orWhere('removed','like', '%'.$request->search.'%')
                ->orWhere('particular','like', '%'.$request->search.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('date','asc')
            ->paginate(25);
        }
        return view('resource.equipments_history',compact('resources_equipment_histories','re'));
    }

    function equipmentsHistoryPrint($id,Request $request){
        $re = ResourcesEquipment::select('id','name')->find($id);
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_equipment_histories = ResourcesEquipmentHistory::where('resources_equipment_id',$id)
            ->Where(function ($query) use ($request) {
                $query->where('name_source','like', '%'.$request->print.'%')
                ->orWhere('date','like', '%'.$request->print.'%')
                ->orWhere('added','like', '%'.$request->print.'%')
                ->orWhere('removed','like', '%'.$request->print.'%')
                ->orWhere('particular','like', '%'.$request->print.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->get();
        }
        else
        {
            $resources_equipment_histories = ResourcesEquipmentHistory::where('resources_equipment_id',$id)
            ->Where(function ($query) use ($request) {
                $query->where('name_source','like', '%'.$request->print.'%')
                ->orWhere('date','like', '%'.$request->print.'%')
                ->orWhere('added','like', '%'.$request->print.'%')
                ->orWhere('removed','like', '%'.$request->print.'%')
                ->orWhere('particular','like', '%'.$request->print.'%');
            })
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('date','asc')
            ->get();
        }
        return view('resource.print.equipments_history_print',compact('resources_equipment_histories','re'));
    }

    function equipmentsStocksStore(Request $request){
        $resources_equipments = new ResourcesEquipmentHistory();
        $resources_equipments->barangay_about_id = Session::get('barangayAbout')->id;
        $resources_equipments->resources_equipment_id = $request->resources_equipment_id;
        $resources_equipments->date = $request->date;
        $resources_equipments->name_source = $request->name_source;
        $resources_equipments->particular = $request->particular;
        $resources_equipments->added = $request->added;
        $resources_equipments->save();

        $re = ResourcesEquipment::find($request->resources_equipment_id);
        $re->good_condition = $re->good_condition + $request->added;
        $re->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$resources_equipments->added.' stocks on '.$re->name.' in Resources equipments';
        $al->activity = 'add';
        $al->save();

        return redirect()->route('resources.equipments')->with('flash_message', $resources_equipments->added.' stocks was Added to '.$re->name.'!!');
    }

    function equipmentsRemoveStore(Request $request){
        $resources_equipments = new ResourcesEquipmentHistory();
        $resources_equipments->barangay_about_id = Session::get('barangayAbout')->id;
        $resources_equipments->resources_equipment_id = $request->resources_equipment_id;
        $resources_equipments->date = $request->date;
        $resources_equipments->name_source = $request->name_source;
        $resources_equipments->particular = $request->particular;
        $resources_equipments->removed = $request->removed;
        $resources_equipments->save();

        $re = ResourcesEquipment::find($request->resources_equipment_id);
        $re->bad_condition = $request->balance;
        $re->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Distributed '.$resources_equipments->distributed.' stocks from '.$re->name.' in Resources equipments';
        $al->activity = 'distributed';
        $al->save();

        return redirect()->route('resources.equipments')->with('flash_message', $resources_equipments->distributed.' stocks was Distributed from '.$re->name.'!!');
    }

    function budget(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_budgets = ResourcesBudget::with('resources_expenses')
            ->Where(function ($query) use ($request) {
                $query->where('fiscal_year','like', '%'.$request->search.'%')
                ->orWhere('code','like', '%'.$request->search.'%')
                ->orWhere('account_name','like', '%'.$request->search.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $resources_budgets = ResourcesBudget::with('resources_expenses')
            ->Where(function ($query) use ($request) {
                $query->where('fiscal_year','like', '%'.$request->search.'%')
                ->orWhere('code','like', '%'.$request->search.'%')
                ->orWhere('account_name','like', '%'.$request->search.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('fiscal_year','desc')
            ->paginate(25);
        }
        return view('resource.budget',compact('resources_budgets'));
    }

    function budgetStore(Request $request){
        $resources_budgets = new ResourcesBudget();
        $resources_budgets->barangay_about_id = Session::get('barangayAbout')->id;
        $resources_budgets->fiscal_year = $request->fiscal_year;
        $resources_budgets->code = $request->code;
        $resources_budgets->account_name = $request->account_name;
        $resources_budgets->funding_amount = $request->funding_amount;
        $resources_budgets->save();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Added '.$resources_budgets->code.' on Resources Budget';
        $al->activity = 'add';
        $al->save();

        return redirect()->route('financial.budget')->with('flash_message', $resources_budgets->code.' was Added!!');
    }

    function budgetUpdate(Request $request){
        $resources_budgets = ResourcesBudget::find($request->id);
        $resources_budgets->fiscal_year = $request->fiscal_year;
        $resources_budgets->code = $request->code;
        $resources_budgets->account_name = $request->account_name;
        $resources_budgets->funding_amount = $request->funding_amount;
        $resources_budgets->update();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Updated '.$resources_budgets->code.' on Resources Budget';
        $al->activity = 'update';
        $al->save();

        return redirect()->route('financial.budget')->with('flash_message',  $resources_budgets->code.' was Updated!!');
    }

    function budgetDelete($id){
        $resources_budgets = ResourcesBudget::find($id);
        $resources_budgets->delete();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$resources_budgets->code.' on Resources Budget';
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('financial.budget')->with('flash_message', $resources_budgets->code.' was Deleted');
    }

    function budgetRestore($id){
        $resources_budgets = ResourcesBudget::onlyTrashed()->find($id);
        $resources_budgets->restore();

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$resources_budgets->code.' on Resources Budget';
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('financial.budget')->with('flash_message', $resources_budgets->code.' was Restored');
    }

    function budgetPrint(Request $request){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_budgets = ResourcesBudget::with('resources_expenses')
            ->Where(function ($query) use ($request) {
                $query->where('fiscal_year','like', '%'.$request->print.'%')
                ->orWhere('code','like', '%'.$request->print.'%')
                ->orWhere('account_name','like', '%'.$request->print.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->get();
        }
        else
        {
            $resources_budgets = ResourcesBudget::with('resources_expenses')
            ->Where(function ($query) use ($request) {
                $query->where('fiscal_year','like', '%'.$request->print.'%')
                ->orWhere('code','like', '%'.$request->print.'%')
                ->orWhere('account_name','like', '%'.$request->print.'%');
            })
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('fiscal_year','desc')
            ->get();
        }
        return view('resource.print.budget_print',compact('resources_budgets'));
    }

    function budgetExpenses(Request $request,$resources_budget_id){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_expenses = ResourcesExpense::with('resources_budgets')
            ->Where(function ($query) use ($request) {
                $query->where('date','like', '%'.$request->search.'%')
                ->orWhere('particular','like', '%'.$request->search.'%')
                ->orWhere('amount','like', '%'.$request->search.'%');
            })
            ->where('resources_budget_id',$resources_budget_id)
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->paginate(25);
        }
        else
        {
            $resources_expenses = ResourcesExpense::with('resources_budgets')
            ->Where(function ($query) use ($request) {
                $query->where('date','like', '%'.$request->search.'%')
                ->orWhere('particular','like', '%'.$request->search.'%')
                ->orWhere('amount','like', '%'.$request->search.'%');
            })
            ->where('resources_budget_id',$resources_budget_id)
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('date','desc')
            ->paginate(25);
        }
        $resource_budget = ResourcesBudget::with('resources_expenses')->find($resources_budget_id);
        return view('resource.expenses',compact('resources_expenses','resource_budget'));
    }

    function budgetExpensesStore(Request $request,$resources_budget_id){
        $resources_expenses = new ResourcesExpense();
        $resources_expenses->barangay_about_id = Session::get('barangayAbout')->id;
        $resources_expenses->resources_budget_id = $request->resources_budget_id;
        $resources_expenses->date = $request->date;
        $resources_expenses->particular = $request->particular;
        $resources_expenses->amount = $request->amount;
        $resources_expenses->remarks = $request->remarks;


        $rb = ResourcesBudget::find($request->resources_budget_id);
        if($this->checkAmountAndType($rb->funding_amount, $resources_expenses->amount,$resources_expenses->particular)) {
            $resources_expenses->save();
            $al = new ActivityLog();
            $al->barangay_about_id = Session::get('barangayAbout')->id;
            $al->username = Session::get('logUser')->username;
            $al->description = 'Added '.$resources_expenses->amount.' Expenses on a budget with a code of '.$rb->code;
            $al->activity = 'add';
            $al->save();

            return redirect()->route('financial.budget.expenses',['resources_budget_id'=>$rb->id])->with('flash_message', $resources_expenses->amount.' was Added!!');
        }
        else {
            Alert::error('Invalid Amount!','Error')->autoclose(2000);
            return redirect()->route('financial.budget.expenses',['resources_budget_id'=>$rb->id]);

        }


    }

    function budgetExpensesUpdate(Request $request,$resources_budget_id){
        $resources_expenses = ResourcesExpense::find($request->id);
        $resources_expenses->resources_budget_id = $resources_budget_id;
        $resources_expenses->date = $request->date;
        $resources_expenses->particular = $request->particular;
        $resources_expenses->amount = $request->amount;
        $resources_expenses->remarks = $request->remarks;

        $rb = ResourcesBudget::find($request->resources_budget_id);

        if($this->checkAmountAndType($rb->funding_amount, $resources_expenses->amount,$resources_expenses->particular)) {
            $resources_expenses->update();
            $al = new ActivityLog();
            $al->barangay_about_id = Session::get('barangayAbout')->id;
            $al->username = Session::get('logUser')->username;
            $al->description = 'Updated ' . $resources_expenses->amount . ' Expenses on a budget with a code of ' . $rb->code;
            $al->activity = 'update';
            $al->save();

            return redirect()->route('financial.budget.expenses', ['resources_budget_id' => $rb->id])->with('flash_message', $resources_expenses->amount . ' was Updated!!');
        }
        else {
            Alert::error('Invalid Amount!','Error')->autoclose(2000);
            return redirect()->route('financial.budget.expenses',['resources_budget_id'=>$rb->id]);

        }
    }

    function budgetExpensesDelete($resources_budget_id,$id){
        $resources_expenses = ResourcesExpense::find($id);
        $resources_expenses->delete();

        $rb = ResourcesBudget::find($resources_budget_id);

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Deleted '.$resources_expenses->amount.' Expenses on a budget with a code of '.$rb->code;
        $al->activity = 'delete';
        $al->save();
        
        return redirect()->route('financial.budget.expenses',['resources_budget_id'=>$rb->id])->with('flash_message', $resources_expenses->amount.' was Deleted');
    }

    function budgetExpensesRestore($resources_budget_id,$id){
        $resources_expenses = ResourcesExpense::onlyTrashed()->find($id);
        $resources_expenses->restore();

        $rb = ResourcesBudget::find($resources_budget_id);

        $al = new ActivityLog();
        $al->barangay_about_id = Session::get('barangayAbout')->id;
        $al->username = Session::get('logUser')->username;
        $al->description = 'Restored '.$resources_expenses->amount.' Expenses on a budget with a code of '.$rb->code;
        $al->activity = 'restore';
        $al->save();

        return redirect()->route('financial.budget.expenses',['resources_budget_id'=>$rb->id])->with('flash_message', $resources_expenses->amount.' was Restored');
    }

    function budgetExpensesPrint(Request $request,$resources_budget_id){
        if(Input::get('sortby')!=null)
        {
            $val = explode(".",Input::get('sortby'));
            $resources_expenses = ResourcesExpense::with('resources_budgets')
            ->Where(function ($query) use ($request) {
                $query->where('date','like', '%'.$request->print.'%')
                ->orWhere('amount','like', '%'.$request->print.'%')
                ->orWhere('particular','like', '%'.$request->print.'%');
            })
            ->where('resources_budget_id',$resources_budget_id)
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy($val[0],$val[1])
            ->get();
        }
        else
        {
            $resources_expenses = ResourcesExpense::with('resources_budgets')
            ->Where(function ($query) use ($request) {
                $query->where('date','like', '%'.$request->print.'%')
                ->orWhere('amount','like', '%'.$request->print.'%')
                ->orWhere('particular','like', '%'.$request->print.'%');
            })
            ->where('resources_budget_id',$resources_budget_id)
            ->withTrashed()
            ->where('barangay_about_id',Session::get('barangayAbout')->id)
            ->orderBy('date','desc')
            ->get();
        }
        $resource_budget = ResourcesBudget::find($resources_budget_id);
        return view('resource.print.expenses_print',compact('resources_expenses','resource_budget'));
    }

    private  function checkAmountAndType($totalAmount, $expenseAmount, $expenseType) {
        $percentage = $expenseType == "Response"? 0.30: 0.70;
        $fAmount = $totalAmount * $percentage;
        return $fAmount >= $expenseAmount;
    }


}
