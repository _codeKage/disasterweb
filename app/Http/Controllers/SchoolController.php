<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\School;
use App\SchoolCategory;
use App\GradeCapacity;
use DB;
use Log;

class SchoolController extends Controller
{

    public function add(){
        return view('education.school_profile');
    }

    public function getSchool()
    {
        $brgy_id =Session::get("brgy_id");
        $school = School::with('school_category','grade_capacity')
            ->where('barangay_id',$brgy_id)
            ->get();

        // return response()->json(['result'=> $school],200, array(),JSON_PRETTY_PRINT);
        return view('education.school',compact('school'));
    }

    public function search($schoolName){
        $school = School::with('school_category','grade_capacity')
            ->where('school_name',$schoolName)
            ->first();

        $capacity = $school->grade_capacity;
        $school->year = count($capacity) > 0 
            ? $capacity[0]->school_year : '';
        $school->uiCapacity = $this->transformGradeCapacity($capacity);
        
        // return response()->json(['result'=> $school],200, array(),JSON_PRETTY_PRINT);
        return view('education.school_profile_edit',compact('school'));
    }

    public function store(Request $request)
    {
        $brgy_id =Session::get("brgy_id");
        Log::info('store school',['request'=>$request->all()]);
        $schoolProfile = new School();
        $schoolProfile->barangay_id = $brgy_id;
        $schoolProfile->school_name = $request->school_name;
        $schoolProfile->purok = $request->purok;
        $schoolProfile->save();

        //insert category
        $category = new SchoolCategory();
        $category->school_id = $schoolProfile->id;
        $category->elem = $request->elem;
        $category->junior = $request->junior;
        $category->senior = $request->senior;
        $category->save();

        $this->saveCapacity($request, $schoolProfile);

        return redirect()->route('education.school.add')->with('flash_message',$schoolProfile->school_name.' is Added');
    }

    public function update(Request $request)
    {
        $brgy_id =Session::get("brgy_id");
        Log::info('store school',['request'=>$request->all()]);
        $school = School::with('school_category','grade_capacity')
            ->where('id',$request->id)
            ->first();

        $school->school_name = $request->school_name;
        $school->purok = $request->purok;

        //insert category
        $category = $school->school_category;
        $category->elem = $request->elem;
        $category->junior = $request->junior;
        $category->senior = $request->senior;

        $school->update();
        $category->update();

        $this->deleteGradeCapacity($school->grade_capacity);
        $this->saveCapacity($request, $school);

        //    return response()->json(['result'=> $school],200, array(),JSON_PRETTY_PRINT);
        return view('education.school_profile')->with('flash_message',$school->school_name.' is Updated');

    }

    public function remove(Request $request) {
        $school = School::find($request->id);
        $school->deleted_at = date('Y-m-d');
        $school->save();
        return redirect()->route('education.school');
    }

    public function report(){

        $brgy_id =Session::get("brgy_id");
        $currentYear = date("Y");
        $availabilityPerGrade = DB::select(
            "   SELECT
                    *
                FROM school_profile school
                INNER JOIN (
                    SELECT school_id, MAX(school_year) AS latestYear FROM grade_capacity GROUP BY school_id
                ) maxYear 
                ON maxYear.school_id = school.id
                LEFT JOIN grade_capacity capacity
                ON capacity.school_id = school.id
                WHERE school.barangay_id = $brgy_id AND capacity.school_year = maxYear.latestYear
                ORDER BY capacity.school_year, capacity.grade
                DESC
            "
        );

        $grade = '';
        $year = '';
        $availablePerGrade = array();

        if(count($availabilityPerGrade)> 0){
            foreach($availabilityPerGrade as $availableSlot){
                $year = $availableSlot->school_year;
                if($availableSlot->grade != $grade && empty($availablePerGrade[$availableSlot->grade])){
                    $grade = $availableSlot->grade;
                    $availablePerGrade[$availableSlot->grade] = array();
                }

                array_push($availablePerGrade[$availableSlot->grade],$availableSlot);
            }

        }

        // return response()->json([
        //     'raw'=>$availabilityPerGrade,
        // 	'availablePerGrade'=>$availablePerGrade
        // ],200, array(),JSON_PRETTY_PRINT);

        return view('report.school',[
            'availablePerGrade'=>$availablePerGrade,
            'schoolYear'=> $year
        ]);
    }

    private function saveCapacity($request, $schoolProfile){
        //insert capacity
        if($request->elem == 1){
            $elemRange = ["min"=> 1, "max"=>6];
            $elemGradesBaseKey = 'elemGrades_';
            $this->saveGradeCapacity($elemRange, 'elemGrades_',$request, $schoolProfile->id);
        }

        if($request->junior == 1){
            $juniorRange = ["min"=> 7, "max"=>10];
            $this->saveGradeCapacity($juniorRange, 'juniorGrades_',$request, $schoolProfile->id);
        }

        if($request->senior == 1){
            $seniorRange = ["min"=> 11, "max"=>12];
            $seniorCategories = ["stem", "ga", "humms", "abm"];
            $this->saveSeniorCapacity($seniorRange, $seniorCategories, $request, $schoolProfile->id);
        }
    }

    private function saveSeniorCapacity($gradeRange, $seniorCategories, $request, $schoolId){
        foreach($seniorCategories as $category){
            $seniorGrade = 'seniorGrades_'.$category;
            for($gradeLevel = $gradeRange["min"]; $gradeLevel <= $gradeRange["max"]; $gradeLevel++){
                $gradeLevelKey = $seniorGrade.$gradeLevel;
                $this->createGradeCapacity($gradeLevelKey, $request, $schoolId)->save();
            }
        }
    }

    private function saveGradeCapacity($gradeRange, $gradeBaseKey, $request, $schoolId){
        for($gradeLevel = $gradeRange["min"]; $gradeLevel <= $gradeRange["max"]; $gradeLevel++){
            $gradeLevelKey = $gradeBaseKey.'grade'.$gradeLevel;
            $this->createGradeCapacity($gradeLevelKey, $request, $schoolId)->save();
        }
    }

    private function createGradeCapacity($key, $request, $shoolId){
        $capacityKey = $key.'_capacity';
        $enrolleesKey = $key.'_enrollees';
        $slotKey = $request[$capacityKey] - $request[$enrolleesKey];
        $labelKey = $key.'_label';

        $gradeCapacity = new GradeCapacity();
        $gradeCapacity->school_id = $shoolId;
        $gradeCapacity->school_year = $request->school_year;
        $gradeCapacity->grade = $request[$labelKey];
        $gradeCapacity->max_capacity = $request[$capacityKey];
        $gradeCapacity->no_enrolled = $request[$enrolleesKey];
        $gradeCapacity->available_slot = $slotKey;

        Log::info('store school',[
            'elem capacity grade '.$request[$labelKey].' '=>$request[$capacityKey],
            'elem enrollees grade '.$request[$labelKey].' '=>$request[$enrolleesKey],
            'elem slot grade '.$request[$labelKey].' '=>$slotKey
        ]);

        return $gradeCapacity;

    }

    private function deleteGradeCapacity($grades){
        foreach($grades as $grade){
            $capacity = GradeCapacity::find($grade->id)
                ->delete();
        }
    }

    private function transformGradeCapacity($capacity){
        $uiData = new \stdClass();
        $uiData->levels = $this->schoolLevel();

        foreach($capacity as $cp){
            $level = $this->findGradeItem($uiData->levels, $cp->grade);
            Log::info('found '.$level);
            if($level != 'n/a'){
                $uiData->levels->$level->capacity = $cp->max_capacity;
                $uiData->levels->$level->enrollees = $cp->no_enrolled;
                $uiData->levels->$level->availSlot = $cp->available_slot;
            }
        }

        return $uiData;
    }

    private function findGradeItem($uiGrade, $grade){
        $gr = 'n/a';
        Log::info('finding '.$grade);
        foreach($uiGrade as $key=>$ui){
            Log::info('ui',[
                'uigrade'=>$ui,
                'key'=>$key]);
            if($ui->label == $grade){
                $gr = $key;
            }
        }


        return $gr;
    }

    private function schoolLevel(){
        $level = new \stdClass();
        $level->grade1 = $this->capacityDetails();
        $level->grade1->label = 'Grade 1';
        $level->grade2 = $this->capacityDetails();
        $level->grade2->label = 'Grade 2';
        $level->grade3 = $this->capacityDetails();
        $level->grade3->label = 'Grade 3';
        $level->grade4 = $this->capacityDetails();
        $level->grade4->label = 'Grade 4';
        $level->grade5 = $this->capacityDetails();
        $level->grade5->label = 'Grade 5';
        $level->grade6 = $this->capacityDetails();
        $level->grade6->label = 'Grade 6';
        $level->grade7 = $this->capacityDetails();
        $level->grade7->label = 'Grade 7';
        $level->grade8 = $this->capacityDetails();
        $level->grade8->label = 'Grade 8';
        $level->grade9 = $this->capacityDetails();
        $level->grade9->label = 'Grade 9';
        $level->grade10 = $this->capacityDetails();
        $level->grade10->label = 'Grade 10';
        $level->stem11 = $this->capacityDetails();
        $level->stem11->label = 'STEM-11';
        $level->ga11 = $this->capacityDetails();
        $level->ga11->label = 'GA-11';
        $level->humms11 = $this->capacityDetails();
        $level->humms11->label = 'HUMMS-11';
        $level->abm11 = $this->capacityDetails();
        $level->abm11->label = 'ABM-11';
        $level->stem12 = $this->capacityDetails();
        $level->stem12->label = 'STEM-12';
        $level->ga12 = $this->capacityDetails();
        $level->ga12->label = 'GA-12';
        $level->humms12 = $this->capacityDetails();
        $level->humms12->label = 'HUMMS-12';
        $level->abm12 = $this->capacityDetails();
        $level->abm12->label = 'ABM-12';

        return $level;
    }

    private function elemLevel(){
        $elemGrades = new \stdClass();
        $elemGrades->grade1 = $this->capacityDetails();
        $elemGrades->grade1->label = 'Grade 1';
        $elemGrades->grade2 = $this->capacityDetails();
        $elemGrades->grade2->label = 'Grade 2';
        $elemGrades->grade3 = $this->capacityDetails();
        $elemGrades->grade3->label = 'Grade 3';
        $elemGrades->grade4 = $this->capacityDetails();
        $elemGrades->grade4->label = 'Grade 4';
        $elemGrades->grade5 = $this->capacityDetails();
        $elemGrades->grade5->label = 'Grade 5';
        $elemGrades->grade6 = $this->capacityDetails();
        $elemGrades->grade6->label = 'Grade 6';

        return $elemGrades;
    }

    private function juniorLevel(){
        $juniorGrades = new \stdClass();
        $juniorGrades->grade7 = $this->capacityDetails();
        $juniorGrades->grade7->label = 'Grade 7';
        $juniorGrades->grade8 = $this->capacityDetails();
        $juniorGrades->grade8->label = 'Grade 8';
        $juniorGrades->grade9 = $this->capacityDetails();
        $juniorGrades->grade9->label = 'Grade 9';
        $juniorGrades->grade10 = $this->capacityDetails();
        $juniorGrades->grade10->label = 'Grade 10';

        return $juniorGrades;
    }

    private function seniorLevel(){
        $seniorGrades = new \stdClass();
        $seniorGrades->stem11 = $this->capacityDetails();
        $seniorGrades->stem11->label = 'STEM-11';
        $seniorGrades->ga11 = $this->capacityDetails();
        $seniorGrades->ga11->label = 'GA-11';
        $seniorGrades->humms11 = $this->capacityDetails();
        $seniorGrades->humms11->label = 'HUMMS-11';
        $seniorGrades->abm11 = $this->capacityDetails();
        $seniorGrades->abm11->label = 'ABM-11';
        $seniorGrades->stem12 = $this->capacityDetails();
        $seniorGrades->stem12->label = 'STEM-12';
        $seniorGrades->ga12 = $this->capacityDetails();
        $seniorGrades->ga12->label = 'GA-12';
        $seniorGrades->humms12 = $this->capacityDetails();
        $seniorGrades->humms12->label = 'HUMMS-12';
        $seniorGrades->abm12 = $this->capacityDetails();
        $seniorGrades->abm12->label = 'ABM-12';

        return $seniorGrades;
    }

    private function capacityDetails(){
        $capacity = new \stdClass();
        $capacity->capacity = '';
        $capacity->enrollees = '';
        $capacity->availSlot = 0;
        $capacity->label = '';

        return $capacity;
    }


}
