<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Log;
use App\User;
use App\BarangayAbout;
use App\ActivityLog;
use App\ResidentHouseholdMember;
use Carbon\Carbon;
use App\TemporaryPassword;
use Mockery\Exception;
use Validator;
use Alert;
use DB;
use Hash;
class AdminController extends Controller
{
	function deleteBarangay($id){
		$brgy = BarangayAbout::where('id',$id)->first();

		try {
            $brgy->delete();
            return redirect()->back();
        }catch (\Illuminate\Database\QueryException $ex) {
            Session::flash('error', 'Cannot Delete Barangay');
        }

		return redirect()->back();
	}
	function listBrgy(){
		$brgy = BarangayAbout::paginate(5);
		return view('admin.list_brgy',[
			'brgy'=>$brgy,
		]);
	}
	function createBrgy(){
		return view('admin.add_brgy',[
		]);
	}

	function createChairman($id){
		$brgy = BarangayAbout::where('id',$id)->first();
		return view('admin.add_chairman',[
			'brgy'=>$brgy,
			'id'=>$id,
		]);
	}

	function storeChairman(Request $request){
		Log::info($request);
		$validator = Validator::make($request->all(), [
			'username' => 'unique:users|required|max:50|min:4',
			'password' => 'required|max:50|min:3',
			'first_name' => 'required|max:50|min:3',			
			'last_name' => 'required|max:50|min:3',
			'email' => 'email',
		]);
		if ($validator->fails()) {
			return redirect()
					->back()
						->withInput()
						->withErrors($validator);
		}

		DB::table('users')->insert([
			'barangay_about_id' => $request->input('brgy_id'),
			'username' => $request->input('username'),
			'password' => Hash::make($request->input('password')),
			'usertype' => 'Chairman',
			'first_name' => $request->input('first_name'),
			'middle_initial' => $request->input('middle_name'),
			'last_name' => $request->input('last_name'),
			'email' => $request->input('email'),
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		return redirect('/admin/barangay/')->with('flash_message', 'Chairman Added!!');
	}

	function storeBrgy(Request $request){
	
		$validator = Validator::make($request->all(), [
			'brgy_name' => 'required|max:50|min:4',
			'municipality' => 'required|max:50|min:3',
			'province' => 'required|max:50|min:3',
			'region' => 'required|max:50|min:3',
		]);

		if ($validator->fails()) {
			return redirect('/admin/barangay/create')
						->withErrors($validator)
						->withInput();
		}

		$brgy = BarangayAbout::create([
			'logo'=>'logo.png',
			'barangay'=>$request->input('brgy_name'),
			'municipality'=>$request->input('municipality'),
			'province'=>$request->input('province'),
			'region'=>$request->input('region'),
		]);

		return redirect('/admin/barangay/')->with('flash_message', 'Barangay Added!!');
	}

	function login(){
		if(Session::has('logUser')){
			return redirect()->route('barangayprofile.about');
		} else {
			return view('login');
		}
	}

		function loginAttempt(Request $request){
			$this->validate($request, [
					 'username'=>'required',
					 'password'=>'required',
				]);
			try {
					$users = User::get();
					foreach($users as $user){
						 if($user->username == $request->username){
									if(password_verify($request->password,$user->password)){
										$barangay = BarangayAbout::find($user->barangay_about_id);
										// $updateAge = ResidentHouseholdMember::get();
										// foreach ($updateAge as $ua) {
										// 	$currentDate = Carbon::now();
										// 	$birthdate = Carbon::parse($ua->birthdate);
										// 	$update_age = ResidentHouseholdMember::find($ua->id);
										// 	$update_age->age = $currentDate->diffInYears($birthdate);
										// 	$update_age->update();
										// }
                                        if($user->access != 1) {
                                            Session::flash('error','Account not activated');
                                            return redirect()->back();
                                        }
										if($user->usertype == 'Chairman' && $user->access == 1){
											$al = new ActivityLog();
											$al->username = $user->username;
											$al->description = 'login successful';
											$al->activity = 'login';
											$al->barangay_about_id = $barangay->id;
											$al->save();

											Session::put('barangayAbout',$barangay);
											Session::put('logUser',$user);
											Session::put('brgy_id',$barangay->id);
											Alert::success('Successfully!','Login!')->autoclose(2000);
											return redirect()->route('barangayprofile.about');
										} else if($user->usertype == 'Treasurer' && $user->access == 1) {
											$al = new ActivityLog();
											$al->username = $user->username;
											$al->description = 'login successful';
											$al->activity = 'login';
											$al->barangay_about_id = $barangay->id;
											$al->save();

											Session::put('barangayAbout',$barangay);
											Session::put('logUser',$user);
											Session::put('brgy_id',$barangay->id);
											Alert::success('Successfully!','Login!')->autoclose(2000);
											return redirect()->route('financial.budget');
										}
                                        else if($user->usertype == 'Teacher' && $user->access == 1) {
                                            $al = new ActivityLog();
                                            $al->username = $user->username;
                                            $al->description = 'login successful';
                                            $al->activity = 'login';
                                            $al->barangay_about_id = $barangay->id;
                                            $al->save();

                                            Session::put('barangayAbout',$barangay);
                                            Session::put('logUser',$user);
                                            Session::put('brgy_id',$barangay->id);
                                            Alert::success('Successfully!','Login!')->autoclose(2000);
                                            return redirect()->route('education.school');
                                        }
                                        else if($user->usertype == 'Secretary' && $user->access == 1) {
											$al = new ActivityLog();
											$al->username = $user->username;
											$al->description = 'login successful';
											$al->activity = 'login';
											$al->barangay_about_id = $barangay->id;
											$al->save();

											Session::put('barangayAbout',$barangay);
											Session::put('logUser',$user);
											Session::put('brgy_id',$barangay->id);
											Alert::success('Successfully!','Login!')->autoclose(2000);
											return redirect()->route('barangayprofile.about');
										} else {
											
											Session::put('logUser',$user);
											
											Alert::success('Successfully!','Login!')->autoclose(2000);
											return redirect()->route('admin.brgy');
										} 
								 }
								 else{
										 Session::flash('error','Incorrect Password!');
										 return redirect()->back();
								 }
						 }
				 }
				 Session::flash('error',"Username doesn't Exist");
				 return redirect()->back();
			}

			//catch exception
			catch(Exception $e) {
				return redirect()->back();
			}
		}

		public function logout(){
				$al = new ActivityLog();
				$al->username = Session::get('logUser')->username;
				$al->description = 'logout successful';
				$al->activity = 'logout';
				$al->barangay_about_id = Session::get('barangayAbout')->id;
				$al->save();

				Session::flush();
				Alert::success('Successfully!','Logout!')->autoclose(2000);
				return redirect()->route('login');
		}

		public function adminLogout(){
			

				Session::flush();
				Alert::success('Successfully!','Logout!')->autoclose(2000);
				return redirect()->route('login');
		}
}
