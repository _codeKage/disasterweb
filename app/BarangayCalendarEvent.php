<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangayCalendarEvent extends Model
{
	use SoftDeletes;
	public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'barangay_calendar_events';
}
