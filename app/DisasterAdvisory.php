<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisasterAdvisory extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'disaster_advisories';

    public function users(){
    	return $this->belongsTo('App\User','user_id');
    }
}
