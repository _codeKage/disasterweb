<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResidentHousehold extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'resident_households';

    public function resident_household_members(){
    	return $this->hasMany('App\ResidentHouseholdMember','resident_household_id');
    }

    public function yearly_survey(){
        return $this->hasMany('App\YearlySurvey','resident_household_id');
    }

    // public function resident_household_survey_as(){
    // 	return $this->hasMany('App\ResidentHouseholdSurveyA','resident_household_id');
    // }

    // public function resident_household_survey_bs(){
    //     return $this->hasMany('App\ResidentHouseholdSurveyB','resident_household_id');
    // }

    // public function resident_household_survey_cs(){
    //     return $this->hasMany('App\ResidentHouseholdSurveyC','resident_household_id');
    // }

    // public function resident_household_survey_ds(){
    //     return $this->hasMany('App\ResidentHouseholdSurveyD','resident_household_id');
    // }
}
