<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResidentHouseholdMember extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'resident_household_members';

    public function resident_households(){
    	return $this->belongsTo('App\ResidentHousehold','resident_household_id');
    }
}
