<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
 //   use SoftDeletes;
    public $timestamps = true;
   // protected $dates = ['deleted_at'];

    protected $table = 'school_profile';

    //

    public function school_category(){
        return $this->hasOne('App\SchoolCategory','school_id');
    }

    public function grade_capacity(){
        return $this->hasMany('App\GradeCapacity','school_id');
    }
}
