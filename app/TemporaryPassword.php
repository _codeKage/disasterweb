<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemporaryPassword extends Model
{
    protected $table = 'temporary_passwords';

     public function users(){
    	return $this->belongsTo('App\User','user_id');
    }
}
