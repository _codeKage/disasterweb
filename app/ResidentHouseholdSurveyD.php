<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResidentHouseholdSurveyD extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'resident_household_survey_ds';

    public function yearlySurvey(){
    	return $this->belongsTo('App\YearlySurvey','survey_d_id');
    }
}
