<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangayAchievement extends Model
{
	use SoftDeletes;
	
    protected $table = 'barangay_achievements';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
}
