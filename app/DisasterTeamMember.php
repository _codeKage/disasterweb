<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisasterTeamMember extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'disaster_team_members';

    public function disaster_teams(){
    	return $this->belongsTo('App\User','disaster_team_id');
    }
}
