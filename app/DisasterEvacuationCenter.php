<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisasterEvacuationCenter extends Model
{
    use SoftDeletes;
	
    protected $table = 'disaster_evacuation_centers';
    public $timestamps = true;
    protected $dates = ['deleted_at'];

}
