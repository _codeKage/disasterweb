<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisasterPlan extends Model
{
    protected $table = 'disaster_plans';
}
