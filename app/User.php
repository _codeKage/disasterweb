<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $table = 'users';

    public function barangay_plans(){
    	return $this->hasMany('App\BarangayPlan','user_id');
    }

    public function disaster_advisories(){
    	return $this->hasMany('App\BarangayPlan','user_id');
    }

    public function temporary_passwords(){
    	return $this->hasOne('App\TemporaryPassword','user_id');
    }

}
