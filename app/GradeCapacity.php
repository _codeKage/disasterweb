<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GradeCapacity extends Model
{
    //use SoftDeletes;
    public $timestamps = true;
   // protected $dates = ['deleted_at'];

    protected $table = 'grade_capacity';

    //

    public function school(){
        return $this->belongsTo('App\School','school_id');
    }

}
