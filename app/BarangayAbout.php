<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangayAbout extends Model
{
    protected $table = 'barangay_abouts';
    protected $fillable = [
		'barangay', 
		'municipality', 
		'province', 
		'region', 
	];

}
