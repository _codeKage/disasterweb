<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResidentHouseholdSurveyB extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'resident_household_survey_bs';

    public function yearlySurvey(){
    	return $this->belongsTo('App\YearlySurvey','survey_b_id');
    }
}
