<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BarangayPlan extends Model
{
	use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'barangay_plans';

    public function users(){
    	return $this->belongsTo('App\User','user_id');
    }
}
