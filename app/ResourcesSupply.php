<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourcesSupply extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'resources_supplies';

    public function resources_supply_histories(){
    	return $this->hasMany('App\ResourcesSupplyHistory','resources_supply_id');
    }
}
