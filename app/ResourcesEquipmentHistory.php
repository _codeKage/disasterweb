<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourcesEquipmentHistory extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'resources_equipment_histories';

    public function resources_equipments(){
    	return $this->belongsTo('App\ResourcesEquipment','resources_equipment_id');
    }
}
