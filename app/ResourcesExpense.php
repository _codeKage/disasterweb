<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourcesExpense extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'resources_expenses';

    public function resources_budgets(){
    	return $this->belongsTo('App\ResourcesBudget','resources_budget_id');
    }
}
