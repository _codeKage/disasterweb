<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourcesBudget extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'resources_budgets';

    public function resources_expenses(){
    	return $this->hasMany('App\ResourcesExpense','resources_budget_id');
    }
}
