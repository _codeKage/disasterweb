<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisasterKit extends Model
{
    protected $table = 'disaster_kits';
}
