<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisasterTeam extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'disaster_teams';

    public function disaster_team_members(){
    	return $this->hasMany('App\DisasterTeamMember','disaster_team_id');
    }
}
