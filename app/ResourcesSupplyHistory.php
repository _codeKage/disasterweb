<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourcesSupplyHistory extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'resources_supply_histories';

    public function resources_supplies(){
    	return $this->belongsTo('App\ResourcesSupply','resources_supply_id');
    }
}
