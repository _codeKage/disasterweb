<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class YearlySurvey extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];

    protected $table = 'yearly_survey';

    public function resident_household(){
        return $this->belongsTo('App\ResidentHousehold','resident_household_id');
    }

    public function resident_household_survey_a(){
        return $this->hasOne('App\ResidentHouseholdSurveyA','survey_a_id','id');
    }

    public function resident_household_survey_b(){
        return $this->hasOne('App\ResidentHouseholdSurveyB','survey_b_id','id');
    }

    public function resident_household_survey_c(){
        return $this->hasOne('App\ResidentHouseholdSurveyC','survey_c_id','id');
    }

    public function resident_household_survey_d(){
        return $this->hasOne('App\ResidentHouseholdSurveyD','survey_d_id','id');
    }
}
