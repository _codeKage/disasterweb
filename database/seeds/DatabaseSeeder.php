<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\BarangayAbout;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rawis = BarangayAbout::create([
            'logo' => 'logo.png',
            'barangay' => 'Rawis',
            'municipality' => 'Virac',
            'province' => 'Catanduanes',
            'region' => 'Region V',
            'address' => '192 I. Gianan Street, 192 Imelda Blvd, Virac, Catanduanes, Philippines',
            'total_land_area' => '1654',
            'latitude' => '13.5831343',
            'longitude' => '124.2385131',
            'barangay_category' => 'Urban',
            'land_classification' => 'Lowland,Coastal',
            'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
            'vision' => 'A self-reliant and law-abiding barangay with a committed, honest and transparent leader ensuring effective delivery of basic services and promoting preservation and protection of environment.',
            'mission' => 'To ensure the delivery of basic services and carry-out mandates to its constituents through upliftment of socio-economic status of the barangay and enforcing all laws for peace and order & public safety through effective quality of governance & active participation of the community.',
            'history' => 'The Barangay Rawis was created by the Virtue of Rep. Act No. 3590, as amended by Rep. Act No. 5676 and by Pres. Decree No. 431 dated June, 1972 before the Declaration of Martial law, the role of military over the civilian authority. Before become Rawis, there were different stories why the word “KARAWISAN” become Rawis. One such story is said to be that Rawis was known earlier as “karawisan” owing to the curve of the coastal area of the Barangay likened to “kalawit” or “karawis”. Another story is that of the Barangay got its name from the sound of the wind if it pass the barangay unknown to many, as the old tale is told, that Rawis was full of bamboo grass then that if the wind passes through it creates a hissing sound that can be heard in the nearby places, the earning its name “lawiswis” meaning whispers of the wind which is turned mutated to Rawis. Other said that the place earned its monikers from a plant that grows on its beaches known as Parawis, whatever was the origin of the name, the same cannot be known with absolute surety as there is neither record to speak of or a tale believable enough to be considered as history. The author and the rest of the Kabataang Barangay Officials exerted all its effort to find the truth, interviewed old people with its golden wisdom and even those in their young age who had tried to come up with “Rawis” historical fact but all came up with different version. Like all other, it is folklore, a myth, and will remain as a mystery forever. 
Rawis before; there are 3 sitio, the one Sitio is called the “PULO” or is very well known as “DUPAKS”, it is near the seashore were there are plantation of “pandan”, ”lagundi”, and  “Kadena de amor” because the water during that time is one kilometer away from the land. According to the stories of the first family named  Albino Isidoro, the first Tenyente Del Barrio are, Clemente Romero, Sr., Pablito Romero, the late Taton Romero at Inocentes Tablizo and other family member and we considered as our guide in our progress. 
Another sitio called “Fatima” because of the small chapel with our Patron Saint Our lady of Fatima, it is also a place of growing “pandan”, “lagundi”, “kadena de Amor” and bamboo. Because of “bayanihan” (one of the Filipino traits which means cooperative endeavor) and a strong leadership of youth (Jovenes) in the Barangay, they build a chapel that until now have been devoted by the people of the place. And the first families in this sitio are Zosimo Sarmiento, Generoso S. Molina, Sr., and Nicolas Sarmiento at the late of family Villafuerte. 
And they said that the center of Brgy. Rawis was called “Pusod” (midpoint). In this area rises the Church of Patron Saint Divino Rostro through the spirit of “Bayanihan” in leadership of Tenyente del Barrio, Albino Isidoro. The first church was destroyed by a typhoon because it is only made up of wooden materials and builds near the sea. The elders said that the Patron saint seen walking throughout the barangay asking for help to rebuild and fix his destructed holy home. From that time the new and more resilient church was built which served as the center of Catholic devotion. The people who helped to build the church are the following skilled carpenters; late Demetrio Tadoy, Dandoy Tablizo, Aurelio Tabirara, Aurelio Tablizo, Gimbo Isidoro, Clemente Romero, Inocentes Tablizo, Taton Tablizo, Zosimo Sarmiento, Hilario Tubalinal, Pablito Romero with the help of their wives and children. The strong foundation of the church was made up of “gasang” (Corals), egg white and limestone.',
            'brief_description' => 'The Barangay Rawis is located in Coastal area of Municipality of Virac with the boundaries of Sea Coast from the East; Francia in North; Sta. Elena is in West and Sta. Cruz in South. The barangay Rawis has a total land area of 9.50 hectare base on the DENR Cadastral Survey. With a terrain characteristic of coastal area with an area of 3.80 hectares and 5.70 hectares is in plain area. The barangay Rawis has a land use of 5.42 hectares for residential area while for commercial area is 0.23 ha. and the institutional is 0.97 ha. for park area 0.34 ha. and 0.20 ha. for river and 21.49 ha. from the Strategic Agricultural and Fisheries Development Zone (SAFDZ). Based on the National Statistics Office (NSO) Census 2010 the total population is one thousand eight hundred seventy one (1,871) with the total of seven (7) Puroks with the total Household of four hundred thirteen (413). The barangay Rawis has a three (3) classifications of road the National Road has 0.18 ha. and Barangay Road in which has a 1.67 ha. There is no Provincial Road constructed. The Barangay Rawis has a coordinates of 13°35\'3.24"North Latitude and 124°14\'20.52" East Longitude.
The distance of Barangay Rawis from the Virac Municipal Hall is 1,355 meters and 250 meters from the Provincial Hall.',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

		$capilihan = BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Capilihan',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		$constantino = BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Constantino',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		$francia = BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Francia',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		$gogoncentro = BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Gogon Centro',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		$ibongsapa =BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Ibong Sapa',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		$lanao = BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Lanao',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		$marceloalberto =BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Marcelo Alberto',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		$palnabdelnorte = BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Palnab Del Norte',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		$palnabdelsur = BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Palnab Del Sur',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		$sanjuan = BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'San Juan',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		$santonino =BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Santo Nino',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		$sogodtibgao = BarangayAbout::create([
			'logo' => 'logo.png',
			'barangay' => 'Sogod-Tibgao',
			'municipality' => 'Virac',
			'province' => 'Catanduanes',
			'region' => 'Region V',
			'address' => '',
			'total_land_area' => '1654',
			'latitude' => '13.5831343',
			'longitude' => '124.2385131',
			'barangay_category' => 'Urban',
			'land_classification' => 'Lowland,Coastal',
			'major_economic_source' => 'Agricultural,Fishing,Industrial,Commercial',
			'vision' => '',
			'mission' => '',
			'history' => '',
			'brief_description' => '',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);



        DB::table('users')->insert([
            'username' => 'admin',
            'password' => bcrypt('admin1'),
            'usertype' => 'Admin',
            'first_name' => 'admin',
            'last_name' => 'admin',
            'access' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Preparedness and Responsive Group',
            'name' => 'Early Warning System and Communication Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Preparedness and Responsive Group',
            'name' => 'Evacuation Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Preparedness and Responsive Group',
            'name' => 'Security Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Preparedness and Responsive Group',
            'name' => 'Relief and Rescue Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Preparedness and Responsive Group',
            'name' => 'Transportation Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Preparedness and Responsive Group',
            'name' => 'Health and Medical Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Mitigation Group',
            'name' => 'Infrastructure Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Mitigation Group',
            'name' => 'Public Utilities Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Mitigation Group',
            'name' => 'Epidemic Outbreak Management Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Rehabilitation Group',
            'name' => 'Agricultural Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Rehabilitation Group',
            'name' => 'Social Welfare Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('disaster_teams')->insert([
            'barangay_about_id' => $rawis->id,
            'code' => 'Rehabilitation Group',
            'name' => 'Trade and Industry Team',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

		DB::table('users')->insert([
			'barangay_about_id' => $rawis->id,
			'username' => 'rawischairman',
			'password' => bcrypt('rawischairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Rawis',
			'middle_initial' => 'R',
			'last_name' => 'Chairman',
			'email' => 'rawischairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' => $rawis->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' => $rawis->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $rawis->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $rawis->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $rawis->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $rawis->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $rawis->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $rawis->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $rawis->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $rawis->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $rawis->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $rawis->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $rawis->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $rawis->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' => $capilihan->id,
			'username' => 'capilihanchairman',
			'password' => bcrypt('capilihanchairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Capilihan',
			'middle_initial' => 'C',
			'last_name' => 'Chairman',
			'email' => 'capilihanchairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'id' => 2,
			'barangay_about_id' => $capilihan->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'id' => 2,
			'barangay_about_id' => $capilihan->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$capilihan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $capilihan->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' => $constantino->id,
			'username' => 'constantinochairman',
			'password' => bcrypt('constantinochairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Constantino',
			'middle_initial' => 'C',
			'last_name' => 'Chairman',
			'email' => 'constantinochairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' => $constantino->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' => $constantino->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$constantino->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $constantino->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' => $francia->id,
			'username' => 'franciachairman',
			'password' => bcrypt('franciachairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Francia',
			'middle_initial' => 'F',
			'last_name' => 'Chairman',
			'email' => 'franciachairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' =>  $francia->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' =>  $francia->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $francia->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $francia->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' =>  $gogoncentro->id,
			'username' => 'gogonchairman',
			'password' => bcrypt('gogonchairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Gogon Centro',
			'middle_initial' => 'G',
			'last_name' => 'Chairman',
			'email' => 'gogonchairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' => $gogoncentro->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' => $gogoncentro->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $gogoncentro->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $gogoncentro->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$gogoncentro->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $gogoncentro->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $gogoncentro->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $gogoncentro->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $gogoncentro->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $gogoncentro->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $gogoncentro->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$gogoncentro->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$gogoncentro->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$gogoncentro->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'username' => 'ibongchairman',
			'password' => bcrypt('ibongchairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Ibong Sapa',
			'middle_initial' => 'C',
			'last_name' => 'Chairman',
			'email' => 'ibongchairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' =>$ibongsapa->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'id' => 6,
			'barangay_about_id' => $ibongsapa->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$ibongsapa->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$ibongsapa->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $ibongsapa->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' => $lanao->id,
			'username' => 'lanaochairman',
			'password' => bcrypt('lanaochairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Lanao',
			'middle_initial' => 'L',
			'last_name' => 'Chairman',
			'email' => 'lanaochairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' => $lanao->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' => $lanao->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$lanao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $lanao->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' =>$marceloalberto->id,
			'username' => 'marcelochairman',
			'password' => bcrypt('marcelochairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Marcelo Alberto',
			'middle_initial' => 'M',
			'last_name' => 'Chairman',
			'email' => 'marcelochairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $marceloalberto->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' => $palnabdelnorte->id,
			'username' => 'palnabnortechairman',
			'password' => bcrypt('palnabnortechairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Palnab Del Norte',
			'middle_initial' => 'P',
			'last_name' => 'Chairman',
			'email' => 'palnabnortechairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' => $palnabdelnorte->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelnorte->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelnorte->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $palnabdelnorte->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' =>  $palnabdelsur->id,
			'username' => 'palnabsurchairman',
			'password' => bcrypt('palnabsurchairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Palnab Del Sur',
			'middle_initial' => 'P',
			'last_name' => 'Chairman',
			'email' => 'palnabsurchairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$palnabdelsur->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $palnabdelsur->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' =>$sanjuan->id,
			'username' => 'sanjuanchairman',
			'password' => bcrypt('sanjuanchairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'San Juan',
			'middle_initial' => 'S',
			'last_name' => 'Chairman',
			'email' => 'sanjuanchairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' => $sanjuan->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' => $sanjuan->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sanjuan->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' => $santonino->id,
			'username' => 'santochairman',
			'password' => bcrypt('santochairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Santo Nino',
			'middle_initial' => 'C',
			'last_name' => 'Chairman',
			'email' => 'santochairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' =>  $santonino->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' =>  $santonino->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $santonino->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>  $santonino->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('users')->insert([
			'barangay_about_id' =>  $sogodtibgao->id,
			'username' => 'sogodchairman',
			'password' => bcrypt('sogodchairman1'),
			'usertype' => 'Chairman',
			'first_name' => 'Sogod-Tibgao',
			'middle_initial' => 'S',
			'last_name' => 'Chairman',
			'email' => 'sogodchairman@gmail.com',
			'access' => 1,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_plans')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_kits')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Early Warning System and Communication Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$sogodtibgao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Evacuation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' =>$sogodtibgao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Security Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Relief and Rescue Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Transportation Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'code' => 'Preparedness and Responsive Group',
			'name' => 'Health and Medical Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'code' => 'Mitigation Group',
			'name' => 'Infrastructure Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'code' => 'Mitigation Group',
			'name' => 'Public Utilities Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'code' => 'Mitigation Group',
			'name' => 'Epidemic Outbreak Management Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Agricultural Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Social Welfare Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
		DB::table('disaster_teams')->insert([
			'barangay_about_id' => $sogodtibgao->id,
			'code' => 'Rehabilitation Group',
			'name' => 'Trade and Industry Team',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
	}
}
