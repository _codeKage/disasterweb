<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentHouseholdSurveyAsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resident_household_survey_as', function (Blueprint $table) {
            $table->increments('id');
            $table->string('a1');
            $table->tinyInteger('a2_1')->default(0);
            $table->string('a3_1');
            $table->string('a4_1');
            $table->tinyInteger('a5_1_1')->default(0);
            $table->tinyInteger('a5_1_2')->default(0);
            $table->tinyInteger('a5_1_3')->default(0);
            $table->tinyInteger('a5_1_4')->default(0);
            $table->tinyInteger('a5_1_5')->default(0);
            $table->tinyInteger('a2_2')->default(0);
            $table->string('a3_2');
            $table->string('a4_2');
            $table->tinyInteger('a5_2_1')->default(0);
            $table->tinyInteger('a5_2_2')->default(0);
            $table->tinyInteger('a5_2_3')->default(0);
            $table->tinyInteger('a5_2_4')->default(0);
            $table->tinyInteger('a5_2_5')->default(0);
            $table->tinyInteger('a2_3')->default(0);
            $table->string('a3_3');
            $table->string('a4_3');
            $table->tinyInteger('a5_3_1')->default(0);
            $table->tinyInteger('a5_3_2')->default(0);
            $table->tinyInteger('a5_3_3')->default(0);
            $table->tinyInteger('a5_3_4')->default(0);
            $table->tinyInteger('a5_3_5')->default(0);
            $table->tinyInteger('a2_4')->default(0);
            $table->string('a3_4');
            $table->string('a4_4');
            $table->tinyInteger('a5_4_1')->default(0);
            $table->tinyInteger('a5_4_2')->default(0);
            $table->tinyInteger('a5_4_3')->default(0);
            $table->tinyInteger('a5_4_4')->default(0);
            $table->tinyInteger('a5_4_5')->default(0);
            $table->tinyInteger('a2_5')->default(0);
            $table->string('a3_5');
            $table->string('a4_5');
            $table->tinyInteger('a5_5_1')->default(0);
            $table->tinyInteger('a5_5_2')->default(0);
            $table->tinyInteger('a5_5_3')->default(0);
            $table->tinyInteger('a5_5_4')->default(0);
            $table->tinyInteger('a5_5_5')->default(0);
            $table->tinyInteger('a2_6')->default(0);
            $table->string('a3_6');
            $table->string('a4_6');
            $table->tinyInteger('a5_6_1')->default(0);
            $table->tinyInteger('a5_6_2')->default(0);
            $table->tinyInteger('a5_6_3')->default(0);
            $table->tinyInteger('a5_6_4')->default(0);
            $table->tinyInteger('a5_6_5')->default(0);
            $table->tinyInteger('a2_7')->default(0);
            $table->string('a3_7');
            $table->string('a4_7');
            $table->tinyInteger('a5_7_1')->default(0);
            $table->tinyInteger('a5_7_2')->default(0);
            $table->tinyInteger('a5_7_3')->default(0);
            $table->tinyInteger('a5_7_4')->default(0);
            $table->tinyInteger('a5_7_5')->default(0);
            $table->tinyInteger('a2_8')->default(0);
            $table->string('a3_8');
            $table->string('a4_8');
            $table->tinyInteger('a5_8_1')->default(0);
            $table->tinyInteger('a5_8_2')->default(0);
            $table->tinyInteger('a5_8_3')->default(0);
            $table->tinyInteger('a5_8_4')->default(0);
            $table->tinyInteger('a5_8_5')->default(0);
            $table->tinyInteger('a2_9')->default(0);
            $table->string('a3_9');
            $table->string('a4_9');
            $table->tinyInteger('a5_9_1')->default(0);
            $table->tinyInteger('a5_9_2')->default(0);
            $table->tinyInteger('a5_9_3')->default(0);
            $table->tinyInteger('a5_9_4')->default(0);
            $table->tinyInteger('a5_9_5')->default(0);
            $table->tinyInteger('a2_10')->default(0);
            $table->string('a3_10');
            $table->string('a4_10');
            $table->tinyInteger('a5_10_1')->default(0);
            $table->tinyInteger('a5_10_2')->default(0);
            $table->tinyInteger('a5_10_3')->default(0);
            $table->tinyInteger('a5_10_4')->default(0);
            $table->tinyInteger('a5_10_5')->default(0);
            $table->tinyInteger('a2_11')->default(0);
            $table->string('a3_11');
            $table->string('a4_11');
            $table->tinyInteger('a5_11_1')->default(0);
            $table->tinyInteger('a5_11_2')->default(0);
            $table->tinyInteger('a5_11_3')->default(0);
            $table->tinyInteger('a5_11_4')->default(0);
            $table->tinyInteger('a5_11_5')->default(0);
            $table->tinyInteger('a2_12')->default(0);
            $table->string('a3_12');
            $table->string('a4_12');
            $table->tinyInteger('a5_12_1')->default(0);
            $table->tinyInteger('a5_12_2')->default(0);
            $table->tinyInteger('a5_12_3')->default(0);
            $table->tinyInteger('a5_12_4')->default(0);
            $table->tinyInteger('a5_12_5')->default(0);
            $table->tinyInteger('a2_13')->default(0);
            $table->string('a3_13');
            $table->string('a4_13');
            $table->tinyInteger('a5_13_1')->default(0);
            $table->tinyInteger('a5_13_2')->default(0);
            $table->tinyInteger('a5_13_3')->default(0);
            $table->tinyInteger('a5_13_4')->default(0);
            $table->tinyInteger('a5_13_5')->default(0);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resident_household_survey_as');
    }
}
