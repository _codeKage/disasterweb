<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesEquipmentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources_equipment_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_about_id')->unsigned();
            $table->integer('resources_equipment_id')->unsigned();
            $table->date('date');
            $table->string('name_source')->nullable();
            $table->string('particular');
            $table->integer('added')->unsigned()->nullable();
            $table->integer('removed')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('resources_equipment_id')->references('id')->on('resources_equipments');
            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resources_equipment_histories');
    }
}
