<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeCapacityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grade_capacity', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned();
            $table->string('school_year');
            $table->string('grade');
            $table->integer('max_capacity');
            $table->integer('no_enrolled');
            $table->integer('available_slot');
            $table->timestamps();

            $table->foreign('school_id')->references('id')->on('school_profile');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grade_capacity');
    }
}
