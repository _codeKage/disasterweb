<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYearlySurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yearly_survey', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_id')->unsigned();
            $table->integer('resident_household_id')->unsigned();
            $table->integer('survey_a_id')->unsigned();
            $table->integer('survey_b_id')->unsigned();
            $table->integer('survey_c_id')->unsigned();
            $table->integer('survey_d_id')->unsigned();
            $table->string('year');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('resident_household_id')->references('id')->on('resident_households');
            $table->foreign('barangay_id')->references('id')->on('barangay_abouts');
            $table->foreign('survey_a_id')->references('id')->on('resident_household_survey_as');
            $table->foreign('survey_b_id')->references('id')->on('resident_household_survey_bs');
            $table->foreign('survey_c_id')->references('id')->on('resident_household_survey_cs');
            $table->foreign('survey_d_id')->references('id')->on('resident_household_survey_ds');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('yearly_survey');
    }
}
