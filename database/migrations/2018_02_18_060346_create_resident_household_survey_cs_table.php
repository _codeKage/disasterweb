<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentHouseholdSurveyCsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resident_household_survey_cs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('c1');
            $table->string('c2');
            $table->string('c3');
            $table->string('c4_1');
            $table->string('c4_2');
            $table->string('c4_3');
            $table->string('c4_4');
            $table->string('c4_5');
            $table->string('c4_6');
            $table->string('c5');
            $table->string('c6');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resident_household_survey_cs');
    }
}
