<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned();
            $table->tinyInteger('elem')->default(0);
            $table->tinyInteger('junior')->default(0);
            $table->tinyInteger('senior')->default(0);
            $table->timestamps();

            $table->foreign('school_id')->references('id')->on('school_profile');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('school_category');
    }
}
