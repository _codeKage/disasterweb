<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesSupplyHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources_supply_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resources_supply_id')->unsigned();
            $table->integer('barangay_about_id')->unsigned();
            $table->date('date');
            $table->string('name_source')->nullable();
            $table->string('particular');
            $table->integer('added')->unsigned()->nullable();
            $table->integer('distributed')->unsigned()->nullable();
            $table->integer('balance')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('resources_supply_id')->references('id')->on('resources_supplies');
            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resources_supply_histories');
    }
}
