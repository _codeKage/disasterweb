<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('resources_budget_id')->unsigned();
            $table->integer('barangay_about_id')->unsigned();
            $table->date('date');
            $table->string('particular');
            $table->double('amount',15,2)->unsinged();
            $table->string('remarks');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('resources_budget_id')->references('id')->on('resources_budgets');
            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resources_expenses');
    }
}
