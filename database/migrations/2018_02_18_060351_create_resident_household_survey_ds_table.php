<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentHouseholdSurveyDsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resident_household_survey_ds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('d1');
            $table->string('d2');
            $table->string('d3');
            $table->string('d4');
            $table->string('d5');
            $table->string('d6');
            $table->string('d7');
            $table->string('d8');
            $table->string('d9');
            $table->tinyInteger('d10_1')->default(0);
            $table->tinyInteger('d10_2')->default(0);
            $table->tinyInteger('d10_3')->default(0);
            $table->tinyInteger('d10_4')->default(0);
            $table->tinyInteger('d10_5')->default(0);
            $table->tinyInteger('d10_6')->default(0);
            $table->tinyInteger('d10_7')->default(0);
            $table->tinyInteger('d10_8')->default(0);
            $table->tinyInteger('d10_9')->default(0);
            $table->tinyInteger('d10_10')->default(0);
            $table->tinyInteger('d10_11')->default(0);
            $table->tinyInteger('d10_12')->default(0);
            $table->tinyInteger('d10_13')->default(0);
            $table->tinyInteger('d10_14')->default(0);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resident_household_survey_ds');
    }
}
