<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisasterEvacuationCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disaster_evacuation_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_about_id')->unsigned();
            $table->string('name');
            $table->double('latitude',10,7)->nullable();
            $table->double('longitude',10,7)->nullable();
            $table->string('purok')->nullable();
            $table->string('street');
            $table->string('barangay')->nullable();
            $table->string('municipality');
            $table->string('province');
            $table->string('address');
            $table->integer('capacity')->unsigned();
            $table->integer('no_people')->unsigned()->default(0);
            $table->string('type');
            $table->string('condition');
            $table->string('incharge');
            $table->string('contact')->nullable();
            $table->tinyInteger('facility_toilet')->default(0);
            $table->tinyInteger('facility_access_to_water')->default(0);
            $table->tinyInteger('facility_with_electricity')->default(0);
            $table->tinyInteger('facility_working_generator')->default(0);
            $table->tinyInteger('facility_with_private_rooms')->default(0);
            $table->tinyInteger('facility_with_lactating_room')->default(0);
            $table->tinyInteger('facility_kitchen')->default(0);
            $table->tinyInteger('applicable_typhoon')->default(0);
            $table->tinyInteger('applicable_flood')->default(0);
            $table->tinyInteger('applicable_storm_surge')->default(0);
            $table->tinyInteger('applicable_landslide')->default(0);
            $table->tinyInteger('applicable_drought')->default(0);
            $table->tinyInteger('applicable_volcanic_eruption')->default(0);
            $table->tinyInteger('applicable_sinkhole')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('disaster_evacuation_centers');
    }
}
