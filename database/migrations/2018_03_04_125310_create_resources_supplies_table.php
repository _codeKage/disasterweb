<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources_supplies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_about_id')->unsigned();
            $table->string('name');
            $table->string('description');
            $table->string('category');
            $table->bigInteger('total_added')->unsigned()->default(0);
            $table->bigInteger('total_distributed')->unsigned()->default(0);
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resources_supplies');
    }
}
