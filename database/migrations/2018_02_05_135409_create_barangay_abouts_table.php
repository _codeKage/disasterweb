<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangayAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangay_abouts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo');
            $table->string('barangay');
            $table->string('municipality');
            $table->string('province');
            $table->string('region');
            $table->string('address');
            $table->string('total_land_area');
            $table->double('latitude',10,7)->nullable();
            $table->double('longitude',10,7)->nullable();
            $table->string('barangay_category');
            $table->string('land_classification');
            $table->string('major_economic_source');
            $table->longText('vision');
            $table->longText('mission');
            $table->longText('history')->nullable();
            $table->longText('brief_description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('barangay_abouts');
    }
}
