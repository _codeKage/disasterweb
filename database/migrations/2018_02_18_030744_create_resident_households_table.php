<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentHouseholdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resident_households', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_about_id')->unsigned();
            $table->string('province');
            $table->string('municipality');
            $table->string('barangay');
            $table->string('purok');
            $table->string('street');
            $table->string('house_identification_number');
            $table->double('latitude',10,7)->nullable();
            $table->double('longitude',10,7)->nullable();
            $table->string('name_of_respondent');
            $table->string('contact');
            $table->integer('no_family')->unsigned();
            $table->integer('no_members')->unsigned();
            $table->string('disability');
            $table->integer('no_disability')->unsigned()->nullable();
            $table->string('building_type');
            $table->string('water_supply');
            $table->string('toilet');
            $table->string('water_filtration');
            $table->string('electricity');
            $table->string('power_generator');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resident_households');
    }
}
