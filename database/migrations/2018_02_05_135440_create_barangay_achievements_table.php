<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangayAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barangay_achievements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_about_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('name');
            $table->date('date');
            $table->string('detail');
            $table->string('other_detail');
            $table->string('type');
            $table->string('image');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('barangay_achievements');
    }
}
