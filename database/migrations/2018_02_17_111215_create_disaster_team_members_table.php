<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisasterTeamMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disaster_team_members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('disaster_team_id')->unsigned();
            $table->integer('barangay_about_id')->unsigned();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->enum('gender',['Male','Female']);
            $table->date('birthdate');
            $table->string('contact');
            $table->date('period_from');
            $table->date('period_to');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('disaster_team_id')->references('id')->on('disaster_teams');
            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('disaster_team_members');
    }
}
