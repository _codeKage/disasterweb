<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources_budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_about_id')->unsigned();
            $table->string('fiscal_year');
            $table->string('code');
            $table->string('account_name');
            $table->double('funding_amount',15,2)->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resources_budgets');
    }
}
