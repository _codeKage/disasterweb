<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentHouseholdMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resident_household_members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_about_id')->unsigned();
            $table->integer('resident_household_id')->unsigned();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->integer('family_belong')->unsigned();
            $table->string('relation');
            $table->string('civil_status');
            $table->enum('gender',['Male','Female']);
            $table->date('birthdate');
            $table->integer('age')->unsigned();
            $table->string('same_address');
            $table->string('attending_school');
            $table->string('year_level')->nullable();
            $table->string('highest_level');
            $table->string('pregnant')->nullable();
            $table->string('have_children');
            $table->string('solo_parent')->nullable();
            $table->string('disability');
            $table->mediumText('disability_type')->nullable();
            $table->mediumText('health_problem');
            $table->string('employed');
            $table->string('occupation')->nullable();
            $table->string('where_occupation')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('resident_household_id')->references('id')->on('resident_households');
            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resident_household_members');
    }
}
