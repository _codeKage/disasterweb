<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_about_id')->unsigned()->nullable();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('first_name');
            $table->char('middle_initial',1)->nullable();
            $table->string('last_name');
            $table->string('email')->nullable();
            $table->string('usertype');
            $table->tinyInteger('access')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
