<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_id')->unsigned();
            $table->string('school_name');
            $table->string('purok');
            $table->timestamps();

            $table->foreign('barangay_id')->references('id')->on('barangay_abouts');
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('school_profile');
    }
}
