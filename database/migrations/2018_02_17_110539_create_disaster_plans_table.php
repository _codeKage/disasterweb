<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisasterPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disaster_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barangay_about_id')->unsigned();
            $table->string('identify_escape_routes')->nullable();
            $table->string('establish_meeting_places')->nullable();
            $table->string('plan_for_children')->nullable();
            $table->string('address_any_special_health_needs')->nullable();
            $table->string('plan_for_pets')->nullable();
            $table->string('plan_for_specific_risks')->nullable();
            $table->string('record_emergency_contact_information')->nullable();
            $table->string('complete_safe_home_instructions')->nullable();
            $table->string('summary')->nullable();
            $table->timestamps();

            $table->foreign('barangay_about_id')->references('id')->on('barangay_abouts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('disaster_plans');
    }
}
