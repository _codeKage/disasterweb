<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResidentHouseholdSurveyBsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resident_household_survey_bs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('b1');
            $table->tinyInteger('b2_1')->default(0);
            $table->tinyInteger('b2_2')->default(0);
            $table->tinyInteger('b2_3')->default(0);
            $table->tinyInteger('b2_4')->default(0);
            $table->tinyInteger('b2_5')->default(0);
            $table->tinyInteger('b2_6')->default(0);
            $table->tinyInteger('b2_7')->default(0);
            $table->tinyInteger('b2_8')->default(0);
            $table->tinyInteger('b2_9')->default(0);
            $table->tinyInteger('b2_10')->default(0);
            $table->tinyInteger('b2_11')->default(0);
            $table->tinyInteger('b2_12')->default(0);
            $table->tinyInteger('b2_13')->default(0);
            $table->tinyInteger('b2_14')->default(0);
            $table->tinyInteger('b3_1')->default(0);
            $table->tinyInteger('b3_2')->default(0);
            $table->tinyInteger('b3_3')->default(0);
            $table->tinyInteger('b3_4')->default(0);
            $table->tinyInteger('b3_5')->default(0);
            $table->tinyInteger('b3_6')->default(0);
            $table->tinyInteger('b3_7')->default(0);
            $table->tinyInteger('b3_8')->default(0);
            $table->tinyInteger('b3_9')->default(0);
            $table->tinyInteger('b3_10')->default(0);
            $table->tinyInteger('b3_11')->default(0);
            $table->tinyInteger('b3_12')->default(0);
            $table->tinyInteger('b3_13')->default(0);
            $table->tinyInteger('b3_14')->default(0);
            $table->tinyInteger('b3_15')->default(0);
            $table->string('b4');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resident_household_survey_bs');
    }
}
